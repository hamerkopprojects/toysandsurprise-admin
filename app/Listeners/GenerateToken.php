<?php

namespace App\Listeners;


// use App\Listeners\GenerateToken;

use App\Events\SendToken;
use App\Mail\TokenGeneraingEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class GenerateToken
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(SendToken $event)
    {
        //  dd($event);
       
        Mail::to($event->email)->send(new TokenGeneraingEmail(
            $event->token)
        );
    }
}
