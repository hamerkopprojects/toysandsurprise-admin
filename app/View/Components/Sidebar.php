<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Sidebar extends Component
{

    public $menuArray;
    public $adminLogoArray;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->adminLogoArray = array(
            'uri' => '#',
            'img' => 'assets/images/sidelogo.png'
        );

        //details for menu
        $this->menuArray = [
            'dashboard' => [
                'position' => 1,
                'level' => 1,
                'name' => 'Dashboard',
                'icon' => 'ti-dashboard',
                'uri' => route('dashboard.get'),
            ],
            'products' => [
                'position' => 3,
                'level' => 2,
                'name' => 'Products',
                'icon' => 'ti-anchor',
                'name' => 'Products',
                'icon' => 'ti-shopping-cart',
                'submenus' => [
                    [
                        'name' => 'Category',
                        'icon' => '',
                        'uri' => route('category'),

                    ],
                    [
                        'name' => 'Brands',
                        'icon' => '',
                        'uri' => route('brand'),
                    ],
                    [
                        'name' => 'Attributes',
                        'icon' => '',
                        'uri' => route('attribute'),
                    ],
                    [
                        'name' => 'Products',
                        'icon' => '',
                        'uri' => route('products'),
                    ],
                    [
                        'name' => 'Promotional Products',
                        'icon' => '',
                        'uri' => route('promoproduct'),
                    ],
                    
                ]
            ],
            'users' => [
                'position' => 6,
                'level' => 1,
                'name' => 'Users',
                'icon' => 'ti-user',
                'uri' => route('user-list.get'),
            ],
            // 'jobcategory' => [
            //     'position' => 6,
            //     'level' => 1,
            //     'name' => 'Job Category',
            //     'icon' => 'ti-layout-tab',
            //     'uri' => route('job.category'),
            // ],
            'jobmanagement' => [
                'position' => 6,
                'level' => 2,
                'name' => 'Job Management',
                'icon' => 'ti-layout-tab',
                'submenus' => [
                    [
                        'name' => 'Job Category',
                        'icon' => '',
                        'uri' => route('job.category'),
                    ],
                    [
                        'name' => 'Jobs',
                        'icon' => '',
                        'uri' => route('job.management'),
                    ],
                ],
            ],
            'cms' => [
                'position' => 7,
                'level' => 2,
                'name' => 'CMS',
                'icon' => 'ti-book',
                'submenus' => [
                    [
                        'name' => 'FAQ',
                        'icon' => '',
                        'uri' => route('faq.get'),
                    ],
                    [
                        'name' => 'Pages',
                        'icon' => '',
                        'uri' => route('pages.get'),
                    ],
                    [
                        'name' => 'How to use the app',
                        'icon' => '',
                        'uri' => route('use.get'),
                    ],

                ]
            ],
            'ads' => [
                'position' => 4,
                'level' => 1,
                'name' => 'Slider',
                'icon' => 'ti-gallery',
                'uri' => route('slider'),
            ],
            'shopmanagement' => [
                'position' => 6,
                'level' => 1,
                'name' => 'Shop Management',
                'icon' => 'ti-book',
                'uri' => route('shop.management'),
            ],
            // 'social-media-link' => [
            //     'position' => 6,
            //     'level' => 1,
            //     'name' => 'Social Media Link',
            //     'icon' => 'ti-shopping-cart',
            //     'uri' => route('social-media-link'),
            // ],
            
            'setting' => [
                'position' => 10,
                'level' => 2,
                'name' => 'Settings',
                'icon' => 'ti-anchor',
                'submenus' => [
                    [
                        'name' => 'Rating segments',
                        'icon' => '',
                        'uri' => route('segments.get'),
                    ],
                    [
                        'name' => 'Cancellation reasons',
                        'icon' => '',
                        'uri' => route('cancel.get'),
                    ],
                    [
                        'name' => 'Social Media Link',
                        'icon' => '',
                        'uri' => route('social-media-link'),
                    ],
                    [
                        'name' => 'Other settings',
                        'icon' => '',
                        'uri' => route('other.get')
                    ],
                ],
            ],

        ];
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.sidebar');
    }
}
