<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
   protected $table = 'advertisements';
   
   public function appcategory()
    {
        return $this->belongsTo('App\Models\Category', 'app_category')->with('lang')->where('deleted_at', NULL);
    }
    public function appproduct()
    {
        return $this->belongsTo('App\Models\Product', 'app_product')->with('lang')->where('deleted_at', NULL);
    }
    public function webcategory()
    {
        return $this->belongsTo('App\Models\Category', 'web_category')->with('lang')->where('deleted_at', NULL);
    }
    public function webproduct()
    {
        return $this->belongsTo('App\Models\Product', 'web_product')->with('lang')->where('deleted_at', NULL);
    }
}

