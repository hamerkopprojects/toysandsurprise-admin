<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use PhpParser\Node\Expr\Cast\String_;

class CustomerAuthenticated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $customer_id;

    /**
     * Create a new event instance.
     *
     * @param  $customer
     * @return void
     */
    public function __construct($customer_id, $customer_name)
    { 

        $this->customer_id = $customer_id;
        $this->customer_name = $customer_name;
    }
}