<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductGender extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "product_gender";

    public function gender()
    {
        return $this->belongsTo('App\Models\GenderLang', 'gender_id', 'gender_id')->where('deleted_at', NULL)->where('language', 'en');
    }
    
}
