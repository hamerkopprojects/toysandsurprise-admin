<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model {

    /**
     * guarded variable

     * @var array
     */
    protected $guarded = [];

    protected $hidden = [
        'password', 'otp'
    ];

    /**
     * $table variable
     *
     * @var string
     */
    protected $table = "customer";
    
}