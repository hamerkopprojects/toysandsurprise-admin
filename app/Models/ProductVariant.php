<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductVariant extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "product_variant_stock";

    public function attribute()
    {
        return $this->belongsTo('App\Models\Attribute', 'attribute_id', 'id')->with('lang');
    }
    public function variant()
    {
        return $this->hasOne('App\Models\VariantLang', 'id', 'attribute_variant_id');
    }
}
