<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobManagementLang extends Model
{
    use SoftDeletes;
    /*
    * guarded variable
    *
    * @var array
    */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected  $table = "job_management_i18n";
}
