<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductAttribute extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "product_attribute";

    public function lang()
    {
        return $this->hasMany(ProductAttributeLang::class, 'product_attribute_id');
    }
    public function attribute()
    {
        return $this->belongsTo('App\Models\Attribute', 'attribute_id', 'id')->with('lang');
    }
    public function variant()
    {
        return $this->hasOne('App\Models\VariantLang', 'id', 'attrinute_value_id');
    }
    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }
}
