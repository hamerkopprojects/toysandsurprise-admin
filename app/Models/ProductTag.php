<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductTag extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "product_tag";

    public function tag()
    {
        return $this->belongsTo('App\Models\TagLang', 'tag_id', 'tag_id')->where('deleted_at', NULL)->where('language', 'en');
    }
}
