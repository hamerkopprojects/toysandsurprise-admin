<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductB2BPrice extends Model
{
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "product_variant_b2b_price";

    public function attribute()
    {
        return $this->belongsTo('App\Models\Attribute', 'variant_attribute_id', 'id')->with('lang');
    }
    public function variant()
    {
        return $this->hasOne('App\Models\VariantLang', 'id', 'variant_id');
    }
}
