<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CancellationReasonLang extends Model
{
    /*
    * guarded variable
    *
    * @var array
    */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "cancellation_reason_i18n";

    public function reason()
    {
        return $this->belongsTo(CancellationReason::class, 'cancellation_id');
    }
}
