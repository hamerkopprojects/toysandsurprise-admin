<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductAge extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "product_age";

    public function age()
    {
        return $this->belongsTo('App\Models\AgeLang', 'gender_id', 'gender_id')->where('deleted_at', NULL)->where('language', 'en');
    }
    
}
