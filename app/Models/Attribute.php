<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attribute extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "attribute";

    public function lang()
    {
        return $this->hasMany(AttributeLang::class, 'attribute_id');
    }
}
