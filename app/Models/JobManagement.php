<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobManagement extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */


    protected $table = "job_management";

    public function lang()
    {
        return $this->hasMany(JobManagementLang::class, 'management_id')->withTrashed();
    }
    public function jobCategory()
    {
        return $this->belongsTo(JobCategory::class, 'job_category_id')->where('deleted_at', NULL);
    }
}
