<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table = 'slider';

    public function slider_category()
    {
        return $this->belongsTo('App\Models\Category', 'category')->with('lang')->where('deleted_at', NULL);
    }
    public function slider_product()
    {
        return $this->belongsTo('App\Models\Product', 'product')->with('lang')->where('deleted_at', NULL);
    }
}
