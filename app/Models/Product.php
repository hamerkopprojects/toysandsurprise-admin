<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "product";

    public function lang()
    {
        return $this->hasMany(ProductLang::class, 'product_id');
    }
    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id')->with('lang')->where('deleted_at', NULL);
    }
    public function age()
    {
        return $this->belongsTo('App\Models\Age', 'age_id')->with('lang')->where('deleted_at', NULL);
    }
    public function brand()
    {
        return $this->belongsTo('App\Models\Brand', 'brand_id')->with('lang')->where('deleted_at', NULL);
    }
    public function b2cprice()
    {
        return $this->hasMany('App\Models\ProductB2CPrice', 'product_id');
    }
    public function product_attribute()
    {
     return $this->hasMany('App\Models\ProductAttribute', 'product_id', 'id')->with('lang');
    }
    public function photos()
    {
        return $this->hasMany('App\Models\ProductImages', 'product_id');
    }
    
    public function b2bprice()
    {
        return $this->hasMany('App\Models\ProductB2BPrice', 'product_id');
    }
    
    public function b2bpricevariant()
    {
        return $this->hasMany('App\Models\ProductVariant', 'product_id');
    }
    public function produc_images()
    {
        return $this->hasMany('App\Models\ProductImages', 'product_id');
    }
    public function product_category()
    {
     return $this->hasMany('App\Models\ProductCategories', 'product_id', 'id');
    }
    public function product_age()
    {
     return $this->hasMany('App\Models\ProductAge', 'product_id', 'id');
    }
    public function product_gender()
    {
     return $this->hasMany('App\Models\ProductGender', 'product_id', 'id');
    }
    public function product_promotional()
    {
     return $this->hasMany('App\Models\PromotionalProduct', 'product_id', 'id');
    }
    public function product_tag()
    {
     return $this->hasMany('App\Models\ProductTag', 'product_id', 'id');
    }
}
