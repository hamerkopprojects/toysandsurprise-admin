<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecentlyViewedProducts extends Model
{
/**
 * guarded variable
 *
 * @var array
 */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table="recently_viewed_products";
   
    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
}
