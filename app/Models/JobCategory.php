<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobCategory extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */


    protected $table = "job_category";

    public function lang()
    {
        return $this->hasMany(JobCategoryLang::class, 'category_id')->withTrashed();
    }
}
