<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TempB2cStock extends Model
{
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "temp_b2c_stock";
}
