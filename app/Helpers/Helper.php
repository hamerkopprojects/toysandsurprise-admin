<?php

namespace App\Helpers;

use App\Models\ProductVariant;
use App\Models\ProductAttribute;
use App\Models\ProductB2CPrice;
use App\Models\ProductB2BPrice;
use App\Models\WishList;
use App\Models\OtherSettings;
use Illuminate\Support\Facades\Session;

class Helper {

    public static function get_variant($pdt_id, $product_type = '') {
        if ($product_type == 'simple') {
            $variants = ProductAttribute::with('attribute')->where(['product_id' => $pdt_id])->count();
        } else {
            $variants = ProductVariant::with('variant')->where(['product_id' => $pdt_id])->count();
        }
        return $variants;
    }

    public static function get_b2c_price($pdt_id) {
        $b2c_count = ProductB2CPrice::where(['product_id' => $pdt_id])->count();
        return $b2c_count;
    }

    public static function get_b2b_price($pdt_id) {
        $b2b_count = ProductB2BPrice::where(['product_id' => $pdt_id])->count();
        return $b2b_count;
    }

    public static function wish_listed_products() {
        //for whish list
        $customer = Session::get('customer_id');
        $wishListAll = [];
        if ($customer) {
            $wishList = WishList::where('customer_id', $customer)->get();
            foreach ($wishList as $wish) {
                $wishListAll[] = $wish->product_id;
            }
        }
        return $wishListAll;
        //end
    }
}
