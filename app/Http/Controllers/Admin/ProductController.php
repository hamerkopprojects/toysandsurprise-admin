<?php

namespace App\Http\Controllers\admin;

use DB;
use Config;
use Validator;
use App\Models\Brand;
//use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;
use App\Models\AgeLang;
use App\Models\Product;
use App\Models\TagLang;
use App\Models\Variant;
use App\Models\Category;
use App\Rules\MaxPhotos;
use App\Models\Attribute;
use App\Models\BrandLang;
use App\Models\GenderLang;
use App\Models\ProductAge;
use App\Models\ProductTag;
use App\Models\ProductLang;
use App\Models\VariantLang;
use App\Models\CategoryLang;
use Illuminate\Http\Request;
use App\Models\AttributeLang;
use App\Models\ProductGender;
use App\Models\ProductImages;
use App\Models\ProductVariant;
use Illuminate\Support\Carbon;
use App\Models\ProductB2BPrice;
use App\Models\ProductB2CPrice;
use App\Models\ProductAttribute;
use App\Models\ProductViewCount;
use App\Models\ProductCategories;
use App\Http\Controllers\Controller;
use App\Models\ProductAttributeLang;
use App\Models\RecentlyViewedProducts;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{

    //list products
    public function index(Request $req)
    {

        $search = $req->search;
        $query = Product::with('lang');
        if ($search) {
            $query->whereHas('lang', function ($query) use ($search) {
                $query->where('name', 'like', "%" . $search . "%");
                $query->orWhere('sku', 'like', "%" . $search . "%");
            });
        }
        if ($req->cat_id) {
            $query->where('category_id', '=', $req->cat_id);
        }
        $pdt_data = $query->where('deleted_at', null)
            ->orderBy('id', 'desc')
            ->paginate(20);


        $cat_data = Category::leftJoin('category_i18n', 'category_i18n.category_id', '=', 'category.id')
            ->select('category_i18n.name as cat_name', 'category_i18n.category_id as id')
            ->where('category_i18n.language', '=', 'en')
            ->where('category.parent_id', NULL)
            ->get(['id', 'cat_name']);

        $data = [
            'pdt_data' => $pdt_data,
            'cat_data' => $cat_data,
            'search' => $req->search,
            'category_id' => $req->cat_id,
        ];
        return view('admin.product.products.index', $data);
    }

    public function create(Request $req)
    {
        $_product_id = $req->id;
        $cat_id = $req->cat_id ? $req->cat_id : NULL;
        $tag_ids =  NULL;
        $age_ids=  NULL;
        $gender_ids = NULL;
        $row_data = $lang_array = $product_data = array();
        $cat_data = Category::with(['lang' => function ($query) {
            $query->where('language', 'en');
        }])->where('parent_id', NULL)->get();
        $multiple_cat = CategoryLang::where(['language' => 'en'])->get();
        $brand_data = BrandLang::where(['language' => 'en'])->get();
        $tag = TagLang::where(['language' => 'en'])->get();
        $age = AgeLang::where(['language' => 'en'])->get();
        $gender = GenderLang::where(['language' => 'en'])->get();
        $is_variant = Attribute::where(['is_variant' => 'yes'])->exists();
        if ($_product_id) {
            $product_data = Product::with('lang')->with('category')->with('brand')->where('id', $_product_id)->first();
            $sku = '';
            $selected_categories = str_replace('}}{{', ',', $product_data->category_ids);
            $selected_categories = str_replace('{{', ' ', $selected_categories);
            $selected_categories = str_replace('}}', ' ', $selected_categories);
            $cat_ids = explode(",", $selected_categories);
            if ($product_data->tag_ids) {
                $selected_tags = str_replace('}}{{', ',', $product_data->tag_ids);
                $selected_tags = str_replace('{{', ' ', $selected_tags);
                $selected_tags = str_replace('}}', ' ', $selected_tags);
                $tag_ids = explode(",", $selected_tags);
            }
            if ($product_data->age_ids) {
                $selected_ages = str_replace('}}{{', ',', $product_data->age_ids);
                $selected_ages = str_replace('{{', ' ', $selected_ages);
                $selected_ages = str_replace('}}', ' ', $selected_ages);
                $age_ids = explode(",", $selected_ages);
            }
            if ($product_data->gender_ids) {
                $selected_genders = str_replace('}}{{', ',', $product_data->gender_ids);
                $selected_genders = str_replace('{{', ' ', $selected_genders);
                $selected_genders = str_replace('}}', ' ', $selected_genders);
                $gender_ids = explode(",", $selected_genders);
            }
        } else {
            $sku = $this->generateSku();
            $cat_ids = '';
        }
        $data = [
            'row_data' => $row_data,
            'cat_data' => $cat_data,
            'multiple_cat' => $multiple_cat,
            'brand_data' => $brand_data,
            'sku' => $sku,
            'row_data' => $product_data,
            'is_variant' => $is_variant,
            'cat_ids' => $cat_ids,
            'tag' => $tag,
            'tag_ids' => $tag_ids,
            'age' => $age,
            'age_ids' => $age_ids,
            'gender' => $gender,
            'gender_ids' => $gender_ids,

        ];
        return view('admin.product.products.create', $data);
    }

    // activate/deactivate attribute
    public function activate(Request $req)
    {
       
        $product_data = Product::where('id', $req->id)->first();
        $variant_count = Helper::get_variant($req->id, $product_data->product_type);
        $b2c_count = Helper::get_b2c_price($req->id);

        $pdt = Product::where('id', $req->id)->first();
        if ($pdt) {
            if ($product_data->product_type == 'complex') {
                if ($variant_count > 0 && $b2c_count > 0) {
                    if ($pdt->status == 'deactive') {
                        Product::where('id', $req->id)
                            ->update([
                                'status' => 'active'
                            ]);
                    } else {
                        Product::where('id', $req->id)
                            ->update([
                                'status' => 'deactive'
                            ]);
                    }
                    return response()->json(['status' => 1, 'message' => 'Status updated successfully']);
                } else {
                    return response()->json(['status' => 0, 'message' => 'Product cannot be activated. Please fill all required data']);
                }
            } else {
                if ($b2c_count > 0) {
                    if ($pdt->status == 'deactive') {
                        Product::where('id', $req->id)
                            ->update([
                                'status' => 'active'
                            ]);
                    } else {
                        Product::where('id', $req->id)
                            ->update([
                                'status' => 'deactive'
                            ]);
                    }
                    return response()->json(['status' => 1, 'message' => 'Status updated successfully']);
                } else {
                    return response()->json(['status' => 0, 'message' => 'Product cannot be activated. Please fill all required data']);
                }
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }

    public function deleteProduct(Request $req)
    {
        if ($req->id) {
            $pdt = Product::find($req->id);
            $product_variant = ProductVariant::where('product_id', $req->id)->first();
            $pdt_cat = ProductCategories::where('product_id', $req->id)->first();

            $product_b2c_price = ProductB2CPrice::where('product_id', $req->id)->first();
            $product_b2c_price = ProductB2CPrice::where('product_id', $req->id)->first();
            // $recently_viewed_products = RecentlyViewedProducts::where('product_id', $req->id)->first();
            // $product_view_count = ProductViewCount::where('product_id', $req->id)->first();
            if (!empty($pdt)) {
                if (!empty($pdt_cat)) {
                    $pdt_cat->delete();
                }
                if (!empty($product_variant)) {
                    $product_variant->delete();
                }

                if (!empty($product_b2c_price)) {
                    $product_b2c_price->delete();
                }
                // if (!empty($recently_viewed_products)) {
                //     $recently_viewed_products->delete();
                // }
                // if (!empty($product_view_count)) {
                //     $product_view_count->delete();
                // }
                $pdt->lang()->delete();
                $pdt->delete();
                return response()->json(['status' => 1, 'message' => 'Product deleted successfully']);
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }

    public function get_tabs(Request $req)
    {
        $product_type = Product::where('id', $req->pdt_id)->pluck('product_type');
        $product_type = $product_type[0];
        $pdt_id = $req->pdt_id;
        if ($req->activeTab == 'PRODUCT INFO') {
            $cat_data = Category::with(['lang' => function ($query) {
                $query->where('language', 'en');
            }])->where('parent_id', NULL)->get();
            $multiple_cat = CategoryLang::where(['language' => 'en'])->get();
            $brand_data = BrandLang::where(['language' => 'en'])->get();
            $tag = TagLang::where(['language' => 'en'])->get();
        $age = AgeLang::where(['language' => 'en'])->get();
        $gender = GenderLang::where(['language' => 'en'])->get();
            $pdt_id = $req->pdt_id;
            $sku = $cat_ids = '';
            if ($req->pdt_id) {
                $row_data = Product::with('lang')->with('category')->with('brand')->where('id', $req->pdt_id)->first();
                $selected_categories = str_replace('}}{{', ',', $row_data->category_ids);
                $selected_categories = str_replace('{{', ' ', $selected_categories);
                $selected_categories = str_replace('}}', ' ', $selected_categories);
                $cat_ids = explode(",", $selected_categories);

                if ($row_data->tag_ids) {
                    $selected_tags = str_replace('}}{{', ',', $row_data->tag_ids);
                    $selected_tags = str_replace('{{', ' ', $selected_tags);
                    $selected_tags = str_replace('}}', ' ', $selected_tags);
                    $tag_ids = explode(",", $selected_tags);
                }
                if ($row_data->age_ids) {
                    $selected_ages = str_replace('}}{{', ',', $row_data->age_ids);
                    $selected_ages = str_replace('{{', ' ', $selected_ages);
                    $selected_ages = str_replace('}}', ' ', $selected_ages);
                    $age_ids = explode(",", $selected_ages);
                }
                if ($row_data->gender_ids) {
                    $selected_genders = str_replace('}}{{', ',', $row_data->gender_ids);
                    $selected_genders = str_replace('{{', ' ', $selected_genders);
                    $selected_genders = str_replace('}}', ' ', $selected_genders);
                    $gender_ids = explode(",", $selected_genders);
                }
            }
            $is_variant = Attribute::where(['is_variant' => 'yes'])->exists();
            return view('admin.product.products.product_info', compact('cat_data', 'multiple_cat', 'brand_data', 'pdt_id', 'row_data', 'sku', 'is_variant', 'cat_ids', 'age_ids', 'gender_ids', 'tag_ids', 'tag', 'age', 'gender'));
        } elseif ($req->activeTab == 'SPEC & STOCK') {
            $pdt_id = $req->pdt_id;
            $pdt_att_var = $pdt_att_row_data = array();
            $pdt_data = Product::where(['id' => $req->pdt_id])->first();

            $pdt_att = Attribute::with('lang')->leftJoin('category_attributes', 'category_attributes.attribute_id', '=', 'attribute.id')
                ->select('attribute.*')
                ->where('category_attributes.category_id', '=', $pdt_data->category_id)
                ->where('attribute.attribute_type', '!=', 'dropdown')
                ->groupBy('category_attributes.attribute_id')
                ->get();

            $pdt_var = Attribute::with('lang')->leftJoin('category_attributes', 'category_attributes.attribute_id', '=', 'attribute.id')
                ->select('attribute.*')
                ->where('category_attributes.category_id', '=', $pdt_data->category_id)
                ->where('attribute.is_variant', 'no')
                ->where('attribute.attribute_type', '=', 'dropdown')
                ->groupBy('category_attributes.attribute_id')
                ->get();

            if ($product_type == 'complex') {
                $pdt_att_var = Attribute::with('lang')->leftJoin('category_attributes', 'category_attributes.attribute_id', '=', 'attribute.id')
                    ->select('attribute.*')
                    ->where('category_attributes.category_id', '=', $pdt_data->category_id)
                    ->where('attribute.is_variant', 'yes')
                    ->where('attribute.attribute_type', '=', 'dropdown')
                    ->groupBy('category_attributes.attribute_id')
                    ->first();
            }
            return view('admin.product.products.product_stock')->with(compact('pdt_id', 'pdt_att', 'pdt_var', 'pdt_att_var', 'product_type'))->render();
        } elseif ($req->activeTab == 'PRICE') {
            $pdt_data = Product::where(['id' => $req->pdt_id])->first();
            $pdt_att_var = array();
            if ($product_type == 'complex') {

                $pdt_att_var = Attribute::with('lang')->leftJoin('category_attributes', 'category_attributes.attribute_id', '=', 'attribute.id')
                    ->select('attribute.*')
                    ->where('category_attributes.category_id', '=', $pdt_data->category_id)
                    ->where('attribute.is_variant', 'yes')
                    ->where('attribute.attribute_type', '=', 'dropdown')
                    ->groupBy('category_attributes.attribute_id')
                    ->first();
            }
            $b2c_simple = ProductB2CPrice::where('product_id', $req->pdt_id)->first();

            return view('admin.product.products.product_b2c_price')->with(compact('pdt_id', 'pdt_att_var', 'product_type', 'b2c_simple'))->render();
        } elseif ($req->activeTab == 'PHOTOS') {
            $pdt_data = Product::where(['id' => $pdt_id])->first();
            $cover_image = $pdt_data->cover_image ? $pdt_data->cover_image : '';
            $pdt_images = ProductImages::where(['product_id' => $pdt_id])->get();
            return view('admin.product.products.product_images')->with(compact('pdt_id', 'pdt_images', 'cover_image'))->render();
        }
    }

    public function product_info(Request $req)
    {
      

        if ($req->pdt_id) {
            $unique = ',' . $req->pdt_id;
        } else {
            $unique = ',NULL';
        }
        $rules = [
            'sku' => 'required',
            'name_en' => 'required|unique:product_i18n,name' . $unique . ',product_id,language,en',
            'name_ar' => 'required',
            'category' => 'required',
            'brand' => 'required',
            'description_en' => 'required',
            'description_ar' => 'required',
        ];
        $messages = [
            'name_en.unique' => 'Product already exist.',
            'sku.required' => 'SKU is required.',
            'name_en.required' => 'Product Name(EN) is required.',
            'name_ar.required' => 'Product Name(AR) is required.',
            'category.required' => 'Category is required.',
            'brand.required' => 'Brand is required.',
            'description_en.required' => 'Description(EN) is required.',
            'description_ar.required' => 'Description(AR) is required.',
        ];


        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            if ($req->product_type == 'complex') {
                $pdt_att_var = Attribute::with('lang')->leftJoin('category_attributes', 'category_attributes.attribute_id', '=', 'attribute.id')
                    ->select('attribute.*')
                    ->where('category_attributes.category_id', '=', $req->category)
                    ->where('attribute.is_variant', 'yes')
                    ->where('attribute.attribute_type', '=', 'dropdown')
                    ->groupBy('category_attributes.attribute_id')
                    ->first();
                if (empty($pdt_att_var)) {
                    return response()->json(['status' => 0, 'message' => 'System not allowed to add complex products under this category <br/>since no variant attributes found under this category']);
                }
            }


            if (empty($req->pdt_id)) {
                $product = DB::transaction(function () use ($req) {
                    $product = Product::create([
                        'category_id' => $req->category,
                        'brand_id' => $req->brand,
                        'sku' => $req->sku,
                        'product_type' => $req->product_type,
                        'created_at' => Carbon::now(),
                        'slug' => $this->createSlug($req->name_en)
                    ]);

                    $product->lang()->createMany([
                        [
                            'name' => $req->name_en,
                            'description' => $req->description_en,
                            'ingredients' => $req->ingredients_en,
                            'how_to_use' => $req->how_to_use_en,
                            'reasons_to_buy' => $req->reasons_to_buy_en,
                            'specification' => $req->summaryen,
                            'language' => 'en',
                        ],
                        [ 'specification' => $req->summaryen,
                            'name' => $req->name_ar,
                            'description' => $req->description_ar,
                            'ingredients' => $req->ingredients_ar,
                            'how_to_use' => $req->how_to_use_ar,
                            'reasons_to_buy' => $req->reasons_to_buy_ar,
                            'specification' => $req->summaryar,
                            'language' => 'ar',
                        ],
                    ]);

                    return $product;
                });
                $msg = 'Product info added successfully';
            } else {
                $product = DB::transaction(function () use ($req) {
                    $product = Product::where('id', $req->pdt_id)
                        ->update([
                            'category_id' => $req->category,
                            'brand_id' => $req->brand,
                            'product_type' => $req->product_type,
                            'slug' => $this->createSlug($req->name_en)
                        ]);
                    ProductLang::where('language', 'en')
                        ->where('product_id', $req->pdt_id)
                        ->update([
                            'name' => $req->name_en,
                            'description' => $req->description_en,
                            'ingredients' => $req->ingredients_en,
                            'how_to_use' => $req->how_to_use_en,
                            'reasons_to_buy' => $req->reasons_to_buy_en,
                            'specification' => $req->summaryen,
                        ]);
                    ProductLang::where('language', 'ar')
                        ->where('product_id', $req->pdt_id)
                        ->update([
                            'name' => $req->name_ar,
                            'description' => $req->description_ar,
                            'ingredients' => $req->ingredients_ar,
                            'how_to_use' => $req->how_to_use_ar,
                            'reasons_to_buy' => $req->reasons_to_buy_ar,
                            'specification' => $req->summaryar,
                        ]);

                    return $product;
                });
                $msg = 'Product info updated successfully';
            }
            if ($product) {
                $pdt_id = !empty($product->id) ? $product->id : $req->pdt_id;
                //For multiple category
                $cat_ids = '';
                $sec_category = $req->input('sec_category');
                if (ProductCategories::where('product_id', $pdt_id)->exists()) {
                    ProductCategories::where('product_id', $pdt_id)->delete();
                }
                if ($sec_category) {
                    foreach ($sec_category as $value) {
                        $cat_ids .= '{{' . $value . '}}';
                        $saved = ProductCategories::create([

                            'product_id' => $pdt_id,
                            'category_id' => $value
                        ]);
                    }
                    $product_categories = Product::where('id', $pdt_id)
                        ->update([
                            'category_ids' => $cat_ids,
                        ]);
                }

                //For multiple tag
                $tag_ids = '';
                $tag = $req->input('tag');
                if (ProductTag::where('product_id', $pdt_id)->exists()) {
                    ProductTag::where('product_id', $pdt_id)->delete();
                }
                if ($tag) {
                    foreach ($tag as $value) {
                        $tag_ids .= '{{' . $value . '}}';
                        $saved = ProductTag::create([

                            'product_id' => $pdt_id,
                            'tag_id' => $value
                        ]);
                    }
                    $product_tag = Product::where('id', $pdt_id)
                        ->update([
                            'tag_ids' => $tag_ids,
                        ]);
                }


                //For multiple age
                $age_ids = '';
                $age = $req->input('age');
                if (ProductAge::where('product_id', $pdt_id)->exists()) {
                    ProductAge::where('product_id', $pdt_id)->delete();
                }
                if ($age) {
                    foreach ($age as $value) {
                        $age_ids .= '{{' . $value . '}}';
                        $saved = ProductAge::create([

                            'product_id' => $pdt_id,
                            'age_id' => $value
                        ]);
                    }
                    $product_age = Product::where('id', $pdt_id)
                        ->update([
                            'age_ids' => $age_ids,
                        ]);
                }

                  //For multiple gender
                  $gender_ids = '';
                  $gender = $req->input('gender');
                  if (ProductGender::where('product_id', $pdt_id)->exists()) {
                    ProductGender::where('product_id', $pdt_id)->delete();
                  }
                  if ($gender) {
                      foreach ($gender as $value) {
                          $gender_ids .= '{{' . $value . '}}';
                          $saved = ProductGender::create([
  
                              'product_id' => $pdt_id,
                              'gender_id' => $value
                          ]);
                      }
                      $product_gender = Product::where('id', $pdt_id)
                          ->update([
                              'gender_ids' => $gender_ids,
                          ]);
                  }
                
                //end multiple category
                $pdt_att_var = $pdt_att_row_data = array();
                $product_type = $req->product_type ? $req->product_type : 'simple';

                $pdt_att = Attribute::with('lang')->leftJoin('category_attributes', 'category_attributes.attribute_id', '=', 'attribute.id')
                    ->select('attribute.*')
                    ->where('category_attributes.category_id', '=', $req->category)
                    ->where('attribute.attribute_type', '!=', 'dropdown')
                    ->groupBy('category_attributes.attribute_id')
                    ->get();

                $pdt_var = Attribute::with('lang')->leftJoin('category_attributes', 'category_attributes.attribute_id', '=', 'attribute.id')
                    ->select('attribute.*')
                    ->where('category_attributes.category_id', '=', $req->category)
                    ->where('attribute.is_variant', 'no')
                    ->where('attribute.attribute_type', '=', 'dropdown')
                    ->groupBy('category_attributes.attribute_id')
                    ->get();

                if ($product_type == 'complex') {
                    $pdt_att_var = Attribute::with('lang')->leftJoin('category_attributes', 'category_attributes.attribute_id', '=', 'attribute.id')
                        ->select('attribute.*')
                        ->where('category_attributes.category_id', '=', $req->category)
                        ->where('attribute.is_variant', 'yes')
                        ->where('attribute.attribute_type', '=', 'dropdown')
                        ->groupBy('category_attributes.attribute_id')
                        ->first();
                }
            }
            $html = view('admin.product.products.product_stock')->with(compact('pdt_id', 'pdt_att', 'pdt_var', 'pdt_att_var', 'product_type'))->render();
            return response()->json(['status' => 1, 'message' => $msg, 'result' => $html]);
        }
    }

    public function product_stock(Request $req)
    {
        $rules = [
            'stock' => 'required',
        ];
        $messages = [
            'stock.required' => 'Stock is required.',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            if ($req->pdt_id) {
                $flag = 0;
                $pdt_att = ProductAttribute::with('lang')->with('attribute')->with('product')
                    ->where('product_id', $req->pdt_id)
                    ->get();
                if (count($pdt_att) > 0) {
                    $flag = 1;
                    foreach ($pdt_att as $row_data_val) {
                        $pdt_att_value = ProductAttribute::find($row_data_val->id);

                        if (!empty($pdt_att_value)) {
                            $pdt_att_value->lang()->delete();
                            $pdt_att_value->delete();
                        }
                    }
                }
                $pdt_att_var = ProductVariant::with('attribute')->with('variant')
                    ->where('product_id', $req->pdt_id)
                    ->get();
                if (count($pdt_att_var) > 0) {
                    $flag = 1;
                    foreach ($pdt_att_var as $row_data) {
                        $pdt_var_value = ProductVariant::find($row_data->id);

                        if (!empty($pdt_var_value)) {
                            $pdt_var_value->delete();
                        }
                    }
                }
                $attribute = DB::transaction(function () use ($req) {
                    $attribute_id = $req->attribute_id;
                    $i = 0;
                    if (!empty($attribute_id)) {
                        foreach ($attribute_id as $att_id) {
                            $att_value_type = $req->attribute_value_type[$i];
                            $att_val_id = 'att_val_' . $att_id . '_id' ? 'att_val_' . $att_id . '_id' : NULL;
                            $variant_id = 'variant_id' . $att_id ? 'variant_id' . $att_id : NULL;
                            $attribute = ProductAttribute::create([
                                'product_id' => $req->pdt_id,
                                'attribute_id' => $att_id,
                                'variant_id' => $req->$variant_id,
                                'attribute_value_type' => $att_value_type,
                                'attrinute_value_id' => $req->$att_val_id,
                                'created_at' => Carbon::now()
                            ]);

                            if ($att_value_type == 'textbox' || $att_value_type == 'textarea') {
                                $name_en = 'att_name_' . $att_id . '_en';
                                $name_ar = 'att_name_' . $att_id . '_ar';
                                $attribute->lang()->createMany([
                                    [
                                        'name' => $req->$name_en,
                                        'language' => 'en',
                                    ],
                                    [
                                        'name' => $req->$name_ar,
                                        'language' => 'ar',
                                    ],
                                ]);
                            }
                            $i++;
                        }
                    }
                    if ($req->product_type == 'complex' && !empty($req->var_attribute_id)) {
                        $attribute_variant_id = $req->attribute_variant_id;
                        if (!empty($attribute_variant_id)) {
                            foreach ($attribute_variant_id as $attribute_var_id) {
                                if (!empty($attribute_var_id)) {
                                    $var_stock = 'var_stock_' . $attribute_var_id;
                                    $attribute = ProductVariant::create([
                                        'attribute_id' => $req->var_attribute_id,
                                        'variant_id' => $req->var_id,
                                        'product_id' => $req->pdt_id,
                                        'attribute_variant_id' => $attribute_var_id,
                                        'stock' => $req->$var_stock,
                                        'created_at' => Carbon::now()
                                    ]);
                                }
                            }
                        }
                    }
                    if (!empty($req->stock)) {
                        $attribute = Product::where('id', $req->pdt_id)
                            ->update([
                                "stock" => $req->stock
                            ]);
                    }
                    return $attribute;
                });
            }
            if ($flag == 1) {
                $msg = "Stock updated successfully";
            } else {
                $msg = "Stock added successfully";
            }
        }
        if ($attribute) {
            $pdt_id = $req->pdt_id;
            $pdt_data = Product::where(['id' => $req->pdt_id])->first();
            $product_type = $pdt_data->product_type;
            $pdt_att_var = $attribute_ids = array();
            if ($product_type == 'complex') {
                $pdt_att_var = Attribute::with('lang')->leftJoin('category_attributes', 'category_attributes.attribute_id', '=', 'attribute.id')
                    ->select('attribute.*')
                    ->where('category_attributes.category_id', '=', $pdt_data->category_id)
                    ->where('attribute.is_variant', 'yes')
                    ->where('attribute.attribute_type', '=', 'dropdown')
                    ->groupBy('category_attributes.attribute_id')
                    ->first();
            }
            $b2c_simple = ProductB2CPrice::where('product_id', $req->pdt_id)->first();
            $html = view('admin.product.products.product_b2c_price')->with(compact('pdt_id', 'pdt_att_var', 'product_type', 'b2c_simple'))->render();
            return response()->json(['status' => 1, 'message' => $msg, 'result' => $html]);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }

    public function product_b2c_price(Request $req)
    {
        if ($req->product_type == 'simple') {
            if(!empty($req->discount_price)){
            $rules = [
                'price' => 'required',
                'discount_price' => 'lt:price',
            ];
            $messages = [
                'price.required' => 'Price is required.',
                'discount_price.lt' => 'Discount price must be less than the price',
            ];
        }else{
            $rules = [
                'price' => 'required',
               
            ];
            $messages = [
                'price.required' => 'Price is required.',
                
            ];  
        }
        } else {
            $rules = [
                'price_var.*' => 'required',
                'discount_price_var.*' => 'required|lt:price_var.*',
            ];
            $messages = [
                'price_var.required' => 'Price is required.',
                'discount_price_var.required' => 'Discount price is required.',
                'lt' => 'Discount price must be less than the price',
            ];
        }
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $flag = 0;
            $pdt_id = $req->pdt_id;
            $prdt_b2c_price = ProductB2CPrice::where('product_id', $req->pdt_id)->get();
            if (count($prdt_b2c_price) > 0) {
                $flag = 1;
                foreach ($prdt_b2c_price as $row_data_val) {
                    $b2c_price = ProductB2CPrice::find($row_data_val->id);
                    $b2c_price->delete();
                }
            }
            $pdt_data = Product::where(['id' => $req->pdt_id])->first();
            $container_type = Config::get('constants.container_type');
            if (!empty($pdt_data)) {
                if ($pdt_data->product_type == 'simple') {
                    ProductB2CPrice::create([
                        'product_id' => $req->pdt_id,
                        'price' => $req->price,
                        'discount_price' => $req->discount_price,
                        'created_at' => Carbon::now()
                    ]);
                } else {
                    if (!empty($req->var_attribute_id)) {
                        $attribute_variant_id = $req->variant_id;
                        $i = 0;
                        foreach ($attribute_variant_id as $attribute_var_id) {
                            if (!empty($attribute_var_id))
                                $attribute = ProductB2CPrice::create([
                                    'variant_attribute_id' => $req->var_attribute_id,
                                    'varient_id_main' => $req->varient_id_main,
                                    'product_id' => $req->pdt_id,
                                    'variant_id' => $attribute_var_id,
                                    'price' => $req->price_var[$i],
                                    'discount_price' => $req->discount_price_var[$i],
                                    'created_at' => Carbon::now()
                                ]);

                            $i++;
                        }
                    }
                }

                $product_type = $pdt_data->product_type;
                $pdt_att_var = array();
                if ($product_type == 'complex') {
                    $pdt_att_var = Attribute::with('lang')->leftJoin('category_attributes', 'category_attributes.attribute_id', '=', 'attribute.id')
                        ->select('attribute.*')
                        ->where('category_attributes.category_id', '=', $pdt_data->category_id)
                        ->where('attribute.is_variant', 'yes')
                        ->where('attribute.attribute_type', '=', 'dropdown')
                        ->groupBy('category_attributes.attribute_id')
                        ->first();
                }
                $container_type = Config::get('constants.container_type');
                $pdt_data = Product::where(['id' => $pdt_id])->first();
                $cover_image = $pdt_data->cover_image ? $pdt_data->cover_image : '';
                $pdt_images = ProductImages::where(['product_id' => $pdt_id])->get();
                
                if ($flag == 1) {
                    $msg = "Price updated successfully";
                } else {
                    $msg = "Price added successfully";
                }
                $html = view('admin.product.products.product_images')->with(compact('pdt_id', 'cover_image', 'pdt_images'))->render();
                return response()->json(['status' => 1, 'message' => $msg, 'result' => $html]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }



    public function product_images(Request $req)
    {
        $pdt_id = $req->pdt_id;
        if ($req->upload_type == 'single') {
            $rules = [
                'photo' => 'required|image|mimes:png,jpg,jpeg|max:1052672'
            ];
            $messages = [
                'photo.max' => 'The image must be less than 2Mb in size',
                'photo.mimes' => "The image must be of the format jpeg or png",
            ];
        } else {
            $rules = [
                'photos' => [
                    'required', 'array'
                ],
                'photos.*' => 'image|mimes:png,jpg,jpeg|max:1052672',
            ];
            $messages = [
                'photos.max' => 'The image must be less than 2Mb in size',
                'photos.mimes' => "The image must be of the format jpeg or png",
            ];
        }
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            if ($req->upload_type == 'single') {
                $file = request()->file('photo');
                $path = $file->store("products/cover", ['disk' => 'public_uploads']);
                Product::where('id', $pdt_id)
                    ->update([
                        "cover_image" => $path
                    ]);
                $full_path = url('uploads/' . $path);
                return response()->json(['status' => 1, 'message' => 'Cover image added successfully', 'path' => $full_path, 'pdt_id' => $pdt_id]);
            } else {
                $productImages = $req->file('photos') ? $req->file('photos') : [];

                foreach ($productImages as $image) {
                    $saved = ProductImages::create([
                        'product_id' => $pdt_id,
                        'path' => $image->store("products/product_images", ['disk' => 'public_uploads']),
                        'created_at' => Carbon::now()
                    ]);
                }
                $pdt_images = ProductImages::where(['id' => $saved['id']])->first();
                $full_path = url('uploads/' . $pdt_images->path);
                return response()->json(['status' => 1, 'message' => 'Product image added successfully', 'path' => $full_path, 'image_id' => $saved['id']]);
            }
        }
    }

    public function detete_img(Request $request)
    {
        if ($request->type == 'cover') {
            $product = Product::where('id', $request->id)->first();
            Product::where('id', $request->id)
                ->update([
                    "cover_image" => Null
                ]);
            if (!empty($product)) {
                $file_path = public_path() . '/uploads/' . $product["cover_image"];
                if (!empty($file_path)) {
                    unlink($file_path);
                }
            }
            $msg = 'Cover image deleted successfully';
        } else {
            $product_img = ProductImages::find($request->id);
            if (!empty($product_img)) {
                $file_path = public_path("uploads/{$product_img["path"]}");
                $product_img->delete();
                if (!empty($file_path)) {
                    unlink($file_path);
                }
            }
            $msg = 'Product image deleted successfully';
        }

        return response()->json(['status' => 1, 'message' => $msg, 'id' => $request->id, 'type' => $request->type]);
    }

    public function generateSku()
    {
        $lastsku = Product::select('sku')
            ->orderBy('id', 'desc')
            ->first();

        $lastId = 0;

        if ($lastsku) {
            $lastId = (int) substr($lastsku->sku, 4);
        }

        $lastId++;

        return 'SKU-' . str_pad($lastId, 5, '0', STR_PAD_LEFT);
    }

    private function createSlug($string)
    {
        $slug = strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), trim($string)));
        return $slug;
    }
}
