<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use App\Models\Category;
use App\Models\Attribute;
use App\Models\CategoryLang;
use Illuminate\Http\Request;
use App\Models\ProductCategories;
use App\Models\CategoryAttributes;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{

    public function index(Request $req)
    {
        $category = Category::with('lang')->get();
        $main_cat = Category::with('lang')
            ->where('parent_id', NULL)
            ->get();
        $search = $req->search ?? '';
        $search_catid = $req->search_catid ?? '';
        $cat_id = $req->select_maincategory ?? '';

        $query = Category::query();
        if (!empty($search)) {
            $query->whereHas('lang', function ($query) use ($search) {
                $query->where('name', 'like', "%" . $search . "%")
                    ->orWhere('category_unique_id', 'like', "%" . $search . "%");
            });
        }
        if (!empty($cat_id)) {
            $cats = array();
            $cats = Category::select('id')->with('lang')
                ->where('parent_id', $cat_id)
                ->get();
            $new_array = [];
            foreach ($cats as $catss) {
                $new_array[] = $catss['id'];
            }
            $query->whereIn('parent_id', $new_array)->orWhere('parent_id', $cat_id);
        }
        $query->with('lang');

        if (!empty($search_catid)) {
            $query->where('category_unique_id', $search_catid);
        }
        $category = $query->paginate(20);

        return view('admin.product.category.index', compact('category', 'main_cat', 'search_catid', 'cat_id', 'search'));
    }

    public function store(Request $req)
    {

        $rules = [
            'category_name_en' => 'required|unique:category_i18n,name,NULL,id,deleted_at,NULL',
            'category_name_ar' => 'required',
        ];
        $messages = [
            'category_name_en.unique' => 'Category already exist.',
            'category_name_en.required' => 'category_name_en is required.',
            'category_name_ar.required' => 'category_name_ar is required.',
            
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {


            $userData = Category::create([
                'category_unique_id' => $this->generateCategoryId(),
                'status' => "deactive",
                'parent_id' => $req->category,
                'slug' => $this->createSlug($req->category_name_en)
            ]);

            $userData->lang()->createMany([
                [
                    'name' => $req->category_name_en,
                    'language' => 'en',
                ],
                [
                    'name' => $req->category_name_ar,
                    'language' => 'ar',
                ],
            ]);
           
            $msg = "Category added successfully";
            if ($userData) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }

    public function generateCategoryId()
    {
        $lastcategory = Category::select('category_unique_id')
            ->orderBy('id', 'desc')
            ->first();

        $lastId = 0;

        if ($lastcategory) {
            $lastId = (int) substr($lastcategory->category_unique_id, 4);
        }

        $lastId++;

        return 'CATG' . str_pad($lastId, 5, '0', STR_PAD_LEFT);
    }

    public function edit($id)
    {
        $category = Category::with('lang')->with('parent')->where('id', $id)->first();

        return [
            'category' => $category
        ];
    }

    public function update(Request $req) {
        if ($req->user_unique) {
            $unique = ',' . $req->user_unique;
        } else {
            $unique = ',NULL';
        }
        $rules = [
            'category_name_en' => 'required|unique:category_i18n,name' . $unique . ',category_id,language,en',
            'category_name_ar' => 'required',
           
        ];
        $messages = [
            'category_name_en.unique' => 'Category already exist.',
            'category_name_en.required' => 'category_name_en is required.',
            'category_name_ar.required' => 'category_name_ar is required.',
           
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {

            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {

            $userData = Category::where('id', $req->user_unique)
                ->update([
                    'parent_id' => $req->category,
                    'slug' => $this->createSlug($req->category_name_en)
                ]);

            $userData = CategoryLang::where('category_id', $req->user_unique)->where('language', 'en')
                ->update([
                    'name' => $req->category_name_en,
                ]);
            $userData = CategoryLang::where('category_id', $req->user_unique)->where('language', 'ar')
                ->update([
                    'name' => $req->category_name_ar,
                ]);
            $msg = "Category updated successfully";
            if ($userData) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }

    public function destroy(Request $req)
    {

        $category = Category::find($req->id);
        if (!empty($category)) {
            $product = Product::select('id')->where('category_id', $req->id)->exists();
            $attribute = Attribute::select('id')->where('category_id', $req->id)->exists();

            $cat_attribute = CategoryAttributes::select('id')->where('category_id', $req->id)->exists();
            $product_category = ProductCategories::select('id')->where('category_id', $req->id)->exists();

            if ($product == false && $attribute == false && $cat_attribute == false && $product_category == false) {
                if (!empty($category->lang[0]->image_path)) {
                    $path_en = $category->lang[0]->image_path;
                    $file = public_path('/uploads/' . $path_en);
                    $img = unlink($file);
                    
                }
                if (!empty($category->lang[1]->image_path)) {
                   $path_ar = $category->lang[1]->image_path;
                    $file = public_path('/uploads/' . $path_ar);
                    $img = unlink($file);
                }

                if (!empty($category->lang[0]->banner_image)) {
                    $pat_en = $category->lang[0]->banner_image;
                    $file = public_path('/uploads/' . $pat_en);
                    $img = unlink($file);
                    
                }
                if (!empty($category->lang[1]->banner_image)) {
                   $pat_ar = $category->lang[1]->banner_image;
                    $file = public_path('/uploads/' . $pat_ar);
                    $img = unlink($file);
                }
                $category->lang()->delete();
                $category->delete();
                $userData = Category::where('parent_id', $req->id)
                    ->update([
                        'parent_id' => NULL
                    ]);
                return response()->json(['status' => 1, 'message' => 'Category deleted successfully']);
            } else {

                return response()->json(['status' => 0, 'message' => 'You cannot delete,have related records']);
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong']);
        }
    }

    public function statusUpdate(Request $req)
    {
        $att = Category::where('id', $req->id)->first();
        if ($att) {
            if ($att->status == 'deactive') {
                Category::where('id', $req->id)
                    ->update([
                        'status' => 'active'
                    ]);
            } else {
                Category::where('id', $req->id)
                    ->update([
                        'status' => 'deactive'
                    ]);
            }
            return response()->json(['status' => 1, 'message' => 'Status updated successfully']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }

    public function featuredUpdate(Request $req)
    {
        $status = $req->status === 'yes' ? 'no' : 'yes';

        $statusUpdate = Category::where('id', $req->id)
            ->update([
                'is_featured' => $status
            ]);

        if ($req->status === 'yes') {
            return response()->json(['status' => 1, 'message' => 'Removed from featured.']);
        } else {
            return response()->json(['status' => 1, 'message' => 'Updated to featured.']);
        }
    }

    public function add_category_products(Request $req)
    {
        if ($req->cat_id) {
            $cat_id = $req->cat_id;
            $pdt_ids = array();
            $cat_name = CategoryLang::select('name')->where(['language' => 'en', 'category_id' => $req->cat_id])->first();
            $sel_pdt_ids = ProductCategories::with('products')->where(['category_id' => $req->cat_id])->get();
            foreach ($sel_pdt_ids as $val) {
                $pdt_ids[] = $val->product_id;
            }
            $products = Product::with('lang')->whereNotIn('id', $pdt_ids)->get();
            $sel_products = ProductCategories::with('products')->where(['category_id' => $req->cat_id])->paginate(20);
            //            print_r($sel_products->toArray());exit;
            return view('admin.product.category.category_products', compact('cat_name', 'cat_id', 'products', 'pdt_ids', 'sel_products'));
        }
    }

    public function save_category_products(Request $req)
    {
        $rules = [
            'products' => 'required',
        ];
        $messages = [
            'products.required' => 'Attributes is required.',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $att_ids = '';
            $products = $req->input('products');
            foreach ($products as $products_val) {
                $pdt_cat_ids = Product::select('category_ids')->where('id', $products_val)->first();
                $category_ids = $pdt_cat_ids->category_ids;
                $category_ids = $category_ids . '{{' . $req->cat_id . '}}';
                $attribute = Product::where('id', $products_val)
                    ->update([
                        'category_ids' => $category_ids,
                    ]);
                    ProductCategories::where(['product_id' => $products_val, 'category_id' => $req->cat_id])->delete();
                ProductCategories::create([

                    'category_id' => $req->cat_id,
                    'product_id' => $products_val
                ]);
            }
            return response()->json(['status' => 1, 'message' => 'Products added successfully']);
        }
    }

    public function delete_product(Request $req)
    {
        if (!empty($req->id) && !empty($req->cat_id)) {
            if (ProductCategories::where(['product_id' => $req->id, 'category_id' => $req->cat_id])->exists()) {
                ProductCategories::where(['product_id' => $req->id, 'category_id' => $req->cat_id])->delete();
            }
            $pdt_cat_ids = Product::select('category_ids')->where('id', $req->id)->first();
            $category_ids = $pdt_cat_ids->category_ids;
            $category_ids = str_replace('{{' . $req->cat_id . '}}', '', $category_ids);
            $attribute = Product::where('id', $req->id)
                ->update([
                    'category_ids' => $category_ids,
                ]);
            return response()->json(['status' => 1, 'message' => 'Product deleted successfully']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong']);
        }
    }

    private function createSlug($string)
    {
        $slug = strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), trim($string)));
        return $slug;
    }







    public function category_images(Request $req)
    {

        $category_id = $req->cat_id;
        if ($req->upload_type == 'image_en') {
            $rules = [
                'photo' => 'required|image|mimes:png,jpg,jpeg|max:10240'
            ];
            $messages = [
                'photo.max' => 'The image must be less than 2Mb in size',
                'photo.mimes' => "The image must be of the format jpeg or png",
            ];
        } elseif ($req->upload_type == 'image_ar') {
            $rules = [
                'photo' => 'required|image|mimes:png,jpg,jpeg|max:10240'
            ];
            $messages = [
                'photo.max' => 'The image must be less than 2Mb in size',
                'photo.mimes' => "The image must be of the format jpeg or png",
            ];
        }  elseif ($req->upload_type == 'banner_en') {
            $rules = [
                'photo' => 'required|image|mimes:png,jpg,jpeg|max:10240'
            ];
            $messages = [
                'photo.max' => 'The image must be less than 2Mb in size',
                'photo.mimes' => "The image must be of the format jpeg or png",
            ];
        } else {
            $rules = [
                'photo' => 'required|image|mimes:png,jpg,jpeg|max:10240'
            ];
            $messages = [
                'photo.max' => 'The image must be less than 2Mb in size',
                'photo.mimes' => "The image must be of the format jpeg or png",
            ];
        }
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {

            if ($req->upload_type == 'image_en') {

                $file = request()->file('photo');
                $path = $file->store("category/cover/en", ['disk' => 'public_uploads']);
                
                
                CategoryLang::where('category_id', $category_id)->where('language', "en")
                    ->update([
                        "image_path" => $path
                    ]);
                $full_path = url('uploads/' . $path);
              

                return response()->json(['status' => 1, 'message' => 'Image added successfully', 'path' => $full_path, 'cat_id' => $category_id]);

            } else if ($req->upload_type == 'image_ar') {

                $file = request()->file('photo');
                $path = $file->store("category/cover/ar", ['disk' => 'public_uploads']);
               
                CategoryLang::where('category_id', $category_id)->where('language', "ar")
                    ->update([
                        "image_path" => $path
                    ]);
                $full_path = url('uploads/' . $path);

                return response()->json(['status' => 1, 'message' => 'Image added successfully', 'path' => $full_path, 'cat_id' => $category_id]);
            }  else  if ($req->upload_type == 'banner_en') {

                $file = request()->file('photo');
                $path = $file->store("category/banner/en", ['disk' => 'public_uploads']);
               
                CategoryLang::where('category_id', $category_id)->where('language', "en")
                    ->update([
                        "banner_image" => $path
                    ]);
                $full_path = url('uploads/' . $path);

                return response()->json(['status' => 1, 'message' => 'Image added successfully', 'path' => $full_path, 'cat_id' => $category_id]);
            }else  {

                $file = request()->file('photo');
                $path = $file->store("category/banner/ar", ['disk' => 'public_uploads']);
              
                CategoryLang::where('category_id', $category_id)->where('language', "ar")
                    ->update([
                        "banner_image" => $path
                    ]);
                $full_path = url('uploads/' . $path);

                return response()->json(['status' => 1, 'message' => 'Image added successfully', 'path' => $full_path, 'cat_id' => $category_id]);
            }
        }
    }
    public function detete_img(Request $request)
    {

        if ($request->type == 'image_en') {
            $category = CategoryLang::where('category_id', $request->id)->where('language', "en")->first();
            if (!empty($category)) {
                CategoryLang::where('category_id', $request->id)->where('language', "en")
                ->update([
                    "image_path" => Null
                ]);
                $file_path = public_path() . '/uploads/' . $category["image_path"];
                unlink($file_path);
            }
           
            $msg = 'Image deleted successfully';
        } else if ($request->type == 'image_ar') {
            $category = CategoryLang::where('category_id', $request->id)->where('language', "ar")->first();
            if (!empty($category)) {
                CategoryLang::where('category_id', $request->id)->where('language', "ar")
                ->update([
                    "image_path" => Null
                ]);
                $file_path = public_path() . '/uploads/' . $category["image_path"];
                unlink($file_path);
            }
           
            $msg = 'Image deleted successfully';
        }   else if ($request->type == 'banner_en') {
            $category = CategoryLang::where('category_id', $request->id)->where('language', "en")->first();
            if (!empty($category)) {
                CategoryLang::where('category_id', $request->id)->where('language', "en")
                ->update([
                    "banner_image" => Null
                ]);
                $file_path = public_path() . '/uploads/' . $category["banner_image"];
                unlink($file_path);
            }
           
            $msg = 'Image deleted successfully';
        }
        else {
           
            $category = CategoryLang::where('category_id', $request->id)->where('language', "ar")->first();
           
            if (!empty($category)) {
                CategoryLang::where('category_id', $request->id)->where('language', "ar")
                ->update([
                    "banner_image" => Null
                ]);
                $file_path = public_path() . '/uploads/' . $category["banner_image"];
                unlink($file_path);
            }
           
            $msg = 'Image deleted successfully';
        }
        return response()->json(['status' => 1, 'message' => $msg, 'id' => $request->id, 'type' => $request->type]);
    }
}
