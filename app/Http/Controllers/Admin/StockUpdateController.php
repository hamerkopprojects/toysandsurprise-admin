<?php

namespace App\Http\Controllers\Admin;

use DB;
use Excel;
use Config;
use Validator;
use App\Models\Product;
use App\Models\VariantLang;
use App\Models\TempB2cStock;
use Illuminate\Http\Request;
use App\Imports\ProductImport;
use App\Models\ProductVariant;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;

class StockUpdateController extends Controller
{
    public function index(Request $request)
    {
        if (!empty($request['type'])) {
            $type = $request['type'];
        }
        if (!empty($request->action)) {
            $type = $request->action;
        }

        TempB2cStock::truncate();
        return view('admin.product.stock_updates.index', compact('type'));
    }

    public function importsubmit(Request $req)
    {
        $type = $req->type;
        $rules = [
            'select_file' => 'required|mimes:xlsx|max:2048',
        ];
        $messages = [
            'select_file.required' => 'File is required.',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            TempB2cStock::truncate();
            $file = $req->file('select_file');
            $data = Excel::toArray(new ProductImport, $file);
            if ($type == "simpleb2c") {
                if (count($data) > 0) {
                    foreach ($data as $key => $sub) {
                        foreach ($sub as $key1 => $val) {
                            if ($key1 > 0) {
                                $errors = "";
                                $temp_products = new TempB2cStock;

                                $temp_products->sku = trim($val[0]);
                                if ($temp_products->sku == "") {
                                    $errors .= "SKU is required,";
                                }
                                $temp_products->stock = $val[1];
                                if ($temp_products->stock == "") {
                                    $errors .= "Stock is required,";
                                }

                                if ($errors) {
                                    $temp_products->error_flag = "Y";
                                    $temp_products->errors = $errors;
                                } else {
                                    $temp_products->error_flag = "N";
                                }
                                if (!empty($temp_products->sku)) {
                                    $pdt_data = TempB2cStock::where('sku', '=', $temp_products->sku)->first();

                                    if (!empty($pdt_data)) {
                                        $temp_products->duplicate_flag = 'Y';
                                    }
                                }
                                $temp_products->created_at = Carbon::now();

                                $temp_products->save();
                            }
                        }
                    }
                    return response()->json(['status' => '1', 'type' => $type]);
                } else {
                    return response()->json(['message' => $validator->errors()->first(), 'status' => '0']);
                }
            } elseif ($type == "complexb2c") {
                TempB2cStock::truncate();
                if (count($data) > 0) {
                    foreach ($data as $key => $sub) {
                        foreach ($sub as $key1 => $val) {
                            if ($key1 > 0) {
                                $errors = "";
                                $temp_products = new TempB2cStock;

                                $temp_products->sku = trim($val[0]);
                                if ($temp_products->sku == "") {
                                    $errors .= "SKU is required,";
                                }
                                $temp_products->varient = $val[1];
                                if ($temp_products->varient == "") {
                                    $errors .= "Variant is required,";
                                }
                                $temp_products->stock = $val[2];
                                if ($temp_products->stock == "") {
                                    $errors .= "Stock is required,";
                                }

                                if ($errors) {
                                    $temp_products->error_flag = "Y";
                                    $temp_products->errors = $errors;
                                } else {
                                    $temp_products->error_flag = "N";
                                }
                                if (!empty($temp_products->sku)) {
                                    $pdt_data = TempB2cStock::where('sku', '=', $temp_products->sku)->where('varient', '=', $temp_products->varient)->first();

                                    if (!empty($pdt_data)) {
                                        $temp_products->duplicate_flag = 'Y';
                                    }
                                }
                                $temp_products->created_at = Carbon::now();

                                $temp_products->save();
                            }
                        }
                    }
                    return response()->json(['status' => '1', 'type' => $type]);
                } else {
                    return response()->json(['message' => $validator->errors()->first(), 'status' => '0']);
                }
            }
        }
    }
    public function b2csimple(Request $req)
    {
        $search = $req->search;
        $error_count  = $success_count = $duplicate_count = 0;
        $result_data = array();
        $pagination_count = 10;
        $query = TempB2cStock::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
            });
        }
        $temp_data = $query->where('cron_flag', 'I')->paginate($pagination_count)->appends(request()->query());

        if (count($temp_data) > 0) {
            foreach ($temp_data as $row_data) {
                $sku_data = Product::where('sku', '=', $row_data->sku)->first();

                if (empty($sku_data)) {
                    $row_data->error_flag = 'Y';
                }
                if (!empty($sku_data)) {
                    if ($sku_data->product_type != 'simple') {
                        $row_data->error_flag = 'Y';
                    }
                }

                if ($row_data->error_flag == 'Y' && $row_data->duplicate_flag == 'N') {
                    $error_count++;
                }
                if ($row_data->duplicate_flag == 'Y') {
                    $duplicate_count++;
                }

                if ($row_data->error_flag == 'N' && $row_data->duplicate_flag == 'N') {
                    $success_count++;
                }
                $result[] = $row_data;
            }
            $result_data = $result;
        }

        $total_count = $error_count + $duplicate_count + $success_count;
        $data = [
            'temp_data' => $temp_data,
            'result_data' => $result_data,
            'error_count' => $error_count,
            'duplicate_count' => $duplicate_count,
            'success_count' => $success_count,
            'total_count' => $total_count,
            'search' => $search
        ];

        return view('admin.product.stock_updates.b2c_simple', $data);
    }
    public function get_errors(Request $req)
    {

        $id = $req->id;
        $result = TempB2cStock::where('id', $id)->first();

        $sku_data = Product::where('sku', '=', $result->sku)->first();

        $new_errors = '';
        if (!empty($result->sku)) {
            if (empty($sku_data)) {

                $new_errors .= "SKU not exist ,";
            }
        }
        if (!empty($sku_data)) {
            if ($sku_data->product_type != 'simple') {
                $new_errors .= "Imported product is not simple ,";
            }
        }
        $all_errors = $result->errors . $new_errors;
        $errors = [];
        $errors = explode(",", $all_errors);

        $data = ['errors' => $errors];

        return view('admin.product.stock_updates.error', $data);
    }

    public function confirm_import()
    {

        $result_data = array();

        $result_data = TempB2cStock::where('cron_flag', 'I')->get();

        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'N') {
                    $sku_data = Product::where('sku', '=', $row_data->sku)->first();
                    if (empty($sku_data)) {
                        $row_data->error_flag = 'Y';
                    } else {
                        if ($sku_data->product_type != 'simple') {
                            $row_data->error_flag = 'Y';
                        } else {
                            $row_data->sku = $row_data->sku;
                        }
                    }

                    if ($row_data->error_flag == 'N') {
                        $pdt_data = Product::where('sku', '=', $row_data->sku)->first();
                        $data_to_save = [
                            'sku' => $row_data->sku,
                            'stock' => $row_data->stock
                        ];
                        $saved_data = Product::where('id', $pdt_data->id)
                            ->update($data_to_save);
                    }
                }
                TempB2cStock::where('id', $row_data->id)->update(['cron_flag' => 'S', 'error_flag' => $row_data->error_flag, 'duplicate_flag' => $row_data->duplicate_flag]);
            }
        }
        $check_temp_emp = TempB2cStock::where('cron_flag', 'I')->first();
        if (!$check_temp_emp) {
            TempB2cStock::where('cron_flag', 'S')->update(['cron_flag' => 'C']);
        }

        return response()->json(['message' => 'Thank you ! Successfully updated stock list', 'status' => '1']);
    }

    public function imported_data(Request $req)
    {
        $search = $req->search;
        $error_count = $success_count = $duplicate_count = 0;
        $pagination_count = 10;
        $result_data = array();
        $query = TempB2cStock::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
            });
        }
        $result_data = $query->where('cron_flag', 'C')
            ->paginate($pagination_count)->appends(request()->query());

        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'Y' && $row_data->duplicate_flag == 'N') {
                    $error_count++;
                }else if ($row_data->duplicate_flag == 'Y'){
                    $duplicate_count++;
                }
                 else {
                    $success_count++;
                }
            }
        }
        $duplicate = TempB2cStock::where('duplicate_flag', 'Y')->get();
        $duplicate_count = $duplicate->count();
        $total_count = $error_count + $success_count + $duplicate_count;
        $data = [
            'result_data' => $result_data,
            'error_count' => $error_count,
            'success_count' => $success_count,
            'duplicate_count' => $duplicate_count,
            'total_count' => $total_count,
            'search' => $search,
        ];

        return view('admin.product.stock_updates.b2c_simple_import', $data);
    }

    public function b2ccomplex(Request $req)
    {
        $search = $req->search;
        $error_count  = $success_count = $duplicate_count = 0;
        $result_data = array();
        $pagination_count = 10;
        $query = TempB2cStock::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
            });
        }
        $temp_data = $query->where('cron_flag', 'I')->paginate($pagination_count)->appends(request()->query());

        if (count($temp_data) > 0) {
            foreach ($temp_data as $row_data) {
                $sku_data = Product::where('sku', '=', $row_data->sku)->first();

                if (empty($sku_data)) {
                    $row_data->error_flag = 'Y';
                }
                if (!empty($sku_data)) {


                    if ($sku_data->product_type != 'complex') {
                        $row_data->error_flag = 'Y';
                    }
                    $varient_da = array();
                    $varient_da = ProductVariant::where('product_id', '=', $sku_data->id)->get();

                    if (!empty($varient_da)) {
                        $val = "";
                        foreach ($varient_da as $varient_data) {
                            $varient = VariantLang::where('id', $varient_data->attribute_variant_id)->where('attribute_id', $varient_data->attribute_id)->first();
                            if (!empty($varient)) {

                                if ((strtolower($varient->name) === strtolower($row_data->varient))) {
                                    $val = "exist";
                                    break;
                                }
                            }
                        }

                        if ($val != "exist") {

                            $row_data->error_flag = 'Y';
                        }
                    } else {
                        $row_data->error_flag = 'Y';
                    }
                }

                if ($row_data->error_flag == 'Y' && $row_data->duplicate_flag == 'N') {
                    $error_count++;
                }
                if ($row_data->duplicate_flag == 'Y') {
                    $duplicate_count++;
                }

                if ($row_data->error_flag == 'N' && $row_data->duplicate_flag == 'N') {
                    $success_count++;
                }
                $result[] = $row_data;
            }
            $result_data = $result;
        }

        $total_count = $error_count + $success_count + $duplicate_count;
        $data = [
            'temp_data' => $temp_data,
            'result_data' => $result_data,
            'error_count' => $error_count,
            'duplicate_count' =>$duplicate_count,
            'success_count' => $success_count,
            'total_count' => $total_count,
            'search' => $search
        ];

        return view('admin.product.stock_updates.b2c_complex', $data);
    }

    public function b2ccomplex_errors(Request $req)
    {

        $id = $req->id;
        $result = TempB2cStock::where('id', $id)->first();

        $sku_data = Product::where('sku', '=', $result->sku)->first();

        $new_errors = '';
        if (!empty($result->sku)) {
            if (empty($sku_data)) {

                $new_errors .= "SKU not exist ,";
            } else {
                if ($sku_data->product_type != 'complex') {
                    $new_errors .= "Imported product is not complex ,";
                }
                $varient_da = array();
                $varient_da = ProductVariant::where('product_id', '=', $sku_data->id)->get();
                if (!empty($result->varient)) {
                    if (!empty($varient_da)) {

                        $val = "";
                        foreach ($varient_da as $varient_data) {
                            $varient = VariantLang::where('id', $varient_data->attribute_variant_id)->where('attribute_id', $varient_data->attribute_id)->first();
                            if (!empty($varient)) {

                                if ((strtolower($varient->name) === strtolower($result->varient))) {
                                    $val = "exist";
                                    break;
                                }
                            }
                        }

                        if ($val != "exist") {

                            $new_errors .= "SKU and Variant mismatch,";
                        }
                    } else {
                        $new_errors .= "SKU and Variant mismatch,";
                    }
                }
            }
        }

        $all_errors = $result->errors . $new_errors;
        $errors = [];
        $errors = explode(",", $all_errors);

        $data = ['errors' => $errors];

        return view('admin.product.stock_updates.error', $data);
    }


    public function confirm_cb2cimport()
    {

        $result_data = array();

        $result_data = TempB2cStock::where('cron_flag', 'I')->get();

        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'N') {
                    $sku_data = Product::where('sku', '=', $row_data->sku)->first();
                    if (empty($sku_data)) {
                        $row_data->error_flag = 'Y';
                    } else {
                        if ($sku_data->product_type != 'complex') {
                            $row_data->error_flag = 'Y';
                        }
                        $varient_da = array();
                        $varient_da = ProductVariant::where('product_id', '=', $sku_data->id)->get();

                        if (!empty($varient_da)) {
                            $val = '';
                            foreach ($varient_da as $varient_data) {
                                $varient = VariantLang::where('id', $varient_data->attribute_variant_id)->where('attribute_id', $varient_data->attribute_id)->first();
                                if (!empty($varient)) {

                                    if ((strtolower($varient->name) === strtolower($row_data->varient))) {
                                        $val = "exist";
                                        break;
                                    }
                                }
                            }

                            if ($val != "exist") {

                                $row_data->error_flag = 'Y';
                            } else {
                                $row_data->sku = $row_data->sku;
                            }
                        }
                    }
                    if ($row_data->error_flag == 'N' ) {

                        $data_to_save = [
                            'stock' => $row_data->stock
                        ];
                        $saved_data = ProductVariant::where('product_id', $sku_data->id)->where('attribute_variant_id', $varient->id)
                            ->update($data_to_save);
                    }
                }
                TempB2cStock::where('id', $row_data->id)->update(['cron_flag' => 'S', 'error_flag' => $row_data->error_flag, 'duplicate_flag' => $row_data->duplicate_flag]);
            }
        }
        $check_temp_emp = TempB2cStock::where('cron_flag', 'I')->first();
        if (!$check_temp_emp) {
            TempB2cStock::where('cron_flag', 'S')->update(['cron_flag' => 'C']);
        }

        return response()->json(['message' => 'Thank you ! Successfully updated stock list', 'status' => '1']);
    }

    public function imported_complex(Request $req)
    {
        $search = $req->search;
        $error_count = $success_count = $duplicate_count = 0;
        $pagination_count = 10;
        $result_data = array();
        $query = TempB2cStock::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
            });
        }
        $result_data = $query->where('cron_flag', 'C')
            ->paginate($pagination_count)->appends(request()->query());

        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'Y' && $row_data->duplicate_flag == 'N') {
                    $error_count++;
                }else if ($row_data->duplicate_flag == 'Y'){
                    $duplicate_count++;
                }
                 else {
                    $success_count++;
                }
            }
        }
        $duplicate = TempB2cStock::where('duplicate_flag', 'Y')->get();
        $duplicate_count = $duplicate->count();
        $total_count = $error_count + $success_count + $duplicate_count;
        $data = [
            'result_data' => $result_data,
            'error_count' => $error_count,
            'success_count' => $success_count,
            'duplicate_count' => $duplicate_count,
            'total_count' => $total_count,
            'search' => $search,
        ];

        return view('admin.product.stock_updates.b2c_complex_import', $data);
    }
}
