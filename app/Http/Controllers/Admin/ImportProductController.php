<?php

namespace App\Http\Controllers\admin;

use DB;
use Excel;
use Config;
use Validator;
//use Illuminate\Support\Facades\DB;
use App\Models\Brand;
use App\Models\AgeLang;
use App\Models\Product;
use App\Models\TagLang;
use App\Models\Category;
use App\Models\BrandLang;
use App\Models\GenderLang;
use App\Models\ProductAge;
use App\Models\ProductTag;
use App\Models\ProductLang;
use App\Models\CategoryLang;
use App\Models\TempProducts;
use Illuminate\Http\Request;
use App\Models\ProductGender;
use App\Imports\ProductImport;
use App\Models\ProductVariant;
use Illuminate\Support\Carbon;
use App\Models\ProductB2CPrice;
use App\Models\ProductAttribute;
use App\Http\Controllers\Controller;
use App\Models\ProductAttributeLang;
use Illuminate\Support\Facades\Storage;

class ImportProductController extends Controller {

    //Import products
    public function index(Request $req) {

        if (isset($req->action) && $req->action == "76GH34RT65") {
            TempProducts::truncate();
            return redirect('admin/importproducts/import');
        }
        $temp_pdts = TempProducts::where('cron_flag', 'C')->first();
        if (!empty($temp_pdts)) {
            return redirect('admin/importproducts/imported_data');
        } else {
            $temp_pdts = TempProducts::where('cron_flag', 'I')
                    ->orWhere('cron_flag', 'S')
                    ->first();
            if (!empty($temp_pdts)) {
                return redirect('admin/importproducts/excel_view');
            }
        }

        return view('admin.product.import.index');
    }

    public function importsubmit(Request $req) {

        $rules = [
            'select_file' => 'required|mimes:xlsx|max:2048',
        ];
        $messages = [
            'select_file.required' => 'File is required.',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $file = $req->file('select_file');
            $data = Excel::toArray(new ProductImport, $file);
            if (count($data) > 0) {
                foreach ($data as $key => $sub) {
                    foreach ($sub as $key1 => $val) {
                        if ($key1 > 0) {
                            $errors = "";
                            $temp_products = new TempProducts;
//
                            $temp_products->sku = $val[0];
                            if ($temp_products->sku == "") {
                                $errors .= "SKU is required,";
                            }
                            $temp_products->product_name_en = $val[1];
                            if ($temp_products->product_name_en == "") {
                                $errors .= "Product Name (EN) is required,";
                            }
                            $temp_products->product_name_ar = $val[2];
                            if ($temp_products->product_name_ar == "") {
                                $errors .= "Product Name (AR) is required,";
                            }
                            $temp_products->category = $val[3];
                            if ($temp_products->category == "") {
                                $errors .= "Category is required,";
                            }
                            $temp_products->brand = $val[4];
                            if ($temp_products->brand == "") {
                                $errors .= "Brand is required,";
                            }
                            $temp_products->tag = $val[5];
                            $temp_products->age = $val[6];
                            $temp_products->gender = $val[7];
                            $temp_products->description_en = $val[8];
                            if ($temp_products->description_en == "") {
                                $errors .= "Description (EN) is required,";
                            }
                            $temp_products->description_ar = $val[9];
                            if ($temp_products->description_ar == "") {
                                $errors .= "Description (AR) is required,";
                            }
                            $temp_products->ingredients_en = $val[10];
                            $temp_products->ingredients_ar = $val[11];
                            $temp_products->how_to_use_en = $val[12];
                            $temp_products->how_to_use_ar = $val[13];
                            $temp_products->reasons_to_buy_en = $val[14];
                            $temp_products->reasons_to_buy_ar = $val[15];
                            $temp_products->stock = $val[16];
                            if ($temp_products->stock == "") {
                                $errors .= "Stock is required,";
                            }
                            $temp_products->price = $val[17];
                            if ($temp_products->price == "") {
                                $errors .= "Stock is required,";
                            }
                            $temp_products->discount_price = $val[18];
                            if ($temp_products->discount_price == "") {
                                $errors .= "Discount Price is required,";
                            }
                            if ($errors) {
                                $temp_products->error_flag = "Y";
                                $temp_products->errors = $errors;
                            } else {
                                $temp_products->error_flag = "N";
                            }
                            $temp_products->created_at = Carbon::now();
                            $temp_products->save();
                        }
                    }
                }
                return response()->json(['status' => '1']);
            } else {
                return response()->json(['message' => $validator->errors()->first(), 'status' => '0']);
            }
        }
    }

    public function excel_view(Request $req) {
        $search = $req->search;
        $error_count = $duplicate_count = $success_count = 0;
        $result_data = array();
        $pagination_count = 10;
        $query = TempProducts::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
                $sub->orWhere('product_name_en', 'like', "%" . $search . "%");
                $sub->orWhere('product_name_ar', 'like', "%" . $search . "%");
            });
        }
        $temp_data = $query->where('cron_flag', 'I')->paginate($pagination_count)->appends(request()->query());
        if (count($temp_data) > 0) {
            foreach ($temp_data as $row_data) {
                $cat_data = CategoryLang::select('id')->where('name', $row_data->category)->exists();
                if ($cat_data == false) {
                    $row_data->error_flag = 'Y';
                }
                $brand_data = BrandLang::select('id')->where('name', $row_data->brand)->exists();
                if ($brand_data == false) {
                    $row_data->error_flag = 'Y';
                }
                $tag_data = TagLang::select('id')->where('name', $row_data->tag)->exists();
                if ($tag_data == false) {
                    $row_data->error_flag = 'Y';
                }
                $age_data = AgeLang::select('id')->where('name', $row_data->age)->exists();
                if ($age_data == false) {
                    $row_data->error_flag = 'Y';
                }
                $gender_data = GenderLang::select('id')->where('name', $row_data->gender)->exists();
                if ($gender_data == false) {
                    $row_data->error_flag = 'Y';
                }
                if ($row_data->error_flag == 'Y') {
                    $error_count++;
                }
                if ($row_data->error_flag == 'N') {
                    $pdt_data = Product::where('sku', '=', $row_data->sku)->first();
                    if (!empty($pdt_data)) {
                        $row_data->duplicate_flag = 'Y';
                        $duplicate_count++;
                    }
                }
                if ($row_data->error_flag == 'N' && $row_data->duplicate_flag == 'N') {
                    $success_count++;
                }
                $result[] = $row_data;
            }
            $result_data = $result;
        }
        $total_count = $error_count + $duplicate_count + $success_count;
        $data = [
            'temp_data' => $temp_data,
            'result_data' => $result_data,
            'error_count' => $error_count,
            'duplicate_count' => $duplicate_count,
            'success_count' => $success_count,
            'total_count' => $total_count,
            'search' => $search
        ];
        return view('admin.product.import.verify_import', $data);
    }

    public function get_errors(Request $req) {
        $id = $req->id;
        $result = TempProducts::where('id', $id)->first();
        $cat_data = CategoryLang::where('name', '=', $result->category)->first();
        $new_errors = '';
        if (!empty($result->category) && empty($cat_data)) {
            $new_errors .= "Category not exist in the system,";
        }
        $brand_data = BrandLang::where('name', '=', $result->brand)->first();
        if (!empty($result->brand) && empty($brand_data)) {
            $new_errors .= "Brand not exist in the system,";
        }
        $tag_data = TagLang::where('name', '=', $result->tag)->first();
        if (!empty($result->tag) && empty($tag_data)) {
            $new_errors .= "Tag not exist in the system,";
        }
        $age_data = AgeLang::where('name', '=', $result->age)->first();
        if (!empty($result->age) && empty($age_data)) {
            $new_errors .= "Age not exist in the system,";
        }
        $gender_data = GenderLang::where('name', '=', $result->gender)->first();
        if (!empty($result->gender) && empty($gender_data)) {
            $new_errors .= "Gender not exist in the system,";
        }
        $all_errors = $result->errors . $new_errors;
        $errors = [];
        $errors = explode(",", $all_errors);

        $data = ['errors' => $errors];
        return view('admin.product.import.errors', $data);
    }

    public function import_view(Request $req) {
        $_temp_id = $req->id;
        $row_data = array();
        if ($_temp_id != '') {
            $row_data = TempProducts::where('id', '=', $_temp_id)->first();
        }

        $data = [
            'row_data' => $row_data
        ];
        return view('admin.product.import.view', $data);
    }

    public function confrim_import() {
        $result_data = array();

        $result_data = TempProducts::where('cron_flag', 'I')->get();
        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'N') {
                    $cat_data = CategoryLang::where('name', '=', $row_data->category)->first();
                    if (empty($cat_data)) {
                        $row_data->error_flag = 'Y';
                    } else {
                        $row_data->category_id = $cat_data['category_id'];
                    }
                    $brand_data = BrandLang::where('name', '=', $row_data->brand)->first();
                    if (empty($brand_data)) {
                        $row_data->error_flag = 'Y';
                    } else {
                        $row_data->brand_id = $brand_data['brand_id'];
                    }

                    $tag_data = TagLang::where('name', '=', $row_data->tag)->first();
                    if (empty($tag_data)) {
                        $row_data->error_flag = 'Y';
                    } else {
                        $row_data->tag_id = $tag_data['tag_id'];
                    }

                    $age_data = AgeLang::where('name', '=', $row_data->age)->first();
                    if (empty($age_data)) {
                        $row_data->error_flag = 'Y';
                    } else {
                        $row_data->age_id = $age_data['age_id'];
                    }

                    $gender_data = GenderLang::where('name', '=', $row_data->gender)->first();
                    if (empty($gender_data)) {
                        $row_data->error_flag = 'Y';
                    } else {
                        $row_data->gender_id = $gender_data['gender_id'];
                    }


                    if ($row_data->error_flag == 'N') {
                        $pdt_data = Product::where('sku', '=', $row_data->sku)->first();
                        $data_to_save = [
                            'sku' => $row_data->sku,
                            'category_id' => $row_data->category_id,
                            'brand_id' => $row_data->brand_id,
                            'stock' => $row_data->stock,
                            'tag_ids'=>"{{".$row_data->tag_id."}}",
                            'age_ids'=>"{{".$row_data->age_id."}}",
                            'gender_ids'=>"{{".$row_data->gender_id."}}",
                            'slug' => $this->createSlug($row_data->product_name_en)
                        ];
                        if (!empty($pdt_data)) {
                            $row_data->duplicate_flag = 'Y';
                            $saved_data = Product::where('id', $pdt_data->id)
                                    ->update($data_to_save);
                            ProductLang::where('language', 'en')
                                    ->where('product_id', $pdt_data->id)
                                    ->update([
                                        'name' => $row_data->product_name_en,
                                        'description' => $row_data->description_en,
                                        'ingredients' => $row_data->ingredients_en,
                                        'how_to_use' => $row_data->how_to_use_en,
                                        'reasons_to_buy' => $row_data->reasons_to_buy_en,
                                        
                            ]);
                            ProductLang::where('language', 'ar')
                                    ->where('product_id', $pdt_data->id)
                                    ->update([
                                        'name' => $row_data->product_name_ar,
                                        'description' => $row_data->description_ar,
                                        'ingredients' => $row_data->ingredients_ar,
                                        'how_to_use' => $row_data->how_to_use_ar,
                                        'reasons_to_buy' => $row_data->reasons_to_buy_ar,
                                        
                            ]);
                            if (ProductTag::where('product_id', $pdt_data->id)->exists()) {
                                ProductTag::where('product_id', $pdt_data->id)->delete();
                            }
                            ProductTag::create([
                                'product_id' => $pdt_data->id,
                                'tag_id' => $row_data->tag_id
                            ]);

                            if (ProductAge::where('product_id', $pdt_data->id)->exists()) {
                                ProductAge::where('product_id', $pdt_data->id)->delete();
                            }

                            ProductAge::create([
                                'product_id' => $pdt_data->id,
                                'age_id' => $row_data->age_id
                            ]);

                            if (ProductGender::where('product_id', $pdt_data->id)->exists()) {
                                ProductGender::where('product_id', $pdt_data->id)->delete();
                              }

                            ProductGender::create([
                                'product_id' => $pdt_data->id,
                                'gender_id' => $row_data->gender_id
                            ]);

                            $prdt_b2c_price = ProductB2CPrice::where('product_id', $pdt_data->id)->get();
                            if (count($prdt_b2c_price) > 0) {
                                foreach ($prdt_b2c_price as $row_data_val) {
                                    $b2c_price = ProductB2CPrice::find($row_data_val->id);
                                    $b2c_price->delete();
                                }
                            }
                            ProductB2CPrice::create([
                                'product_id' => $pdt_data->id,
                                'price' => $row_data->price,
                                'discount_price' => $row_data->discount_price,
                            ]);
                        } else {
                            $data_to_save['created_at'] = Carbon::now();
                            $saved_data = Product::create($data_to_save);
                            $saved_data->lang()->createMany([
                                [
                                    'name' => $row_data->product_name_en,
                                    'description' => $row_data->description_en,
                                    'ingredients' => $row_data->ingredients_en,
                                    'how_to_use' => $row_data->how_to_use_en,
                                    'reasons_to_buy' => $row_data->reasons_to_buy_en,
                                    'language' => 'en',
                                    
                                ],
                                [
                                    'name' => $row_data->product_name_ar,
                                    'description' => $row_data->description_ar,
                                    'ingredients' => $row_data->ingredients_ar,
                                    'how_to_use' => $row_data->how_to_use_ar,
                                    'reasons_to_buy' => $row_data->reasons_to_buy_ar,
                                    'language' => 'ar',
                                    
                                ],
                            ]);
                            ProductB2CPrice::create([
                                'product_id' => $saved_data->id,
                                'price' => $row_data->price,
                                'discount_price' => $row_data->discount_price,
                                'created_at' => Carbon::now()
                            ]);
                        }
                    }
                }
                TempProducts::where('id', $row_data->id)->update(['cron_flag' => 'S', 'duplicate_flag' => $row_data->duplicate_flag, 'error_flag' => $row_data->error_flag]);
            }
        }
        $check_temp_emp = TempProducts::where('cron_flag', 'I')->first();
        if (!$check_temp_emp) {
            TempProducts::where('cron_flag', 'S')->update(['cron_flag' => 'C']);
        }
        
        return response()->json(['message' => 'Thank you! Successfully imported', 'status' => '1']);
    }

    public function imported_data(Request $req) {
        $search = $req->search;
        $error_count = $success_count = 0;
        $pagination_count = 10;
        $result_data = array();
        $query = TempProducts::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
                $sub->orWhere('product_name_en', 'like', "%" . $search . "%");
                $sub->orWhere('product_name_ar', 'like', "%" . $search . "%");
            });
        }
        $result_data = $query->where('cron_flag', 'C')
                        ->paginate($pagination_count)->appends(request()->query());

        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'Y') {
                    $error_count++;
                } else {
                    $success_count++;
                }
            }
        }
        $total_count = $error_count + $success_count;
        $data = [
            'result_data' => $result_data,
            'error_count' => $error_count,
            'success_count' => $success_count,
            'total_count' => $total_count,
            'search' => $search,
        ];
        return view('admin.product.import.product_imported', $data);
    }
    private function createSlug($string)
    {
        $slug = strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), trim($string)));
        return $slug;
    }
}
