<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\SocialMediaLink;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class SocialMediaLinkController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->social_media_select;
        $search_field = $request->search_field ?? '';


        if ($search_field) {
            $data = SocialMediaLink::select('id')->where('name', 'like', "%" . $search_field . "%")->get()->toArray();
            $da = [];
            foreach ($data as  $d) {
                $da[] = $d['id'];
            }
            $socialMedia = SocialMediaLink::whereIn('id', $da)->paginate(20);
        } else {
            $socialMedia = SocialMediaLink::paginate(20);
        }
        return view('admin.socialmedia.index', compact('socialMedia', 'search_field'));
    }
    public function store(Request $request)
    {
        $link = DB::transaction(function () use ($request) {
            $link = SocialMediaLink::create([
                'name' => $request->title_en,
                'link' => $request->link,
                'visibility' => $request->visibility,
                'position' => $request->position,
                'status' => 'active',
            ]);


            return $link;
        });
        if ($link) {
            return [
                "msg" => "success"
            ];
        } else {
            return [
                "msg" => "error"
            ];
        }
    }
    public function edit($id)
    {
        $link = SocialMediaLink::where('id', $id)
            ->first();
        return [
            'page' => $link,

        ];
    }
    public function update(Request $request)
    {
        $management = DB::transaction(function () use ($request) {

            SocialMediaLink::where('id', $request->id)
                ->update(
                    [
                        'name' => $request->title_en,
                        'link' => $request->link,
                        'visibility' => $request->visibility,
                        'position' => $request->position,
                    ]
                );
        });

        return [
            'msg' => "success"
        ];
    }
    public function statusUpdate(Request $request)
    {
        $status = $request->status === 'active' ? 'deactive' : 'active';
        SocialMediaLink::where('id', $request->id)
            ->update([
                'status' => $status
            ]);
        return [
            "msg" => 'success'
        ];
    }
    public function search(Request $req)
    {
        $link = SocialMediaLink::where('name', 'like', "%" . $req->search . "%")->get();
        $response = array();
        foreach ($link as $lan) {
            $response[] = array("value" => $lan->id, "label" => $lan->name);
        }

        return response()->json($response);
    }
    public function isNumberExist(Request $req)
    {
        $link = SocialMediaLink::select('id')->where('position', $req->position)->first();

        $status = 'false';
        if (!empty($link->id)) {
            $status = 'true';
        }

        return response()->json($status);
    }
}
