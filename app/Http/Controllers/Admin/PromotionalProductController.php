<?php

namespace App\Http\Controllers\Admin;

use App\Models\ProductLang;
use Illuminate\Http\Request;
use App\Models\PromotionalProduct;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Collection;
class PromotionalProductController extends Controller
{

    public function index()
    {

        return view('admin.product.promotional_poduct.index');
    }
    public function promoproducts(Request $req)
    {
        $promocode = PromotionalProduct::with(['promotionalproduct' => function ($query)  {
         
        }])->get();

        return [
            'promocode' => $promocode
        ];
    }
    public function autocomplete(Request $request)
    {

        $data = ProductLang::select("name", "product_id")->where("name", "LIKE", "%{$request->input('q')}%")->where("language", "en")->get();

        return response()->json($data);
    }

    public function store(Request $req)
    {

        $rules = [
            'tag' => 'required|array',
        ];
        $messages = [
            'tag.required' => 'Product is required.'
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {

            $tag = $req->tag;
           

            if(PromotionalProduct::exists()){
                 PromotionalProduct::whereNotNull('id')->delete();
                
               }
            foreach ($tag as $product) {

                $userData = PromotionalProduct::create([

                    'product_id' => $product
                ]);
            }
            if ($userData) {
                return response()->json(['status' => 1, 'message' => 'Promotional products added successfully']);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }
}
