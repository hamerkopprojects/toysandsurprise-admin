<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
//use Illuminate\Support\Facades\DB;
use App\Models\Attribute;
use App\Models\AttributeLang;
use App\Models\Variant;
use App\Models\VariantLang;
use App\Models\Category;
use App\Models\CategoryLang;
use App\Models\ProductAttribute;
use App\Models\CategoryAttributes;
use App\Models\ProductVariant;
use Validator;
use Config;
use DB;

class AttributeController extends Controller {

    //list attributes
    public function index(Request $req) {
        $search = $req->search;
        $query = Attribute::with('lang');
        if ($search) {
            $att_lang = AttributeLang::select('attribute_id')->where('name', 'like', "%" . $search . "%")->get()->toArray();
            $new_array = [];
            foreach ($att_lang as $att_lang_val) {
                $new_array[] = $att_lang_val['attribute_id'];
            }
            $query->whereIn('id', $new_array);
        }
        if ($req->attribute_type) {
            $query->Where('attribute_type', '=', $req->attribute_type);
        }

        if ($req->is_variant) {
            $query->Where('is_variant', '=', $req->is_variant);
        }
        if ($req->cat_id) {
            $cat_att = CategoryAttributes::select('attribute_id')->where('category_id', '=', $req->cat_id)->get()->toArray();
            $cat_array = [];
            foreach ($cat_att as $cat_att_val) {
                $cat_array[] = $cat_att_val['attribute_id'];
            }
            $query->whereIn('id', $cat_array);
        }
        $att_data = $query->where('deleted_at', null)
                ->orderBy('id', 'desc')
                ->paginate(20);
//         dd($pages);
        $cat_data = CategoryLang::where(['language' => 'en'])->get();
        $data = [
            'att_data' => $att_data,
            'search' => $search,
            'attribute_type' => $req->attribute_type,
            'is_variant' => $req->is_variant,
            'category_id' => $req->cat_id,
            'cat_data' => $cat_data,
        ];
        return view('admin.attribute.index', $data);
    }

    public function create(Request $req) {
        $_attribute_id = $req->id;
        $cat_id = $req->cat_id ? $req->cat_id : NULL;
        $row_data = $lang_array = array();
        if ($_attribute_id != '') {
            $row_data = Attribute::with('lang')->where('id', $req->id)->first();
            $variant = Variant::leftJoin('variant_i18n', 'variant_i18n.variant_id', '=', 'variant.id')
                    ->select('variant_i18n.id', 'variant_i18n.name', 'variant_i18n.variant_id as var_id')
                    ->where('variant.attribute_id', '=', $row_data['id'])
                    ->where('variant_i18n.language', '=', 'en')
                    ->get(['id', 'var_id', 'name']);
            foreach ($variant as $val) {
                $var_ar = VariantLang::where(['en_matching_id' => $val['id'], 'variant_id' => $val['var_id'], 'language' => 'ar'])->first();
                $variant_array['en_name'] = $val['name'];
                $variant_array['en_variant_id'] = $val['id'];
                $variant_array['ar_name'] = $var_ar['name'];
                $variant_array['ar_variant_id'] = $var_ar['id'];
                $lang_array[] = $variant_array;
            }
        }

        $data = [
            'row_data' => $row_data,
            'variant_data' => $lang_array,
            'cat_id' => $cat_id,
        ];
        return view('admin.attribute.create', $data);
    }

    public function save(Request $req) {
        $rules = [
            'name_en' => 'required',
            'name_ar' => 'required',
        ];
        $messages = [
            'name_en.required' => 'Name(EN) is required.',
            'name_ar.required' => 'Name(AR) is required.',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $variant_array = ([
                [
                    'name' => $req->att_value_en,
                    'language' => 'en',
                ],
                [
                    'name' => $req->att_value_ar,
                    'language' => 'ar',
                ],
            ]);
            if (empty($req->_attribute_id)) {
                $attribute = DB::transaction(function () use ($req) {
                            if ($req->attribute_type == 'textbox' || $req->attribute_type == 'textarea') {
                                $attribute = Attribute::create([
                                            'is_mandatory' => $req->is_mandatory,
                                            'attribute_type' => $req->attribute_type,
                                            'created_at' => Carbon::now()
                                ]);
                            } else {
                                $is_mandatory = $req->is_variant == 'no' ? $req->is_mandatory : 'yes';
                                $attribute = Attribute::create([
                                            'is_mandatory' => $is_mandatory,
                                            'attribute_type' => $req->attribute_type,
                                            'is_variant' => $req->is_variant,
                                            'created_at' => Carbon::now()
                                ]);
                            }
                            $attribute->lang()->createMany([
                                [
                                    'name' => $req->name_en,
                                    'language' => 'en',
                                ],
                                [
                                    'name' => $req->name_ar,
                                    'language' => 'ar',
                                ],
                            ]);

                            return $attribute;
                        });


                if ($req->attribute_type == 'dropdown') {
                    $variant = DB::transaction(function () use ($req, $variant_array, $attribute) {

                                $variant = Variant::create([
                                            'attribute_id' => $attribute->id,
                                            'created_at' => Carbon::now()
                                ]);
                                foreach ($req->att_value_en as $key => $var_value) {
                                    $variant_lang = VariantLang::create([
                                                'variant_id' => $variant->id,
                                                'attribute_id' => $attribute->id,
                                                'name' => $var_value,
                                                'language' => 'en',
                                    ]);
                                    VariantLang::create([
                                        'variant_id' => $variant->id,
                                        'attribute_id' => $attribute->id,
                                        'en_matching_id' => $variant_lang->id,
                                        'name' => $req->att_value_ar[$key],
                                        'language' => 'ar',
                                    ]);
                                }
                            });
                }
                $msg = "Attribute added successfully";
            } else {
                $get_attribute = Attribute::where('id', $req->_attribute_id)->first();
                if ($get_attribute->is_variant == 'yes' && $req->is_variant == 'no') {
                    $get_variant = Variant::where('attribute_id', $req->_attribute_id)->first();
                    $variants = ProductVariant::where(['variant_id' => $get_variant->id])->count();
                    if ($variants > 0) {
                        return response()->json(['status' => 0, 'message' => 'Variant attribute cannot be changed because it is assigned to some of the existing products']);
                        exit;
                    }
                }
                $attribute = DB::transaction(function () use ($req) {
                            if ($req->attribute_type == 'textbox' || $req->attribute_type == 'textarea') {
                                $get_variant = Variant::where('attribute_id', $req->_attribute_id)->first();
                                if (!empty($get_variant)) {
                                    $get_variant->lang()->delete();
//                                    $get_variant->delete();
                                }

                                $attribute = Attribute::where('id', $req->_attribute_id)
                                        ->update([
                                    'is_mandatory' => $req->is_mandatory,
                                    'attribute_type' => $req->attribute_type,
                                    'is_variant' => 'no',
                                ]);
                            } else {
                                $is_mandatory = $req->is_variant == 'no' ? $req->is_mandatory : 'yes';
                                $attribute = Attribute::where('id', $req->_attribute_id)
                                        ->update([
                                    'is_mandatory' => $is_mandatory,
                                    'attribute_type' => $req->attribute_type,
                                    'is_variant' => $req->is_variant,
                                ]);
                            }
                            AttributeLang::where('language', 'en')
                                    ->where('attribute_id', $req->_attribute_id)
                                    ->update([
                                        'name' => $req->name_en,
                            ]);
                            AttributeLang::where('language', 'ar')
                                    ->where('attribute_id', $req->_attribute_id)
                                    ->update([
                                        'name' => $req->name_ar,
                            ]);

                            return $attribute;
                        });
                if ($req->attribute_type == 'dropdown') {
//                    $get_variant = Variant::where('attribute_id', $req->_attribute_id)->first();
//                    if (!empty($get_variant)) {
//                        $get_variant->lang()->delete();
////                        $get_variant->delete();
//                    }
                    $variant = DB::transaction(function () use ($req, $variant_array) {
                        $var_ids =array();
                                $variant = Variant::where('attribute_id', $req->_attribute_id)->first();
                                if (empty($variant)) {
                                    $variant = Variant::create([
                                                'attribute_id' => $req->_attribute_id,
                                                'created_at' => Carbon::now()
                                    ]);
                                }

                                foreach ($req->att_value_en as $key => $var_value) {
                                    if ($req->att_value_en_id[$key] == '') {
                                        $variant_lang1 = VariantLang::create([
                                                    'variant_id' => $variant->id,
                                                    'attribute_id' => $req->_attribute_id,
                                                    'name' => $var_value,
                                                    'language' => 'en',
                                        ]);
                                        $variant_lang2 = VariantLang::create([
                                                    'variant_id' => $variant->id,
                                                    'attribute_id' => $req->_attribute_id,
                                                    'en_matching_id' => $variant_lang1->id,
                                                    'name' => $req->att_value_ar[$key],
                                                    'language' => 'ar',
                                        ]);
                                        $var_ids[] = $variant_lang1->id;
                                        $var_ids[] = $variant_lang2->id;
                                    } else {
                                        VariantLang::where('id', $req->att_value_en_id[$key])
                                                ->update([
                                                    'name' => $var_value,
                                                    'language' => 'en',
                                        ]);
                                        VariantLang::where('id', $req->att_value_ar_id[$key])
                                                ->update([
                                                    'name' => $req->att_value_ar[$key],
                                                    'language' => 'ar',
                                        ]);
                                        $var_ids[] = $req->att_value_en_id[$key];
                                        $var_ids[] = $req->att_value_ar_id[$key];
                                    }
                                }
                                if (!empty($var_ids)) {
                                    VariantLang::whereNotIn('id', $var_ids)->where(['attribute_id' => $req->_attribute_id, 'variant_id' => $variant->id])->delete();
                                }
                            });
                }
                $msg = "Attribute updated successfully";
            }
            if ($attribute) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }

    // activate/deactivate attribute
    public function activate(Request $req) {
        $att = Attribute::where('id', $req->id)->first();
        if ($att) {
            if ($att->status == 'deactive') {
                Attribute::where('id', $req->id)
                        ->update([
                            'status' => 'active'
                ]);
            } else {
                Attribute::where('id', $req->id)
                        ->update([
                            'status' => 'deactive'
                ]);
            }
            return response()->json(['status' => 1, 'message' => 'Status updated successfully']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }

    public function deleteAttribute(Request $req) {
        $attribute = Attribute::find($req->id);
        if (!empty($attribute)) {
            $pdt_att = ProductAttribute::select('id')->where('attribute_id', $req->id)->exists();
            $cat_att = CategoryAttributes::select('id')->where('attribute_id', $req->id)->exists();
            if ($pdt_att == false && $cat_att == false) {
                $attribute->lang()->delete();
                $attribute->delete();
                return response()->json(['status' => 1, 'message' => 'Attribute deleted successfully']);
            } else {
                return response()->json(['status' => 0, 'message' => 'This attribute cannot be deleted because it is assigned to some of the existing products or category.']);
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }

    public function add_category_attribute(Request $req) {
        if ($req->cat_id) {
            $cat_id = $req->cat_id;
            $att_ids = array();
            $cat_name = CategoryLang::select('name')->where(['language' => 'en', 'category_id' => $req->cat_id])->first();
            $sel_att_ids = CategoryAttributes::with('attributes')->where(['category_id' => $req->cat_id])->get();
            foreach ($sel_att_ids as $val) {
                $att_ids[] = $val->attribute_id;
            }
            $attributes = Attribute::with('lang')->whereNotIn('id', $att_ids)->get();
            $sel_attributes = CategoryAttributes::with('attributes')->where(['category_id' => $req->cat_id])->paginate(20);

            return view('admin.attribute.att_category', compact('attributes', 'sel_attributes', 'cat_name', 'cat_id'));
        }
    }

    public function save_cat_attribute(Request $req) {
        $rules = [
            'attributes' => 'required',
        ];
        $messages = [
            'attributes.required' => 'Attributes is required.',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $att_ids = '';
            $existing_attributes = CategoryAttributes::where(['category_id' => $req->cat_id])->pluck('attribute_id')->toArray();
            $attributes = $req->input('attributes');
            $final_array = array_merge($attributes, $existing_attributes);
            $var_count = Attribute::whereIn('id', $final_array)->where('is_variant', 'yes')->count();
            if ($var_count > 1) {
                return response()->json(['status' => 0, 'message' => 'Allow only one variant attribute to category']);
            } else {
                foreach ($attributes as $value) {
                    $cat_attribute_ids = Category::select('attribute_ids')->where('id', $req->cat_id)->first();
                    $attribute_ids = $cat_attribute_ids->attribute_ids;
                    $attribute_ids = $attribute_ids . '{{' . $value . '}}';
                    $categories = Category::where('id', $req->cat_id)
                            ->update([
                        'attribute_ids' => $attribute_ids,
                    ]);
                    CategoryAttributes::where(['attribute_id' => $value, 'category_id' => $req->cat_id])->delete();
                    $saved = CategoryAttributes::create(['category_id' => $req->cat_id,
                                'attribute_id' => $value
                    ]);
                }

                return response()->json(['status' => 1, 'message' => 'Attributes added successfully']);
            }
        }
    }

    public function delete_attributes(Request $req) {
        if (!empty($req->attribute_id) && !empty($req->cat_id)) {
            if (CategoryAttributes::where(['attribute_id' => $req->attribute_id, 'category_id' => $req->cat_id])->exists()) {
                CategoryAttributes::where(['attribute_id' => $req->attribute_id, 'category_id' => $req->cat_id])->delete();
            }
            $att_ids = Category::select('attribute_ids')->where('id', $req->cat_id)->first();
            $att_ids = $att_ids->attribute_ids;
            $att_ids = str_replace('{{' . $req->attribute_id . '}}', '', $att_ids);
            Category::where('id', $req->cat_id)
                    ->update([
                        'attribute_ids' => $att_ids,
            ]);
            return response()->json(['status' => 1, 'message' => 'Attribute deleted successfully']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong']);
        }
    }

}
