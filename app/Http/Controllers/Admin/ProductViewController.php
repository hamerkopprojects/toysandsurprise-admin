<?php

namespace App\Http\Controllers\Admin;

use Config;
use App\Models\Product;
use App\Models\ProductAge;
use App\Models\ProductTag;
use Illuminate\Http\Request;
use App\Models\ProductGender;
use App\Models\ProductImages;
use App\Models\ProductVariant;
use App\Models\ProductB2BPrice;
use App\Models\ProductB2CPrice;
use App\Models\ProductAttribute;
use App\Models\ProductCategories;
use App\Http\Controllers\Controller;

class ProductViewController extends Controller
{
    public function details(Request $req)
    {
        $product_id = $req->id;

        $product = Product::with('lang')->with('category')->with('brand')->where('id', $product_id)->get();
        $product_category = ProductCategories::with('category')->where('product_id', $product_id)->get();
        $product_tags = ProductTag::with('tag')->where('product_id', $product_id)->get();
        $product_gender = ProductGender::with('gender')->where('product_id', $product_id)->get();
        $product_age = ProductAge::with('age')->where('product_id', $product_id)->get();
       
        return view('admin.product.products.product_view', compact('product','product_category','product_tags','product_gender'));
    }
    public function get_viewtabs(Request $req)
    {

        $product_id = $req->id;
        $product_type = Product::where('id', $product_id)->pluck('product_type');
        if ($req->activeTab == 'PRODUCT INFO') {
            $product_id = $req->id;
            $product = Product::with('lang')->with('category')->with('brand')->where('id', $product_id)->get();
            $product_category = ProductCategories::where('product_id', $product_id)->get();
            dd($product_category);
            return view('admin.product.products.product_info_view', compact('product','product_category'))->render();
        } elseif ($req->activeTab == 'SPEC & STOCK') {
            $pro = Product::with('lang')->where('id', $product_id)
            ->get();
            
            $pdt_att = ProductAttribute::with('lang')->with('attribute')->with('product')
                ->where('attribute_value_type', 'textbox')
                ->where('product_id', $product_id)
                ->get();

            $pdt_var = ProductAttribute::with('lang')->with('attribute')->with('variant')
                ->where('attribute_value_type', 'dropdown')
                ->where('product_id', $product_id)
                ->get();

            $pdt_att_var = ProductVariant::with('attribute')->with('variant')
                ->where('product_id', $product_id)
                ->get();



            return view('admin.product.products.product_stock_view', compact('pdt_att', 'pdt_var', 'pdt_att_var', 'product_type', 'pro'))->render();
        } elseif ($req->activeTab == 'PRICE') {

            $prdt = ProductB2CPrice::where('product_id', $product_id)->get();

            $pdt_att_var = ProductB2CPrice::with('attribute')->with('variant')->where('product_id', $product_id)->get();

            return view('admin.product.products.product_b2c_price_view')->with(compact('prdt', 'pdt_att_var', 'product_type'))->render();
        }else{
            
            $prdt = ProductImages::where('product_id', $product_id)->get();
           
            $cover = Product::where('id', $product_id)->get();
            return view('admin.product.products.product_images_view')->with(compact('prdt', 'cover'))->render();
        }
    }   
}
