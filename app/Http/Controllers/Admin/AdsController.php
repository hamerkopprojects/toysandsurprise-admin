<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Ads;
use App\Slider;
use App\Models\CategoryLang;
use App\Models\ProductLang;
use Validator;
use Config;
use DB;

class AdsController extends Controller
{

    //list customer
    public function index(Request $req)
    {
        $ads_data = Ads::with('appcategory')
            ->with('appproduct')->with('webcategory')
            ->with('webproduct')->get();
        $data = [
            'ads_data' => $ads_data,
        ];
        return view('admin.ads.index', $data);
    }

    public function upload_image(Request $request)
    {
        $rules = [
            'photo' => 'required|image|mimes:png,jpg,jpeg|max:10240'
        ];
        $messages = [
            'photo.max' => 'The image must be less than 2Mb in size',
            'photo.mimes' => "The image must be of the format jpeg or png",
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {

            $file = request()->file('photo');
            $path = $file->store("{$request->type}", ['disk' => 'public_uploads']);
            Ads::where('id', $request->id)
                ->update([
                    "{$request->type}" => $path
                ]);
            return response()->json(['status' => 1, 'message' => 'File uploaded successfully']);
        }
    }

    public function detete_img(Request $request)
    {
        $cust = Ads::where('id', $request->id)->first();
        if (!empty($cust)) {
            if ($request->type == 'web_image') {
                Ads::where('id', $request->id)
                    ->update([
                        "{$request->type}" => '',
                        "web_category" => NULL,
                        "web_product" => NULL,
                        "url" => NULL
                    ]);
            } else {
                Ads::where('id', $request->id)
                    ->update([
                        "{$request->type}" => '',
                        "app_category" => NULL,
                        "app_product" => NULL
                    ]);
            }
            $file_path = public_path() . '/uploads/' . $cust["{$request->type}"];
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        return response()->json(['status' => 1, 'message' => 'File deleted successfully']);
    }

    public function link_category(Request $request)
    {
        $_ad_id = $request->id;
        $type = $request->type;
        if ($type == 'A') {
            $flag_type = 'app_flag';
        } else {
            $flag_type = 'web_flag';
        }
        $row_data = array();
        if ($_ad_id != '') {
            $row_data = Ads::where('id', '=', $_ad_id)->first();
        }

        $cat_data = CategoryLang::where(['language' => 'en'])->get();
        $pdt_data = ProductLang::where(['language' => 'en'])->get();

        $data = [
            'row_data' => $row_data,
            'type' => $type,
            'flag_type' => $flag_type,
            'cat_data' => $cat_data,
            'pdt_data' => $pdt_data,
        ];
        return view('admin.ads.create_link', $data);
    }

    public function add_link(Request $req)
    {
        if ($req->type == 'W') {
            $rules = [
                'url' => 'required_without_all:category,product',
                'category' => 'required_without_all:url,product',
                'product' => 'required_without_all:url,category',
            ];
            $messages = [
                'url.required' => 'Url is required.',
                'category.required' => 'Category is required.',
                'product.required' => 'Product is required.',
            ];
        } else {
            $rules = [
                'category' => 'required_without_all:product',
                'product' => 'required_without_all:category',
            ];
            $messages = [
                'category.required' => 'Category is required.',
                'product.required' => 'Product is required.',
            ];
        }
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            if ($req->type == 'W') {
                $data_to_save = [
                    'web_flag' => $req->link_value,
                ];
                if ($req->link_value == 'U') {
                    $data_to_save['url'] = $req->url;
                    $data_to_save['web_category'] = NULL;
                    $data_to_save['web_product'] = NULL;
                } elseif ($req->link_value == 'C') {
                    $data_to_save['url'] = '';
                    $data_to_save['web_category'] = $req->category;
                    $data_to_save['web_product'] = NULL;
                } else {
                    $data_to_save['url'] = '';
                    $data_to_save['web_category'] = NULL;
                    $data_to_save['web_product'] = $req->product;
                }
            } else {
                $data_to_save = [
                    'app_flag' => $req->link_value,
                ];
                if ($req->link_value == 'C') {
                    $data_to_save['app_category'] = $req->category;
                    $data_to_save['app_product'] = NULL;
                } else {
                    $data_to_save['app_category'] = NULL;
                    $data_to_save['app_product'] = $req->product;
                }
            }
            $saved_data = Ads::where('id', $req->_ad_id)
                ->update($data_to_save);
            $msg = 'Link updated successfully.';
            return response()->json(['status' => 1, 'message' => $msg]);
        }
    }

    public function get_tabs(Request $req)
    {
        if ($req->activeTab == 'ADS') {
            return response()->json(['status' => 'ads']);
        } else {
            return response()->json(['status' => 'slider']);
        }
    }

    public function slider(Request $req)
    {
        $slider_data = Slider::with('slider_category')
            ->with('slider_product')->get();
        $data = [
            'slider_data' => $slider_data,
        ];
        return view('admin.ads.slider', $data);
    }

    public function upload_slider_image(Request $request)
    {
        $rules = [
            'photo' => 'required|image|mimes:png,jpg,jpeg|max:10240'
        ];
        $messages = [
            'photo.max' => 'The image must be less than 2Mb in size',
            'photo.mimes' => "The image must be of the format jpeg or png",
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {

            $file = request()->file('photo');
            $path = $file->store("slider", ['disk' => 'public_uploads']);
            Slider::where('id', $request->id)
                ->update([
                    "web_image" => $path
                ]);
            return response()->json(['status' => 1, 'message' => 'File uploaded successfully']);
        }
    }

    public function slider_category(Request $request)
    {
        $_slider_id = $request->id;
        $type = $request->type;
        $row_data = array();
        if ($_slider_id != '') {
            $row_data = Slider::where('id', '=', $_slider_id)->first();
        }

        $cat_data = CategoryLang::where(['language' => 'en'])->get();
        $pdt_data = ProductLang::where(['language' => 'en'])->get();

        $data = [
            'row_data' => $row_data,
            'type' => $type,
            'cat_data' => $cat_data,
            'pdt_data' => $pdt_data,
        ];
        return view('admin.ads.create_slider_link', $data);
    }

    public function add_slider_category(Request $req)
    {
        $rules = [
            'url' => 'required_without_all:category,product',
            'category' => 'required_without_all:url,product',
            'product' => 'required_without_all:url,category',
        ];
        $messages = [
            'url.required' => 'Url is required.',
            'category.required' => 'Category is required.',
            'product.required' => 'Product is required.',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $data_to_save = [
                'web_flag' => $req->link_value,
            ];
            if ($req->link_value == 'U') {
                $data_to_save['url'] = $req->url;
                $data_to_save['category'] = NULL;
                $data_to_save['product'] = NULL;
            } elseif ($req->link_value == 'C') {
                $data_to_save['url'] = '';
                $data_to_save['category'] = $req->category;
                $data_to_save['product'] = NULL;
            } else {
                $data_to_save['url'] = '';
                $data_to_save['category'] = NULL;
                $data_to_save['product'] = $req->product;
            }
            $saved_data = Slider::where('id', $req->_slider_id)
                ->update($data_to_save);
            $msg = 'Link updated successfully.';
            return response()->json(['status' => 1, 'message' => $msg]);
        }
    }

    public function detete_slider_img(Request $request)
    {
        $slider = Slider::where('id', $request->id)->first();
        if (!empty($slider)) {
            Slider::where('id', $request->id)
                ->update([
                    "web_image" => '',
                    "category" => NULL,
                    "product" => NULL,
                    "url" => NULL
                ]);
            $file_path = public_path() . '/uploads/' . $slider['web_image'];
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        return response()->json(['status' => 1, 'message' => 'File deleted successfully']);
    }

    public function add_slider_data(Request $req)
    {
        //        $rules = [
        //            'title' => 'required',
        //            'content' => 'required',
        //        ];
        //        $messages = [
        //            'title.required' => 'Title is required.',
        //            'content.required' => 'Content is required.',
        //        ];
        //        $validator = Validator::make($req->all(), $rules, $messages);
        //        if (!$validator->passes()) {
        //            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        //        } else {
        if (!empty($req->slider_id)) {
            if (!empty($req->title) || !empty($req->content)) {
                Slider::where('id', $req->slider_id)
                    ->update([
                        'title' => $req->title,
                        'content' => $req->content
                    ]);
                return response()->json(['status' => 1, 'message' => "Data updated successfully"]);
            } else {
                Slider::where('id', $req->slider_id)
                    ->update([
                        'title' => '',
                        'content' => ''
                    ]);
                return response()->json(['status' => 1, 'message' => "Data updated successfully"]);
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
        //        }
    }
}
