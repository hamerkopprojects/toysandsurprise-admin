<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\OtherSettings;
use App\Http\Controllers\Controller;

class OtherSettingsController extends Controller
{
    public function get()
    {
        $other = OtherSettings::first();

        return view('admin.settings.other.index', compact('other'));
    }


    public function store(Request $request)
    {


        $other = OtherSettings::first();
        $data = [
            'currency' => json_encode([
                'currency_en' => $request->currency_en,
                'currency_ar' => $request->currency_ar
            ]),
            'driver_loc_fetch_interval' => json_encode([
                'fetch_time' => $request->fetch_time,
                'cancel_time' => $request->cancel_time
            ]),
            'payment_options' => json_encode([
                'cod' => $request->cod,
                'online' => $request->online, 'ofline' => $request->ofline
            ]),
            'warehouse_loc' => json_encode([
                'latitude' => $request->latitude,
                'longitude' => $request->longitude
            ]),
            'cod_fee' => $request->codfee,
            'auto_approval_for_cod_orders' => $request->order ? "enable" : "disable",
            'operational_area' => $request->latlng

        ];
        if (!empty($other)) {
            $setting_save = OtherSettings::where('id', $other->id)->update(
                $data
            );
            return redirect()->route('other.get')->with('success', 'Setting Updated Successfully');
        } else {
            $setting_save = OtherSettings::Create(
                $data
            );
            return redirect()->route('other.get')->with('success', 'Setting Added Successfully');
        }
    }
}
