<?php

namespace App\Http\Controllers\Admin;

use App\Models\JobCategory;
use Illuminate\Http\Request;
use App\Models\JobCategoryLang;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class JobCategoryController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->job_category_select;
        $search_field = $request->search_field ?? '';

        $jobCategoryLang = JobCategoryLang::where('language', 'en')->get();

        if ($search_field) {
            $data = JobCategoryLang::select('category_id')->where('name', 'like', "%" . $search_field . "%")->get()->toArray();
            $da = [];
            foreach ($data as  $d) {
                $da[] = $d['category_id'];
            }
            $jobCategory = JobCategory::with('lang')->whereIn('id', $da)->paginate(20);
        } else {
            $jobCategory = JobCategory::with('lang')->paginate(20);
        }
        return view('admin.job.category.index', compact('jobCategory',  'jobCategoryLang', 'search_field'));
    }
    public function store(Request $request)
    {
        $category = DB::transaction(function () use ($request) {
            $category = JobCategory::create([
                'status' => 'active',
                'category_unique_id' => $this->generateCategoryId(),
            ]);
            $category->lang()->createMany([
                [
                    'name' => $request->title_en,
                    'language' => 'en',
                ],
                [
                    'name' => $request->title_ar,
                    'language' => 'ar',
                ],
            ]);

            return $category;
        });
        if ($category) {
            return [
                "msg" => "success"
            ];
        } else {
            return [
                "msg" => "error"
            ];
        }
    }
    public function edit($id)
    {
        $cate = JobCategory::with('lang')
            ->where('id', $id)
            ->first();
        return [
            'page' => $cate,

        ];
    }
    public function update(Request $request)
    {
        $category = DB::transaction(function () use ($request) {

            JobCategoryLang::where('category_id', $request->id)
                ->where('language', 'en')
                ->update(
                    [
                        'name' => $request->title_en,

                    ]
                );
            JobCategoryLang::where('category_id', $request->id)
                ->where('language', 'ar')
                ->update(
                    [
                        'name' => $request->title_ar,

                    ]
                );
        });

        return [
            'msg' => "success"
        ];
    }
    public function generateCategoryId()
    {
        $lastcategory = JobCategory::select('category_unique_id')
            ->orderBy('id', 'desc')
            ->first();

        $lastId = 0;

        if ($lastcategory) {
            $lastId = (int) substr($lastcategory->category_unique_id, 4);
        }

        $lastId++;

        return 'CATG' . str_pad($lastId, 5, '0', STR_PAD_LEFT);
    }
    public function statusUpdate(Request $request)
    {
        $status = $request->status === 'active' ? 'deactive' : 'active';
        JobCategory::where('id', $request->id)
            ->update([
                'status' => $status
            ]);
        return [
            "msg" => 'success'
        ];
    }
    public function destroy(Request $request)
    {
        $faq = JobCategory::find($request->id);
        $faq->lang()->delete();
        $faq->delete();
        return [
            'msg' => 'success'
        ];
    }
    public function search(Request $req)
    {
        $lang = JobCategoryLang::where('name', 'like', "%" . $req->search . "%")
            ->where('language', '=', 'en')
            ->where('deleted_at', NULL)->get();
        $response = array();
        foreach ($lang as $lan) {
            $response[] = array("value" => $lan->category_id, "label" => $lan->name);
        }
        return response()->json($response);
    }
}
