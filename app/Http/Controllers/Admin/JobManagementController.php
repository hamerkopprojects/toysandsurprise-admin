<?php

namespace App\Http\Controllers\Admin;

use App\Models\JobCategory;
use Illuminate\Http\Request;
use App\Models\JobManagement;
use App\Models\JobCategoryLang;
use App\Models\JobManagementLang;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class JobManagementController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->job_category_select;
        $search_field = $request->search_field ?? '';

        $jobManagementLang = JobManagementLang::where('language', 'en')->get();

        $jobCategory = JobCategory::with('lang')->get();
        // dd($jobCategory);

        if ($search_field) {
            $data = JobManagementLang::select('management_id')->where('name', 'like', "%" . $search_field . "%")->get()->toArray();
            $da = [];
            foreach ($data as  $d) {
                $da[] = $d['management_id'];
            }
            $jobManagement = JobManagement::with('lang')->with(['jobCategory' => function ($query) {
                $query->with('lang:id,category_id,name');
            }])->whereIn('id', $da)->paginate(20);
        } else {
            $jobManagement = JobManagement::with('lang')->with(['jobCategory' => function ($query) {
                $query->with('lang:id,category_id,name');
            }])->paginate(20);
        }
        return view('admin.job.management.index', compact('jobManagementLang',  'jobManagement', 'search_field', 'jobCategory'));
    }
    public function store(Request $request)
    {

        $management = DB::transaction(function () use ($request) {
            $management = JobManagement::create([
                'job_category_id' => $request->category,
                'package' => $request->package,
                'experience' => $request->experience,
                'status' => 'active',
            ]);

            $management->lang()->createMany([
                [
                    'name' => $request->title_en,
                    'description' => $request->description_en,
                    'language' => 'en',
                ],
                [
                    'name' => $request->title_ar,
                    'description' => $request->description_ar,
                    'language' => 'ar',
                ],
            ]);

            return $management;
        });
        if ($management) {
            return [
                "msg" => "success"
            ];
        } else {
            return [
                "msg" => "error"
            ];
        }
    }
    public function edit($id)
    {
        $management = JobManagement::with('lang')->with('jobCategory')
            ->where('id', $id)
            ->first();
        return [
            'page' => $management,

        ];
    }
    public function update(Request $request)
    {
        $management = DB::transaction(function () use ($request) {

            JobManagement::where('id', $request->id)
                ->update(
                    [
                        'job_category_id' => $request->category,
                        'package' => $request->package,
                        'experience' => $request->experience,
                    ]
                );
            JobManagementLang::where('management_id', $request->id)
                ->where('language', 'en')
                ->update(
                    [
                        'name' => $request->title_en,
                        'description' => $request->description_en,

                    ]
                );
            JobManagementLang::where('management_id', $request->id)
                ->where('language', 'ar')
                ->update(
                    [
                        'name' => $request->title_ar,
                        'description' => $request->description_ar,
                    ]
                );
        });

        return [
            'msg' => "success"
        ];
    }

    public function statusUpdate(Request $request)
    {
        $status = $request->status === 'active' ? 'deactive' : 'active';
        JobManagement::where('id', $request->id)
            ->update([
                'status' => $status
            ]);
        return [
            "msg" => 'success'
        ];
    }
    public function destroy(Request $request)
    {
        $management = JobManagement::find($request->id);
        $management->lang()->delete();
        $management->jobCategory()->delete();
        $management->delete();
        return [
            'msg' => 'success'
        ];
    }
    public function search(Request $req)
    {
        $lang = JobManagementLang::where('name', 'like', "%" . $req->search . "%")
            ->where('language', '=', 'en')->get();
        $response = array();
        foreach ($lang as $lan) {
            $response[] = array("value" => $lan->management_id, "label" => $lan->name);
        }

        return response()->json($response);
    }
}
