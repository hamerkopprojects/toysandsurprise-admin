<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\ShopManagement;
use App\Models\ShopManagementLang;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ShopManagementController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->shop_management_select;
        $search_field = $request->search_field ?? '';

        $shopManagementLang = ShopManagementLang::where('language', 'en')->get();



        if ($search_field) {
            $data = ShopManagementLang::select('shop_id')->where('name', 'like', "%" . $search_field . "%")->get()->toArray();
            $da = [];
            foreach ($data as  $d) {
                $da[] = $d['shop_id'];
            }
            $shopManagement = ShopManagement::with('lang')->whereIn('id', $da)->paginate(20);
        } else {
            $shopManagement = ShopManagement::with('lang')->paginate(20);
        }
        return view('admin.shop.management.index', compact('shopManagementLang',  'shopManagement', 'search_field'));
    }
    public function store(Request $request)
    {
        $management = DB::transaction(function () use ($request) {
            $management = ShopManagement::create([
                'location' => $request->location,
                'latitude' => $request->latitude,
                'longitude' => $request->longitude,
                'status' => 'active',
            ]);

            $management->lang()->createMany([
                [
                    'name' => $request->title_en,
                    'address' => $request->address_en,
                    'language' => 'en',
                ],
                [
                    'name' => $request->title_ar,
                    'address' => $request->address_ar,
                    'language' => 'ar',
                ],
            ]);

            return $management;
        });
        if ($management) {
            return [
                "msg" => "success"
            ];
        } else {
            return [
                "msg" => "error"
            ];
        }
    }
    public function edit($id)
    {
        $management = ShopManagement::with('lang')
            ->where('id', $id)
            ->first();
        return [
            'page' => $management,

        ];
    }
    public function update(Request $request)
    {
        $management = DB::transaction(function () use ($request) {

            ShopManagement::where('id', $request->id)
                ->update(
                    [
                        'location' => $request->location,
                        'latitude' => $request->latitude,
                        'longitude' => $request->longitude,
                    ]
                );
            ShopManagementLang::where('shop_id', $request->id)
                ->where('language', 'en')
                ->update(
                    [
                        'name' => $request->title_en,
                        'address' => $request->address_en,

                    ]
                );
            ShopManagementLang::where('shop_id', $request->id)
                ->where('language', 'ar')
                ->update(
                    [
                        'name' => $request->title_ar,
                        'address' => $request->address_ar,
                    ]
                );
        });

        return [
            'msg' => "success"
        ];
    }
    public function statusUpdate(Request $request)
    {
        $status = $request->status === 'active' ? 'deactive' : 'active';
        ShopManagement::where('id', $request->id)
            ->update([
                'status' => $status
            ]);
        return [
            "msg" => 'success'
        ];
    }
    public function destroy(Request $request)
    {
        $management = ShopManagement::find($request->id);
        $management->lang()->delete();
        $management->delete();
        return [
            'msg' => 'success'
        ];
    }
    public function search(Request $req)
    {
        $lang = ShopManagementLang::where('name', 'like', "%" . $req->search . "%")
            ->where('language', '=', 'en')->where('deleted_at', '=', 'NULL')->get();
        $response = array();
        foreach ($lang as $lan) {
            $response[] = array("value" => $lan->shop_id, "label" => $lan->name);
        }

        return response()->json($response);
    }
}
