<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\CancellationReason;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\CancellationReasonLang;

class CancellationReasonController extends Controller
{
    public function get(Request $request)
    {

        $search_field = $request->cancellation_search ?? '';
        $search = $request->search_field ?? '';

        $reason = CancellationReasonLang::where('language', 'en')->get();
        if ($search) {
            $data = CancellationReasonLang::select('cancellation_id')->where('name', 'like', "%" . $search . "%")->get()->toArray();
            $da = [];
            foreach ($data as  $d) {
                $da[] = $d['cancellation_id'];
            }
            $cancel = CancellationReason::with('lang')->whereIn('id', $da)->paginate(20);
        } else {
            $cancel = CancellationReason::with('lang')->paginate(20);
        }


        return view('admin.settings.cancel.index', compact('reason', 'cancel', 'search'));
    }
    public function store(Request $request)
    {
        $cancel = DB::transaction(function () use ($request) {
            $cancel = new CancellationReason();
            $cancel->save();
            $cancel->lang()->createMany([
                [
                    'name' => $request->cancel_en,
                    'language' => 'en',
                ],
                [
                    'name' => $request->cancel_ar,
                    'language' => 'ar',
                ],
            ]);
            return $cancel;
        });
        if ($cancel) {
            return [
                "status" => '200',
                "msg" => 'success'
            ];
        }
    }

    public function edit($id)
    {
        $cancelReason = CancellationReason::with('lang')
            ->where('id', $id)
            ->first();
        return [
            "cancel" => $cancelReason
        ];
    }
    public function update(Request $request)
    {
        $cancel = DB::transaction(function () use ($request) {
            CancellationReasonLang::where('cancellation_id', $request->id)
                ->where('language', 'en')
                ->update([
                    'name' => $request->cancel_en,

                ]);
            CancellationReasonLang::where('cancellation_id', $request->id)
                ->where('language', 'ar')
                ->update([
                    'name' => $request->cancel_ar,

                ]);
        });
        return [
            "status" => '200',
            "msg" => ''
        ];
    }
    public function destroy(Request $req)
    {
        $model = CancellationReason::find($req->id);
        $model->lang()->delete();
        $model->delete();
        return [
            'status' => '200'
        ];
    }
    public function autoComplete(Request $req)
    {
        $lang = CancellationReasonLang::where('name', 'like', "%" . $req->search . "%")
            ->where('language', '=', 'en')->get();
        $response = array();
        foreach ($lang as $lan) {
            $response[] = array("value" => $lan->cancellation_id, "label" => $lan->name);
        }

        return response()->json($response);
    }
}
