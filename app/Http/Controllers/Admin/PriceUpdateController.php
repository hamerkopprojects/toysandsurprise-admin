<?php

namespace App\Http\Controllers\Admin;

use DB;
use Excel;
use Config;
use Validator;
use App\Models\Product;
use App\Models\VariantLang;
use App\Models\TempB2cPrice;
use Illuminate\Http\Request;
use App\Imports\ProductImport;
use Illuminate\Support\Carbon;
use App\Models\ProductB2BPrice;
use App\Models\ProductB2CPrice;
use App\Http\Controllers\Controller;

class PriceUpdateController extends Controller
{
    public function index(Request $request)
    {

        if (!empty($request['type'])) {
            $type = $request['type'];
        }
        if (!empty($request->action)) {
            $type = $request->action;
        }
        TempB2cPrice::truncate();
        return view('admin.product.price_updates.index', compact('type'));
    }

    public function pricesubmit(Request $req)
    {

        $type = $req->type;
        $rules = [
            'select_file' => 'required|mimes:xlsx|max:2048',
        ];
        $messages = [
            'select_file.required' => 'File is required.',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $file = $req->file('select_file');
            $data = Excel::toArray(new ProductImport, $file);

            if ($type == "simpleb2c") {
                TempB2cPrice::truncate();
                if (count($data) > 0) {
                    foreach ($data as $key => $sub) {
                        foreach ($sub as $key1 => $val) {
                            if ($key1 > 0) {
                                $errors = "";
                                $temp_products = new TempB2cPrice;

                                $temp_products->sku = trim($val[0]);
                                if ($temp_products->sku == "") {
                                    $errors .= "SKU is required,";
                                }
                                $temp_products->price = $val[1];
                                if ($temp_products->price == "") {
                                    $errors .= "Price is required,";
                                }
                                $temp_products->discount_price = $val[2];
                                if ($temp_products->discount_price == "") {
                                    $errors .= "Discount price is required,";
                                }


                                if ($errors) {
                                    $temp_products->error_flag = "Y";
                                    $temp_products->errors = $errors;
                                } else {
                                    $temp_products->error_flag = "N";
                                }
                                if (!empty($temp_products->sku)) {
                                    $pdt_data = TempB2cPrice::where('sku', '=', $temp_products->sku)->first();

                                    if (!empty($pdt_data)) {
                                        $temp_products->duplicate_flag = 'Y';
                                    }
                                }
                                $temp_products->created_at = Carbon::now();

                                $temp_products->save();
                            }
                        }
                    }
                    return response()->json(['status' => '1', 'type' => $type]);
                } else {
                    return response()->json(['message' => $validator->errors()->first(), 'status' => '0']);
                }
            } elseif ($type == "complexb2c") {
                TempB2cPrice::truncate();
                if (count($data) > 0) {
                    foreach ($data as $key => $sub) {
                        foreach ($sub as $key1 => $val) {
                            if ($key1 > 0) {
                                $errors = "";
                                $temp_products = new TempB2cPrice;

                                $temp_products->sku = trim($val[0]);
                                if ($temp_products->sku == "") {
                                    $errors .= "SKU is required,";
                                }
                                $temp_products->varient = $val[1];
                                if ($temp_products->varient == "") {
                                    $errors .= "Varient is required,";
                                }
                                $temp_products->price = $val[2];
                                if ($temp_products->price == "") {
                                    $errors .= "Price is required,";
                                }
                                $temp_products->discount_price = $val[3];
                                if ($temp_products->discount_price == "") {
                                    $errors .= "Discount price is required,";
                                }

                                if ($errors) {
                                    $temp_products->error_flag = "Y";
                                    $temp_products->errors = $errors;
                                } else {
                                    $temp_products->error_flag = "N";
                                }
                                if (!empty($temp_products->sku)) {
                                    $pdt_data = TempB2cPrice::where('sku', '=', $temp_products->sku)->where('varient', '=', $temp_products->varient)->first();

                                    if (!empty($pdt_data)) {
                                        $temp_products->duplicate_flag = 'Y';
                                    }
                                }
                                $temp_products->created_at = Carbon::now();

                                $temp_products->save();
                            }
                        }
                    }
                    return response()->json(['status' => '1', 'type' => $type]);
                } else {
                    return response()->json(['message' => $validator->errors()->first(), 'status' => '0']);
                }
            } elseif ($type == "simpleb2b") {
                TempB2cPrice::truncate();
                if (count($data) > 0) {
                    foreach ($data as $key => $sub) {
                        foreach ($sub as $key1 => $val) {
                            if ($key1 > 0) {
                                $errors = "";
                                $temp_products = new TempB2cPrice;

                                $temp_products->sku = trim($val[0]);
                                if ($temp_products->sku == "") {
                                    $errors .= "SKU is required,";
                                }
                                $temp_products->no_of_items = $val[1];
                                if ($temp_products->no_of_items == "") {
                                    $errors .= "No of items is required,";
                                }
                                $temp_products->type = $val[2];
                                if ($temp_products->type == "") {
                                    $errors .= "Type is required,";
                                }
                                $temp_products->price = $val[3];
                                if ($temp_products->price == "") {
                                    $errors .= "Price is required,";
                                }
                                $temp_products->discount_price = $val[4];
                                if ($temp_products->discount_price == "") {
                                    $errors .= "Discount price is required,";
                                }

                                if ($errors) {
                                    $temp_products->error_flag = "Y";
                                    $temp_products->errors = $errors;
                                } else {
                                    $temp_products->error_flag = "N";
                                }
                                if (!empty($temp_products->sku)) {
                                    $pdt_data = TempB2cPrice::where('sku', '=', $temp_products->sku)->where('no_of_items', '=', $temp_products->no_of_items)->where('type', '=', $temp_products->type)->first();

                                    if (!empty($pdt_data)) {
                                        $temp_products->duplicate_flag = 'Y';
                                    }
                                }
                                $temp_products->created_at = Carbon::now();

                                $temp_products->save();
                            }
                        }
                    }
                    return response()->json(['status' => '1', 'type' => $type]);
                } else {
                    return response()->json(['message' => $validator->errors()->first(), 'status' => '0']);
                }
            } elseif ($type == "complexb2b") {
                TempB2cPrice::truncate();
                if (count($data) > 0) {
                    foreach ($data as $key => $sub) {
                        foreach ($sub as $key1 => $val) {
                            if ($key1 > 0) {
                                $errors = "";
                                $temp_products = new TempB2cPrice;

                                $temp_products->sku = trim($val[0]);
                                if ($temp_products->sku == "") {
                                    $errors .= "SKU is required,";
                                }
                               
                                $temp_products->varient = $val[1];
                                if ($temp_products->varient == "") {
                                    $errors .= "Variant is required,";
                                }
                                $temp_products->no_of_items = $val[2];
                                if ($temp_products->no_of_items == "") {
                                    $errors .= "No of items is required,";
                                }
                                $temp_products->type = $val[3];
                                if ($temp_products->type == "") {
                                    $errors .= "No of items is required,";
                                }
                                $temp_products->price = $val[4];
                                if ($temp_products->price == "") {
                                    $errors .= "Price is required,";
                                }
                                $temp_products->discount_price = $val[5];
                                if ($temp_products->discount_price == "") {
                                    $errors .= "Discount price is required,";
                                }

                                if ($errors) {
                                    $temp_products->error_flag = "Y";
                                    $temp_products->errors = $errors;
                                } else {
                                    $temp_products->error_flag = "N";
                                }
                                if (!empty($temp_products->sku)) {
                                    $pdt_data = TempB2cPrice::where('sku', '=', $temp_products->sku)->where('varient', '=', $temp_products->varient)->where('no_of_items', '=', $temp_products->no_of_items)->where('type', '=', $temp_products->type)->first();

                                    if (!empty($pdt_data)) {
                                        $temp_products->duplicate_flag = 'Y';
                                    }
                                }
                                $temp_products->created_at = Carbon::now();

                                $temp_products->save();
                            }
                        }
                    }
                    return response()->json(['status' => '1', 'type' => $type]);
                } else {
                    return response()->json(['message' => $validator->errors()->first(), 'status' => '0']);
                }
            }
        }
    }

    public function priceB2cSimple(Request $req)
    {
        $search = $req->search;
        $error_count  = $success_count = $duplicate_count = 0;
        $result_data = array();
        $pagination_count = 10;
        $query = TempB2cPrice::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
            });
        }
        $temp_data = $query->where('cron_flag', 'I')->paginate($pagination_count)->appends(request()->query());

        if (count($temp_data) > 0) {
            foreach ($temp_data as $row_data) {
                $sku_data = Product::where('sku', '=', $row_data->sku)->first();

                if (empty($sku_data)) {
                    $row_data->error_flag = 'Y';
                }
                if (!empty($sku_data)) {
                    if ($sku_data->product_type != 'simple') {
                        $row_data->error_flag = 'Y';
                    }
                }
                if (!empty($row_data->price) && !empty($row_data->discount_price)) {
                    if ($row_data->price < $row_data->discount_price) {
                        $row_data->error_flag = 'Y';
                    }
                }


                if ($row_data->error_flag == 'Y' && $row_data->duplicate_flag == 'N') {
                    $error_count++;
                }
                if ($row_data->duplicate_flag == 'Y') {
                    $duplicate_count++;
                }

                if ($row_data->error_flag == 'N' && $row_data->duplicate_flag == 'N') {
                    $success_count++;
                }
                $result[] = $row_data;
            }
            $result_data = $result;
        }

        $total_count = $error_count + $success_count + $duplicate_count;
        $data = [
            'temp_data' => $temp_data,
            'result_data' => $result_data,
            'error_count' => $error_count,
            'duplicate_count' => $duplicate_count,
            'success_count' => $success_count,
            'total_count' => $total_count,
            'search' => $search
        ];

        return view('admin.product.price_updates.b2c_simple', $data);
    }

    public function b2cSimpleErrors(Request $req)
    {

        $id = $req->id;
        $result = TempB2cPrice::where('id', $id)->first();

        $sku_data = Product::where('sku', '=', $result->sku)->first();

        $new_errors = '';
        if (!empty($result->sku)) {
            if (empty($sku_data)) {

                $new_errors .= "SKU not exist ,";
            }
        }
        if (!empty($sku_data)) {
            if ($sku_data->product_type != 'simple') {
                $new_errors .= "Imported product is not simple ,";
            }
        }
        if (!empty($result->price) && !empty($result->discount_price)) {
            if (($result->price) < ($result->discount_price)) {
                $new_errors .= "Discount price is greater than price,";
            }
        }

        $all_errors = $result->errors . $new_errors;
        $errors = [];
        $errors = explode(",", $all_errors);

        $data = ['errors' => $errors];

        return view('admin.product.price_updates.error', $data);
    }

    public function confirmSimpleB2c()
    {

        $result_data = array();

        $result_data = TempB2cPrice::where('cron_flag', 'I')->get();

        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'N') {
                    $sku_data = Product::where('sku', '=', $row_data->sku)->first();
                    if (empty($sku_data)) {
                        $row_data->error_flag = 'Y';
                    } else {
                        if ($sku_data->product_type != 'simple') {
                            $row_data->error_flag = 'Y';
                        } else {
                            $row_data->sku = $row_data->sku;
                        }
                    }
                    if (!empty($row_data->price) && !empty($row_data->discount_price)) {
                        if (($row_data->price) < ($row_data->discount_price)) {
                            $row_data->error_flag = 'Y';
                        }
                    }

                    if ($row_data->error_flag == 'N') {
                        $pdt_data = Product::where('sku', '=', $row_data->sku)->first();
                        $data_to_save = [
                            'price' => $row_data->price,
                            'discount_price' => $row_data->discount_price
                        ];
                        $saved_data = ProductB2CPrice::where('product_id', $pdt_data->id)
                            ->update($data_to_save);
                    }
                }
                TempB2cPrice::where('id', $row_data->id)->update(['cron_flag' => 'S', 'error_flag' => $row_data->error_flag, 'duplicate_flag' => $row_data->duplicate_flag]);
            }
        }
        $check_temp_emp = TempB2cPrice::where('cron_flag', 'I')->first();
        if (!$check_temp_emp) {
            TempB2cPrice::where('cron_flag', 'S')->update(['cron_flag' => 'C']);
        }

        return response()->json(['message' => 'Thank you! Successfully updated price list', 'status' => '1']);
    }

    public function importedSimpleB2c(Request $req)
    {
        $search = $req->search;
        $error_count = $success_count = $duplicate_count = 0;
        $pagination_count = 10;
        $result_data = array();
        $query = TempB2cPrice::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
            });
        }
        $result_data = $query->where('cron_flag', 'C')
            ->paginate($pagination_count)->appends(request()->query());

        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'Y' && $row_data->duplicate_flag == 'N') {
                    $error_count++;
                }else if ($row_data->duplicate_flag == 'Y'){
                    $duplicate_count++;
                }
                 else {
                    $success_count++;
                }
            }
        }
        $duplicate = TempB2cPrice::where('duplicate_flag', 'Y')->get();
        $duplicate_count = $duplicate->count();
        $total_count = $error_count + $success_count + $duplicate_count;
        $data = [
            'result_data' => $result_data,
            'error_count' => $error_count,
            'duplicate_count' => $duplicate_count,
            'success_count' => $success_count,
            'total_count' => $total_count,
            'search' => $search,
        ];

        return view('admin.product.price_updates.b2c_simple_imported', $data);
    }

    public function priceB2cComplex(Request $req)
    {
        $search = $req->search;
        $error_count  = $success_count = $duplicate_count = 0;
        $result_data = array();
        $pagination_count = 10;
        $query = TempB2cPrice::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
            });
        }
        $temp_data = $query->where('cron_flag', 'I')->paginate($pagination_count)->appends(request()->query());

        if (count($temp_data) > 0) {
            foreach ($temp_data as $row_data) {
                $sku_data = Product::where('sku', '=', $row_data->sku)->first();

                if (empty($sku_data)) {
                    $row_data->error_flag = 'Y';
                }

                if (!empty($row_data->price) && !empty($row_data->discount_price)) {
                    if ($row_data->price < $row_data->discount_price) {
                        $row_data->error_flag = 'Y';
                    }
                }
                if (!empty($sku_data)) {
                    if ($sku_data->product_type != 'complex') {
                        $row_data->error_flag = 'Y';
                    }
                    $skdata = Product::where('sku', '=', $row_data->sku)->first();
                    if (!empty($skdata)) {
                        $varient_da = ProductB2CPrice::where('product_id', '=', $skdata->id)->get();
                        if (!empty($varient_da)) {
                            $val = "";
                            foreach ($varient_da as $varient_data) {

                                $varient = VariantLang::where('id', $varient_data->variant_id)->where('attribute_id', $varient_data->variant_attribute_id)->first();
                                if (!empty($varient)) {

                                    if ((strtolower($varient->name) === strtolower($row_data->varient))) {
                                        $val = "exist";
                                        break;
                                    }
                                }
                            }

                            if ($val != "exist") {

                                $row_data->error_flag = 'Y';
                            }
                        }
                    } else {
                        $row_data->error_flag = 'Y';
                    }
                }

                if ($row_data->error_flag == 'Y' && $row_data->duplicate_flag == 'N') {
                    $error_count++;
                }
                if ($row_data->duplicate_flag == 'Y') {
                    $duplicate_count++;
                }

                if ($row_data->error_flag == 'N' && $row_data->duplicate_flag == 'N') {
                    $success_count++;
                }
                $result[] = $row_data;
            }
            $result_data = $result;
        }

        $total_count = $error_count + $success_count + $duplicate_count;
        $data = [
            'temp_data' => $temp_data,
            'result_data' => $result_data,
            'error_count' => $error_count,
            'duplicate_count' =>$duplicate_count,
            'success_count' => $success_count,
            'total_count' => $total_count,
            'search' => $search
        ];

        return view('admin.product.price_updates.b2c_complex', $data);
    }
    public function b2cComplexErrors(Request $req)
    {

        $id = $req->id;
        $result = TempB2cPrice::where('id', $id)->first();

        $sku_data = Product::where('sku', '=', $result->sku)->first();

        $new_errors = '';
        if (!empty($result->sku)) {
            if (empty($sku_data)) {

                $new_errors .= "SKU not exist ,";
            }
        }
        if (!empty($result->price) && !empty($result->discount_price)) {
            if (($result->price) < ($result->discount_price)) {
                $new_errors .= "Discount price is greater than price,";
            }
        }
        if (!empty($sku_data)) {
            if ($sku_data->product_type != 'complex') {
                $new_errors .= "Imported product is not complex ,";
            }
            $varient_da = ProductB2CPrice::where('product_id', '=', $sku_data->id)->get();

            if (!empty($varient_da)) {

                $val = "";
                foreach ($varient_da as $varient_data) {
                    $varient = VariantLang::where('id', $varient_data->variant_id)->where('attribute_id', $varient_data->variant_attribute_id)->first();
                    if (!empty($varient)) {

                        if ((strtolower($varient->name) === strtolower($result->varient))) {
                            $val = "exist";
                            break;
                        }
                    }
                }

                if ($val != "exist") {

                    $new_errors .= "SKU and Variant mismatch,";
                }
            } else {
                $new_errors .= "SKU and Variant mismatch,";
            }
        }

        $all_errors = $result->errors . $new_errors;
        $errors = [];
        $errors = explode(",", $all_errors);

        $data = ['errors' => $errors];

        return view('admin.product.price_updates.error', $data);
    }

    public function confirmComplexB2c()
    {

        $result_data = array();
        $result_data = TempB2cPrice::where('cron_flag', 'I')->get();
        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'N') {
                    $sku_data = Product::where('sku', '=', $row_data->sku)->first();
                    if (empty($sku_data)) {
                        $row_data->error_flag = 'Y';
                    } else {
                        $varient_da = ProductB2CPrice::where('product_id', '=', $sku_data->id)->get();

                        if (!empty($varient_da)) {
                            $val = '';
                            foreach ($varient_da as $varient_data) {
                                $varient = VariantLang::where('id', $varient_data->variant_id)->where('attribute_id', $varient_data->variant_attribute_id)->first();
                                if (!empty($varient)) {

                                    if ((strtolower($varient->name) === strtolower($row_data->varient))) {
                                        $val = "exist";
                                        break;
                                    }
                                }
                            }

                            if ($val != "exist") {

                                $row_data->error_flag = 'Y';
                            } else {
                                $row_data->sku = $row_data->sku;
                            }
                        }
                    }
                    if (!empty($row_data->price) && !empty($row_data->discount_price)) {
                        if (($row_data->price) < ($row_data->discount_price)) {
                            $row_data->error_flag = 'Y';
                        }
                    }

                    if ($row_data->error_flag == 'N') {

                        $data_to_save = [
                            'price' => $row_data->price,
                            'discount_price' => $row_data->discount_price
                        ];
                        $saved_data = ProductB2CPrice::where('product_id', $sku_data->id)->where('variant_id', $varient->id)
                            ->update($data_to_save);
                    }
                }
                TempB2cPrice::where('id', $row_data->id)->update(['cron_flag' => 'S', 'error_flag' => $row_data->error_flag, 'duplicate_flag' => $row_data->duplicate_flag]);
            }
        }
        $check_temp_emp = TempB2cPrice::where('cron_flag', 'I')->first();
        if (!$check_temp_emp) {
            TempB2cPrice::where('cron_flag', 'S')->update(['cron_flag' => 'C']);
        }

        return response()->json(['message' => 'Thank you! Successfully updated price list', 'status' => '1']);
    }

    public function importedComplexB2c(Request $req)
    {
        $search = $req->search;
        $error_count = $success_count = $duplicate_count = 0;
        $pagination_count = 10;
        $result_data = array();
        $query = TempB2cPrice::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
            });
        }
        $result_data = $query->where('cron_flag', 'C')
            ->paginate($pagination_count)->appends(request()->query());

        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'Y' && $row_data->duplicate_flag == 'N') {
                    $error_count++;
                }else if ($row_data->duplicate_flag == 'Y'){
                    $duplicate_count++;
                }
                 else {
                    $success_count++;
                }
            }
        }
        $total_count = $error_count + $success_count + $duplicate_count;
        $data = [
            'result_data' => $result_data,
            'error_count' => $error_count,
            'duplicate_count' => $duplicate_count,
            'success_count' => $success_count,
            'total_count' => $total_count,
            'search' => $search,
        ];

        return view('admin.product.price_updates.b2c_complex_imported', $data);
    }

    public function priceB2bSimple(Request $req)
    {
        $search = $req->search;
        $error_count  = $success_count = $duplicate_count = 0;
        $result_data = array();
        $pagination_count = 10;
        $query = TempB2cPrice::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
            });
        }
        $temp_data = $query->where('cron_flag', 'I')->paginate($pagination_count)->appends(request()->query());

        if (count($temp_data) > 0) {
            foreach ($temp_data as $row_data) {
                $sku_data = Product::where('sku', '=', $row_data->sku)->first();

                if (empty($sku_data)) {
                    $row_data->error_flag = 'Y';
                }
                if (!empty($sku_data)) {
                    if ($sku_data->product_type != 'simple') {
                        $row_data->error_flag = 'Y';
                    }

                }
               
                if (!empty($row_data->type)) {
                    $container_type = (Config::get('constants.container_type'));
                    $key = array_search ($row_data->type, $container_type);
                   if($key == false){
                    $row_data->error_flag = 'Y'; 
                   }
                }
                if (!empty($row_data->price) && !empty($row_data->discount_price)) {
                    if ($row_data->price < $row_data->discount_price) {
                        $row_data->error_flag = 'Y';
                    }
                }


                if ($row_data->error_flag == 'Y' && $row_data->duplicate_flag == 'N') {
                    $error_count++;
                }
                if ($row_data->duplicate_flag == 'Y') {
                    $duplicate_count++;
                }

                if ($row_data->error_flag == 'N' && $row_data->duplicate_flag == 'N') {
                    $success_count++;
                }
                $result[] = $row_data;
            }
            $result_data = $result;
        }

        $total_count = $error_count + $duplicate_count + $success_count;
        $data = [
            'temp_data' => $temp_data,
            'result_data' => $result_data,
            'error_count' => $error_count,
            'duplicate_count' => $duplicate_count,
            'success_count' => $success_count,
            'total_count' => $total_count,
            'search' => $search
        ];

        return view('admin.product.price_updates.b2b_simple', $data);
    }

    public function b2bSimpleErrors(Request $req)
    {

        $id = $req->id;
        $result = TempB2cPrice::where('id', $id)->first();

        $sku_data = Product::where('sku', '=', $result->sku)->first();

        $new_errors = '';
        if (!empty($result->sku)) {
            if (empty($sku_data)) {

                $new_errors .= "SKU not exist ,";
            }
        }
        if (!empty($sku_data)) {
            if ($sku_data->product_type != 'simple') {
                $new_errors .= "Imported product is not simple ,";
            }
        }
        if (!empty($result->type)) {
            $container_type = (Config::get('constants.container_type'));
            $key = array_search ($result->type, $container_type);
           if($key == false){
            $new_errors .= "Container type not exist,";
           }
        }
        if (!empty($result->price) && !empty($result->discount_price)) {
            if (($result->price) < ($result->discount_price)) {
                $new_errors .= "Discount price is greater than  price,";
            }
        }

        $all_errors = $result->errors . $new_errors;
        $errors = [];
        $errors = explode(",", $all_errors);

        $data = ['errors' => $errors];

        return view('admin.product.price_updates.error', $data);
    }

    public function confirmSimpleB2b()
    {

        $result_data = array();

        $result_data = TempB2cPrice::where('cron_flag', 'I')->get();

        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'N') {
                    $sku_data = Product::where('sku', '=', $row_data->sku)->first();
                    if (empty($sku_data)) {
                        $row_data->error_flag = 'Y';
                    } else {
                        if ($sku_data->product_type != 'simple') {
                            $row_data->error_flag = 'Y';
                        } else {
                            $row_data->sku = $row_data->sku;
                        }
                    }
                    if (!empty($row_data->price) && !empty($row_data->discount_price)) {
                        if (($row_data->price) < ($row_data->discount_price)) {
                            $row_data->error_flag = 'Y';
                        }
                    }

                    if ($row_data->error_flag == 'N') {
                        $pdt_data = Product::where('sku', '=', $row_data->sku)->first();
                        $container_type = (Config::get('constants.container_type'));
                        $key = array_search ($row_data->type, $container_type);
                       $items = ProductB2BPrice::select('id')->where('product_id', $pdt_data->id)->where('quantity', $row_data->no_of_items)->where('container_type',$key)->get();
                  
                       if(count($items) > 0){
                       
                        $data_to_save = [
                            'price' => $row_data->price,
                            'discount_price' => $row_data->discount_price
                        ];
                        $saved_data = ProductB2BPrice::where('product_id', $pdt_data->id)->where('quantity', $row_data->no_of_items)->where('container_type',$key)
                            ->update($data_to_save);
                    }else{
                       
                        $type = ProductB2BPrice::where('product_id', $pdt_data->id)->first();
                     
                         ProductB2BPrice::create([
                            'product_id' => $pdt_data->id,
                            'container_type' => $key,
                            'quantity' => $row_data->no_of_items,
                            'price' => $row_data->price,
                            'discount_price' => $row_data->discount_price
                        ]);
                    }
                    }
                }
                TempB2cPrice::where('id', $row_data->id)->update(['cron_flag' => 'S', 'error_flag' => $row_data->error_flag, 'duplicate_flag' => $row_data->duplicate_flag]);
            }
        }
        $check_temp_emp = TempB2cPrice::where('cron_flag', 'I')->first();
        if (!$check_temp_emp) {
            TempB2cPrice::where('cron_flag', 'S')->update(['cron_flag' => 'C']);
        }

        return response()->json(['message' => 'Thank you! Successfully updated price list', 'status' => '1']);
    }

    public function importedSimpleB2b(Request $req)
    {
        $search = $req->search;
        $error_count = $success_count = $duplicate_count = 0;
        $pagination_count = 10;
        $result_data = array();
        $query = TempB2cPrice::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
            });
        }
        $result_data = $query->where('cron_flag', 'C')
            ->paginate($pagination_count)->appends(request()->query());

        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'Y' && $row_data->duplicate_flag == 'N') {
                    $error_count++;
                }else if ($row_data->duplicate_flag == 'Y'){
                    $duplicate_count++;
                }
                 else {
                    $success_count++;
                }
            }
        }
        $duplicate = TempB2cPrice::where('duplicate_flag', 'Y')->get();
        $duplicate_count = $duplicate->count();
        $total_count = $error_count + $success_count + $duplicate_count;
        $data = [
            'result_data' => $result_data,
            'error_count' => $error_count,
            'success_count' => $success_count,
            'duplicate_count' => $duplicate_count,
            'total_count' => $total_count,
            'search' => $search,
        ];
        return view('admin.product.price_updates.b2b_simple_imported', $data);
    }

    public function priceB2bComplex(Request $req)
    {
        $search = $req->search;
        $error_count  = $success_count = $duplicate_count = 0;
        $result_data = array();
        $pagination_count = 10;
        $query = TempB2cPrice::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
            });
        }
        $temp_data = $query->where('cron_flag', 'I')->paginate($pagination_count)->appends(request()->query());

        if (count($temp_data) > 0) {
            foreach ($temp_data as $row_data) {
                $sku_data = Product::where('sku', '=', $row_data->sku)->first();

                if (empty($sku_data)) {
                    $row_data->error_flag = 'Y';
                }
                if (!empty($row_data->type)) {
                    $container_type = (Config::get('constants.container_type'));
                    $key = array_search ($row_data->type, $container_type);
                   if($key == false){
                    $row_data->error_flag = 'Y'; 
                   }
                }

                if (!empty($row_data->price) && !empty($row_data->discount_price)) {
                    if ($row_data->price < $row_data->discount_price) {
                        $row_data->error_flag = 'Y';
                    }
                }
                if ($sku_data->product_type != 'complex') {
                    $row_data->error_flag = 'Y';
                }
                $skdata = Product::where('sku', '=', $row_data->sku)->first();
                if (!empty($skdata)) {
                    $varient_da = ProductB2BPrice::where('product_id', '=', $skdata->id)->get();
                    if (!empty($varient_da)) {
                        $val = "";
                        foreach ($varient_da as $varient_data) {

                            $varient = VariantLang::where('id', $varient_data->variant_id)->where('attribute_id', $varient_data->variant_attribute_id)->first();
                            if (!empty($varient)) {

                                if ((strtolower($varient->name) === strtolower($row_data->varient))) {
                                    $val = "exist";
                                    break;
                                }
                            }
                        }

                        if ($val != "exist") {

                            $row_data->error_flag = 'Y';
                        }
                    }
                } else {
                    $row_data->error_flag = 'Y';
                }

                if ($row_data->error_flag == 'Y' && $row_data->duplicate_flag == 'N') {
                    $error_count++;
                }
                if ($row_data->duplicate_flag == 'Y') {
                    $duplicate_count++;
                }

                if ($row_data->error_flag == 'N' && $row_data->duplicate_flag == 'N') {
                    $success_count++;
                }
                $result[] = $row_data;
            }
            $result_data = $result;
        }

        $total_count = $error_count + $success_count + $duplicate_count;
        $data = [
            'temp_data' => $temp_data,
            'result_data' => $result_data,
            'error_count' => $error_count,
            'duplicate_count' => $duplicate_count,
            'success_count' => $success_count,
            'total_count' => $total_count,
            'search' => $search
        ];


        return view('admin.product.price_updates.b2b_complex', $data);
    }

    public function b2bComplexErrors(Request $req)
    {

        $id = $req->id;
        $result = TempB2cPrice::where('id', $id)->first();

        $sku_data = Product::where('sku', '=', $result->sku)->first();

        $new_errors = '';
        if (!empty($result->sku)) {
            if (empty($sku_data)) {

                $new_errors .= "SKU not exist ,";
            }
        }
        if (!empty($result->type)) {
            $container_type = (Config::get('constants.container_type'));
            $key = array_search ($result->type, $container_type);
           if($key == false){
            $new_errors .= "Container type not exist,";
           }
        }
        if (!empty($result->price) && !empty($result->discount_price)) {
            if (($result->price) < ($result->discount_price)) {
                $new_errors .= "Discount price is greater than price,";
            }
        }
        if (!empty($sku_data)) {
            if ($sku_data->product_type != 'complex') {
                $new_errors .= "Imported product is not complex ,";
            }
            $varient_da = ProductB2BPrice::where('product_id', '=', $sku_data->id)->get();

            if (!empty($varient_da)) {

                $val = "";
                foreach ($varient_da as $varient_data) {
                    $varient = VariantLang::where('id', $varient_data->variant_id)->where('attribute_id', $varient_data->variant_attribute_id)->first();
                    if (!empty($varient)) {

                        if ((strtolower($varient->name) === strtolower($result->varient))) {
                            $val = "exist";
                            break;
                        }
                    }
                }

                if ($val != "exist") {

                    $new_errors .= "SKU and Variant mismatch,";
                }
            } else {
                $new_errors .= "SKU and Variant mismatch,";
            }
        }

        $all_errors = $result->errors . $new_errors;
        $errors = [];
        $errors = explode(",", $all_errors);

        $data = ['errors' => $errors];
        return view('admin.product.price_updates.error', $data);
    }

    public function confirmComplexB2b()
    {

        $result_data = array();
        $result_data = TempB2cPrice::where('cron_flag', 'I')->get();
        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'N') {
                    $sku_data = Product::where('sku', '=', $row_data->sku)->first();
                    if (empty($sku_data)) {
                        $row_data->error_flag = 'Y';
                    } else {
                        $varient_da = ProductB2BPrice::where('product_id', '=', $sku_data->id)->get();

                        if (!empty($varient_da)) {
                            $val = '';
                            foreach ($varient_da as $varient_data) {
                                $varient = VariantLang::where('id', $varient_data->variant_id)->where('attribute_id', $varient_data->variant_attribute_id)->first();
                                if (!empty($varient)) {

                                    if ((strtolower($varient->name) === strtolower($row_data->varient))) {
                                        $val = "exist";
                                        break;
                                    }
                                }
                            }

                            if ($val != "exist") {

                                $row_data->error_flag = 'Y';
                            } else {
                                $row_data->sku = $row_data->sku;
                            }
                        }
                    }
                    if (!empty($row_data->price) && !empty($row_data->discount_price)) {
                        if (($row_data->price) < ($row_data->discount_price)) {
                            $row_data->error_flag = 'Y';
                        }
                    }
                    if ($row_data->error_flag == 'N' ) {

                        $pdt_data = Product::where('sku', '=', $row_data->sku)->first();
                        $container_type = (Config::get('constants.container_type'));
                        $key = array_search ($row_data->type, $container_type);

                        $items = ProductB2BPrice::select('id')->where('product_id', $pdt_data->id)->where('quantity', $row_data->no_of_items)->where('container_type',$key)->where('variant_id', $varient->id)->get();
                        if(count($items) > 0){
                        $data_to_save = [
                            'price' => $row_data->price,
                            'discount_price' => $row_data->discount_price
                        ];
                        $saved_data = ProductB2BPrice::where('product_id', $pdt_data->id)->where('quantity', $row_data->no_of_items)->where('container_type',$key)->where('variant_id', $varient->id)->update($data_to_save);

                    }else{
                        $type = ProductB2BPrice::where('product_id', $pdt_data->id)->where('variant_id', $varient->id)->first();
                   
                        ProductB2BPrice::create([
                           'product_id' => $pdt_data->id,
                           'variant_attribute_id' => $type['variant_attribute_id'],
                           'variant_id' => $type['variant_id'],
                           'container_type' => $key,
                           'quantity' => $row_data->no_of_items,
                           'price' => $row_data->price,
                           'discount_price' => $row_data->discount_price
                       ]);
                    }
                    }
                }
                TempB2cPrice::where('id', $row_data->id)->update(['cron_flag' => 'S', 'error_flag' => $row_data->error_flag, 'duplicate_flag' => $row_data->duplicate_flag]);
            }
        }
        $check_temp_emp = TempB2cPrice::where('cron_flag', 'I')->first();
        if (!$check_temp_emp) {
            TempB2cPrice::where('cron_flag', 'S')->update(['cron_flag' => 'C']);
        }


        return response()->json(['message' => 'Thank you! Successfully updated price list', 'status' => '1']);
    }

    public function importedComplexB2b(Request $req)
    {
        $search = $req->search;
        $error_count = $success_count = $duplicate_count = 0;
        $pagination_count = 10;
        $result_data = array();
        $query = TempB2cPrice::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
            });
        }
        $result_data = $query->where('cron_flag', 'C')
            ->paginate($pagination_count)->appends(request()->query());

        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'Y' && $row_data->duplicate_flag == 'N') {
                    $error_count++;
                }else if ($row_data->duplicate_flag == 'Y'){
                    $duplicate_count++;
                }
                 else {
                    $success_count++;
                }
            }
        }
        $duplicate = TempB2cPrice::where('duplicate_flag', 'Y')->get();
        $duplicate_count = $duplicate->count();
        $total_count = $error_count + $success_count + $duplicate_count;
        $data = [
            'result_data' => $result_data,
            'error_count' => $error_count,
            'success_count' => $success_count,
            'duplicate_count' => $duplicate_count,
            'total_count' => $total_count,
            'search' => $search,
        ];


        return view('admin.product.price_updates.b2b_complex_imported', $data);
    }
}
