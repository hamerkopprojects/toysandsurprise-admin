<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Age;
use App\Models\Brand;
use App\Models\Gender;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Traits\FormatLanguage;
use App\Http\Controllers\Controller;

class BrandController extends Controller
{   
     const pagination = 6;
    use FormatLanguage;

    // Brands
    public function index(Request $req)
    {
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        $brands = Brand::with('lang')->where(['status' => 'active'])->get();

        return view('frontend.brand', compact('lang', 'brands'));
    }

    public function search(Request $req)
    {
       
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        
        $brand_slug = request()->segment(2);

        $query = Product::with(['lang' => function ($query) use ($lang) {
            $query->where('language', $lang);
        }]);
        $query->with('produc_images')->with(['b2cprice' => function ($query) { }])->with(['product_attribute' => function ($query) use ($lang) {
            $query->with(['lang' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }])->with(['attribute' => function ($query) use ($lang) {
                $query->with(['lang' => function ($query) use ($lang) {
                    $query->where('language', $lang);
                }]);
            }])->with(['variant' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }]);
        }])->with('category')->with('b2bprice');
        $query->whereHas('brand', function ($query) use ($lang, $brand_slug) {
            $query->with(['lang' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }])->where('slug', $brand_slug);
        });
        $query->with(['brand' => function ($query) use ($lang,$brand_slug) {
            $query->with(['lang' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }])->where('slug', $brand_slug);
        }]);

        $product_data = $query->where('status', 'active')
        ->where('deleted_at', null)
        ->orderBy('id', 'desc')
        ->paginate(static::pagination);

        $brand_id = Brand::select('id')->where('slug', $brand_slug)->first();
       
        $brand_ids[] = $brand_id->id;
      
        $cat_id=$brand_id->id;
        $categories = Category::with('lang')->where(['status' => 'active'])->get();
        $brands = Brand::with('lang')->where(['status' => 'active'])->get();
        $ages = Age::with('lang')->where(['status' => 'active'])->get();
        $genders = Gender::with('lang')->where(['status' => 'active'])->get();

        return view('frontend.productlist', compact('lang', 'categories','product_data','brands', 'ages', 'genders','brand_ids','cat_id'));
    }

}
