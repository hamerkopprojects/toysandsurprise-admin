<?php

namespace App\Http\Controllers\Frontend;

use App\Slider;
use App\Models\Brand;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\PromotionalProduct;
use App\Http\Controllers\Controller;
use App\Models\RecentlyViewedProducts;
use Illuminate\Support\Facades\Cookie;

class HomeController extends Controller
{
    protected $promotionalProducts;
    protected $product;

    public function __construct(PromotionalProduct $promotionalProducts, Product $product)
    {
        $this->promotionalProducts = $promotionalProducts;
        $this->product = $product;
    }
    public function index(Request $req, $lang = 'en')
    {

        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';

        // Shop by brand

        $shop_by_brand = Brand::with('lang')->where(['status' => 'active'])->limit(7)->get();

        $shop_by_brand_count = Brand::where(['status' => 'active'])->count();

        // end
        $slider = Slider::with('slider_category')->with('slider_product')->get();


        // promotional products

        $promotionalProducts = $this->promotionalProducts
            ->with(['product' => function ($query) use ($lang) {
                $query->with(['lang' => function ($query) use ($lang) { }])
                    ->with('b2cprice');
            }])
            ->take(8)->get();
        $promo_count = $this->promotionalProducts->with('product')->count();

        // end

        // newly in products

        $product = $this->product->with(['lang' => function ($query) use ($lang) {
            $query->where('language', $lang);
        }])->with(['product_tag' => function ($query) {
            $query->where('tag_id', 1);
        }])
            ->with('b2cprice')->take(3)->get();

        $product_count = $this->product->count();

        // end


        // recently viewed products

        // $cookie_id = Cookie::get('emtyaz_session');
        // $cookie_id = $cookie_id ? $cookie_id : '';
        $recently_viewed_products = RecentlyViewedProducts::with(['product' => function ($query) use ($lang) {
            $query->with(['lang' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }])
                ->with('b2cprice')
                ->with(['product_tag' => function ($query) {
                    $query->with(['tag' => function ($query)  {
                      
                    }]);
                }]);
        }])
            // ->where('product_id', '!=', $product->id)
            // ->where('browser_cookie_id', '=', $cookie_id)
            ->groupBy('product_id')->orderBy('created_at', 'desc')->limit(4)->get();
        //                                print_r($recently_viewed_products->toArray());
        //                                exit;
        //end

// dd($recently_viewed_products);

        return view('frontend/index', compact('shop_by_brand', 'shop_by_brand_count', 'slider', 'promo_count', 'promotionalProducts', 'product', 'product_count', 'lang', 'recently_viewed_products'));
    }
}
