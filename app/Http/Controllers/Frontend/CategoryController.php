<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Age;
use App\Models\Tag;
use App\Models\Brand;
use App\Models\Gender;
use App\Models\AgeLang;
use App\Models\Product;
use App\Models\Category;
use App\Models\GenderLang;
use App\Models\CategoryLang;
use Illuminate\Http\Request;
use App\Traits\FormatLanguage;
use App\Http\Controllers\Controller;
use App\Models\TagLang;

class CategoryController extends Controller
{
    const pagination = 12;
    use FormatLanguage;

    // Category
    public function index(Request $req)
    {
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        $categories = Category::with('lang')->where(['status' => 'active'])->get();

        return view('frontend.category', compact('lang', 'categories'));
    }

    public function category(Request $req)
    {
        $category_id[] = "";
        $filter_cat_ids = $filter_brand_ids = "";
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';

        $slug = request()->segment(2);
        $type = request()->segment(1);

      

        if ($type == 'brand') {
            $slug_name = Brand::with('lang')->where(['slug' => $slug])->first();
            $brand_ids[] = $slug_name->id;
            $category_id = array();
            $tag_id = "";
            $cat_id ="";
        }
        if ($type == 'category') {
            $slug_name = Category::with('lang')->where(['slug' => $slug])->first();
            $category_id[] = $slug_name->id;
            $brand_ids = array();
            $cat_id = $slug_name->id;
            $tag_id = "";
            $slug_name ="";
        }
        if ($type == 'limited-edition') {
            $cat_id ="";    
            $tag =  TagLang::where('name', "Limited Edition")->first();
            $tag_id = $tag->tag_id;
            $brand_ids = array();
            $slug_name ="";
        }
        if ($type == 'offers') {
            $cat_id ="";
            $tag =  TagLang::where('name', "Offer")->first();
            $tag_id = $tag->tag_id;
            $brand_ids = array();
            $slug_name ="";
        }
        if ($type == 'newlyin') {
            $cat_id ="";
            $tag =  TagLang::where('name', "Offer")->first();
            $tag_id = $tag->tag_id;
            $brand_ids = array();
            $slug_name ="";
        }
        
    
        $query = Product::with(['lang' => function ($query) use ($lang) {
            $query->where('language', $lang);
        }]);
        if ($type == 'promo-product') {
            $query->whereHas('product_promotional', function ($query) { });
            $query->with(['product_promotional' => function ($query) { }]);
        }

        if ($type == 'limited-edition') {
            $query->whereHas('product_tag', function ($query) use ($tag_id) {
                $query->where('tag_id', $tag_id);
            });
            $query->with(['product_tag' => function ($query) use ($tag_id) {
                $query->where('tag_id', $tag_id);
            }]);
        }

        if ($type == 'offers') {
            $query->whereHas('product_tag', function ($query) use ($tag_id) {
                $query->where('tag_id', $tag_id);
            });
            $query->with(['product_tag' => function ($query) use ($tag_id) {
                $query->where('tag_id', $tag_id);
            }]);
        }
        $query->with('produc_images')->with(['b2cprice' => function ($query) { }])->with(['product_attribute' => function ($query) use ($lang) {
            $query->with(['lang' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }])->with(['attribute' => function ($query) use ($lang) {
                $query->with(['lang' => function ($query) use ($lang) {
                    $query->where('language', $lang);
                }]);
            }])->with(['variant' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }]);
        }]);

        $query->whereHas('category', function ($query) use ($lang, $slug, $type) {
            $query->with(['lang' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }]);
            if ($type == "category") {
                $query->where('slug', $slug);
            }
        });

        $query->with(['category' => function ($query) use ($lang, $slug, $type) {
            $query->with(['lang' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }]);
            if ($type == "category") {
                $query->where('slug', $slug);
            }
        }]);

      

        $query->whereHas('brand', function ($query) use ($lang, $slug, $type) {
            $query->with(['lang' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }]);
            if ($type == "brand") {
                $query->where('slug', $slug);
            }
        });

        $query->with(['brand' => function ($query) use ($lang, $slug, $type) {
            $query->with(['lang' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }]);
            if ($type == "brand") {
                $query->where('slug', $slug);
            }
        }]);

        // if(!($category_id)){
            if($cat_id !=""){
            
            $query->orWhereHas('product_category', function ($query) use ($cat_id) {
        
                    $query->where('category_id', $cat_id);
                
            });
            $query->with(['product_category' => function ($query) use ($cat_id) {
    
                    $query->where('category_id', $cat_id);
            }]);
            }

        $product_data = $query->with('b2bprice')->with('brand')->where('status', 'active')
            ->where('deleted_at', null)
            ->orderBy('id', 'desc')
            ->paginate(static::pagination);
        

        if ($type == "brand") {
            $brand_id = Brand::select('id')->where('slug', $slug)->first();

            $brand_ids[] = $brand_id->id;
            $cat_id = $brand_id->id;
            if (!empty($brand_id)) {
                $filter_brand_ids = implode(',', $brand_ids);
            }
        }

        if ($type == "category") {
            $category_ids = Category::select('id')->where('slug', $slug)->first();
            $category_id[] = $category_ids->id;
            $cat_id = $category_ids->id;
            if (!empty($category_id)) {
                $filter_cat_ids = implode(',', $category_id);
            }
        }

     
        $categories = Category::with('lang')->where(['status' => 'active'])->get();
        $brands = Brand::with('lang')->where(['status' => 'active'])->get();
        $ages = Age::with('lang')->where(['status' => 'active'])->get();
        $genders = Gender::with('lang')->where(['status' => 'active'])->get();
        $search = 'filter';

        return view('frontend.productlist', compact('lang', 'categories', 'product_data', 'brands', 'ages', 'genders', 'category_id', 'brand_ids', 'type', 'slug_name', 'filter_brand_ids', 'filter_cat_ids'));
    }
}
