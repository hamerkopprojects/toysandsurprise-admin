<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Product;
use App\Models\Customer;
use App\Models\WishList;
use Illuminate\Http\Request;
use App\Traits\FormatLanguage;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use App\Events\CustomerAuthenticated;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class Common extends Controller
{
    use FormatLanguage;

    public function index(Request $req)
    {
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        return view('frontend.account', compact('lang'));
    }
    public function register(Request $req)
    {
        if ($req->_customer_id) {
            $unique = ',' . $req->_customer_id;
        } else {
            $unique = '';
        }
        request()->validate([
            'email' => 'required|email|unique:customer,email' . $unique,
            'password' => 'required|min:6',
            'confirm_password' => 'required|min:6|max:20|same:password',
            'phone_no' => 'required|unique:customer,phone' . $unique,
        ], [
            'email.required' => 'Email is required.',
            'email.unique' => 'Customer with same email already exists',
            'password.required' => 'Password is required.',
            'password.min' => 'Password minimum of 6 characters is required.',
            'phone_no.required' => 'Phone number is required.',
            'phone_no.unique' => 'Customer with same Phone number already exists',
        ]);
      
         $data_to_save = [
            'cust_name' => $req->name,
            'email' => $req->email,
            'password' => md5($req->password),
            'phone' => (int)$req->phone_no,
            'otp' => $this->generateOtp()
        ];

        if ($req->_customer_id == '') {

            $cust_id = $this->generateCustomerId();
            $data_to_save['cust_id'] = $cust_id;
            $data_to_save['created_at'] = Carbon::now();
            $saved_data = Customer::insertGetId($data_to_save);
           
            Session::put('cust_to_verify', $saved_data);
            return redirect()->route('customer.index')->with('success', 'Customer registered successfully');

        } else {

            $saved_data = Customer::where('id', $req->_customer_id)
                    ->update($data_to_save);
            return redirect()->route('customer.index')->with('success', 'Profile Updated Successfully');

        }
   
    }
    private function generateCustomerId() {
        $lastcustomer = Customer::select('cust_id')
                ->orderBy('id', 'desc')
                ->first();

        $lastId = 0;

        if ($lastcustomer) {
            $lastId = (int) substr($lastcustomer->cust_id, 10);
        }

        $lastId++;

        return 'TOYS-CUST-' . str_pad($lastId, 5, '0', STR_PAD_LEFT);
    }
    private function generateOtp() {
        $otp = mt_rand(1111, 9999);
        return 1234;
    }

    public function login(Request $req) {
        
        $username = $req->user_name;
        $password = $req->password;

        if ($password == NULL || $password == '' || $username == '' || $username == NULL) {

            return redirect()->route('customer.register')->with('error', 'Username and password required');
        }

        $customer = Customer::where('email', $username)->where('password', md5($password))->first();
      
        if ($customer) {
           
            event(new CustomerAuthenticated($customer->id, $customer->cust_name));
            
           $customer_name =  "anu";
           $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
            return view('frontend.account', compact('customer_name','lang'))->with('success', 'Logged in successfully');
        } else {
            
            return redirect()->route('customer.index')->with('error', 'Invalid username or password');
           
        }
    }

    public function add_to_favourite(Request $request) {
       
        $product_id = $request->product_id;
       
        $customer = Session::get('customer_id');
     

        if (!$product_id || !$customer) {

            return response()->json(['success' => 0, 'message' => 'Please login for wishlisting a product']);
        }

        $product = Product::where('id', $product_id)->first();
        if (!$product) {
            return response()->json(['success' => 0, 'message' => 'Product not found.']);
        }

        $wishList = WishList::where('customer_id', $customer)->where('product_id', $product_id)->first();
       
        if ($wishList) {
            $wishList->delete();
            $message = 'Product removed from wish list';
        } else {
            $wishList = new WishList();
            $wishList->customer_id = $customer;
            $wishList->product_id = $product_id;
            $wishList->save();
            $message = 'Product added to wish list';
        }

        return response()->json(['success' => 1, 'message' => $message]);
    }
}
