<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Age;
use App\Models\Brand;
use App\Models\Gender;
use App\Models\Product;
use App\Models\Category;
use App\Models\ProductTag;
use Illuminate\Http\Request;
use App\Traits\FormatLanguage;
use Illuminate\Support\Carbon;
use App\Models\ProductViewCount;
use App\Models\ProductCategories;
use App\Models\PromotionalProduct;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use App\Models\RecentlyViewedProducts;
use Illuminate\Support\Facades\Cookie;
use App\Helpers\General\CollectionHelper;

class ProductController extends Controller
{
    use FormatLanguage;
    protected $promotionalProducts;
    protected $product;
    public function __construct(PromotionalProduct $promotionalProducts, Product $product)
    {
        $this->promotionalProducts = $promotionalProducts;
        $this->product = $product;
    }
    public function index(Request $req)
    {
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        $slug = request()->segment(2);

        $product_data = Product::with(['lang' => function ($query) use ($lang) {
            $query->where('language', $lang);
        }])->with('produc_images')->with(['b2cprice' => function ($query) { }])->with(['product_attribute' => function ($query) use ($lang) {
            $query->with(['lang' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }])->with(['attribute' => function ($query) use ($lang) {
                $query->with(['lang' => function ($query) use ($lang) {
                    $query->where('language', $lang);
                }]);
            }])->with(['variant' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }]);
        }])->with('b2bprice')->with('brand')->where('slug', $slug)->first();

        $product_tags = ProductTag::with('tag')->where('product_id', $product_data->id)->get();
        
        //add products to recently viewed table
        $cookie_id = Cookie::get('emtyaz_session');
        $cookie_id = $cookie_id ? $cookie_id : '';
        $most_viewed_products_count = ProductViewCount::select('view_count')->where('product_id', $product_data->id)->first();
        if (isset($most_viewed_products_count) && $most_viewed_products_count->view_count > 0) {
            $view_count = $most_viewed_products_count->view_count + 1;
            ProductViewCount::where('product_id', $product_data->id)
                ->update([
                    "view_count" => $view_count
                ]);
        } else {
            ProductViewCount::create([
                'product_id' => $product_data->id,
                'view_count' => 1,
                'created_at' => Carbon::now()
            ]);
        }
        RecentlyViewedProducts::create([
            'browser_cookie_id' => $cookie_id,
            'product_id' => $product_data->id,
            'created_at' => Carbon::now()
        ]);

        //end
        //similar product
        $category = explode(",", str_replace(
            array("{{", "}}"),
            array(" ", " ,"),
            $product_data->category_ids
        ));

        $list_id = ProductCategories::whereIn('category_id', $category)->select('product_id')->get();
        foreach ($list_id as $item) {
            $tmpArray[] = $item->product_id;
        }
        $similarProducts = Product::with(['lang' => function ($query) use ($lang) {
            $query->where('language', $lang);
        }])
            ->with('b2cprice')

            ->WhereIn('id', array_unique($tmpArray))
            ->where('id', '!=', $product_data->id)
            ->inRandomOrder()
            ->limit(4)
            ->get();

        return view('frontend.product', compact('lang', 'product_data', 'product_tags', 'similarProducts'));
    }
    public function filter(Request $req)
    {

        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        $category_id = $req->category_id;
        $brand_id = $req->brand_id;
        $age_id = $req->age_id;
        $gender_id = $req->gender_id;
        $min_price = $req->amount1;
        $max_price = $req->amount2;
        

        if (!empty($req->sort)) {
          
            if (!empty($category_id)) {
                $category_id = explode(',', $category_id);
            }
            if (!empty($brand_id)) {
                $brand_id = explode(',', $brand_id);
            }
            if (!empty($age_id)) {
                $age_id = explode(',', $age_id);
            }
        } else {


            $filter_cat_ids = $filter_brand_ids = $filter_age_ids = '';

            $filter_gender_ids = '';

            if (!empty($category_id)) {
                $filter_cat_ids = implode(',', $category_id);
            }

            if (!empty($brand_id)) {
                $filter_brand_ids = implode(',', $brand_id);
            }

            if (!empty($age_id)) {
                $filter_age_ids = implode(',', $age_id);
            }
            
            if (!empty($gender_id)) {
                $filter_gender_ids = $gender_id;
            }
        }

        $query = Product::with(['lang' => function ($query) use ($lang) {
            $query->where('language', $lang);
        }]);
        $query->with('produc_images');
        $query->whereHas('b2cprice', function ($query) use ($min_price, $max_price) {

            if (!empty($min_price) && !empty($max_price)) {
                $query->whereBetween('price', [$min_price, $max_price]);
            }
        });
        $query->with(['b2cprice' => function ($query) use ($min_price, $max_price) {
            if (!empty($min_price) && !empty($max_price)) {
                $query->whereBetween('price', [$min_price, $max_price]);
            }
        }]);
        $query->with(['product_attribute' => function ($query) use ($lang) {
            $query->with(['lang' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }])->with(['attribute' => function ($query) use ($lang) {
                $query->with(['lang' => function ($query) use ($lang) {
                    $query->where('language', $lang);
                }]);
            }])->with(['variant' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }]);
        }]);

        $query->whereHas('category', function ($query) use ($lang, $category_id) {
            $query->with(['lang' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }]);
            if (!empty($category_id)) {
                $query->whereIn('id', $category_id);
            }
        });
        $query->with(['category' => function ($query) use ($lang, $category_id) {
            $query->with(['lang' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }]);
            if (!empty($category_id)) {
                $query->whereIn('id', $category_id);
            }
        }]);

        $query->with('b2bprice');
       
        $query->whereHas('brand', function ($query) use ($lang, $brand_id) {
            $query->with(['lang' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }]);
            if (!empty($brand_id)) {
                $query->whereIn('id', $brand_id);
            }
        });
   
        $query->with(['brand' => function ($query) use ($lang, $brand_id) {
            $query->with(['lang' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }]);
            if (!empty($brand_id)) {
                $query->whereIn('id', $brand_id);
            }
        }]);
        $query->whereHas('product_age', function ($query) use ($age_id) {

            if (!empty($age_id)) {
                $query->whereIn('age_id', $age_id);
            }
        });
        $query->with(['product_age' => function ($query) use ($age_id) {

            if (!empty($age_id)) {
                $query->whereIn('age_id', $age_id);
            }
        }]);

        $query->whereHas('product_gender', function ($query) use ($gender_id) {

            if (!empty($gender_id)) {
                $query->where('gender_id', $gender_id);
            }
        });
        $query->with(['product_gender' => function ($query) use ($gender_id) {

            if (!empty($gender_id)) {
                $query->where('gender_id', $gender_id);
            }
        }]);
      
            $query->orWhereHas('product_category', function ($query) use ($category_id) {
                if($category_id !=""){
                $query->whereIn('category_id', $category_id);
            }  
        });
          
            $query->with(['product_category' => function ($query) use ($category_id) {
    
                    $query->with('category');
                    if($category_id !=""){
                        $query->whereIn('category_id', $category_id);
                }
            }]);
        

        $query->where('status', 'active');
        $product = $query->where('deleted_at', null);

        if (!empty($req->sort)) {
            $product =  $product->get();

            if (($req->sort) == "low-high") {
                $products = $product->sortBy(function ($product, $key) {
                    return (int) $product['b2cprice'][0]['price'];
                });
            }
            if (($req->sort) == "high-low") {
                $products = $product->sortByDesc(function ($product, $key) {
                    return (int) $product['b2cprice'][0]['price'];
                });
            }
            $product_data = CollectionHelper::paginate($products, 12);
        } else {

            $product_data = $product->paginate(12);
        }

        if (!empty($req->sort)) {
            $filter_cat_ids = $filter_brand_ids = $filter_age_ids = '';

            $filter_gender_ids = '';
            if (!empty($category_id)) {
                $filter_cat_ids = $req->category_id;
            }
            if (!empty($brand_id)) {

                $filter_brand_ids = $req->brand_id;
            }
            if (!empty($age_id)) {
                $filter_age_ids = $req->age_id;
            }
            if (!empty($gender_id)) {
                $filter_gender_ids = $req->gender_id;
            }
        }
        $brand_ids = $brand_id;

        $cat_id = "";
        $categories = Category::with('lang')->where(['status' => 'active'])->get();
        $brands = Brand::with('lang')->where(['status' => 'active'])->get();
        $ages = Age::with('lang')->where(['status' => 'active'])->get();
        $genders = Gender::with('lang')->where(['status' => 'active'])->get();
        $search = 'filter';
        
     
        return view('frontend.productlist.filter', compact('lang', 'categories', 'product_data', 'brands', 'ages', 'genders', 'category_id', 'cat_id', 'brand_ids', 'age_id', 'search', 'gender_id', 'filter_cat_ids', 'filter_brand_ids', 'filter_age_ids', 'filter_gender_ids', 'min_price', 'max_price'));
    }
    public function loadMore(Request $req)
    {

        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';

        $category_id = $req->category_id;
        $brand_id = $req->brand_id;
        $age_id = $req->age_id;
        $gender_id = $req->gender_id;


        if (!empty($category_id)) {
            $category_id = explode(',', $category_id);
        }
        if (!empty($brand_id)) {
            $brand_id = explode(',', $brand_id);
        }
        if (!empty($age_id)) {
            $age_id = explode(',', $age_id);
        }
        if (!empty($gender_id)) {
            $gender_id = explode(',', $gender_id);
        }

        $query = Product::with(['lang' => function ($query) use ($lang) {
            $query->where('language', $lang);
        }]);
        $query->with('produc_images');
        $query->with(['b2cprice' => function ($query) { }]);
        $query->with(['product_attribute' => function ($query) use ($lang) {
            $query->with(['lang' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }])->with(['attribute' => function ($query) use ($lang) {
                $query->with(['lang' => function ($query) use ($lang) {
                    $query->where('language', $lang);
                }]);
            }])->with(['variant' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }]);
        }]);

        $query->whereHas('category', function ($query) use ($lang, $category_id) {
            $query->with(['lang' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }]);
            if (!empty($category_id)) {
                $query->whereIn('id', $category_id);
            }
        });
        $query->with(['category' => function ($query) use ($lang, $category_id) {
            $query->with(['lang' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }]);
            if (!empty($category_id)) {
                $query->whereIn('id', $category_id);
            }
        }]);

        $query->with('b2bprice');

        $query->whereHas('brand', function ($query) use ($lang, $brand_id) {
            $query->with(['lang' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }]);
            if (!empty($brand_id)) {
                $query->whereIn('id', $brand_id);
            }
        });
        $query->with(['brand' => function ($query) use ($lang, $brand_id) {
            $query->with(['lang' => function ($query) use ($lang) {
                $query->where('language', $lang);
            }]);
            if (!empty($brand_id)) {
                $query->whereIn('id', $brand_id);
            }
        }]);
        if($category_id !=""){
            $query->orWhereHas('product_category', function ($query) use ($category_id) {
        
                $query->where('category_id', $category_id);
            
        });
          
            $query->with(['product_category' => function ($query) use ($category_id) {
    
                    $query->with('category')->whereIn('category_id', $category_id);
            }]);
        }

        $query->whereHas('product_age', function ($query) use ($age_id) {

            if (!empty($age_id)) {
                $query->whereIn('age_id', $age_id);
            }
        });
        $query->with(['product_age' => function ($query) use ($age_id) {

            if (!empty($age_id)) {
                $query->whereIn('age_id', $age_id);
            }
        }]);

        $query->whereHas('product_gender', function ($query) use ($gender_id) {

            if (!empty($gender_id)) {
                $query->where('gender_id', $gender_id);
            }
        });
        $query->with(['product_gender' => function ($query) use ($gender_id) {

            if (!empty($gender_id)) {
                $query->where('gender_id', $gender_id);
            }
        }]);

        $query->where('status', 'active');
        $product_data = $query->where('deleted_at', null)
            ->orderBy('id', 'desc')
            ->paginate(12);

        $page = $req->page;


        return view('frontend.productlist.loadmore', compact('lang', 'product_data', 'page'));
    }

    public function promoProduct(Request $req)
    {
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';

      
    $promo_count = $this->promotionalProducts->with('product')->count();

    $categories = Category::with('lang')->where(['status' => 'active'])->get();
    $brands = Brand::with('lang')->where(['status' => 'active'])->get();
    $ages = Age::with('lang')->where(['status' => 'active'])->get();
    $genders = Gender::with('lang')->where(['status' => 'active'])->get();
    $search = 'filter';
    $type = "promotional product";
   

    return view('frontend.productlist', compact('lang', 'categories', 'product_data', 'brands', 'ages', 'genders', 'category_id', 'cat_id','brand_ids','type','slug_name','filter_brand_ids','filter_cat_ids'));
    }
}
