$(window).on("load", function () {

	$(".same-height , .products .item , .carousel .item").matchHeight();

	$.fn.slideFadeToggle = function (speed, easing, callback) {
		return this.animate({
			opacity: 'toggle',
			height: 'toggle'
		}, speed, easing, callback);
	};

	$(".menu-btn").click(function () {
		$(this).toggleClass('active');
		$("body").toggleClass('overflow-hidden');
		$('#main-navigation').toggleClass('open');
		$("#main-navigation ul").toggle();
		return false;
	});

	setTimeout(function () {

		$(".carousel").owlCarousel({
			loop: false,
			margin: 30,
			nav: true,
			autoplay: true,
			navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
			responsive: {
				0: {
					items: 1
				},
				768: {
					items: 2
				},
				992: {
					items: 3
				},
				1199: {
					items: 4
				}
			}
		});

		

	}, 500)


	$("#back-to-top").click(function (event) {
		event.preventDefault();
		$("html, body").animate({
			scrollTop: 0
		}, "slow");
		return false;
	});

	$(window).on("scroll", function () {
		var height = $(window).scrollTop();
		if (height > 100) {
			$('#back-to-top').fadeIn();
		} else {
			$('#back-to-top').fadeOut();
		}
	});

	

	$(".faq h3").on("click", function () {
		var $this = $(this);
		if ($(this).hasClass("active")) {
			$this.next().slideFadeToggle("300", function () {
				$this.removeClass("active");
			});
		} else {
			$this.addClass("active");
			$this.next().slideFadeToggle("300");
		}

		return false;
	});

	$("#myTab li:nth-child(1)").addClass("parent").find("a").addClass("active");
	$("#myTab li:nth-child(2)").addClass("next");

	$(document).on('show.bs.tab', '.nav-tabs-responsive [data-toggle="tab"]', function (e) {
		var $target = $(e.target);
		var $tabs = $target.closest('.nav-tabs-responsive');
		var $current = $target.closest('li');
		var $next = $current.next();
		var $prev = $current.prev();
		var updateDropdownMenu = function ($el, position) {
			$el
				.find('>li')
				.removeClass('pull-xs-left pull-xs-center pull-xs-right')
				.addClass('pull-xs-' + position);
		};

		$tabs.find('>li').removeClass('next prev parent');
		$prev.addClass('prev');
		$next.addClass('next');
		$current.addClass("parent");

		updateDropdownMenu($prev, 'left');
		updateDropdownMenu($current, 'center');
		updateDropdownMenu($next, 'right');
	});

	$('.slider-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.slider-nav'
	});

	$('.slider-nav').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		asNavFor: '.slider-for',
		dots: false,
		prevArrow: '<button class="slide-arrow prev-arrow"><i class="fa fa-chevron-up"></i></button>',
		nextArrow: '<button class="slide-arrow next-arrow"><i class="fa fa-chevron-down"></i></button>',
		centerMode: false,
		vertical: true,
		focusOnSelect: true,
		responsive: [{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					vertical: true
				}
			},
			{
				breakpoint: 767,
				settings: {
					vertical: false,
					slidesToShow: 1,
					centerMode: true
				}
			}
		]
	});
	
	$('[data-toggle="tooltip"]').tooltip()

	objectFitImages();

});