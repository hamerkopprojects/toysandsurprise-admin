<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Frontend\Common;
use App\Http\Controllers\Admin\FaqsController;
use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\PagesController;
use App\Http\Controllers\admin\ProductController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\admin\AttributeController;
use App\Http\Controllers\Admin\DashBoardController;
use App\Http\Controllers\UserVerificationController;
use App\Http\Controllers\Admin\HowtoUseAppController;
use App\Http\Controllers\Admin\JobCategoryController;
use App\Http\Controllers\Admin\PriceUpdateController;
use App\Http\Controllers\Admin\StockUpdateController;
use App\Http\Controllers\admin\ImportProductController;
use App\Http\Controllers\Admin\JobManagementController;
use App\Http\Controllers\Admin\OtherSettingsController;
use App\Http\Controllers\Admin\RatingSegmentsController;
use App\Http\Controllers\Admin\ShopManagementController;
use App\Http\Controllers\Admin\UserManagementController;
use App\Http\Controllers\Admin\SocialMediaLinkController;
use App\Http\Controllers\Admin\CancellationReasonController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('setlocale/{locale}', function ($locale) {
 
    if (in_array($locale, \Config::get('app.locales'))) {
      session(['locale' => $locale]);
    }
   
    return redirect()->back();
  });

Route::get('/', 'Frontend\HomeController@index')->name('frontend');
Route::get('/products/{slug}', 'Frontend\ProductController@index')->name('products.single');
Route::get('/category', 'Frontend\CategoryController@index')->name('frontend.category');
Route::get('/category/{slug}', 'Frontend\CategoryController@category')->name('frontend.category.search');
Route::post('/product/loadMore', 'Frontend\ProductController@loadMore')->name('product.loadMore');
Route::post('/product/filter', 'Frontend\ProductController@filter')->name('product.filter');
Route::get('/brand', 'Frontend\BrandController@index')->name('frontend.brand');
Route::get('/brand/{slug}', 'Frontend\CategoryController@category')->name('frontend.brand.search');
Route::get('/promo-product', 'Frontend\CategoryController@category')->name('product.promo-product');
Route::get('/limited-edition', 'Frontend\CategoryController@category')->name('product.limited-edition');
Route::get('/offers', 'Frontend\CategoryController@category')->name('product.offers');
Route::get('/newlyin', 'Frontend\CategoryController@category')->name('product.newlyin');
Route::get('/recentlyviewed', 'Frontend\CategoryController@category')->name('product.recentlyviewed');

Route::get('/customer/create', 'Frontend\Common@index')->name('customer.index');
Route::post('/customer/register', 'Frontend\Common@register')->name('customer.register');
Route::post('/customer/login', 'Frontend\Common@login')->name('customer.login');


Route::post('/product/favourite', 'Frontend\Common@add_to_favourite')->name('product.favourite');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix' => 'admin', 'middleware' => 'auth:web'], function () {
Route::get("/dashboard", [DashBoardController::class, 'get'])->name('dashboard.get');

    // Faqs
    Route::get("/faqs", [FaqsController::class, 'get'])->name('faq.get');
    Route::get("/faq/edit/{id}", [FaqsController::class, 'edit'])->name('faq.edit');
    Route::post("/faqs/update", [FaqsController::class, 'update'])->name('faq.update');
    Route::post("/faqs/store", [FaqsController::class, 'store'])->name('faq.store');
    Route::post("/faqs/status-update", [FaqsController::class, 'statusUpdate'])->name('faq.status.update');
    Route::post("/faqs/delete", [FaqsController::class, 'destroy'])->name('faq.delete');
    Route::post("/faqs/search", [FaqsController::class, 'search'])->name('faq.search');

    // Pages

    Route::get("/pages", [PagesController::class, 'get'])->name('pages.get');
    Route::get("/pages/edit/{id}", [PagesController::class, 'edit'])->name('pages.edit');
    Route::post("/pages/update", [PagesController::class, 'update'])->name('pages.update');

    // How to use app

    Route::get("/howto-use", [HowtoUseAppController::class, 'get'])->name('use.get');
    Route::get("/howto-use/edit/{id}", [HowtoUseAppController::class, 'edit'])->name('use.edit');
    Route::post("/howto-use/update", [HowtoUseAppController::class, 'update'])->name('use.update');
    Route::post("/howto-use/file/delete", [HowtoUseAppController::class, 'removeFile'])->name('use.image.delete');
    Route::post("/howto-use/file/delete", [HowtoUseAppController::class, 'removeFile'])->name('use.image.delete');
    Route::post("/howto-use/upload/file/{id}", [HowtoUseAppController::class, 'uploadFile'])->name('use.image.upload');

    Route::get("/segments", [RatingSegmentsController::class, 'get'])->name('segments.get');
    Route::get("/segments/edit/{id}", [RatingSegmentsController::class, 'edit'])->name('segments.edit');
    Route::post("/segments/update", [RatingSegmentsController::class, 'update'])->name('segments.update');
    Route::post("/segments/status", [RatingSegmentsController::class, 'statusUpdate'])->name('segments.status');

    Route::get("/cancel-reasons", [CancellationReasonController::class, 'get'])->name('cancel.get');
    Route::post("/cancel-reasons/store", [CancellationReasonController::class, 'store'])->name('cancel.store');
    Route::get("/cancel-reasons/edit/{id}", [CancellationReasonController::class, 'edit'])->name('cancel.edit');
    Route::post("/cancel-reasons/update", [CancellationReasonController::class, 'update'])->name('cancel.update');
    Route::post("/cancel-reasons/delete", [CancellationReasonController::class, 'destroy'])->name('cancel.delete');
    Route::post("/cancel-reasons/auto-complete", [CancellationReasonController::class, 'autoComplete'])->name('cancel.auto');

    Route::get("/other-settings", [OtherSettingsController::class, 'get'])->name('other.get');
    Route::post("/other-settings/store", [OtherSettingsController::class, 'store'])->name('other.store');

     // Ads
     Route::get('/ads', 'Admin\AdsController@index')->name('ads');
     Route::post('ads/upload_image', 'Admin\AdsController@upload_image')->name('upload_image');
     Route::post('ads/detete_img', 'Admin\AdsController@detete_img')->name('detete_img');
     Route::get('/ads/link_category', 'Admin\AdsController@link_category')->name('link_category');
     Route::post('/ads/add_link', 'Admin\AdsController@add_link')->name('add_link');
     Route::get('/ads/tabs', 'Admin\AdsController@get_tabs')->name('ads_tab');
     Route::get('/ads/slider', 'Admin\AdsController@slider')->name('slider');
     Route::post('ads/slider_image', 'Admin\AdsController@upload_slider_image')->name('slider_image');
     Route::get('/ads/slider_category', 'Admin\AdsController@slider_category')->name('slider_category');
     Route::post('/ads/add_slider_category', 'Admin\AdsController@add_slider_category')->name('add_slider_category');
     Route::post('ads/detete_slider_img', 'Admin\AdsController@detete_slider_img')->name('detete_slider_img');
     Route::post('/ads/add_slider_data', 'Admin\AdsController@add_slider_data')->name('add_slider_data');

    Route::get("/user", [UserManagementController::class, 'get'])->name('user-list.get');
    Route::post("/user/store", [UserManagementController::class, 'store'])->name('user-list.store');
    Route::get("/user-list/edit/{id}", [UserManagementController::class, 'edit'])->name('user-list.edit');
    Route::post("/user-list/update", [UserManagementController::class, 'update'])->name('user-list.update');
    Route::post("/user-list/status/update", [UserManagementController::class, 'statusUpdate'])->name('user-list.status.update');
    Route::post("/user-list/delete", [UserManagementController::class, 'destroy'])->name('user-list.delete');
    Route::post("/user-list/sendPassword", [UserManagementController::class, 'passwordSend'])->name('user-list.setPassword');

    // Job Category
    Route::get("/job-category", [JobCategoryController::class, 'index'])->name('job.category');
    Route::get("/job-category/edit/{id}", [JobCategoryController::class, 'edit'])->name('job.category.edit');
    Route::post("/job-category/update", [JobCategoryController::class, 'update'])->name('job.category.update');
    Route::post("/job-category/store", [JobCategoryController::class, 'store'])->name('job.category.store');
    Route::post("/job-category/status-update", [JobCategoryController::class, 'statusUpdate'])->name('job.category.status.update');
    Route::post("/job-category/delete", [JobCategoryController::class, 'destroy'])->name('job.category.delete');
    Route::post("/job-category/search", [JobCategoryController::class, 'search'])->name('job.category.search');

    // Job Management
    Route::get("/job-management", [JobManagementController::class, 'index'])->name('job.management');
    Route::get("/job-management/edit/{id}", [JobManagementController::class, 'edit'])->name('job.management.edit');
    Route::post("/job-management/update", [JobManagementController::class, 'update'])->name('job.management.update');
    Route::post("/job-management/store", [JobManagementController::class, 'store'])->name('job.management.store');
    Route::post("/job-management/status-update", [JobManagementController::class, 'statusUpdate'])->name('job.management.status.update');
    Route::post("/job-management/delete", [JobManagementController::class, 'destroy'])->name('job.management.delete');
    Route::post("/job-management/search", [JobManagementController::class, 'search'])->name('job.management.search');

    // Shop Management
    Route::get("/shop-management", [ShopManagementController::class, 'index'])->name('shop.management');
    Route::get("/shop-management/edit/{id}", [ShopManagementController::class, 'edit'])->name('shop.management.edit');
    Route::post("/shop-management/update", [ShopManagementController::class, 'update'])->name('shop.management.update');
    Route::post("/shop-management/store", [ShopManagementController::class, 'store'])->name('shop.management.store');
    Route::post("/shop-management/status-update", [ShopManagementController::class, 'statusUpdate'])->name('shop.management.status.update');
    Route::post("/shop-management/delete", [ShopManagementController::class, 'destroy'])->name('shop.management.delete');
    Route::post("/shop-management/search", [ShopManagementController::class, 'search'])->name('shop.management.search');

    Route::get("/social-media-link", [SocialMediaLinkController::class, 'index'])->name('social-media-link');
    Route::get("/social-media-link/edit/{id}", [SocialMediaLinkController::class, 'edit'])->name('social-media-link.edit');
    Route::post("/social-media-link/update", [SocialMediaLinkController::class, 'update'])->name('social-media-link.update');
    Route::post("/social-media-link/store", [SocialMediaLinkController::class, 'store'])->name('social-media-link.store');
    Route::post("/social-media-link/status-update", [SocialMediaLinkController::class, 'statusUpdate'])->name('social-media-link.status.update');
    Route::post("/social-media-link/delete", [SocialMediaLinkController::class, 'destroy'])->name('social-media-link.delete');
    Route::post("/social-media-link/search", [SocialMediaLinkController::class, 'search'])->name('social-media-link.search');
    Route::post("/social-media-link/number-exist", [SocialMediaLinkController::class, 'isNumberExist'])->name('social-media-link.number-exist');


     // Category
     Route::get('/category', 'Admin\CategoryController@index')->name('category');
     Route::post('/category/store', 'Admin\CategoryController@store')->name('category.store');
     Route::get("/category/edit/{id}", 'Admin\CategoryController@edit')->name('category.edit');
     Route::post("/category/update", 'Admin\CategoryController@update')->name('category.update');
     Route::post("/category/status/update", 'Admin\CategoryController@statusUpdate')->name('category.status.update');
     Route::post("/category/featured/update", 'Admin\CategoryController@featuredUpdate')->name('category.featured.update');
     Route::post("/category/delete", 'Admin\CategoryController@destroy')->name('category.delete');
     Route::post('/category/category_images', 'Admin\CategoryController@category_images')->name('upload_category_images');
     Route::post('category/detete_img', 'Admin\CategoryController@detete_img')->name('detete_category_img');
     
 
 
     Route::get('/category/add_category_products', 'Admin\CategoryController@add_category_products')->name('add_category_products');
     Route::post('/category/save_category_products', 'Admin\CategoryController@save_category_products')->name('save_category_products');
     Route::post("/category/delete_product", 'Admin\CategoryController@delete_product')->name('delete_cat_products');
 
     //brand
     Route::get('/brand', 'Admin\BrandController@index')->name('brand');
     Route::post('/brand/store', 'Admin\BrandController@store')->name('brand.store');
     Route::post("/brand/update", 'Admin\BrandController@update')->name('brand.update');
     Route::post("/brand/status/update", 'Admin\BrandController@statusUpdate')->name('brand.status.update');
     Route::post("/brand/delete", 'Admin\BrandController@destroy')->name('brand.delete');
     Route::get("/brand/edit/{id}", 'Admin\BrandController@edit')->name('brand.edit');
     Route::post('/brand/brand_images', 'Admin\BrandController@brand_images')->name('upload_brand_images');
     Route::post('brand/detete_img', 'Admin\BrandController@detete_img')->name('detete_brand_img');
     // Logs
 
     Route::get('/logs', 'Admin\LogController@index')->name('logs');
 
 
 
 // Attribute
 
     Route::get("/attribute", 'Admin\AttributeController@index')->name('attribute');
     Route::get('/attribute/create', 'Admin\AttributeController@create')->name('create_attribute');
     Route::post('/attribute/save', 'Admin\AttributeController@save')->name('save_attribute');
     Route::post('/attribute/delete', 'Admin\AttributeController@deleteAttribute')->name('deleteAttribute');
     Route::post('/attribute/activate', 'Admin\AttributeController@activate')->name('activate_attribute');
     Route::get('/attribute/add_category_attribute', 'Admin\AttributeController@add_category_attribute')->name('add_category_attribute');
     Route::post('/attribute/save_category_attribute', 'Admin\AttributeController@save_cat_attribute')->name('save_cat_attribute');
 
     Route::post("/attribute/delete_attributes", 'Admin\AttributeController@delete_attributes')->name('delete_cat_attributes');
 
 
     // Products
 
     Route::get("/products", 'Admin\ProductController@index')->name('products');
     Route::get('/products/create', 'Admin\ProductController@create')->name('create_product');
     Route::get('/products/tabs', 'Admin\ProductController@get_tabs')->name('product_tabs');
     Route::post('/products/product_info', 'Admin\ProductController@product_info')->name('save_product_info');
     Route::post('/products/product_stock', 'Admin\ProductController@product_stock')->name('save_product_stock');
     Route::post('/products/product_b2c_price', 'Admin\ProductController@product_b2c_price')->name('save_product_b2c_price');
     Route::post('/products/product_b2b_price', 'Admin\ProductController@product_b2b_price')->name('save_product_b2b_price');
     Route::post('/products/product_images', 'Admin\ProductController@product_images')->name('upload_product_images');
     Route::post('products/detete_img', 'Admin\ProductController@detete_img')->name('detete_pdt_img');
     Route::post('/products/delete', 'Admin\ProductController@deleteProduct')->name('deleteProduct');
     Route::post('/products/activate', 'Admin\ProductController@activate')->name('activate_product');
     Route::get('/products/details/{id}', 'Admin\ProductViewController@details')->name('view_product');
     Route::get('/products/viewtabs', 'Admin\ProductViewController@get_viewtabs')->name('productview_tabs');
 
 
 
 //Import products
     Route::get('/importproducts/import', 'Admin\ImportProductController@index')->name('import_products');
     Route::post('/importproducts/importsubmit', 'Admin\ImportProductController@importsubmit')->name('import_submit');
     Route::get('/importproducts/excel_view', 'Admin\ImportProductController@excel_view')->name('product_excel_view');
     Route::get('/importproducts/errors', 'Admin\ImportProductController@get_errors')->name('import_errors');
     Route::get('/importproducts/import_view', 'Admin\ImportProductController@import_view')->name('import_view');
     Route::get('/importproducts/confrim', 'Admin\ImportProductController@confrim_import')->name('confrim_import');
     Route::get('/importproducts/imported_data', 'Admin\ImportProductController@imported_data')->name('imported_data');
 
 
 
 
 // stock update
     Route::get('/stock_update', 'Admin\StockUpdateController@index')->name('stock_update');
     Route::post('/importstock/stocksubmit', 'Admin\StockUpdateController@importsubmit')->name('stock_import_submit');
     Route::get('/stock_update/b2csimple', 'Admin\StockUpdateController@b2csimple')->name('b2csimple');
     Route::get('/stock_update/errors', 'Admin\StockUpdateController@get_errors')->name('import_stockerrors');
     Route::get('/stock_update/confirm', 'Admin\StockUpdateController@confirm_import')->name('confrim_import_sb2c');
     Route::get('/stock_update/imported_data', 'Admin\StockUpdateController@imported_data')->name('imported_sb2cdata');
 
     Route::get('/stock_update/cerrors', 'Admin\StockUpdateController@b2ccomplex_errors')->name('b2ccomplex_errors');
     Route::get('/stock_update/b2ccomplex', 'Admin\StockUpdateController@b2ccomplex')->name('b2ccomplex');
     Route::get('/stock_update/confirm_complex', 'Admin\StockUpdateController@confirm_cb2cimport')->name('confrim_import_cb2c');
     Route::get('/stock_update/imported_complex', 'Admin\StockUpdateController@imported_complex')->name('imported_cb2cdata');
 
 // price update update
     Route::get('/price_update', 'Admin\PriceUpdateController@index')->name('price_update');
     Route::post('/importprice/pricesubmit', 'Admin\PriceUpdateController@pricesubmit')->name('price_import_submit');
     Route::get('/price_update/priceB2cSimple', 'Admin\PriceUpdateController@priceB2cSimple')->name('price_b2csimple');
     Route::get('/price_update/b2c_simple_errors', 'Admin\PriceUpdateController@b2cSimpleErrors')->name('b2c_simple_errors');
     Route::get('/price_update/confirm_simpleb2c', 'Admin\PriceUpdateController@confirmSimpleB2c')->name('confirm_import_simpleb2c');
     Route::get('/price_update/imported_simpleb2c', 'Admin\PriceUpdateController@importedSimpleB2c')->name('imported_simpleB2c');
 
     Route::get('/price_update/priceB2cComplex', 'Admin\PriceUpdateController@priceB2cComplex')->name('price_b2ccomplex');
     Route::get('/price_update/b2c_complex_errors', 'Admin\PriceUpdateController@b2cComplexErrors')->name('b2c_complex_errors');
     Route::get('/price_update/confirm_complexb2c', 'Admin\PriceUpdateController@confirmComplexB2c')->name('confirm_import_complexb2c');
     Route::get('/price_update/imported_complexb2c', 'Admin\PriceUpdateController@importedComplexB2c')->name('imported_complexB2c');
 
     Route::get('/price_update/priceB2bSimple', 'Admin\PriceUpdateController@priceB2bSimple')->name('price_b2bsimple');
     Route::get('/price_update/b2b_simple_errors', 'Admin\PriceUpdateController@b2bSimpleErrors')->name('b2b_simple_errors');
     Route::get('/price_update/confirm_simpleb2b', 'Admin\PriceUpdateController@confirmSimpleB2b')->name('confirm_import_simpleb2b');
     Route::get('/price_update/imported_simpleb2b', 'Admin\PriceUpdateController@importedSimpleB2b')->name('imported_simpleB2b');
 
     Route::get('/price_update/priceB2bComplex', 'Admin\PriceUpdateController@priceB2bComplex')->name('price_b2bcomplex');
     Route::get('/price_update/b2b_complex_errors', 'Admin\PriceUpdateController@b2bComplexErrors')->name('b2b_complex_errors');
     Route::get('/price_update/confirm_complexb2b', 'Admin\PriceUpdateController@confirmComplexB2b')->name('confirm_import_complexb2b');
     Route::get('/price_update/imported_complexb2b', 'Admin\PriceUpdateController@importedComplexB2b')->name('imported_complexB2b');
 
  // Promotional Products
  Route::get("/promoproduct", 'Admin\PromotionalProductController@index')->name('promoproduct');
  Route::get("/autocomplete", 'Admin\PromotionalProductController@autocomplete')->name('autocomplete');
  Route::post('/promoproduct/store', 'Admin\PromotionalProductController@store')->name('promoproduct.store');
  Route::get("/promoproducts", 'Admin\PromotionalProductController@promoproducts')->name('promoproducts');
  
});

Route::post("token/verify", [UserVerificationController::class, 'resetUserPassword'])->name('user.verify');
Route::post("verify", [UserVerificationController::class, 'verify'])->name('token.verify');
Route::post("user/password/change", [UserVerificationController::class, 'passwordUpdating'])->name('user.reset');
Route::get("user/logout", [UserVerificationController::class, 'logout'])->name('user.logout');
