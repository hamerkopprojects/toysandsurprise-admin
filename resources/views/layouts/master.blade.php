<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title', 'TOYS AND SURPRISE')</title>

        <!-- Styles -->
        <link href="{{asset('assets/css/lib/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/lib/themify-icons.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/lib/menubar/sidebar.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/lib/bootstrap.min.css')}}" rel="stylesheet">

        {{-- <link href="{{asset('assets/css/custom.css')}}" rel="stylesheet"> --}}

        <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/custom.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/custom2.css')}}" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/tagmanager/3.0.2/tagmanager.min.css">
        @stack('css')
        <style>
            table .right-align {
                text-align: right;
            }
            table.dataTable tbody td {
                padding: 10px 18px;
            }
        </style>
    </head>

    <link rel="stylesheet" href="{{asset('assets/css/jquery-confirm.css')}}">
    @stack('css')
</head>

<body>

<x-sidebar />

@include('layouts.header')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">

            @if(Request::segment(2) == "products")

            <div class="row">
                <div class="col-lg-1 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>@yield('content-title')</h1>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 ml-0">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                @yield('stock-update')
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                @yield('price-update')
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2" style="margin-right: 20px">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                @yield('add-import')
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 ml-3">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12">
                                @yield('add-btn')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /# row -->

            @else
            <div class="row">
                <div class="col-lg-8 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>@yield('content-title')</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
                <div class="col-lg-4">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                @yield('add-btn')
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /# column -->
            </div>
            <!-- /# row -->
            @endif
            <section id="main-content">
                @yield('content')
            </section>
        </div>
    </div>
</div>
<div id="search">
    <button type="button" class="close">×</button>
    <form>
        <input type="search" value="" placeholder="type keyword(s) here" />
        <button type="submit" class="btn btn-primary">Search</button>
    </form>
</div>
<!-- jquery vendor -->
<script src="{{ asset('assets/js/lib/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/jquery.nanoscroller.min.js') }}"></script>
<!-- nano scroller -->
<script src="{{ asset('assets/js/lib/menubar/sidebar.js') }}"></script>
<script src="{{ asset('assets/js/lib/preloader/pace.min.js') }}"></script>
<!-- sidebar -->
<script src="{{ asset('assets/js/lib/bootstrap.min.js') }}"></script>

 
<!-- bootstrap -->
<script src="{{ asset('assets/js/scripts.js') }}"></script>
<!-- scripit init-->
<script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
{{-- Dattable --}}
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script> --}}
{{-- JQuery validation --}}

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
{{-- Sweet alert --}}
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
{{-- select2 --}}

<script src="{{asset('assets/js/jquery-confirm.js')}}"></script>
<script src="{{asset('assets/js/jquery.validate.js')}}"></script>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tagmanager/3.0.2/tagmanager.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>  
    
<script>
const Toast = Swal.mixin({
toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: false,
        onOpen: (toast) => {
toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
});
</script>

@stack('scripts');
</body>

</html>