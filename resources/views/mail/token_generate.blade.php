@component('mail::message')
## Dear ,


@component('mail::panel')

<h3> The verification code is : {{$token}}</h3>
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
