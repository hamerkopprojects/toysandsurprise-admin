@extends('frontend.layouts.app')
@section('content')

{{-- @include('frontend.brand.banner')  --}}

<div id="content">
    <div class="container">
      <div id="breadcrumbs">
        <p><a href="{{route('frontend')}}">Home</a> > <span>By brand</span></p>
      </div>
      @include('frontend.brand.brands') 
    </div>
</div>
@include('frontend.common.common_script')
@endsection      