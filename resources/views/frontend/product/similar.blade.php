@if(count($similarProducts) > 0)
<div class="box1 mb-5">
  <div class="container">
    <div class="box-header">
      <div class="d-md-flex align-items-center justify-content-between">
        <h2 class="mb-md-0">You may like this:</h2>
      </div>
    </div>
    <div class="carousel clearfix">
      @foreach($similarProducts as $product)
      @php
      $name = isset($product->lang[0]->name) ? $product->lang[0]->name : '';
      try {
      $image = $product->cover_image;
      } catch (Exception $e) {
      $image = '';
      }
      @endphp      
      <div class="item">
        <div class="carousel-inner">
          <div class="prod-notification">
            <div class="d-flex align-items-center w-100 justify-content-end">
            
              <a onclick="event.preventDefault(); addToFavourites(this, {{ $product->id }});"  class="wishlist-trigger" href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
            </div>
          </div>
          <div class="product-image">
            <a href="{{route('products.single', $product->slug)}}">
              <img src="{{ asset('uploads/'.$image) }}" alt="image">
              </a>
          </div>
          <div class="product-description">
            <a href="{{route('products.single', $product->slug)}}"> <h5> {{ $name }}</h5></a>
            <div class="d-flex align-items-center justify-content-center">
              <span class="price mr-3"><span class="currency">SAR</span>{{$product->b2cprice[0]->price}}</span>
              <a href="#" class="button">Add to cart</a>
            </div>
          </div>
        </div>
      </div>
     
      @endforeach
    </div>
  </div>
</div>
@endif