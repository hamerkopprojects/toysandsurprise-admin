<div class="product">
    <div class="row">
      <div class="col-xl-7">
        @if(count($product_data->produc_images) > 0)
        <div class="gallery">
          <div class="slider slider-for">
            @php
           $pos = app()->getLocale() == 'en' ? 0 : 1;
            @endphp
        @foreach ($product_data->produc_images as $row_data)
        @if(isset($row_data->path) && is_file( public_path() .'/uploads/'. $row_data->path))
            <div class="item">
              <img src="{{ url('uploads/'. $row_data->path) }}" alt="Img" />
            </div>
            @endif
            @endforeach
          </div>
          <div class="slider slider-nav">
            @foreach ($product_data->produc_images as $row_data)
            @if(isset($row_data->path) && is_file( public_path() .'/uploads/'. $row_data->path))
            <div class="item">
              <img src="{{ url('uploads/'. $row_data->path) }}" alt="Img" />
            </div>
            @endif
            @endforeach
          </div>
         
        </div>
        @endif
      </div>
      <div class="col-xl-5">
        <div class="product-details">
          <header class="clearfix">
            <img src="assets/images/disney-img.jpg" alt="" />
          
            @foreach($product_tags as $row_val)
            <span class="badge-secondary mr-1">{{ $row_val['tag']['name'] }}</span>
            @endforeach
          </header>
          @php
          $pos = app()->getLocale() == 'en' ? 0 : 1;
           @endphp
          <h1>{{$product_data->lang[0]->name}}</h1>
          <p>{{$product_data->lang[0]->description	}} </p>
          <div class="reviews">
            <ul>
              <li><i class="fa fa-star" aria-hidden="true"></i></li>
              <li><i class="fa fa-star" aria-hidden="true"></i></li>
              <li><i class="fa fa-star" aria-hidden="true"></i></li>
              <li><i class="fa fa-star-half-o" aria-hidden="true"></i></li>
              <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
            </ul>
            <a href="#">20 Reviews</a>
          </div>
          <div class="price">
            <p><span>SAR</span> {{$product_data->b2cprice[0]->price}} <a href="#">In Stock</a></p>
          </div>
          <div class="offer">
            <p>Offer Valid:<span>04:20:20 hrs</span></p>
          </div>
          <div class="quantity">
            <span><strong>Quantity:</strong></span>
            <input type="button" value="-" class="minus">
            <input type="number" class="qty" step="1" min="0" max="" value="1">
            <input type="button" value="+" class="plus">
          </div>
          <button type="submit" class="cart-btn">Add to cart</button>
          <div class="wishlist">
            <a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i>Add to Wish List</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="product-info">
    <ul id="myTab" class="nav nav-tabs nav-tabs-responsive" role="tablist">
      <li role="presentation">
        <a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">
          <span class="text">Products Specification</span>
        </a>
      </li>
      <li role="presentation">
        <a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">
          <span class="text">Shipping & Delivery </span>
        </a>
      </li>
      <li role="presentation">
        <a href="#samsa" role="tab" id="samsa-tab" data-toggle="tab" aria-controls="samsa">
          <span class="text">Reviews</span>
        </a>
      </li>
    </ul>
    <div id="myTabContent" class="tab-content">
      <div role="tabpanel" class="tab-pane fade show active" id="home" aria-labelledby="home-tab">
        <p>
        
          <img src="{{ url('uploads/'. $product_data->brand->lang[0]->image_path) }}" alt="{{$product_data->brand->lang[0]->name}}">
        </p>
        <p class="lead">{{$product_data->lang[0]->description	}} </p>
        <p><strong>Magic in the details</strong></p>
      
       
        {!!$product_data->lang[0]->specification	!!}

     
      </div>
      <div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">
       
      </div>
      <div role="tabpanel" class="tab-pane fade" id="samsa" aria-labelledby="samsa-tab">
       
      </div>
    </div>
  </div>