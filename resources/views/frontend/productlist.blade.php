@extends('frontend.layouts.app')
@section('content')

<div id="content" class="pb-5">
    <div class="container">
      <div id="breadcrumbs">
          <?php
       if($type == 'category'){
           $link = route('frontend.category');
       }
       if($type == 'brand'){
           $link = route('frontend.brand');
       }
       if($type == 'limited-edition'){
           $link = route('product.limited-edition');
       }
       if($type == 'offers'){
           $link = route('product.offers');
       }
       if($type == 'newlyin'){
           $link = route('product.newlyin');
       }
       ?>
      <p><a href="{{route('frontend')}}">Home</a> > 
        <a href={{$link}}>{{isset($type) ? $type :''}}</a> > <span>{{ isset($slug_name->lang[0]->name) ? $slug_name->lang[0]->name : '' }}</span></p>
      </div>
      <div class="row" id="pdt_list">
          @include('frontend.productlist.filter')  
     </div>
     </div>
</div>
@include('frontend.common.common_script')
@endsection
@push('scripts')

<script>
  
 
    $(function() {
    $( "#slider-range" ).slider({
        range: true,
		min: 0,
		max: 15000,
		values: [1000, 10000],
      slide: function( event, ui ) {
        $( "#amount" ).html( "SAR " + ui.values[ 0 ] + " - SAR " + ui.values[ 1 ] );
        $( "#amount1" ).val(ui.values[ 0 ]);
        $( "#amount2" ).val(ui.values[ 1 ]);
      }
    });
    $( "#amount" ).html( "SAR " + $( "#slider-range" ).slider( "values", 0 ) +
     " - SAR " + $( "#slider-range" ).slider( "values", 1 ) );
  });


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $("#frm_categoey_filter").submit(function () {
        $('.ajax-loading-buz').show();
        var form = $(this);

        $.ajax({
            type: "POST",
            url: "{{route('product.filter')}}",
            data: form.serialize(), // serializes the form's elements.
            success: function (data)
            {
              
                $('#pdt_list').html(data);
                $('.ajax-loading-buz').hide();
            }
        });
    });
  
    $(document).ready(function () {
        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                var maxPage = $('#maxPage').val();
              
                var page = $('#page').val();
                if (maxPage > page) {
                    var page_new = parseInt(page) + 1;
                    $('#page').val(page_new);
                    $('.ajax-loading').hide();
                    var age_id = $('#age_id').val();
                    var gender_id = $('#gender_id').val();
                    var cat_id = $('#cat_id').val();
                    var brand_id = $('#brand_id').val();
                    var amount1 = $('#amount1').val();
                    var amount2 = $('#amount2').val();
                    loadMoreData(page_new, maxPage, cat_id, brand_id,age_id,gender_id,amount1,amount2);
                }
            }
        });
    });
 

    function loadMoreData(page, maxPage, category_id , brand_id, age_id,gender_id,amount1,amount2) {
     
        $.ajax(
                {
                    url: "{{route('product.loadMore')}}",
                    data: {'page': page,  'category_id': category_id, 'brand_id': brand_id, 'age_id':age_id, 'gender_id':gender_id, 'amount1':amount1, 'amount2':amount2},
                    type: "POST",
                    beforeSend: function ()
                    {
                        $('.ajax-loading').show();
                    }
                })
                .done(function (data)
                {
                  console.log(data);
                    if (data.length == 0) {
                      $('.ajax-loading').hide();
                    }
                    $('.ajax-loading').hide();
                    $(".products").append(data);
                })
                .fail(function (jqXHR, ajaxOptions, thrownError)
                {
                    $('.ajax-loading').hide();
                });
    }

  function sortData(sort){

   
    var age_id = $('#age_id').val();
    var gender_id = $('#gender_id').val();
    var cat_id = $('#cat_id').val();
    var brand_id = $('#brand_id').val();
    var sort = sort;
    var amount1 = $('#amount1').val();
    var amount2 = $('#amount2').val();
    console.log(amount1);
    $.ajax({
            type: "POST",
            url: "{{route('product.filter')}}",
            data: {'category_id': cat_id, 'brand_id': brand_id, 'age_id':age_id, 'gender_id':gender_id, 'sort':sort, 'amount1':amount1, 'amount2':amount2
            },           
            success: function (data)
            {
              
                $('#pdt_list').html(data);
                $('.ajax-loading-buz').hide();
            }
        });
  }
</script>


@endpush