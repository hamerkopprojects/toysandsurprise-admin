
@if(count($recently_viewed_products) > 0)
<div class="box1">
    <div class="box-header">
      <div class="d-md-flex align-items-center justify-content-between">
        <h2 class="mb-md-0">Recently Viewed</h2>
        @if (count($recently_viewed_products) > 1)
        <a href="{{route('product.recentlyviewed')}}" class="link-secondary">View all <span>+</span></a>
        @endif
      </div>
    </div>
    <div class="carousel clearfix">
      @foreach ($recently_viewed_products as $row_data)
      @php
      $pos = app()->getLocale() == 'en' ? 0 : 1;
      @endphp
      <div class="item">
        <div class="carousel-inner">
          <div class="prod-notification">
            <div class="d-flex align-items-center w-100 justify-content-end">
              @foreach ($row_data->product->product_tag as $tags)
              <span class="badge-secondary mr-3">{{ $tags->tag->name }}</span>
              @endforeach
              <a onclick="event.preventDefault(); addToFavourites(this, {{ $row_data->product->id }});"  class="wishlist-trigger" href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
            </div>
          </div>
          <div class="product-image">
            <a href="{{route('products.single', $row_data->product->slug)}}">
              @if(isset($row_data->product->cover_image) && is_file( public_path() .'/uploads/'. $row_data->product->cover_image))
              <img src="{{ url('uploads/'. $row_data->product->cover_image) }}" alt="Product">
              @else
              <img src="{{ asset('b2b/images/no-image.png') }}" alt="Product">
              @endif
            </a>
          </div>
          <div class="product-description">
            <a href="{{route('products.single', $row_data->product->slug)}}">
            <h5>{{ $row_data->product->lang[0]->name }}</h5>
            </a>
            <div class="d-flex align-items-center justify-content-center">
              <span class="price mr-3"><span class="currency">SAR</span>{{$row_data->product->b2cprice[0]->price}}</span>
              <a href="#" class="button">Add to cart</a>
            </div>
          </div>
        </div>
      </div>
     
      @endforeach 
    </div>
  </div>

  @endif