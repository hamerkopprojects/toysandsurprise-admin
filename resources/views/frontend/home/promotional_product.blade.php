@php $wishListAll = Helper::wish_listed_products();@endphp
@if (count($promotionalProducts) > 0)

<div class="box1-wrapper">
    <div class="container">
      <div class="box1 featured">
        <div class="box-header">
          <div class="d-md-flex align-items-center justify-content-between">
            <h2 class="mb-md-0">Featured Products</h2>
            @if ($promo_count > 4)
            <a href="{{route('product.promo-product')}}" class="link-secondary">View all <span>+</span></a>
            @endif
          </div>
        </div>
        <div class="carousel clearfix">
          @foreach($promotionalProducts as $product)
                 @php
                 $pos = app()->getLocale() == 'en' ? 0 : 1;
                
                    $name = isset($product->product->lang[$pos]->name) ? $product->product->lang[$pos]->name : '';
                    
                    try {
                        $image = $product->product->cover_image;
                    } catch (Exception $e) {
                        $image = '';
                    }

                    $inFav = in_array($product->product->id, $wishListAll) ? 'active' : '';
                 @endphp
          <div class="item">
            <div class="carousel-inner">
              <div class="prod-notification">
                <div class="d-flex align-items-center w-100 justify-content-end">
                  <a onclick="event.preventDefault(); addToFavourites(this, {{ $product->product->id }});"  class="wishlist-trigger" href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                </div>
              </div>
              <div class="product-image">
                <a href="{{route('products.single', $product->product->slug)}}">
              <img src="{{ asset('uploads/'.$image) }}" alt="image">
            </a>
              </div>
              <div class="product-description">
                <a href="{{route('products.single', $product->product->slug)}}"> <h5>{{$product->product->lang[$pos]->name}}</h5>
                </a>
                <div class="d-flex align-items-center justify-content-center">
                  <span class="price mr-3"><span class="currency">SAR</span>{{$product->product->b2cprice[0]->price}}</span>
                  <a href="#" class="button">Add to cart</a>
                </div>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
  @endif
  