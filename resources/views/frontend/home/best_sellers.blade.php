
      <div class="box1">
        <div class="box-header">
          <div class="d-md-flex align-items-center justify-content-between">
            <h2 class="mb-md-0">Best Sellers</h2>
            <a href="#" class="link-secondary">View all <span>+</span></a>
          </div>
        </div>
        <div class="carousel clearfix">
          <div class="item">
            <div class="carousel-inner">
              <div class="prod-notification">
                <div class="d-flex align-items-center w-100 justify-content-end">
                  <span class="badge-secondary mr-3">Offer</span>
                  <a href="#" class="wishlist-trigger"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                </div>
              </div>
              <div class="product-image">
                <img src="assets/images/image10.png" alt="image">
              </div>
              <div class="product-description">
                <h5>Forza Horizon 4 Speed Champions</h5>
                <div class="d-flex align-items-center justify-content-center">
                  <span class="price mr-3"><span class="currency">SAR</span> 163.00</span>
                  <a href="#" class="button">Add to cart</a>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="carousel-inner">
              <div class="prod-notification">
                <div class="d-flex align-items-center w-100 justify-content-end">
                  <a href="#" class="wishlist-trigger"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                </div>
              </div>
              <div class="product-image">
                <img src="assets/images/image2.png" alt="image">
              </div>
              <div class="product-description">
                <h5>Disney Cinderella's Castle Play set</h5>
                <div class="d-flex align-items-center justify-content-center">
                  <span class="price mr-3"><span class="currency">SAR</span> 163.00</span>
                  <a href="#" class="button">Add to cart</a>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="carousel-inner">
              <div class="prod-notification">
                <div class="d-flex align-items-center w-100 justify-content-end">
                  <span class="badge-secondary mr-3">Offer</span>
                  <a href="#" class="wishlist-trigger"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                </div>
              </div>
              <div class="product-image">
                <img src="assets/images/image3.png" alt="image">
              </div>
              <div class="product-description">
                <h5>Townhouse Toy Store (31105) set</h5>
                <div class="d-flex align-items-center justify-content-center">
                  <span class="price mr-3"><span class="currency">SAR</span> 163.00</span>
                  <a href="#" class="button">Add to cart</a>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="carousel-inner">
              <div class="prod-notification">
                <div class="d-flex align-items-center w-100 justify-content-end">
                  <a href="#" class="wishlist-trigger"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                </div>
              </div>
              <div class="product-image">
                <img src="assets/images/image4.png" alt="image">
              </div>
              <div class="product-description">
                <h5>Star Wars TM Poe's X-Wing Fighter</h5>
                <div class="d-flex align-items-center justify-content-center">
                  <span class="price mr-3"><span class="currency">SAR</span> 163.00</span>
                  <a href="#" class="button">Add to cart</a>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="carousel-inner">
              <div class="prod-notification">
                <div class="d-flex align-items-center w-100 justify-content-end">
                  <span class="badge-secondary mr-3">Offer</span>
                  <a href="#" class="wishlist-trigger"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                </div>
              </div>
              <div class="product-image">
                <img src="assets/images/image1.png" alt="image">
              </div>
              <div class="product-description">
                <h5>Forza Horizon 4 Speed Champions</h5>
                <div class="d-flex align-items-center justify-content-center">
                  <span class="price mr-3"><span class="currency">SAR</span> 163.00</span>
                  <a href="#" class="button">Add to cart</a>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="carousel-inner">
              <div class="prod-notification">
                <div class="d-flex align-items-center w-100 justify-content-end">
                  <a href="#" class="wishlist-trigger"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                </div>
              </div>
              <div class="product-image">
                <img src="assets/images/image2.png" alt="image">
              </div>
              <div class="product-description">
                <h5>Disney Cinderella's Castle Play set</h5>
                <div class="d-flex align-items-center justify-content-center">
                  <span class="price mr-3"><span class="currency">SAR</span> 163.00</span>
                  <a href="#" class="button">Add to cart</a>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="carousel-inner">
              <div class="prod-notification">
                <div class="d-flex align-items-center w-100 justify-content-end">
                  <span class="badge-secondary mr-3">Offer</span>
                  <a href="#" class="wishlist-trigger"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                </div>
              </div>
              <div class="product-image">
                <img src="assets/images/image3.png" alt="image">
              </div>
              <div class="product-description">
                <h5>Townhouse Toy Store (31105) set</h5>
                <div class="d-flex align-items-center justify-content-center">
                  <span class="price mr-3"><span class="currency">SAR</span> 163.00</span>
                  <a href="#" class="button">Add to cart</a>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="carousel-inner">
              <div class="prod-notification">
                <div class="d-flex align-items-center w-100 justify-content-end">
                  <a href="#" class="wishlist-trigger"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                </div>
              </div>
              <div class="product-image">
                <img src="assets/images/image4.png" alt="image">
              </div>
              <div class="product-description">
                <h5>Star Wars TM Poe's X-Wing Fighter</h5>
                <div class="d-flex align-items-center justify-content-center">
                  <span class="price mr-3"><span class="currency">SAR</span> 163.00</span>
                  <a href="#" class="button">Add to cart</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
     