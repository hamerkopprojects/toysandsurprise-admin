@if (count($product) > 0)
<div class="container">
    <div class="box2">
      <div class="box-header">
        <div class="d-md-flex align-items-center justify-content-between">
          <h2 class="mb-md-0">Newly In</h2>
          @if ($product_count > 3)
          <a href="{{route('product.newlyin')}}" class="link-secondary">View all <span>+</span></a>
          @endif
        </div>
      </div>
      <div class="row">
        @foreach($product as $pro)
        @php
        $pos = app()->getLocale() == 'en' ? 0 : 1;
        try {
            $image = $pro->cover_image;
        } catch (Exception $e) {
            $image = '';
        }
     @endphp
        <div class="col-lg-4">
          <div class="box-image">
            <img src="{{ asset('uploads/'.$image) }}" alt="image">
          </div>
          <div class="box-description">
            <h3>{{ $pro->lang[0]->name }}</h3>
            <p>{{ $pro->lang[0]->description }}</p>
            <a href="#">Shop now &gt;</a>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
  @endif