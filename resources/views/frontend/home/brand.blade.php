<div class="box1-wrapper">
  @if (count($shop_by_brand) > 0)
    <div class="container">
      <div class="box1 brand">
        <div class="box-header">
          <div class="d-md-flex align-items-center justify-content-between">
            <h2 class="mb-md-0">Shop by Brand</h2>
            @if ($shop_by_brand_count > 7)
            <a href="{{route('frontend.brand')}}" class="link-secondary">View all <span>+</span></a>
            @endif
          </div>
        </div>
        <ul class="list-unstyled list1">
          @foreach ($shop_by_brand as $brand_val)
          @php $pos = app()->getLocale() == 'en' ? 0 : 1;
          @endphp
          <li>
            <a href="#">
              <div class="content">
                <figure class="image1">
                  @if(isset($brand_val->lang[$pos]->image_path))
                  <a href="{{route('frontend.brand.search', $brand_val->slug)}}">
                  <img src="{{ url('uploads/'. $brand_val->lang[$pos]->image_path) }}" alt="fav">
                  </a>
                  @elseif(isset($brand_val->lang[$pos]->image_path))
                  <a href="{{route('frontend.brand.search', $brand_val->slug)}}">
                  <img src="{{ url('uploads/'. $brand_val->lang[$pos]->image_path) }}" alt="fav">
                </a>
                  @endif
                </figure>
                <p>{{$brand_val->lang[$pos]->name}}</p>
              </div>
            </a>
          </li>
          @endforeach
        </ul>
      </div>
    </div>
    @endif
  </div>