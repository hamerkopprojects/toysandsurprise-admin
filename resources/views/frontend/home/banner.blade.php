 <!-- banner -->
 @if (count($slider) > 0)
 <div id="banner">
  <div class="container">
    <div class="carousel2 clearfix">
      @foreach ($slider as $row_data)
        @php if($row_data->web_flag == 'U')
        $slider_link = $row_data->url;
        elseif($row_data->web_flag == 'C')
        $slider_link = isset($row_data->slider_category->slug) ? 'category/'.$row_data->slider_category->slug : '';
        elseif($row_data->web_flag == 'P')
        $slider_link = isset($row_data->slider_product->slug) ? 'products/'.$row_data->slider_product->slug : '';
        @endphp
        @if(!empty($row_data->web_image))
      <div class="item">
        <div class="carousel-inner">
          <div class="banner-image">
            <a href="{{ (isset($slider_link) && !empty($slider_link)) ? $slider_link : 'javascript:void(0)' }}" {{$row_data->web_flag == 'U' ? 'target=_blank' : ''}}>   
              @if(is_file( public_path() .'/uploads/'. $row_data->web_image))
             <img src="{{ url('uploads/'. $row_data->web_image) }}" class="w-100" alt="banner">
              @endif
         </a>
          </div>
          @if(!empty($row_data->title)|| !empty($row_data->content))
          <a href="{{ (isset($slider_link) && !empty($slider_link)) ? $slider_link : 'javascript:void(0)' }}" {{$row_data->web_flag == 'U' ? 'target=_blank' : ''}}> 
          <div class="banner-description">
            <div class="text">
              @if(!empty($row_data->title))<h1>{{ $row_data->title }}</h1>@endif
              @if(!empty($row_data->content))<p>{{ $row_data->content }}<p>@endif
                @if(!empty($slider_link))<a href="{{ $slider_link }}" {{$row_data->web_flag == 'U' ? 'target=_blank' : ''}} class="button">Shop Now</a>@endif
            </div>
          </div>
        </a>
        @endif
        </div>
      </div>
      @endif
        @endforeach
    </div>
  </div>
</div>
@endif
<!-- banner -->