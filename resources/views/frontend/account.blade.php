 
 @extends('frontend.layouts.app')
 @section('content')
 @include('user.flash-message')
 <!-- content -->
 <div id="content">
    <div class="container">
      <div id="breadcrumbs">
        <p><a href="#">Home</a> > <span>My Account</span></p>
      </div>
      <div class="row pb-5">
        <div class="col-lg-6">
          <h2 class="mb-5">Sign up with us</h2>
          <div class="form pb-5">
            <form action="{{ route('customer.register') }}" id="account_form" method="post">
              @csrf
              <div class="form-group">
                <label for="email">Enter Email</label>
                <input type="email" name="email" @error('email') is-invalid @enderror  class="form-control" id="email" value="{{ old('email') }}">
                @if ($errors->has('email'))
                <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
              </div>
              <div class="form-group">
                <label for="password">Enter Password</label>
                <input type="password" name="password"  @error('password') is-invalid @enderror class="form-control" id="password">
                @if ($errors->has('password'))
                <span class="text-danger">{{ $errors->first('password') }}</span>
                @endif
              </div>
              <div class="form-group">
                <label for="confirmPass">Confirm Password</label>
                <input type="password" name="confirm_password" @error('password') is-invalid @enderror class="form-control" id="confirmPass">
                @if ($errors->has('confirm_password'))
                <span class="text-danger">{{ $errors->first('confirm_password') }}</span>
                @endif
              </div>
              <div class="form-group">
                <label for="gender">Select Gender</label>
                <select class="form-control"name="gender" id="gender" @error('gender') is-invalid @enderror value="{{ old('gender') }}">
                  <option value="Male"> Male </option>
                  <option value="Female">Female</option>
                  <option value="Other">Other</option>
              </select>
              @if ($errors->has('gender'))
                <span class="text-danger">{{ $errors->first('gender') }}</span>
                @endif
              </div>
              <div class="form-group">
                <label for="phone_no">Mobile number</label>
                <input type="phone_no" name="phone_no" class="form-control" id="phone_no" @error('phone_no') is-invalid @enderror value="{{ old('phone_no') }}">
                @if ($errors->has('phone_no'))
                <span class="text-danger">{{ $errors->first('phone_no') }}</span>
                @endif
              </div>
              <div class="form-group">
                <button type="submit" class="button">
                  create account
              </button>
              </div>
            </form>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="user-login ml-xl-5">
            <h2>Login</h2>
            <form action="{{route('customer.login')}}"  method="post">
              @csrf
          
              <ul class="list-unstyled">
                <li>
                  <input type="email" name="user_name" class="email-field" placeholder="Enter Email" />
                </li>
                <li>
                  <input type="password" name="password" class="password-field" placeholder="Password" />
                </li>
                <li>
                  <button type="submit" class="button">
                    Login
                </button>
                </li>
              </ul>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- content -->
  @endsection  
  