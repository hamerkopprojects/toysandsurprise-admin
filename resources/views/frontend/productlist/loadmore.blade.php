
  @if(count($product_data) > 0)

  @foreach($product_data as $product)
  @php
  $pos = app()->getLocale() == 'en' ? 0 : 1;
 
     $name = isset($product->lang[$pos]->name) ? $product->lang[$pos]->name : '';
     
     try {
         $image = $product->cover_image;
     } catch (Exception $e) {
         $image = '';
     }
  @endphp
<div class="item">
  <div class="carousel-inner">
    <div class="prod-notification">
      <div class="d-flex align-items-center w-100 justify-content-end">
        <a href="#" class="wishlist-trigger"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
      </div>
    </div>
    <div class="product-image">
        <a href="{{route('products.single', $product->slug)}}">
            <img src="{{ asset('uploads/'.$image) }}" alt="image">
          </a>
    </div>
    <div class="product-description">
        <a href="{{route('products.single', $product->slug)}}"> <h5>{{$name}}</h5>
        </a>
      <div class="d-flex align-items-center justify-content-center">
        <span class="price mr-3"><span class="currency">SAR</span> {{$product->b2cprice[0]->price}}</span>
        <a href="#" class="button">Add to cart</a>
      </div>
    </div>
  </div>
</div>
@endforeach
@endif

<script>
 $(document).ready(function () {
        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                var maxPage = $('#maxPage').val();
                var page = $('#page').val();
                if (maxPage > page) {
                    var page_new = parseInt(page) + 1;
                    $('#page').val(page_new);
                    $('.ajax-loading').hide();
                
                    var cat_id = $('#cat_id').val();
                    var brand_id = $('#brand_id').val();
                    loadMoreData(page_new, maxPage, cat_id, brand_id);
                }
            }
        });
    });
</script>