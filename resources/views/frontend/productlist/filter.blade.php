<div class="col-xl-3 col-lg-4">
    <a href="#" class="filters"><i class="fa fa-filter" aria-hidden="true"></i> Filters </a>
    <form id="frm_categoey_filter" action="javascript:;" method="POST">
    <aside id="sidebar">
      <a href="#" class="close"><i class="fa fa-times-circle-o" aria-hidden="true"></i> </a>
      <header class="clearfix">
        {{-- <button type="button" href="#" onclick="location.href =''">Reset all</button> --}}
      </header>
      @if (count($categories) > 0)
      <div class="widget">
        <h2>Categories</h2>
        <ul>
        
          @foreach ($categories as $category)
           @php 
            $pos = app()->getLocale() == 'en' ? 0 : 1;
           $checked = (isset($category_id) && in_array($category->id, $category_id)) ? 'checked="checked"' : '';@endphp
          <li><input type="checkbox" id ="category_id" name="category_id[]" value="{{ $category->id }}" {{ $checked }} />{{$category->lang[$pos]->name}}</li>
          @endforeach
        </ul>
      </div>
      @endif
      @if (count($ages) > 0)
      <div class="widget">
        <h2>Age</h2>
        <ul>
          @foreach ($ages as $age)
          @php 
          $checked = (isset($age_id) && in_array($age->id, $age_id)) ? 'checked="checked"' : '';@endphp
          <li><input type="checkbox" name="age_id[]" value="{{ $age->id }}" {{ $checked }}/>{{$age->lang[$pos]->name}}</li>
          @endforeach
        </ul>
      </div>
      @endif
      @if (count($brands) > 0)
      <div class="widget">
        <h2>Brands</h2>
        <ul>
          @foreach ($brands as $brand)
          @php 
          $checked = (isset($brand_ids) && in_array($brand->id, $brand_ids)) ? 'checked="checked"' : '';@endphp
          <li><input type="checkbox" name="brand_id[]" value="{{ $brand->id }}" {{ $checked }}/>{{$brand->lang[$pos]->name}}</li>
          @endforeach
        </ul>
      </div>
      @endif
      @if (count($genders) > 0)
      <div class="widget">
        <h2>Gender</h2>
        <ul>
          @foreach ($genders as $gender)
          @php 
          $checked = (isset($gender_id) && ($gender_id)) ? 'checked="checked"' : '';@endphp
          <li><input type="radio" name="gender_id"  id="gender{{$gender->id}}"  value="{{$gender->id}}"   {{ $checked }} >{{$gender->lang[$pos]->name}}</li>
          @endforeach
        </ul>
      </div>
      @endif
      <div class="widget">
        <p>
          Price Range:<p id="amount"></p>
        </p>
      <div id="slider-range"></div>
        <input type="hidden" id="amount1" name="amount1" value="">
        <input type="hidden" id="amount2" name="amount2" value="">
      </div>  
      <header class="clearfix">
        <button type="submit" href="#" class="apply">Apply</button>
      </header>  
    </form>          
    </aside>
  </div>
  <div class="col-xl-9 col-lg-8">
    <div id="main" class="pb-5">
      <header class="clearfix">
        <div class="title">
          <p class="pagination">Showing {{($product_data->currentPage()-1)* $product_data->perPage() + 1}} - {{ ($product_data->currentPage()-1)* $product_data->perPage() + $product_data->perPage() }} of <strong>{{ $product_data->total() }}</strong> results</p>
        </div>

        <input type="hidden" name="category_id" value="{{ isset($filter_cat_ids) ? $filter_cat_ids : '' }}" id="cat_id">
        <input type="hidden" name="brand_id" value="{{ isset($filter_brand_ids) ? $filter_brand_ids : '' }}" id="brand_id">
    <input type="hidden" name="age_id" value="{{ isset($filter_age_ids) ? $filter_age_ids : '' }}" id="age_id"> 
    <input type="hidden" name="gender_id" value="{{ isset($gender_id) ? $gender_id : '' }}" id="gender_id"> 
    <input type="hidden" id="amount1" name="amount1" value="{{ isset($min_price) ? $min_price : '' }}">
    <input type="hidden" id="amount2" name="amount2" value="{{ isset($max_price) ? $max_price : '' }}">
    <input type="hidden" name="maxPage" value="{{ $product_data->lastPage() }}" id="maxPage">
    <input type="hidden" name="page" value="{{ isset($page) ? $page : 1 }}" id="page">
        <div class="sort">
          <p>Sort By</p>
          <div class="dropdown show">
            <a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">
              Low to High
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
              <a class="dropdown-item" onclick='sortData("low-high")' href="#">Price Low to High</a>
              <a class="dropdown-item" onclick='sortData("high-low")' href="#">Price High to Low</a>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </div>
        </div>
      </header>
      @if(count($product_data) > 0)
      <div class="products clearfix">
        @foreach($product_data as $product)
        @php
        $pos = app()->getLocale() == 'en' ? 0 : 1;
       
           $name = isset($product->lang[$pos]->name) ? $product->lang[$pos]->name : '';
           
           try {
               $image = $product->cover_image;
           } catch (Exception $e) {
               $image = '';
           }
        @endphp
        <div class="item">
          <div class="carousel-inner">
            <div class="prod-notification">
              <div class="d-flex align-items-center w-100 justify-content-end">
                <a onclick="event.preventDefault(); addToFavourites(this, {{ $product->id }});"  class="wishlist-trigger" href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
              </div>
            </div>
            <div class="product-image">
                <a href="{{route('products.single', $product->slug)}}">
                    <img src="{{ asset('uploads/'.$image) }}" alt="image">
                  </a>
            </div>
            <div class="product-description">
                <a href="{{route('products.single', $product->slug)}}"> <h5>{{$name}}</h5>
                </a>
              <div class="d-flex align-items-center justify-content-center">
                <span class="price mr-3"><span class="currency">SAR</span> {{$product->b2cprice[0]->price}}</span>
                <a href="#" class="button">Add to cart</a>
              </div>
            </div>
          </div>
        </div>
        @endforeach
      </div>
      @endif
      <footer>
        <p class="pagination">Showing {{($product_data->currentPage()-1)* $product_data->perPage() + 1}} - {{ ($product_data->currentPage()-1)* $product_data->perPage() + $product_data->perPage() }} of <strong>{{ $product_data->total() }}</strong> results</p>

       
        <input type="hidden" name="category_id" value="{{ isset($filter_cat_ids) ? $filter_cat_ids : '' }}" id="cat_id">
        <input type="hidden" name="brand_id" value="{{ isset($filter_brand_ids) ? $filter_brand_ids : '' }}" id="brand_id">
    <input type="hidden" name="age_id" value="{{ isset($filter_age_ids) ? $filter_age_ids : '' }}" id="age_id"> 
    <input type="hidden" name="gender_id" value="{{ isset($gender_id) ? $gender_id : '' }}" id="gender_id">
    <input type="hidden" id="amount1" name="amount1" value="{{ isset($min_price) ? $min_price : '' }}">
    <input type="hidden" id="amount2" name="amount2" value="{{ isset($max_price) ? $max_price : '' }}"> 
    <input type="hidden" name="maxPage" value="{{ $product_data->lastPage() }}" id="maxPage">
    <input type="hidden" name="page" value="{{ isset($page) ? $page : 1 }}" id="page">
    <div class="text-center">
      <div class="ajax-loading" style="display: none"><img src="{{ asset('assets/images/loader.gif') }}"/></div>
  </div>
        {{-- <a href="" id = "loadmore">Load More</a> --}}
        
      </footer>
    </div>
  </div>
  @if(isset($search) && $search == 'filter')
<script>
  
   $(function() {
    $( "#slider-range" ).slider({
        range: true,
		min: 0,
		max: 15000,
		values: [2000, 10000],
      slide: function( event, ui ) {
        $( "#amount" ).html( "SAR " + ui.values[ 0 ] + " - SAR " + ui.values[ 1 ] );
        $( "#amount1" ).val(ui.values[ 0 ]);
        $( "#amount2" ).val(ui.values[ 1 ]);
      }
    });
    $( "#amount" ).html( "SAR " + $( "#slider-range" ).slider( "values", 0 ) +
     " - SAR " + $( "#slider-range" ).slider( "values", 1 ) );
  });
  
    $("#frm_categoey_filter").submit(function () {
    $('.ajax-loading-buz').show();
    var form = $(this);
    $.ajax({
    type: "POST",
    url: "{{route('product.filter')}}",
            data: form.serialize(), // serializes the form's elements.
            success: function (data)
            {
            $('#pdt_list').html(data);
            $('.ajax-loading-buz').hide();
            }
    });
    });
</script>
@endif