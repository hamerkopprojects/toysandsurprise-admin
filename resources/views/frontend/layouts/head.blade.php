<head>
 <!-- Meta Tags -->
 <meta charset="utf-8" />
 <meta name="viewport" content="width=device-width" />
 <meta name="description" content="" />
 <meta name="csrf-token" content="{{ csrf_token() }}" />
 <!-- Page Title & Favicon -->
 <title>Index | Toy and surprise</title>
 <!-- Stylesheets -->

 @php
 try {
 $_to_load = $lang == 'ar' ? '-rtl' : '';
 } catch(Exception $e) {
 $_to_load = '';
 }
 @endphp

 <link rel="stylesheet" href="{{ asset('assets/frontend/stylesheets/bootstrap' . $_to_load . '.min.css') }}"/>
 
 <link rel="stylesheet" href="{{ asset('assets/frontend/stylesheets/font-awesome.min.css') }}" />
 <link rel="stylesheet" href="{{ asset('assets/frontend/stylesheets/owl.carousel.min.css') }}" />
 <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
 <link rel="stylesheet" href="{{ asset('assets/frontend/stylesheets/main.css') }}" />
 <link rel="stylesheet" href="{{ asset('assets/frontend/stylesheets/slick-theme.css') }}" />
 <!-- Javascript -->
 <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
 <script src="{{ asset('assets/frontend/javascripts/vendor/head.core.js') }}"></script>
 @if($lang == 'ar')
<style>
     #banner .banner-description .text {
    right: 20%;}
</style>
@endif
</head>

