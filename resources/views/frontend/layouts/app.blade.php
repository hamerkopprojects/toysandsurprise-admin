<!DOCTYPE html>
<html>

@include('frontend.layouts.head')

<body class="home">
    <!--[if lt IE 11]>
          <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
          <![endif]-->
    <div id="container">
        @include('frontend.layouts.top')
        @include('frontend.layouts.header')

        @yield('content')

        @include('frontend.layouts.footer')
        @include('frontend.layouts.footerjs')
        @stack('scripts');
    </div>
</body>

</html>        