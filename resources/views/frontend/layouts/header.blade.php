<!-- header -->
<header id="header">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-3">
          <a href="{{route('frontend')}}" class="logo"><img src="{{ asset('assets/frontend/images/logo.png') }}" alt="logo"></a>
        </div>
        <div class="col-lg-9 col-xl-3 d-none d-lg-inline-block order-xl-2">
          <form class="search-form">
            <input type="text" placeholder="Search..">
          </form>
        </div>
        <div class="col-lg-12 col-xl-6 order-xl-1 pr-0">
          <button type="button" class="menu-btn"> <span class="line"></span> <span class="line"></span> <span
              class="line"></span>
          </button>
          <nav id="main-navigation">
            <ul class="list-unstyled">
              <li class="active"><a href="{{route('frontend')}}">Home</a>
              </li>
            <li><a href="{{route('frontend.category')}}">By category</a>
              </li>
              <li><a href="{{route('frontend.brand')}}">By brand</a>
              </li>
              <li><a href="#">By age</a>
              </li>
              <li><a href="{{route('product.limited-edition')}}">Limited edition</a>
              </li>
              <li><a href="{{route('product.offers')}}">Offers</a>
              </li>
            </ul>
          </nav>

        </div>

      </div>
    </div>
  </header>
  <!-- header -->