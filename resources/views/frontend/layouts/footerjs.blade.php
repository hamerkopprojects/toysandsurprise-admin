 <!-- Javascript -->
 <script src="{{ asset('assets/frontend/javascripts/vendor/jquery-3.2.1.min.js') }}"></script>
 <script src="{{ asset('assets/frontend/javascripts/vendor/jquery.matchHeight-min.js') }}"></script>
 <script src="{{ asset('assets/frontend/javascripts/vendor/popper.min.js') }}"></script>
 <script src="{{ asset('assets/frontend/javascripts/vendor/bootstrap.min.js') }}"></script>
 <script src="{{ asset('assets/frontend/javascripts/vendor/ofi.browser.js') }}"></script>
 <script src="{{ asset('assets/frontend/javascripts/vendor/owl.carousel.js') }}"></script>
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 <script src="{{ asset('assets/frontend/javascripts/vendor/slick.min.js') }}"></script>
 <script src="{{ asset('assets/frontend/javascripts/jquery.toaster.js') }}"></script>
 <!-- Custom JS Code for all pages -->
 <script src="{{ asset('assets/frontend/javascripts/main.js') }}"></script>
 @if($lang == 'en')
<script>
$(".carousel2").owlCarousel({
			loop: true,
			
			margin: 30,
			nav: true,
			autoplay: true,
			navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
			responsive: {
				0: {
					items: 1
				}
			}
		});
</script>
@else
<script>
$(".carousel2").owlCarousel({
			loop: true,
			rtl: true,
			margin: 30,
			nav: true,
			autoplay: true,
			navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
			responsive: {
				0: {
					items: 1
				}
			}
		});


</script>
@endif
