 <!-- footer -->
 <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-xl-3 col-lg-7 col-sm-6 mb-3">
          <a href="#" class="logo"><img src="{{ asset('assets/frontend/images/footer-logo.png') }}" alt="logo"></a>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
            dolore magna aliqua.</p>
        </div>
        <div class="col-xl-3 col-lg-5 col-sm-6 mb-3">
          <h6>Find a store</h6>
          <form method="POST" action="#">
            <span class="selectbox">
              <select>
                <option>Find your nearest Toy & Surprise Showroom.</option>
                <option>option:2</option>
              </select>
            </span>
          </form>
        </div>
        <div class="col-xl-3 col-sm-8 mb-3">
          <div class="row">
            <div class="col-6">
              <h6>About</h6>
              <ul class="list-unstyled nav-list">
                <li><a href="#">Overview</a></li>
                <li><a href="#">Careers</a></li>
                <li><a href="#">Store Locator</a></li>
              </ul>
            </div>
            <div class="col-6">
              <h6>Support</h6>
              <ul class="list-unstyled nav-list">
                <li><a href="#">Contact Us</a></li>
                <li><a href="#">FAQ’s</a></li>
                <li><a href="#">Shipping & Delivery</a></li>
                <li><a href="#">Return Policy</a></li>
                <li><a href="#">Privacy Policy</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-sm-4 mb-3">
          <div class="follow-us">
            <h6>Follow us</h6>
            <ul class="list-unstyled sociable">
              <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <img src="{{ asset('assets/frontend/images/payment-option.png') }}" alt="payment">
          <p class="copyright mb-md-0 mr-md-5">Copyright &copy;2020 Toys & Surprise.</p>
        </div>
      </div>
    </div>
  </footer>
  <!-- footer -->
  <!-- back-to-top -->
  <a id="back-to-top" title="Back to top" href="#" style="display: inline;">
    <img src="{{ asset('assets/frontend/images/back-to-top.png') }}" alt="Back to top">
  </a>
  <!-- back-to-top -->