 <!-- top -->
 <div id="top">
    <div class="container">
      <div class="d-md-flex align-items-center justify-content-end">
        <ul class="list-unstyled action-list">
          <li>
            <i><img src="{{ asset('assets/frontend/images/icon4.png') }}" alt="icon"></i>
            <span>+966 27834 123</span>
          </li>
          @php
          $customer_id = \Session::get('customer_id');
       
          $customer_name = \Session::get('customer_name');
          
          // $profile_image = \App\Models\Customer::where('id', $customer_id)->pluck('profile_image')->first();
          @endphp
          @if($customer_id != '')
          <li>
            <i><img src="{{ asset('assets/frontend/images/icon1.png') }}" alt="icon"></i>
            <span><a href="{{route('customer.index')}}">My Account</a></span>
          <li>
          @else
          <li>
            <i><img src="{{ asset('assets/frontend/images/icon1.png') }}" alt="icon"></i>
            <span><a href="{{route('customer.index')}}">Login</a>|<a href="{{route('customer.index')}}">Register</a></span>
          </li>
            @endif
            <a href="#"><i><img src="{{ asset('assets/frontend/images/icon2.png') }}" alt="icon"></i>
              @php
              $customer_id = \Session::get('customer_id');
              $wishlist = \App\Models\WishList::where('customer_id', '=', $customer_id)->get();
              $wishlist_count = count($wishlist) ?? 0;
              @endphp
              <span><small>Wishlist</small> ({{$wishlist_count}})</span></a>
          </li>
          <li>
            <a href="#"><i><img src="{{ asset('assets/frontend/images/icon3.png') }}" alt="icon"></i>
              <span><small>Bag</small> (0)</span></a>
          </li>
        </ul>

        @php
        $lang1= app()->getLocale() == 'en' ? 'ar' : 'en';
        $display = $lang1 == 'en' ? 'English' : 'العربية';
        @endphp
        <a href="{{ url('setlocale', $lang1) }}" class="language"><span>{{ $display }}</span></a>
      </div>
    </div>
  </div>
  <!-- top -->