@php
$count = $cartItems->parent->item_count ?? 0;
$customer_id = \Session::get('customer_id');
@endphp

<h1 class="h2">Shopping Cart (3 Item)</h1>
<div class="row">
  <div class="col-xl-9">
    <form action="#" method="post">
      <table class="cart" cellspacing="0">
        <thead>
          <tr>
            <th class="product">Product</th>
            <th class="product-price">Price</th>
            <th class="product-quantity">QTY</th>
            <th class="product-subtotal">Sub Total</th>
            <th class="product-actions">&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          <tr class="cart_item">
            <td class="product" data-title="Product">
              <figure> <img src="assets/images/image10.png">
              </figure>
              <p><strong>Forza Horizon 4 Speed Champions</strong></p>
              <p><strong>Item#: 123 </strong></p>
              <p>Nam sollicitudin magna in ante mattis, vitae aliquam tellus pellentesque.</p>
            </td>
            <td class="product-price" data-title="Price">
              <span class="amount"><span class="currencySymbol">SAR</span>1,300.00</span>
            </td>
            <td class="product-quantity" data-title="Quantity">
              <div class="quantity">
                <input type="button" value="-" class="minus">
                <input type="number" title="Qty" placeholder="" class="qty">
                <input type="button" value="+" class="plus">
              </div>
            </td>
            <td class="product-subtotal" data-title="Subtotal">
              <span class="amount"><span class="currencySymbol">SAR</span>5,200.00</span>
            </td>
            <td class="product-actions">
              <a class="edit-product" href="#" aria-label="Edit this item"><i class="fa fa-pencil" aria-hidden="true"></i> </a>
              <a class="delete-product" href="#" aria-label="Remove this item"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
            </td>
          </tr>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="6" class="actions">
              <button type="submit" class="button" name="update_cart" value="Update cart">Update cart</button>
            </td>
          </tr>
        </tfoot>
      </table>
    </form>
  </div>
  <div class="col-xl-3">
    <div class="order-summary">
      <a href="#" class="button">CONTINUE SHOPPING</a>
      <h3>Order Summary</h3>
      <ul class="list-unstyled">
        <li>Sub-Total:<span><span class="currencySymbol">SAR</span> 163.00</span></li>
        <li>Shipping: (Ground)<span><span class="currencySymbol">SAR</span> 00.00</span></li>
        <li>Tax:<span><span class="currencySymbol">SAR</span> 00.00</span></li>
        <li><strong>Total:<span><span class="currencySymbol">SAR</span> 163.00</span></strong></li>
        <li>You will earn:<span>36 points</span></li>
      </ul>
      <p><input type="checkbox" />I agree the <a href="#">terms of service</a> and I adhere to them
        unconditionally (read)</p>
      <a href="#" class="button">checkout</a>
    </div>
  </div>
</div>
