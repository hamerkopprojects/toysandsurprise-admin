
@if(count($categories) > 0)
<ul class="list category clearfix">
    @foreach($categories as $category)
    @php
    $pos = app()->getLocale() == 'en' ? 0 : 1;
    $name = isset($category->lang[$pos]->name) ? $category->lang[$pos]->name : '';
    try {
    $image = $category->lang[$pos]->image_path;
   
    } catch (Exception $e) {
    $image = '';
    }
    @endphp
    <li> <a href="{{route('frontend.category.search', $category->slug)}}">
        <figure><img src="{{ asset('uploads/'.$image) }}" alt="{{ $name }}" /></figure>
        <p>{{ $name }}</p>
      </a></li>
      @endforeach
  </ul>
  
  @endif