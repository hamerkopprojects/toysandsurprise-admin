 <!-- banner -->
 <div id="banner" class="category-page">
    <div class="container">
      <div class="carousel-inner">
        <div class="banner-image">
          <img src="assets/images/category-banner-img.jpg" class="w-100" alt="banner">
        </div>
        <div class="banner-description">
          <div class="text">
            <h1 class="text-white">Seek out toys that encourage your child to be active.</h1>
            <p class="text-white">Nullam malesuada ligula auctor, efficitur ante a tellus vulputate dictum.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- banner -->