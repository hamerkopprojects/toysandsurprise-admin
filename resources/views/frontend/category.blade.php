@extends('frontend.layouts.app')
@section('content')

{{-- @include('frontend.category.banner')  --}}

<div id="content">
    <div class="container">
      <div id="breadcrumbs">
        <p><a href="{{route('frontend')}}">Home</a> > <span>By category</span></p>
      </div>
      @include('frontend.category.categories') 
    </div>
</div>
@include('frontend.common.common_script')
@endsection      
