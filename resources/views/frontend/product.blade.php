@extends('frontend.layouts.app')
@section('content')

<div id="content" class="pb-5">
    <div class="container">
      <div id="breadcrumbs">
        {{-- <p><a href="#">Home</a> > <a href="#">category</a> > <span>Dolls, Beauty and Accessories</span></p> --}}
      </div>

      @include('frontend.product.details')  
      @include('frontend.product.similar')   

    </div>
</div>
@include('frontend.common.common_script')
@endsection