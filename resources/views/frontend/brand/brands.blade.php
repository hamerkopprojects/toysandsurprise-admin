@if(count($brands) > 0)
<ul class="list brands clearfix">
    @foreach($brands as $brand)
    @php
    $pos = app()->getLocale() == 'en' ? 0 : 1;
    $name = isset($brand->lang[$pos]->name) ? $brand->lang[$pos]->name : '';
    try {
    $image = $brand->lang[$pos]->image_path;
   
    } catch (Exception $e) {
    $image = '';
    }
    @endphp

    <li>
      <a href="{{route('frontend.brand.search', $brand->slug)}}">
    <figure><img src="{{ asset('uploads/'.$image) }}" alt="{{ $name }}" /></figure>
    <p>{{ $name }}</p>
    </a>
    </li>
      @endforeach
  </ul>
  @endif