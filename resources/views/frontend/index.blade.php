@extends('frontend.layouts.app')
@section('content')

@include('frontend.home.banner')

<div id="content">

    @include('frontend.home.brand')

    @include('frontend.home.promotional_product')

    @include('frontend.home.newly_in')
    <div class="box1-wrapper">
    <div class="container">
    {{-- @include('frontend.home.best_sellers') --}}
    <hr>
    @include('frontend.home.recently_viewed')
    </div>
    </div>
   
</div>
@include('frontend.common.common_script')
@endsection