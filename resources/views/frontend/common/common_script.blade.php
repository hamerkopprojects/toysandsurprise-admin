@push('scripts')

<script>
    
    $(document).ready(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        addToFavourites = (item, product_id, from='') => {
           
            var isInFav = $(item).hasClass('active');
            console.log(isInFav);
            if(isInFav) {
                $(item).removeClass('active');
            } else {
                $(item).addClass('active');
            }
            
            $.ajax({
                type: "POST",
                url: "{{route('product.favourite')}}",
                data: {
                    'product_id': product_id
                },
                success: function (data) {
                    if(data.success == true) {
                        $.toaster({
                            priority : 'success',
                            title    : 'Success',
                            message  : data.message,
                            timeout  : 3000,
                        });
                        if(from == 'wishlist') {
                            $(item).closest('div.product').remove();
                            window.location.reload();
                        }
                    } else {
                        if(typeof data.code != 'undefined' && data.code == 'no.login') {
                            $('#myModal').modal('show');
                        }
                        // On fail revert.
                        if(isInFav) {
                            $(item).addClass('active');
                        } else {
                            $(item).removeClass('active');
                        }
                        $.toaster({
                            priority : 'danger',
                            title    : 'Error',
                            message  : data.message,
                            timeout  : 3000,
                        });
                    }
                },
                complete: function() {
                    $('.ajax-loading-buz').hide();
                }
            });
        }








    });
</script>

@endpush