@extends('frontend.layouts.app')
@section('content')

<div id="content" class="cart-form">
    <div class="container">
      <div id="breadcrumbs">
        <p><a href="#">Home</a> > <span>Shopping Cart</span></p>
      </div>

      @include('frontend.cart.cart')

    </div>
</div>

      @include('frontend.common.common_script')
      @endsection  
