@extends('layouts.auth')

@section('auth-form')
<div class="login-form">
    <h4>FORGOT PASSWORD</h4>
    <h6 class="sub-title">Please enter your email to reset your password</h6>
    <form method="POST" action="{{ route('user.verify') }}">
        @csrf
        <div class="form-group">
            <label>Email *</label>
            <input placeholder="Enter email" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
            @if($errors->any())
                <strong class="error">{{$errors->first()}}</strong>
            @endif
        </div>
        <div class="action-btns">
            <a href="{{ route('login') }}" class="btn btn-default btn-flat m-b-15">BACK TO LOGIN</a>
            <button type="submit" class="btn btn-primary btn-flat m-b-15">Submit</button>
        </div>
    </form>
</div>
@endsection

@push('css')
<style>
    .sub-title {
        color: #656565;
        margin-bottom: 20px;
    }

    .action-btns {
        display: flex;
        justify-content: space-between;
    }

    .action-btns .btn {
        width: 47%;
    }
    .error{
        color: red;
    }
</style>
@endpush
