@extends('layouts.master')
@section('content-title')
ATTRIBUTES
@endsection
@section('add-btn')
<a href="javascript:void(0);" class="btn btn-info create_btn" data-cat-id='{{$category_id}}'><font style="vertical-align: inherit;"><i class="ti-plus"></i> Add New Attributes
    </font></a>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="posts-filter" method="get" action="{{ route('attribute') }}">
                    <div class="row tablenav top text-left">
                        <div class="col-md-4 ml-0">
                            <input class="form-control" type="text" name="search" value="{{ $search }}" placeholder="Search by Attribute Name">
                        </div>
                        <div class="col-md-4 ml-0">
                            <span class="small"><strong>Attribute Type</strong></span><br/>
                            <label class="radio-inline">
                                <input type="radio" name="attribute_type" value="dropdown" {{ isset($attribute_type) && $attribute_type == 'dropdown' ? 'checked="checked"' : ''}}> Dropdown
                            </label>
                            <label class="radio-inline ml-3">
                                <input type="radio" name="attribute_type" value="textbox" {{ isset($attribute_type) && $attribute_type == 'textbox' ? 'checked="checked"' : ''}}> Textbox
                            </label>
                            <label class="radio-inline ml-3">
                                <input type="radio" name="attribute_type" value="textarea" {{ isset($attribute_type) && $attribute_type == 'textarea' ? 'checked="checked"' : ''}}> Textarea
                            </label>
                        </div>
                        <div class="col-md-4 ml-0">
                            {{-- <span class="small"><strong>Variant</strong></span><br/>
                            <label class="radio-inline">
                                <input type="radio" name="is_variant" value="yes" {{ isset($is_variant) && $is_variant == 'yes' ? 'checked="checked"' : ''}}> Yes
                            </label>
                            <label class="radio-inline ml-3">
                                <input type="radio" name="is_variant" value="no" {{ isset($is_variant) && $is_variant == 'no' ? 'checked="checked"' : ''}}> No
                            </label> --}}
                        </div>
                        <div class="col-md-4 ml-0">
                            <select class="form-control search_val" name="cat_id">
                                <option value="">By Category</option>
                                @foreach($cat_data as $value)
                                <option value="{{$value['category_id']}}" {{ $value['category_id'] == $category_id ? 'selected' : '' }}>{{$value['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4 text-left">
                            <button type="submit" class="btn btn-info"><font style="vertical-align: inherit;">Search</font></button>
                            <a href="{{ route('attribute') }}" class="btn btn-default">Reset</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th>Attribute Name (EN)</th>
                                <th class="text-right">Attribute Name (AR)</th>
                                <th>Attribute Type</th>
                                <th>Mandatory</th>
                                {{-- <th>Variant</th> --}}
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($att_data) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($att_data as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->lang[0]->name ?? '' }}</td>
                                <td class="text-right">{{ $row_data->lang[1]->name ?? '' }}</td>
                                <td>{{ ucfirst($row_data->attribute_type) }}</td>
                                <td>{{ ucfirst($row_data->is_mandatory) }}</td>
                                {{-- <td>{{ ucfirst($row_data->is_variant) }}</td> --}}
                                <td class="text-center">
                                    <button
                                        type="button"
                                        class="change-status btn btn-sm btn-toggle ml-0 {{ $row_data['status'] == 'active' ? 'active' : ''}}"
                                        data-toggle="button"
                                        data-id="{{ $row_data->id }}"
                                        data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}"
                                        aria-pressed="true"
                                        autocomplete="off">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white edit_btn" title="Edit Attributes" data-id="{{ $row_data->id }}" data-cat-id='{{$category_id}}'><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white" title="Delete Attributes" onclick="deleteAttributes({{ $row_data->id }})"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $att_data->appends(request()->input())->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal pooup -->
<div class="modal fade none-border" id="formModal">
    <div class="modal-dialog modal-lg">
        <form id="frm_create_attribute" action="javascript:;" method="POST">
            <div class="modal-content">

            </div>
        </form>
    </div>
</div>
<!-- END MODAL -->
@endsection
@push('scripts')
<script>
    $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $('.create_btn').on('click', function () {
    var cat_id = $(this).data("cat-id");
    $.ajax({
    type: "GET",
            url: "{{route('create_attribute')}}",
            data: {'cat_id': cat_id},
            success: function (data) {
            $('.modal-content').html(data);
            $('#formModal').modal('show');
            }
    });
    });
    $('.edit_btn').on('click', function () {
    var attribute_id = $(this).data("id");
    var cat_id = $(this).data("cat-id");
    $.ajax({
    type: "GET",
            url: "{{route('create_attribute')}}",
            data: {'id': attribute_id, 'cat_id': cat_id},
            success: function (data) {
            $('.modal-content').html(data);
            $('#formModal').modal('show');
            }
    });
    });
    $("#frm_create_attribute").validate({
    normalizer: function (value) {
    return $.trim(value);
    },
            rules: {
            name_en: {
            required: true,
            },
                    name_ar: {
                    required: true,
                    },
                    attribute_type: {
                    required: true,
                    },
                    "att_value_en[]": {
                    required: true,
                    },
                    "att_value_ar[]": {
                    required: true,
                    },
            },
            messages: {
            name_en: {
            required: 'Name(EN) is required.'
            },
                    name_ar: {
                    required: 'Name(AR) is required.'
                    },
                    attribute_type: {
                    required: 'Attribute Type is required.'
                    },
                    "att_value_en[]": {
                    required: 'Attribute Value[EN] is required.'
                    },
                    "att_value_ar[]": {
                    required: 'Attribute Value[ER] is required.'
                    },
            },
            submitHandler: function (form) {
            $.ajax({
            type: "POST",
                    url: "{{route('save_attribute')}}",
                    data: $('#frm_create_attribute').serialize(),
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function () {
                    window.location.href = '{{route("attribute")}}';
                    }, 1000);
                    $("#frm_create_attribute")[0].reset();
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    $('button:submit').attr('disabled', false);
                    },
                    error: function (err) {
                    $('button:submit').attr('disabled', false);
                    }
            });
            return false;
            }
    });
    function deleteAttributes(id) {
    $.confirm({
    title: '<span class="small">Are you sure to delete this attribute?</span>',
            content: 'You wont be able to revert this',
            buttons: {
            Yes: function () {
            $.ajax({
            type: "POST",
                    url: "{{route('deleteAttribute')}}",
                    data: {id: id},
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                    if (data.status == 1) {
                    window.setTimeout(function () {
                    window.location.href = '{{route("attribute")}}';
                    }, 1000);
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function () {
                    console.log('cancelled');
                    }
            }
    });
    }
    $('.change-status').on('click', function () {
    var cust_id = $(this).data("id");
    var act_value = $(this).data("activate");
    $.confirm({
    title: act_value + ' Attributes',
            content: 'Are you sure to ' + act_value + ' the attribute?',
            buttons: {
            Yes: function () {
            $.ajax({
            type: "POST",
                    url: "{{route('activate_attribute')}}",
                    data: {id: cust_id},
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function () {
                    window.location.href = '{{route("attribute")}}';
                    }, 1000);
//                    window.location.reload();
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function () {
                    window.location.reload();
                    }
            }
    });
    });

</script>
@endpush