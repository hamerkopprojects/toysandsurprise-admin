<div class="modal-header">
    <h4 class="modal-title"><strong>
            Link Slider
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="msg_div"></div>
    <div class="row">
        <div class="col-md-12">
            <label class="radio-inline">
                <input type="radio" name="link_value" value="U" class="chk_link" {{ $row_data['web_flag'] == 'U' ? 'checked="checked"' : ''}}> Url
            </label>
            <label class="radio-inline ml-3">
                <input type="radio" name="link_value" value="C" class="chk_link" {{ $row_data['web_flag'] == 'C' ? 'checked="checked"' : ''}}> Category
            </label>
            <label class="radio-inline ml-3">
                <input type="radio" name="link_value" value="P" class="chk_link" {{ $row_data['web_flag'] == 'P' ? 'checked="checked"' : ''}}> Product
            </label>
        </div>
        <div class="col-md-12" style="{{ $row_data['web_flag'] == 'U' ? 'display:block' : 'display:none'}}" id="link_url">
            <label class="control-label">Url <span class="text-danger">*</span></label>
            <input class="form-control form-white" placeholder="Enter url" type="text" name="url" id="url" value="{{ isset($row_data['id']) ? $row_data['url'] : ''}}"/>
        </div>
        <div class="col-md-12" style="{{ $row_data['web_flag'] == 'C' ? 'display:block' : 'display:none'}}" id="link_cat">
            <label class="control-label">Category <span class="text-danger">*</span></label>
            <select class="form-control" name="category" id="category">
                <option value=""> Select Category </option>
                @foreach($cat_data as $cat_value)
                <option value="{{$cat_value['category_id']}}" {{ ($cat_value['category_id'] == $row_data['category']) ? 'selected' : '' }}>{{$cat_value['name']}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-12" style="{{ $row_data['web_flag'] == 'P' ? 'display:block' : 'display:none'}}" id="link_pdt">
            <label class="control-label">Product <span class="text-danger">*</span></label>
            <select class="form-control" name="product" id="product">
                <option value=""> Select Product </option>
                @foreach($pdt_data as $pdt_value)
                <option value="{{$pdt_value['product_id']}}" {{ ($pdt_value['product_id'] == $row_data['product']) ? 'selected' : '' }}>{{$pdt_value['name']}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="modal-footer">
    <input type="hidden" id="_slider_id" name="_slider_id" value="@if(isset($row_data['id'])) {{ $row_data['id'] }}@endif">
    <button type="submit" class="btn btn-info waves-effect waves-light save-categorys">
        Save
    </button>
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
</div>
<script>

    $(".chk_link").change(function () {
        if ($(this).val() == 'U') {
            $("#link_url").css({'display': 'block'});
            $("#link_cat").css({'display': 'none'});
            $("#link_pdt").css({'display': 'none'});
        } else if ($(this).val() == 'C') {
            $("#link_url").css({'display': 'none'});
            $("#link_cat").css({'display': 'block'});
            $("#link_pdt").css({'display': 'none'});
        } else
        {
            $("#link_url").css({'display': 'none'});
            $("#link_cat").css({'display': 'none'});
            $("#link_pdt").css({'display': 'block'});
        }
    });
</script>