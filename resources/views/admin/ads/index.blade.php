@extends('layouts.master')
@section('content-title')
ADS
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-tabs" role="tablist">
            {{-- <li class="nav-item"> <a class="nav-link active" href="#ads" role="tab" data-toggle="tab"><span class="hidden-sm-up"><i class="ti-id-badge"></i></span> <span class="hidden-xs-down">ADS</span></a> </li> --}}
            <li class="nav-item disabled"> <a class="nav-link" href="#slider" role="tab" data-toggle="tab"><span class="hidden-sm-up"><i class="ti-image"></i></span> <span class="hidden-xs-down">SLIDER</span></a> </li>
        </ul>
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th>Ads</th>
                                <th>App Images
                                    <p class="small">Max file size: 1024KB</p>
                                    <p class="small" style="margin-top:-30px">Supported formats: jpeg,png</p>
                                    <p class="small" style="margin-top:-30px">File dimension: 600 x 150 pixels</p>
                                </th>
                                <th class="cls_last_child">Website Images
                                    <p class="small">Max file size: 1024KB</p>
                                    <p class="small" style="margin-top:-30px">Supported formats: jpeg,png</p>
                                    <p class="small" style="margin-top:-30px">File dimension: 1200 x 350 pixels</p>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($ads_data) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($ads_data as $row_data)
                            @php
                            if($row_data->app_flag == 'C')
                            $app_link = $row_data->appcategory->lang[0]->name ?? '';
                            else
                            $app_link = $row_data->appproduct->lang[0]->name ?? '';
                            if($row_data->web_flag == 'U')
                            $web_link = $row_data->url;
                            elseif($row_data->web_flag == 'C')
                            $web_link = $row_data->webcategory->lang[0]->name ?? '';
                            else
                            $web_link = $row_data->webproduct->lang[0]->name ?? '';

                            @endphp
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->ad_type }}</td>
                                <td>

                                    <div class="cover-photo">
                                        <div id="app_photo_upload{{$row_data->id}}" class="add">+</div>
                                        <div id="app_image{{$row_data->id}}_loader" class="loader" style="display: none;">
                                            <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                                        </div>
                                        <div class="preview-image-container" @if(empty($row_data->app_image)) style="display:none;" @endif id="app_image{{$row_data->id}}_image_preview">
                                             <div class="scrn-link" style="position: relative;top: -20px;">
                                                <button type="button" class="scrn-img-close delete-img" data-id="{{$row_data->id}}" data-type="app_image">
                                                    <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                                </button>
                                                <img class="scrn-img" style="max-width: 200px" src="{{ !empty($row_data->app_image) ? url('uploads/'.$row_data->app_image) : '' }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <form method="POST" action="javascript:void(0);" enctype="multipart/form-data" id="frm_ad_image{{$row_data->id}}">
                                        <div class="control-fileupload" style="display: none;">
                                            <label for="app_image{{$row_data->id}}" data-nocap="1">Select cover photo:</label>
                                            <input type="file" id="app_image{{$row_data->id}}" name="app_image" data-imgw="600" data-imgh="150" data-id="{{$row_data->id}}"/>
                                        </div>
                                    </form>
                                    @if(!empty($row_data->app_image))
                                    <div class="mt-2">
                                        <a class="link_btn" title="Link Image" data-type="A" data-id="{{ $row_data->id }}"><i class="fa fa-link"></i> Link: {{isset($row_data->id) ? $app_link : '' }}</a>
                                        </font></a>
                                    </div>
                                    @endif
                                    @push('scripts')
                                    <script>
                                        $('#app_photo_upload{{$row_data->id}}').click(function(e) {
                                        $('#app_image{{$row_data->id}}').click();
                                        });
                                        $('#app_image{{$row_data->id}}').on('change', function() {
                                        const file = $(this)[0].files[0];
                                        var imgw = $(this).data('imgw');
                                        var imgh = $(this).data('imgh');
                                        var ad_id = $(this).data('id');
                                        readUrl(file, 'app_image', uploadFile, imgw, imgh, ad_id);
                                        //                                        uploadFile(file, 'app_image', {{$row_data->id}});
                                        });
                                    </script>
                                    @endpush
                                </td>
                                <td class="cls_last_child">
                                    <div class="cover-photo">
                                        <div id="web_photo_upload{{$row_data->id}}" class="add">+</div>
                                        <div id="web_image{{$row_data->id}}_loader" class="loader" style="display: none;">
                                            <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                                        </div>
                                        <div class="preview-image-container" @if(empty($row_data->web_image)) style="display:none;" @endif id="web_image{{$row_data->id}}_image_preview">
                                             <div class="scrn-link" style="position: relative;top: -20px;">
                                                <button type="button" class="scrn-img-close delete-img" data-id="{{$row_data->id}}" data-type="web_image">
                                                    <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                                </button>
                                                <img class="scrn-img" style="max-width: 200px" src="{{ !empty($row_data->web_image) ? url('uploads/'.$row_data->web_image) : '' }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <form method="POST" action="javascript:void(0);" enctype="multipart/form-data" id="frm_ad_web_image{{$row_data->id}}">
                                        <div class="control-fileupload" style="display: none;">
                                            <label for="web_image{{$row_data->id}}" data-nocap="1">Select cover photo:</label>
                                            <input type="file" id="web_image{{$row_data->id}}" name="web_image" data-imgw="1200" data-imgh="350" data-id="{{$row_data->id}}"/>
                                        </div>
                                    </form>
                                    @if(!empty($row_data->web_image))
                                    <div class="mt-2">
                                        <a class="link_btn" title="Link Image" data-type="W" data-id="{{ $row_data->id }}"><i class="fa fa-link"></i> Link: {{isset($row_data->id) ? $web_link : '' }}</a>
                                        </font></a>
                                    </div>
                                    @endif
                                    @push('scripts')
                                    <script>
                                        $('#web_photo_upload{{$row_data->id}}').click(function(e) {
                                        $('#web_image{{$row_data->id}}').click();
                                        });
                                        $('#web_image{{$row_data->id}}').on('change', function() {
                                        const file = $(this)[0].files[0];
                                        var imgw = $(this).data('imgw');
                                        var imgh = $(this).data('imgh');
                                        var ad_id = $(this).data('id');
                                        //                                        uploadFile(file, 'web_image', {{$row_data->id}});
                                        readUrl(file, 'web_image', uploadFile, imgw, imgh, ad_id);
                                        });
                                    </script>
                                    @endpush
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal pooup -->
<div class="modal fade none-border" id="formModal">
    <div class="modal-dialog modal-xl">
        <form id="frm_create_link" action="javascript:;" method="POST">
            <div class="modal-content">

            </div>
        </form>
    </div>
</div>
<!-- END MODAL -->
<div class="modal fade none-border" id="image-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crop Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="crop-images">
                <div class="row">
                    <div class="col-md-8">
                        <img id="cropper" src="" alt="" style="max-width: 100%">
                    </div>
                    <div class="col-md-4">
                        <div class="preview"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="crop" type="button" class="button button-primary button-xl">Crop & Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/lib/cropper/cropper.min.css') }}">
<style type="text/css">
    img {
        display: block;
        max-width: 100%;
    }
    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
</style>
@endpush
@push('scripts')

{{-- Sweet alert --}}
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
{{-- select2 --}}
<script src="{{ asset('assets/js/lib/cropper/cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/cropper/jquery-cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/custom_crop.js') }}"></script>
<script>
                                        $('.nav-tabs a').on('click', function (e) {
                                        var x = $(e.target).text();
                                        $.ajax({
                                        type: "GET",
                                                url: "{{route('ads_tab')}}",
                                                data: {'activeTab': x},
                                                success: function (result) {
                                                if (result.status == 'ads'){
                                                window.location.href = '{{route("ads")}}';
                                                } else{
                                                window.location.href = '{{route("slider")}}';
                                                }
                                                }
                                        });
                                        });
                                        $.ajaxSetup({
                                        headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        }
                                        });
                                        function uploadFile(file, type, id) {
                                        var formData = new FormData();
                                        formData.append('photo', file);
                                        formData.append('type', type);
                                        formData.append('id', id);
                                        $.ajax({
                                        url: "{{ route('upload_image')}}",
                                                type: 'POST',
                                                data: formData,
                                                processData: false, // tell jQuery not to process the data
                                                contentType: false, // tell jQuery not to set contentType
                                                beforeSend: function() {
                                                $(`#${type}${id}_loader`).show();
                                                $('#image-modal').modal('hide');
                                                },
                                                success: function(data) {
                                                if (data.status == 1) {
                                                window.setTimeout(function() {
                                                window.location.href = '{{route("ads")}}';
                                                }, 1000);
                                                Toast.fire({
                                                icon: 'success',
                                                        title: data.message
                                                });
                                                $(`#${type}${id}_loader`).hide();
                                                } else {
                                                Toast.fire({
                                                icon: 'error',
                                                        title: data.message
                                                });
                                                $(`#${type}${id}_loader`).hide();
                                                }
                                                }
                                        });
                                        }

                                        $('.delete-img').click(function(e) {
                                        let id = $(this).data('id');
                                        let type = $(this).data('type');
                                        Swal.fire({
                                        title: 'Are you sure to delete?',
                                                text: "You won't be able to revert this!",
                                                icon: 'warning',
                                                showCancelButton: true,
                                                confirmButtonColor: '#3085d6',
                                                cancelButtonColor: '#d33',
                                                confirmButtonText: 'Yes, delete it!'
                                        })
                                                .then((result) => {
                                                if (result.value) {
                                                $.ajax({
                                                url: "{{ route('detete_img') }}",
                                                        data: {
                                                        id: id,
                                                                type: type
                                                        },
                                                        type: 'POST',
                                                        success: function(data) {
                                                        if (data.status == 1) {
                                                        window.setTimeout(function() {
                                                        window.location.href = '{{route("ads")}}';
                                                        }, 1000);
                                                        Toast.fire({
                                                        icon: 'success',
                                                                title: data.message
                                                        });
                                                        } else {
                                                        Toast.fire({
                                                        icon: 'error',
                                                                title: data.message
                                                        });
                                                        }
                                                        }
                                                });
                                                }
                                                })
                                        });
                                        $('.link_btn').on('click', function() {
                                        var id = $(this).data("id");
                                        var type = $(this).data("type");
                                        $.ajax({
                                        type: "GET",
                                                url: "{{route('link_category')}}",
                                                data: {
                                                'id': id,
                                                        'type': type
                                                },
                                                success: function(data) {
                                                $('.modal-content').html(data);
                                                $('#formModal').modal('show');
                                                }
                                        });
                                        });
                                        $("#frm_create_link").validate({
                                        normalizer: function(value) {
                                        return $.trim(value);
                                        },
                                                rules: {
                                                url: {
                                                required: '#url:blank',
                                                        url: true
                                                },
                                                        category: {
                                                        required: '#category:blank'
                                                        },
                                                        product: {
                                                        required: '#product:blank'
                                                        },
                                                },
                                                messages: {
                                                url: {
                                                required: 'Url is required.'
                                                },
                                                        category: {
                                                        required: 'Category is required.'
                                                        },
                                                        product: {
                                                        required: 'Product is required.'
                                                        }
                                                },
                                                submitHandler: function(form) {
                                                $.ajax({
                                                type: "POST",
                                                        url: "{{route('add_link')}}",
                                                        data: $('#frm_create_link').serialize(),
                                                        dataType: "json",
                                                        headers: {
                                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                        },
                                                        success: function(data) {
                                                        if (data.status == 1) {
                                                        Toast.fire({
                                                        icon: 'success',
                                                                title: data.message
                                                        });
                                                        window.setTimeout(function() {
                                                        window.location.href = '{{route("ads")}}';
                                                        }, 1000);
                                                        $("#frm_create_link")[0].reset();
                                                        } else {
                                                        Toast.fire({
                                                        icon: 'error',
                                                                title: data.message
                                                        });
                                                        }
                                                        $('button:submit').attr('disabled', false);
                                                        },
                                                        error: function(err) {
                                                        $('button:submit').attr('disabled', false);
                                                        }
                                                });
                                                return false;
                                                }
                                        });
</script>
@endpush