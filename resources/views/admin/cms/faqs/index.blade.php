@extends('layouts.master')

@section('content-title')
FAQ'S
@endsection
@section('add-btn')
<button  class="btn btn-info create_btn" id="faq_add_btn">
    <i class="ti-plus"></i> Add New FAQ
</button>
@endsection
@section('content')
{{-- @include('admin.cms.faqs.popup') --}}
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row tablenav top text-right">
                    <div class="col-md-6 ml-0">
                        <form action="{{route('faq.get')}}" id="search-faq-form" method="get">
                        <input type="text" value ="{{$search_field ?? ''}}"class="form-control" id="search_field" name="search_field" placeholder="Search FAQS">
                            <input type="hidden" name="faq_select" id="faq_select">
                            {{-- <select class="form-control search_val" name="faq_select" >
                                <option value="">Search FAQ</option>
                                @foreach ($faqLang as $item)
                                    <option value="{{$item->faq_id}}">{{$item->question}}</option>
                                @endforeach
                            </select> --}}
                        </form>
                       
                    </div>
                    <div class="col-md-6 text-left">
                        <button type="button" onclick="event.preventDefault(); 
                        document.getElementById('search-faq-form').submit();" class="btn btn-info"><font style="vertical-align: inherit;">Search</font></button>
                        <a href="{{route('faq.get')}}" class="btn btn-default cancel_style">Reset</a>
                        
                    </div>
                </div>
            </div>
        </div>                  
            <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>

                                <th>Question (EN)</th>
                                <th>Answer (EN)</th>
                                <th class="right-align">Question (AR)</th>
                                <th class="right-align">Answer (AR)</th>
                                <th width="15%">Apps</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($faq) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($faq as $row_data)
                            <tr>
                                {{-- <th>{{ $i++ }}</th> --}}
                                <td>{{ $row_data->lang[0]->question ?? '' }}</td>
                                <td>{!! \Illuminate\Support\Str::limit($row_data->lang[0]->answer ?? '', 50, '...') !!}</td>
                                <td class="right-align">{{ $row_data->lang[1]->question ?? '' }}</td>
                                <td class="right-align">{!! \Illuminate\Support\Str::limit($row_data->lang[1]->answer ?? '', 50, '...') !!}</td>
                                <td>{{ $row_data->appType->pluck('name')->join(',') }}</td>
                                <td class="text-center">
                                    <button
                                        type="button"
                                        class="change-status btn btn-sm btn-toggle mr-md-4 ml-0 {{ $row_data['status']}}"
                                        data-toggle="button"
                                        data-id="{{ $row_data->id }}"
                                        data-activate="{{ $row_data['status']}}"
                                        aria-pressed="true"
                                        autocomplete="off"
                                        id="active_faq">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white edit_btn page_edit faq_edit"  title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white faq_id_del" data-id="{{ $row_data->id }}" title="Delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $faq->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- popup --}}
<div class="modal fade" id="faq-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Add FAQ</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="brdar_photo_loader" class="loader" style="display: none;"> </div>
            <div class="modal-body" data-no-padding="no-padding">
                <form id="add-faq" method="POST" action="#">
                    <input type="hidden" id="id_pg" name="id_pg">
                    @csrf
                    {{-- @method('PUT') --}}
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label>Question (EN) <sup class="star">*</sup></label>
                            <textarea class="form-control area" name="title_en" id="title_en" placeholder="Enter your question"></textarea>
                            <label class="title_en_error"></label>
                        </div>
                        <div class="col-md-6">
                            <label>Question (AR) <sup class="star">*</sup></label>
                            <textarea class="form-control area" name="title_ar" id="title_ar"
                                style="text-align:right !important" placeholder="أدخل سؤالك" ></textarea>
                            <label class="title_ar_error"></label>
                        </div>
                        <div class="col-md-6">
                            <label>Answer (EN) <sup class="star">*</sup></label>
                            <textarea class="form-control" id="summaryen" name="summaryen"placeholder="Enter answer"></textarea>
                            <label class="summaryen_error"></label>
                        </div>
                        <div class="col-md-6">
                            <label>Answer (AR) <sup class="star">*</sup></label>
                            <textarea class="form-control" id="summaryar" name="summaryar" placeholder="أدخل سؤالك"></textarea>
                            <label class="summaryar_error"></label>
                        </div>
                         <div class="col-md-12 form-group adbot01">
                           
                            <br/>
                            @foreach ($app_type as $item)
                                <input type="checkbox" name="site"  value="{{$item->id}}">{{$item->name}}
                            @endforeach
                            <label class="site_error"></label>
                        </div> 
                        <div class="siteErrot"> 
                           
                         </div> 
                        <div class="col-lg-12">
                            <div class="row 5">
                                <div class="col-md-12 text-md-left">
                                    <button type="submit"  class="btn btn-info waves-effect waves-light save_page">
                                        Save
                                    </button>
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
     input[type=checkbox],input[type=radio]{
        box-sizing: border-box;
        padding: 0;
        margin: 0 10px 0 5px;
    }
    .cancel_style{
        margin-left: 20px;
    }
    #site-error{
    float:right;
    margin-right: 260px;
    margin-top: 10px;

}
.area{
        height: 80px;
    }
.star{
    color:red;
} 

.loader{
    position: fixed;
    top:0px;
    right:0px;
    width:100%;
    height:100%;
    background-color:#eceaea;
    background-image:url('../assets/images/loader.gif');
    background-repeat:no-repeat;
    background-size: 50px;
    z-index:10000000;
    opacity: 0.4;
    filter: alpha(opacity=40);
}
</style>

@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdn.ckeditor.com/4.14.0/basic/ckeditor.js"></script>

<script>
 
CKEDITOR.replace('summaryen', {
    removePlugins: 'link',
    placeholder:'Enter answer',
    // fullPage:true
} );
    CKEDITOR.replace('summaryar', {
    removePlugins: 'link',
    contentsLangDirection: 'rtl'
    } );

</script>
<script>
   
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});
        CKEDITOR.instances.summaryen.on('change', function() { 	
    if(CKEDITOR.instances.summaryen.getData().length >  0) {
     $('label[for="summaryen"]').hide();
    }
});
CKEDITOR.instances.summaryar.on('change', function() { 	
    if(CKEDITOR.instances.summaryar.getData().length >  0) {
     $('label[for="summaryar"]').hide();
    }
});
        $('#faq_add_btn').on('click',function(e){
            $('#add-faq').trigger("reset")
            CKEDITOR.instances['summaryen'].setData(" ");
            CKEDITOR.instances['summaryar'].setData(" ");
            $('#exampleModalLabel').html('Add FAQ');
            $('#faq-popup').modal({
                show:true
            })
        })

        $('.faq_edit').on('click',function(e){
            e.preventDefault();
            $('#exampleModalLabel').html('Edit FAQ');

            page = $(this).data('id')
            var url = "faq/edit/";
        
            $.get(url  + page, function (data) {
                console.log(data.page.app_type);
                $('#title_en').val(data.page.lang[0].question);
                $('#title_ar').val(data.page.lang[1].question ?? '');
                $('#id_pg').val(data.page.id)
                CKEDITOR.instances['summaryen'].setData(data.page.lang[0].answer);
                CKEDITOR.instances['summaryar'].setData(data.page.lang[1].answer);
                $("input[name='site']").prop('checked', false); 
                $.each(data.page.app_type, function(i, val){
                    // console.log(val);
                    $("input[name='site'][value='" + val.id + "']").prop('checked', true);

                });
                $('#faq-popup').modal({
                    show: true

                });
            }) 
        })

        $('.save_page').on('click',function(e){
          
            // e.preventDefault();
            // validation();
            console.log("inside")
            $("#add-faq").validate({
                ignore: [],
                rules: {
                title_en  : {        
                    required: true,         
                },
                title_ar: {          
                    required: true,
                },
                summaryen :{
                    required: function(textarea) {
                     CKEDITOR.instances[textarea.id].updateElement();
                        var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                     return editorcontent.length === 0;
                        //  required:  
                        //  function() {
                        //     var messageLength = CKEDITOR.instances['summaryen'].getData().replace(/<[^>]*>/gi, '').length;
                        //     return messageLength === 0;
                         }
                       
                    },
                summaryar :{
                        //  required: function() 
                        // {
                        //  CKEDITOR.instances.summaryar.updateElement();
                        required: function(textarea) {
                            CKEDITOR.instances[textarea.id].updateElement();
                            var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
                            return editorcontent.length === 0;
                        },
                    },
                'site': {        
                    required: true,         
                },
                },
                messages: {               
                title_en: {
                      required:"Question (EN) required",
                      
                      },
                title_ar: {
                      required: "Question (AR) required",
                      },
                summaryen: {
                      required: "Answer (EN) required",
                      },
                summaryar: {
                      required: "Answer (AR) required",
                      },
                'site':{
                          required:"Select atleast one user type"
                      }

                 },
                 errorPlacement: function(error, element) {

                    //  console.log('error',#site-error.error);
                    if (element.attr("name") == "site" ) {
                        $(".site_error").html(error);
                    }
                    if (element.attr("name") == "summaryar" ) {
                        $(".summaryar_error").html(error);
                    }
                    if (element.attr("name") == "summaryen" ) {
                        $(".summaryen_error").html(error);
                    }
                    if (element.attr("name") == "title_ar" ) {
                        $(".title_ar_error").html(error);
                    }
                    if (element.attr("name") == "title_en" ) {
                        $(".title_en_error").html(error);
                    }
                    
                 },
                 submitHandler: function(form) {
                    $(`#brdar_photo_loader`).show();
                    let edit_val=$('#id_pg').val();
                    var test = new Array();
                    $("input[name='site']:checked").each(function() {
                            test.push($(this).val());
                    });
                    if(edit_val){
                    $.ajax({
                        type:"POST",
                        url: "{{route('faq.update')}}",
                        data:{ 
                            title_en:$('#title_en').val(),
                            title_ar:$('#title_ar').val(),
                            content_en: CKEDITOR.instances['summaryen'].getData(),
                            content_ar:CKEDITOR.instances['summaryar'].getData(),
                            app_type:test,
                            id:edit_val
                   
                        },
                        success: function(result){
                            $(`brdar_photo_loader`).hide();
                            console.log(result.msg)
                            $('#faq-popup').modal('hide')
                                Toast.fire({
                                icon: 'success',
                                title: 'FAQ updated successfully'
                                });
                               
                                window.location.href = '{{route("faq.get")}}';
                         }       
                    });
                }else{
                    $.ajax({
                        type:"POST",
                        url: "{{route('faq.store')}}",
                        data:{ 
                            title_en:$('#title_en').val(),
                            title_ar:$('#title_ar').val(),
                            content_en: CKEDITOR.instances['summaryen'].getData(),
                            content_ar:CKEDITOR.instances['summaryar'].getData(),
                            app_type:test
                   
                        },
    
                        success: function(result){
                            $(`brdar_photo_loader`).hide();
                            console.log(result.msg)
                            $('#faq-popup').modal('hide')
                            if(result.msg ==="success")
                                   {
                                    Toast.fire({
                                    icon: 'success',
                                    title: 'FAQ added successfully'
                                    });
                                    window.location.href = '{{route("faq.get")}}';
                                   }else{
                                    $(`brdar_photo_loader`).hide();
                                    Toast.fire({
                                    icon: 'error',
                                    title: 'some errors'
                                    });
                                   }
                             }
                         }) ;
                    }
                
                }
            });
            
           
        });

        $('.change-status').on('click',function(){
            
            activate = $(this).data('activate');
            console.log(activate);
            $.ajax({
                        type:"POST",
                        url: "{{route('faq.status.update')}}",
                        data:{ 
                           status:activate,
                           id: $(this).data('id')
                   
                        },
                        success: function(result){                        
                                    Toast.fire({
                                    icon: 'success',
                                    title: 'Status updated successfully'
                                    });
                                    window.location.reload();
                                   
                        }
                })
        });
        $('.faq_id_del').on('click',function(){
           
            Swal.fire({  
                title: 'Are you sure to delete?',  
                text: "You won't be able to revert this!",  
                icon: 'warning',  
                showCancelButton: true,  
                confirmButtonColor: '#3085d6',  
                cancelButtonColor: '#d33',  
                confirmButtonText: 'Yes, delete it!'
            })
            .then((result) => {  
                if (result.value) {    
                    $.ajax({
                        url: "{{route('faq.delete')}}" ,
                        type: 'POST',
                        data:{
                            id:$(this).data('id')
                        },
                        success: function(data) {
                            Toast.fire({
                                    icon: 'success',
                                    title: 'Deleted successfully'
                                    });
                                    window.location.reload();
                        }
                    });
                }
            })
        });
        $( "#search_field" ).autocomplete({
            source: function( request, response ) {
        $.ajax( {
          url: "{{route('faq.search')}}",
          method:'post',
          data: {
            search: request.term
          },
          success: function( data ) {
            response( data );
          }
        } );
      },
      minLength: 1,
      select: function( event, ui ) {
        $('#search_field').val(ui.item.label); // display the selected text
        $('#faq_select').val(ui.item.value); // save selected id to input
           return false;
      }
    });

   

    </script>
@endpush