@extends('layouts.master')
@section('content-title')
PROMOTIONAL PRODUCTS
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">

                <form id="frm_promo_product" action="javascript:;" method="POST">
                    @csrf
                    <div class="col-md-12">
                        <select class="js-example-basic-multiple" id="tag" name="tag[]" multiple="multiple">
                            <option value="a">please select</option>
                        
                          </select>
                        <div class="svbtn">
                            <button type="submit" class="btn btn-info "id="save_data">
                                SAVE
                            </button>

                        </div>


                </form>
            </div>
        </div>
    </div>
</div>

<style>
    .svbtn{
        padding-left: 15px;
         margin-top: 30px;
    }
    .select2-container--default .select2-selection--multiple {
    width: 100%;
    height: 100%;
}

select + .select2-container {
  width: 100% !important;
}
</style>

@endsection

@push('scripts')
<script>
    $(document).ready(function() {

        var url = "promoproducts";
        $.get(url, function(data) {
        
            $.each( data.promocode, function( key, value ) {
                $('.js-example-basic-multiple').append("<option value='"+value.promotionalproduct.product_id+"' selected>"+value.promotionalproduct.name+"</option>");
                $('.js-example-basic-multiple').trigger('change');
           });
        });

    $('.js-example-basic-multiple').select2({
        ajax: {
          url: "{{ route('autocomplete') }}",
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results:  $.map(data, function (item) {
                  console.log(data);
                    return {
                        text: item.name,
                        id: item.product_id
                    }
                })
            };
          },
          cache: true
        }
      });
    });
    $("#frm_promo_product").submit(function (e) {
        var tag = $('#tag').val();
        
    $.ajax({
        type: "POST",
        url: "{{ route('promoproduct.store') }}",
        data: {
            tag: tag
        },
        dataType: "json",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            if (data.status == 1) {
                        Toast.fire({
                            icon: 'success',
                            title: data.message
                        });
                        window.setTimeout(function() {
                            window.location.reload();
                        }, 1000);
                        $("#frm_promo_product")[0].reset();
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
        }
    });

});
  
</script>
@endpush