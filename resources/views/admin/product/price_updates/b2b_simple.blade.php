@extends('layouts.master')
@section('content-title')
Price Update - Step 2
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <p><strong>Total records to Import: {{ $total_count }}</strong></p>
        <p>Ready for stock update: {{ $success_count }} </p>
        <p>Errors: {{$error_count}}</p>
        <div class="card">
            <div class="card-body">
                <form class="frm-search" id="posts-filter" method="get" action="">
                    <div class="row tablenav top text-right">
                        <div class="col-md-8 ml-0">
                            <input class="form-control search_txt" type="text" name="search" value="{{ $search }}" placeholder="Search by SKU ">
                        </div>
                        <div class="col-md-3 text-left">
                            <button type="submit" class="btn btn-info"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Search</font></font></button>
                            <a href="{{route('price_b2bsimple')}}" class="btn btn-default btn-reset">Reset</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-12 text-right">
            <a href="{{route('price_update')}}?action=simpleb2b" class="btn btn-default ml-2"><font style="vertical-align: inherit;">Fix the Excel and upload again</font></a>
            <a href="javascript:void(0);" class="btn btn-info ml-2 cls_import_confirm"><font style="vertical-align: inherit;">Continue</font></a>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th>SKU</th>
                                <th>No.of.item</th>
                                <th>Type</th>
                                <th>Price</th>
                                <th>Discount Price</th>
                                <th class="cls_last_child"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($result_data) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($result_data as $row_data)
                            <tr>
                                <th>{{$i++}}</th>
                                <td>{{$row_data->sku}}</td>
                                <td>{{$row_data->no_of_items}}</td>
                                <td>{{$row_data->type}}</td>
                                <td>{{$row_data->price}}</td>
                                <td>{{$row_data->discount_price}}</td>
                                @if ($row_data->error_flag=="N" && $row_data->duplicate_flag == "N")
                                <td class="cls_last_child" style="background:green;color:white;">Ready for Price Update</td>
                                @elseif($row_data->error_flag=="Y")
                                <td class="cls_last_child" style="background:red;color:white;">Errors
                                    <a href="#" class="btn btn-sm btn-success text-white error-data float-right" title="View Log" data-id="{{$row_data->id}}"><i class="fa fa-eye"></i></a></td>
                                    @elseif($row_data->duplicate_flag=="Y")
                                    <td class="cls_last_child" style="background:orange;color:white;">Duplicate</td>     
                                @endif
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="12" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $temp_data->appends(request()->query())->links() }}
                </div>
            </div>
        </div>
        <div class="col-md-12 text-right">
            <a href="{{route('price_update')}}?action=simpleb2b" class="btn btn-default ml-2"><font style="vertical-align: inherit;">Fix the Excel and upload again</font></a>
            <a href="javascript:void(0);" class="btn btn-info ml-2 cls_import_confirm"><font style="vertical-align: inherit;">Continue</font></a>
        </div>
    </div>
</div>
<!-- Modal pooup -->
<div class="modal fade none-border" id="formModal">
    <div class="modal-dialog modal-lg" style="max-height:85%;  margin-top: 50px; margin-bottom:50px;max-width: 100%">
        <div class="modal-content">

        </div>
    </div>
</div>
<!-- END MODAL -->
@endsection
@push('scripts')
<script>
    $(window).load(function () {
        $('.loading_box').hide();
        $('.loading_box_overlay').hide();
    });
    $('.error-data').on('click', function () {
        var id = $(this).data("id");
        $.dialog({
            title: 'Error: Data with Errors',
            content: "url:{{route('b2b_simple_errors')}}?id=" + id,
            //                            animation: 'scale',
            columnClass: 'large',
            //                            closeAnimation: 'scale',
            backgroundDismiss: true,
        });
    });

   
    $('.cls_import_confirm').on('click', function () {
        $('.loading_box').show();
        $('.loading_box_overlay').show();
        $.ajax({
            type: "GET",
            url: "{{route('confirm_import_simpleb2b')}}",
            success: function (data) {
                if (data.status == 1) {
                    $('.loading_box').hide();
                    $('.loading_box_overlay').hide();
                    Toast.fire({
                        icon: 'success',
                        title: data.message
                    });
                    window.setTimeout(function () {
                        window.location.href = '{{route("imported_simpleB2b")}}';
                    }, 1000);
                }
            }
        });
    });

</script>
@endpush