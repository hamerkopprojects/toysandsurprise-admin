<div class="table-responsive">
    <table class="table table-striped">
        <tbody>
            @if(count($errors)>0)
            @php
            $i = 1;
            @endphp
            @foreach($errors as $key=>$val)
            @if($val)
            <tr>
                <td>{{$i++}}</td>
                <td class="cls_last_child">{{$val}}</td>
            </tr>
            @endif
            @endforeach
            @else
            @endif
        </tbody>
    </table>
</div>