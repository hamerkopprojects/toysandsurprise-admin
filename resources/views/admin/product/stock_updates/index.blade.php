@extends('layouts.master')
@section('content-title')
STOCK UPDATE - STEP1
@endsection
@section('add-btn')
<a href="{{ route('products') }}">
    <font style="vertical-align: inherit;">Back
    </font>
</a>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="frm-excel-import" action="" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-12">
                            <p><strong>Import Stock List</strong></p>
                            <p class="small"><b>Insructions </b><br />
                                1. Supported file formats: <b>xlsx</b><br>
                                2. Max. file size: <b>2 MB</b><br>
                                3. Follow the excel fields and order of fields in the format
                            </p>
                            <hr>
                        
                            @if($type=='simpleb2c')
                            <a href="{{ asset('sample/simple_stock/Emtyaz-Simple_stock-Sample.xlsx') }}" class="cl-btn-dwnld float-right" style="color: blue; padding-top: 18px;"><i class="fa fa-download"></i> Download the sample Excel file</a>
                            @elseif($type=='complexb2c')
                            <a href="{{ asset('sample/complex_stock/Emtyaz-Complex_stock-Sample.xlsx') }}" class="cl-btn-dwnld float-right" style="color: blue; padding-top: 18px;"><i class="fa fa-download"></i> Download the sample Excel file</a>
                            @endif
                            <div class="col-md-8 custmfl">
                                <div class="custom-file">
                                    <input type="hidden" name="type" id="type" value="{{$type}}">
                                    <input type="file" class="custom-file-input" name="select_file" id="select_file">
                                    <label class="custom-file-label" for="customFile">Choose the excel file to import stock list</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 text-left">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-info" id="btn-import-submit">Import</button>
                            <button type="button" class="btn btn-default" onclick="location.href ='{{route('products')}}'">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<style>
    .custmfl{
    padding-left: 0px!important;
    }
</style>
@push('scripts')
<script>
    $("#frm-excel-import").submit(function () {
        $('.loading_box').show();
        $('.loading_box_overlay').show();
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $("#btn-import-submit").on('click', function () {
      
        $('.loading_box').show();
        $('.loading_box_overlay').show();
        var form_data = new FormData($('form')[0]);
        console.log(form_data);
        $.ajax({
            type: "POST",
            url: "{{route('stock_import_submit')}}",
            data: form_data,
            dataType: "json",
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function () {
                $('#btn-import-submit').button('loading');
            },
            complete: function () {
                $('#btn-import-submit').button('reset');
            },
            success: function (data) {
                if (data.status == 1 && data.type == "simpleb2c") {
                    window.location.href = '{{route("b2csimple")}}';
                }
                else if (data.status == 1 && data.type == "complexb2c") {
                    window.location.href = '{{route("b2ccomplex")}}';
                } else {
                    $('.loading_box').hide();
                    $('.loading_box_overlay').hide();
                    Toast.fire({
                        icon: 'error',
                        title: data.message
                    });
                }
            },
            error: function () {}
        });
        return false;
    });
</script>
@endpush