@extends('layouts.master')
@section('content-title')
ADD PRODUCTS TO CATEGORY   '{{ strtoupper($cat_name['name']) }}'
@endsection
@section('add-btn')
<a href="{{ route('category') }}">
    <font style="vertical-align: inherit;">Back
    </font>
</a>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">

                <form id="frm_pdt_cat" action="javascript:;" method="POST">
                    <div class="row">
                        <div class= "col-md-12">
                            <label class="control-label"></label>
                            <select class="select2_multiple form-control" name="products[]" multiple="multiple">
                                @foreach($products as $row_data)
                                <option value="{{$row_data->id}}">{{$row_data->lang[0]->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class= "col-md-12">
                            <input type="hidden" name='cat_id' value="{{$cat_id}}">
                            <button type="submit" class="btn btn-info">
                                SAVE
                            </button>
                        </div>

                    </div>

                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th>Product Name</th>
                                <th width="10%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($sel_products) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($sel_products as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ !empty($row_data->products) ? $row_data->products->lang[0]['name'] : '' }}</td>
                                <td class="text-center">
                                    <a class="btn btn-sm btn-danger text-white" title="Delete Customer" onclick="deleteProducts({{ $row_data->product_id }}, {{$cat_id}})"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $sel_products->appends(request()->input())->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(".select2_multiple").select2({
    placeholder: "Select Products",
            allowClear: true
    });
    $(".select2-selection--multiple").addClass("form-control");
    $(".select2-selection--multiple").css({"border": "1px solid #ccc", "border-radius": "0", "padding": "3px 9px 20px 0px", "height": "auto"});
    $("#frm_pdt_cat").validate({
    normalizer: function (value) {
    return $.trim(value);
    },
            rules: {
            "products[]": {
            required: true,
            },
            },
            messages: {
            "products[]": {
            required: 'Select atleast one products'
            },
            },
            submitHandler: function (form) {
            $.ajax({
            type: "POST",
                    url: "{{route('save_category_products')}}",
                    data: $('#frm_pdt_cat').serialize(),
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                    if (data.status == 1) {
                    $("#frm_pdt_cat")[0].reset();
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function() {
                    window.location.reload();
                    }, 1000);
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            return false;
            }
    });
    function deleteProducts(id, cat_id) {
    $.confirm({
    title: '<span class="small">Are you sure to delete this product?</span>',
            content: 'You wont be able to revert this',
            buttons: {
            Yes: function() {
            $.ajax({
            type: "POST",
                    url: "{{route('delete_cat_products')}}",
                    data: {
                    id: id, cat_id : cat_id
                    },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function() {
                    window.location.reload();
                    }, 1000);
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function() {
                    console.log('cancelled');
                    }
            }
    });
    }
</script>
@endpush