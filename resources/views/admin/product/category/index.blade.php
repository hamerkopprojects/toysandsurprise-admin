@extends('layouts.master')
@section('content-title')
CATEGORY
@endsection
@section('add-btn')
<button class="btn btn-primary" id="add_new_category">
    <i class="ti-plus"></i> Add New Category
</button>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="posts-filter" method="get" action="{{ route('category') }}">
                    <div class="row tablenav top text-right">
                        <div class="col-md-4 ml-0">
                            <input class="form-control" type="text" name="search" value="{{$search}}" placeholder="Search by Category Name/ Category ID">
                        </div>
                        <!-- <div class="col-md-3 ml-0">
                            <input class="form-control" type="text" name="search_catid" value="{{$search_catid}}" placeholder="Search by Category ID">
                        </div> -->
                        <div class="col-md-3 ml-0">
                            <select class="form-control search_val search_wid" name="select_maincategory" id="select_maincategory">
                                <option value="">Search by Parent Category</option>
                                @foreach($main_cat as $categorylist)
                                <option value="{{$categorylist->id}}" {{ $categorylist->id ==  $cat_id  ? 'selected' : '' }}>{{$categorylist->lang[0]->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3 text-left">
                            <button type="submit" class="btn btn-info">
                                <font style="vertical-align: inherit;">Search</font>
                            </button>
                            <a href="{{ route('category') }}" class="btn btn-default reset_style">Reset</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th>Category ID</th>
                                <th>Category Name (EN)</th>
                                <th>Category Name (AR)</th>
                                <th>Parent Category</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($category) > 0)
                            @php
                            $i = 1;
                            @endphp

                            @foreach ($category as  $key => $row_data)
                            <tr>
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->category_unique_id }}</td>
                                <td>{{ $row_data->lang[0]->name ?? '' }}</td>
                                <td class="text-right">{{ $row_data->lang[1]->name ?? '' }}</td>
                                <td>
                                    {{ substr($row_data->getParentsNames(),3)}}
                                 
                                </td>
                                <td class="text-left">
                                    <a class="btn btn-sm btn-success text-white edit_btn upload_image" title="Edit" data-image-en ="{{ $row_data->lang[0]->image_path}}" data-image-ar ="{{ $row_data->lang[1]->image_path}}"  data-banner-en ="{{ $row_data->lang[0]->banner_image}}" data-banner-ar ="{{ $row_data->lang[1]->banner_image}}"  data-id="{{ $row_data->id}}"><i class="fa fa-camera"></i></a>

                                    <a href="{{ route('add_category_products', ['cat_id' => $row_data->id]) }}" class="btn btn-sm btn-info text-white" title="Add products to category"><i class="fa fa-plus-square"></i></a>
                                    
                                    <a href="{{ route('add_category_attribute', ['cat_id' => $row_data->id]) }}" class="btn btn-sm btn-info text-white" title="Add attributes to category"><i class="fa fa-plus-circle"></i></a>

                                    {{-- <a class="btn btn-sm {{ $row_data->is_featured == 'yes' ? 'btn-fea' : 'btn-danger' }} text-white set_featured" title="{{ $row_data->is_featured == 'yes' ? 'Remove from featured category' : 'Make this featured category' }}" data-status="{{$row_data->is_featured}}" data-id="{{ $row_data->id }}"><i class="fa fa-star"></i></a> --}}

                                    <a class="btn btn-sm btn-success text-white edit_btn edit_user" title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                   
                                    <button type="button" class="change-status btn btn-sm btn-toggle mr-md-4 ml-0 {{ $row_data['status'] == 'active' ? 'active' : ''}}" data-toggle="button" data-id="{{ $row_data->id }}" data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" aria-pressed="true" autocomplete="off">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-danger text-white" title="Delete Category" onclick="deleteCategory({{ $row_data->id }})"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">

                </div>
            </div>
        </div>
    </div>
</div>

{{-- Pop Up --}}
<div class="modal fade" id="user_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="name_change"><strong>
                        ADD PRODUCT CATEGORY
                    </strong></h4>
                <a href="{{ route('category') }}" class="close">x</a>
            </div>
            <form id="user_form" action="javascript:;" enctype="multipart/form-data" method="POST">
                <div class="modal-body">
                    <div class="msg_div"></div>
                    <div class="row">
                        <input type="hidden" name="user_unique" id='user_unique'>
                        <div class="col-md-6">
                            <label class="control-label">Category Name (EN) <span class="text-danger">*</span></label>
                            <input class="form-control form-white" placeholder="Enter category name" type="text" name="category_name_en" id="category_name_en" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Category Name (AR) <span class="text-danger">*</span></label>
                            <input class="form-control form-white text-right" placeholder="أدخل اسم الفئة" type="text" id="category_name_ar" name="category_name_ar" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Select parent category </label>
                            <select class="form-control" name="category" id="category">

                                <option value=""> Select Category </option>
                                @foreach($main_cat as $categorylist)
                                <option value="{{$categorylist->id}}">{{$categorylist->lang[0]->name}}</option>
                                @if(count($categorylist->subcategory))
                                @include('admin.product.category.subCategoryList',['subcategories' => $categorylist->subcategory])
                                @endif
                                @endforeach

                            </select>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info waves-effect waves-light save-categorys" id="save_data">
                        ADD
                    </button>
                    <a href="{{ route('category') }}" class="btn btn-default reset_style">Cancel</a>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- end popup --}}


{{-- Image upload popup --}}
<div class="modal fade" id="imageupload_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg popup-promo" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="name_change"><strong>
                        ADD CATEGORY IMAGES
                    </strong></h4>
                <a href="{{ route('category') }}" class="close">x</a>
            </div>
                <div class="modal-body">
                    <div class="msg_div"></div>
                    <div class="row">
                      
                        <div class="col-md-6">
                            <label class="control-label">Category Image (EN)</label>
                            <div class="cover-photo" id="pdt_caten_image">
                                <div id="caten-photo-upload" class="add">+</div>
                                <div id="caten_photo_loader" class="loader" style="display: none;">
                                    <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                                </div>
                                <div class="preview-image-container" id="caten_photo_image_preview">
                                     <div class="scrn-link" style="position: relative;top: -20px;">
                                        <button type="button" class="scrn-img-close delete-caten" data-type="image_en" id="delete-caten" data-id="">
                                            <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                        </button>
                                        <img class="scrn-img" id= "scrn-img-en" style="max-width: 200px" src="" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="control-fileupload" style="display: none;">
                                <label for="caten_photo" data-nocap="1">Select cover photo:</label>
                                <input type="file" id="caten_photo" name="caten_photo" data-id="" data-imgw="180" data-imgh="180"/>
                            </div>
                            <div class="mt-2 small">
                                <p>Max file size: 1024KB<br />
                                    Supported formats: jpeg,png<br />
                                    File dimension: 180 x 180 pixels<br />
                                </p>
                                <span style="display: none;" class="error" id="caten-photo-error">Error</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Category Image (AR) </label>
                            <div class="cover-photo" id="pdt_catar_image">
                                <div id="catar-photo-upload" class="add">+</div>
                                <div id="catar_photo_loader" class="loader" style="display: none;">
                                    <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                                </div>
                                <div class="preview-image-container" id="catar_photo_image_preview">
                                     <div class="scrn-link" style="position: relative;top: -20px;">
                                        <button type="button" class="scrn-img-close delete-catar" data-type="image_ar" id="delete-catar" data-id="">
                                            <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                        </button>
                                        <img class="scrn-img" id= "scrn-img-ar" style="max-width: 200px" src="" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="control-fileupload" style="display: none;">
                                <label for="catar_photo" data-nocap="1">Select cover photo:</label>
                                <input type="file" id="catar_photo" name="catar_photo" data-id="" data-imgw="180" data-imgh="180"/>
                            </div>
                            <div class="mt-2 small">
                                <p>Max file size: 1024KB<br />
                                    Supported formats: jpeg,png<br />
                                    File dimension: 180 x 180 pixels<br />
                                </p>
                                <span style="display: none;" class="error" id="catar-photo-error">Error</span>
                            </div> 
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Banner Image (EN) </label>
                            <div class="cover-photo" id="pdt_bnren_image">
                                <div id="bnren-photo-upload" class="add">+</div>
                                <div id="bnren_photo_loader" class="loader" style="display: none;">
                                    <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                                </div>
                                <div class="preview-image-container" id="bnren_photo_image_preview">
                                     <div class="scrn-link" style="position: relative;top: -20px;">
                                        <button type="button" class="scrn-img-close delete-bnren" data-type="banner_en" id="delete-bnren" data-id="">
                                            <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                        </button>
                                        <img class="scrn-img" id= "scrn-img-bnren" style="max-width: 200px" src="" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="control-fileupload" style="display: none;">
                                <label for="bnren_photo" data-nocap="1">Select cover photo:</label>
                                <input type="file" id="bnren_photo" name="bnren_photo" data-id="" data-imgw="1200" data-imgh="300"/>
                            </div>
                            <div class="mt-2 small">
                                <p>Max file size: 1024KB<br />
                                    Supported formats: jpeg,png<br />
                                    File dimension: 1200 x 300 pixels<br />
                                </p>
                                <span style="display: none;" class="error" id="bnren-photo-error">Error</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Banner Image (AR) </label>
                            <div class="cover-photo" id="pdt_bnrar_image">
                                <div id="bnrar-photo-upload" class="add">+</div>
                                <div id="bnrar_photo_loader" class="loader" style="display: none;">
                                    <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                                </div>
                                <div class="preview-image-container" id="bnrar_photo_image_preview">
                                     <div class="scrn-link" style="position: relative;top: -20px;">
                                        <button type="button" class="scrn-img-close delete-bnrar" data-type="banner_ar" id="delete-bnrar" data-id="">
                                            <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                        </button>
                                        <img class="scrn-img" id= "scrn-img-bnrar" style="max-width: 200px" src="" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="control-fileupload" style="display: none;">
                                <label for="bnrar_photo" data-nocap="1">Select cover photo:</label>
                                <input type="file" id="bnrar_photo" name="bnrar_photo" data-id="" data-imgw="1200" data-imgh="300"/>
                            </div>
                            <div class="mt-2 small">
                                <p>Max file size: 1024KB<br />
                                    Supported formats: jpeg,png<br />
                                    File dimension: 1200 x 300 pixels<br />
                                </p>
                                <span style="display: none;" class="error" id="bnrar-photo-error">Error</span>
                            </div>
                        </div>       
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="{{ route('category') }}" class="btn btn-info waves-effect waves-light save-categorys">Done</a>
                    <a href="{{ route('category') }}" class="btn btn-default reset_style">Cancel</a>
                </div>
        </div>
    </div>
</div>

{{-- end popup --}}
<!-- For cropping -->
<div class="modal none-border" id="image-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crop Image</h5>
                <button type="button" class="close" data-dismiss-modal="modal2" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="crop-images">
                <div class="row">
                    <div class="col-md-6">
                        <img id="cropper" src="" alt="" style="max-width: 100%">
                    </div>
                    <div class="col-md-4">
                        <div class="preview"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="crop" type="button" class="button button-primary button-xl">Crop & Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss-modal="modal2">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<style>
    .reset_style {
        margin-left: 15px;
    }
    .search_wid{
        width: 234px;
    }
   
    .btn-fea{
        background-color: #87b23e;
    }
    .validation
    {
      color: red;
     
    }
    img {
        display: block;
        max-width: 100%;
    }
    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
    .popup-promo {
        max-height: 100%;
        max-width: 60%;
    }

    #formModal { overflow-y:auto; }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/lib/cropper/cropper.min.css') }}">
@endpush
@push('scripts')
{{-- Sweet alert --}}

<script src="{{ asset('assets/js/lib/cropper/cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/cropper/jquery-cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/custom_crop.js') }}"></script>
<script>
    $('#add_new_category').on('click', function() {
        $('#name_change').html('Add Category');
        $('#save_data').text('Add').button("refresh");
        $("#user_form")[0].reset();
        $('#user_popup').modal({
            show: true
        });
    })
   
    $('.upload_image').on('click', function() {
        page = $(this).data('id')
        img_en = $(this).data('image-en')
        img_ar = $(this).data('image-ar')
        banner_en = $(this).data('banner-en')
        banner_ar = $(this).data('banner-ar')
      

        if (img_en) {
            document.getElementById("caten_photo_image_preview").style.display = "block";
            var uploadsUrl = "<?php echo asset('/uploads') ?>";
            var imgurl = uploadsUrl + '/' + img_en;
            $('#scrn-img-en').attr("src", imgurl);
            $('#caten_photo').data('id',page);
            $('#delete-caten').data('id',page);
        }else{
            document.getElementById("caten_photo_image_preview").style.display = "none"; 
            $('#caten_photo').data('id',page);
        $('#delete-caten').data('id',page);  
        }
        if (img_ar) {
            document.getElementById("catar_photo_image_preview").style.display = "block";
            var uploadsUrl = "<?php echo asset('/uploads') ?>";
            var imgurl = uploadsUrl + '/' + img_ar;
            $('#scrn-img-ar').attr("src", imgurl);
            $('#catar_photo').data('id',page);
        $('#delete-catar').data('id',page);
        }else{
            document.getElementById("catar_photo_image_preview").style.display = "none";   
            $('#catar_photo').data('id',page);
        $('#delete-catar').data('id',page);
        }
        if (banner_en) {
            document.getElementById("bnren_photo_image_preview").style.display = "block";
            var uploadsUrl = "<?php echo asset('/uploads') ?>";
            var imgurl = uploadsUrl + '/' + banner_en;
            $('#scrn-img-bnren').attr("src", imgurl);
            $('#bnren_photo').data('id',page);
        $('#delete-bnren').data('id',page);
        }else{
            document.getElementById("bnren_photo_image_preview").style.display = "none"; 
            $('#bnren_photo').data('id',page);
        $('#delete-bnren').data('id',page);  
        }

        if (banner_ar) {
            document.getElementById("bnrar_photo_image_preview").style.display = "block";
            var uploadsUrl = "<?php echo asset('/uploads') ?>";
            var imgurl = uploadsUrl + '/' + banner_ar;
            $('#scrn-img-bnrar').attr("src", imgurl);
            $('#bnrar_photo').data('id',page);
        $('#delete-bnrar').data('id',page);
        }else{
            document.getElementById("bnrar_photo_image_preview").style.display = "none"; 
            $('#bnrar_photo').data('id',page);
        $('#delete-bnrar').data('id',page);  
        }


        $('#imageupload_popup').modal({
            show: true
        });
    })


    $('#caten-photo-upload').click(function (e) {
    $('#caten_photo').click();
    });
    $('#caten_photo').on('change', function () {
    var id = $(this).data("id");
    var imgw = $(this).data('imgw');
    var imgh = $(this).data('imgh');
    const file = $(this)[0].files[0];
    readUrl(file, 'cover_photo', uploadFile, imgw, imgh, id);
    });

    $('#catar-photo-upload').click(function (e) {
    $('#catar_photo').click();
    });
    $('#catar_photo').on('change', function () {
    var id = $(this).data("id");
    var imgw = $(this).data('imgw');
    var imgh = $(this).data('imgh');
    const file = $(this)[0].files[0];
    
    readUrl(file, 'cover_photo', uploadFiles, imgw, imgh, id);
    });

    $('#bnren-photo-upload').click(function (e) {
    $('#bnren_photo').click();
    });
    $('#bnren_photo').on('change', function () {
    var id = $(this).data("id");
    var imgw = $(this).data('imgw');
    var imgh = $(this).data('imgh');
    const file = $(this)[0].files[0];
    readUrl(file, 'cover_photo', uploadBanner, imgw, imgh, id);
    });

    $('#bnrar-photo-upload').click(function (e) {
    $('#bnrar_photo').click();
    });
    $('#bnrar_photo').on('change', function () {
    var id = $(this).data("id");
    var imgw = $(this).data('imgw');
    var imgh = $(this).data('imgh');
    const file = $(this)[0].files[0];
    readUrl(file, 'cover_photo', uploadBanners, imgw, imgh, id);
    });


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.save-categorys').on('click', function(e) {


        $("#user_form").validate({
            rules: {
                category_name_en: {
                    required: true,
                },
                category_name_ar: {
                    required: true,
                },
                
                
            },
            messages: {
                category_name_en: {
                    required: "Category name (En) required",
                },
                category_name_ar: {
                    required: "Category name (Ar) required",
                },
               
            },
            submitHandler: function(form) {
                user_unique = $("#user_unique").val();
                if (user_unique) {
                    $.ajax({
                        type: 'POST',
                        url: "{{route('category.update')}}",
                        data: new FormData(form),
                        mimeType: "multipart/form-data",
                        dataType: 'JSON',
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#user_form")[0].reset();
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                } else {
                    $.ajax({
                        type: 'POST',
                        data: new FormData(form),
                        mimeType: "multipart/form-data",
                        dataType: 'JSON',
                        contentType: false,
                        processData: false,
                        url: "{{route('category.store')}}",
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#user_form")[0].reset();
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                }
            }
        })
    });

    $('.edit_user').on('click', function(e) {
        page = $(this).data('id')
        $('#name_change').html('Edit Category');
        $('#save_data').text('Save').button("refresh");
        var url = "category/edit/";
        $.get(url + page, function(data) {
            $('#category_name_en').val(data.category.lang[0].name),
                $('#category_name_ar').val(data.category.lang[1].name),
                $('#category_ID').val(data.category.category_unique_id),
                $('#category').val(data.category.parent_id),
                $('#user_unique').val(data.category.id)
                $("#category option[value=" + data.category.id + "]").remove();
            $('#user_popup').modal({
                show: true

            });
        });
    });



    function deleteCategory(id) {
        
        $.confirm({
            title: false,
            content: 'Are you sure to delete this category? <br><br>You wont be able to revert this',
            
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('category.delete')}}",
                        data: {
                            id: id
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("category")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    }


    $('.change-status').on('click', function() {
        var cust_id = $(this).data("id");
        var act_value = $(this).data("activate");
        $.confirm({
            title: act_value + ' Category',
            content: 'Are you sure to ' + act_value + ' the Category?',
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('category.status.update')}}",
                        data: {
                            id: cust_id
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("category")}}';
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    window.location.reload();
                }
            }
        });
    });

    
     
$('.set_featured').on('click', function() {
    console.log($(this).data('status'));
    status = $(this).data('status');
        var cust_id = $(this).data("id");
        if (status == 'yes') {
            content = "Are you sure to remove this featured ?"
            title = "Remove featured Category"
        } else {
            title = "Make featured Category"
            content = "Are you sure to make this featured ?"
        }
       
        $.confirm({
            title:  title,
            content: content,
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('category.featured.update')}}",
                        data: {
                            id: cust_id,
                            status: status
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("category")}}';
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    window.location.reload();
                }
            }
        });
    });
    function uploadFile(file, type, id) {
    var formData = new FormData();
    formData.append('photo', file);
    formData.append('cat_id', id);
    formData.append('upload_type', 'image_en');
    $.ajax({
    type: "POST",
            url: "{{route('upload_category_images')}}",
            dataType: "json",
            data: formData,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
            $(`#caten_photo_loader`).show();
            $('#image-modal').modal('hide');
            },
            success: function(data) {
            if (data.status == 1) {
            Toast.fire({
            icon: 'success',
                    title: data.message
            });
            $('#pdt_caten_image').html('<div id="caten-photo-upload" class="add">+</div>' +
                    '<div id="caten_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                    '<div class="preview-image-container" id="caten_photo_image_preview">' +
                    '<div class="scrn-link" style="position: relative;top: -20px;">' +
                    '<button type="button" class="scrn-img-close delete-caten" data-type="image_en" data-id="' + data.cat_id + '">' +
                    '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                    '<img class="scrn-img" style="max-width: 200px" src="' + data.path + '" alt="">' +
                    '</div></div>' +
                    '<script>' +
                    '$(".delete-caten").click(function(e) {' +
                    'delete_image(' + data.cat_id + ' , "image_en");' +
                    '});<\/script>');
            } else {
            Toast.fire({
            icon: 'error',
                    title: data.message
            });
            $(`caten_photo_loader`).hide();
            }
            }
    });
    }
   
    
     $("button[data-dismiss-modal=modal2]").click(function () {
    $('#image-modal').modal('hide');
    });

    $('.delete-caten').click(function(e) {
    let id = $(this).data('id');
    let type = $(this).data('type');
    
    delete_image(id, type);
    });

    function delete_image(id, type){
        
    Swal.fire({
    title: 'Are you sure to delete?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
    })
            .then((result) => {
            if (result.value) {
            $.ajax({
            url: "{{ route('detete_category_img') }}",
                    data: {
                    id: id,
                            type: type
                    },
                    type: 'POST',
                    success: function(data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    if (data.type == 'image_en') {
                       
                    $('#pdt_caten_image').html('<div id="caten-photo-upload" class="add">+</div>' +
                            '<div id="caten_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                            '<div class="preview-image-container" style="display: none;" id="caten_photo_image_preview">' +
                            '<div class="scrn-link" style="position: relative;top: -20px;">' +
                            '<button type="button" class="scrn-img-close" data-type="image_en" data-id="' + data.id + '">' +
                            '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                            '<img class="scrn-img" style="max-width: 200px" src="" alt="">' +
                            '</div></div>' +
                            '<script>' +
                            '$("#caten-photo-upload").click(function (e) {' +
                            '$("#caten_photo").click();});<\/script>');
                }else
                {
                    $("#pdt_image_val_"+data.id).remove();
                }
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            }
            })
    }
     $("button[data-dismiss-modal=modal2]").click(function () {
    $('#image-modal').modal('hide');
    });
    function uploadFiles(file, type, id) {
    var formData = new FormData();
    formData.append('photo', file);
    formData.append('cat_id', id);
    formData.append('upload_type', 'image_ar');
    $.ajax({
    type: "POST",
            url: "{{route('upload_category_images')}}",
            dataType: "json",
            data: formData,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
            $(`#catar_photo_loader`).show();
            $('#image-modal').modal('hide');
            },
            success: function(data) {
            if (data.status == 1) {
            Toast.fire({
            icon: 'success',
                    title: data.message
            });
           
            $('#pdt_catar_image').html('<div id="catar-photo-upload" class="add">+</div>' +
                    '<div id="catar_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                    '<div class="preview-image-container" id="catar_photo_image_preview">' +
                    '<div class="scrn-link" style="position: relative;top: -20px;">' +
                    '<button type="button" class="scrn-img-close delete-catar" data-type="image_ar" data-id="' + data.cat_id + '">' +
                    '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                    '<img class="scrn-img" style="max-width: 200px" src="' + data.path + '" alt="">' +
                    '</div></div>' +
                    '<script>' +
                    '$(".delete-catar").click(function(e) {' +
                    'delete_images(' + data.cat_id + ' , "image_ar");' +
                    '});<\/script>');
            } else {
            Toast.fire({
            icon: 'error',
                    title: data.message
            });
            $(`catar_photo_loader`).hide();
            }
            }
    });
    }

    $('.delete-catar').click(function(e) {
       
    let id = $(this).data('id');
    let type = $(this).data('type');
    
    delete_images(id, type);
    });


    function delete_images(id, type){
       
        Swal.fire({
        title: 'Are you sure to delete?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
        })
                .then((result) => {
                if (result.value) {
                $.ajax({
                url: "{{ route('detete_category_img') }}",
                        data: {
                        id: id,
                                type: type
                        },
                        type: 'POST',
                        success: function(data) {
                        if (data.status == 1) {
                        Toast.fire({
                        icon: 'success',
                                title: data.message
                        });
                        if (data.type == 'image_ar') {
                           
                        $('#pdt_catar_image').html('<div id="catar-photo-upload" class="add">+</div>' +
                                '<div id="catar_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                                '<div class="preview-image-container" style="display: none;" id="catar_photo_image_preview">' +
                                '<div class="scrn-link" style="position: relative;top: -20px;">' +
                                '<button type="button" class="scrn-img-close" data-type="image_en" data-id="' + data.id + '">' +
                                '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                                '<img class="scrn-img" style="max-width: 200px" src="" alt="">' +
                                '</div></div>' +
                                '<script>' +
                                '$("#catar-photo-upload").click(function (e) {' +
                                '$("#catar_photo").click();});<\/script>');
                    }else
                    {
                        $("#pdt_image_val_"+data.id).remove();
                    }
                        } else {
                        Toast.fire({
                        icon: 'error',
                                title: data.message
                        });
                        }
                        }
                });
                }
                })
        }
         $("button[data-dismiss-modal=modal2]").click(function () {
        $('#image-modal').modal('hide');
        });



function uploadBanner(file, type, id) {
           
    var formData = new FormData();
    formData.append('photo', file);
    formData.append('cat_id', id);
    formData.append('upload_type', 'banner_en');
    $.ajax({
    type: "POST",
            url: "{{route('upload_category_images')}}",
            dataType: "json",
            data: formData,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
            $(`#bnren_photo_loader`).show();
            $('#image-modal').modal('hide');
            },
            success: function(data) {
            if (data.status == 1) {
            Toast.fire({
            icon: 'success',
                    title: data.message
            });
            $('#pdt_bnren_image').html('<div id="bnren-photo-upload" class="add">+</div>' +
                    '<div id="bnren_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                    '<div class="preview-image-container" id="bnren_photo_image_preview">' +
                    '<div class="scrn-link" style="position: relative;top: -20px;">' +
                    '<button type="button" class="scrn-img-close delete-bnren" data-type="banner_en" data-id="' + data.cat_id + '">' +
                    '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                    '<img class="scrn-img" style="max-width: 200px" src="' + data.path + '" alt="">' +
                    '</div></div>' +
                    '<script>' +
                    '$(".delete-bnren").click(function(e) {' +
                    'delete_banner(' + data.cat_id + ' , "banner_en");' +
                    '});<\/script>');
            } else {
            Toast.fire({
            icon: 'error',
                    title: data.message
            });
            $(`bnren_photo_loader`).hide();
            }
            }
    });
    }
    $('.delete-bnren').click(function(e) {
    let id = $(this).data('id');
    let type = $(this).data('type');
    
    delete_banner(id, type);
    });

    function delete_banner(id, type){
        
    Swal.fire({
    title: 'Are you sure to delete?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
    })
            .then((result) => {
            if (result.value) {
            $.ajax({
            url: "{{ route('detete_category_img') }}",
                    data: {
                    id: id,
                            type: type
                    },
                    type: 'POST',
                    success: function(data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    if (data.type == 'banner_en') {
                       
                    $('#pdt_bnren_image').html('<div id="bnren-photo-upload" class="add">+</div>' +
                            '<div id="bnren_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                            '<div class="preview-image-container" style="display: none;" id="bnren_photo_image_preview">' +
                            '<div class="scrn-link" style="position: relative;top: -20px;">' +
                            '<button type="button" class="scrn-img-close" data-type="banner_en" data-id="' + data.id + '">' +
                            '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                            '<img class="scrn-img" style="max-width: 200px" src="" alt="">' +
                            '</div></div>' +
                            '<script>' +
                            '$("#bnren-photo-upload").click(function (e) {' +
                            '$("#bnren_photo").click();});<\/script>');
                }else
                {
                    $("#pdt_image_val_"+data.id).remove();
                }
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            }
            })
    }

    function uploadBanners(file, type, id) {
           
           var formData = new FormData();
           formData.append('photo', file);
           formData.append('cat_id', id);
           formData.append('upload_type', 'banner_ar');
           $.ajax({
           type: "POST",
                   url: "{{route('upload_category_images')}}",
                   dataType: "json",
                   data: formData,
                   processData: false, // tell jQuery not to process the data
                   contentType: false, // tell jQuery not to set contentType
                   beforeSend: function() {
                   $(`#bnrar_photo_loader`).show();
                   $('#image-modal').modal('hide');
                   },
                   success: function(data) {
                   if (data.status == 1) {
                   Toast.fire({
                   icon: 'success',
                           title: data.message
                   });
                   $('#pdt_bnrar_image').html('<div id="bnrar-photo-upload" class="add">+</div>' +
                           '<div id="bnrar_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                           '<div class="preview-image-container" id="bnrar_photo_image_preview">' +
                           '<div class="scrn-link" style="position: relative;top: -20px;">' +
                           '<button type="button" class="scrn-img-close delete-bnrar" data-type="banner_ar" data-id="' + data.cat_id + '">' +
                           '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                           '<img class="scrn-img" style="max-width: 200px" src="' + data.path + '" alt="">' +
                           '</div></div>' +
                           '<script>' +
                           '$(".delete-bnrar").click(function(e) {' +
                           'delete_banners(' + data.cat_id + ' , "banner_ar");' +
                           '});<\/script>');
                   } else {
                   Toast.fire({
                   icon: 'error',
                           title: data.message
                   });
                   $(`bnrar_photo_loader`).hide();
                   }
                   }
           });
           }

    $('.delete-bnrar').click(function(e) {
    let id = $(this).data('id');
    let type = $(this).data('type');
    console.log("id");
    delete_banners(id, type);
    });

    function delete_banners(id, type){
        
    Swal.fire({
    title: 'Are you sure to delete?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
    })
            .then((result) => {
            if (result.value) {
            $.ajax({
            url: "{{ route('detete_category_img') }}",
                    data: {
                    id: id,
                            type: type
                    },
                    type: 'POST',
                    success: function(data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    if (data.type == 'banner_ar') {
                       
                    $('#pdt_bnrar_image').html('<div id="bnrar-photo-upload" class="add">+</div>' +
                            '<div id="bnrar_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                            '<div class="preview-image-container" style="display: none;" id="bnrar_photo_image_preview">' +
                            '<div class="scrn-link" style="position: relative;top: -20px;">' +
                            '<button type="button" class="scrn-img-close" data-type="banner_ar" data-id="' + data.id + '">' +
                            '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                            '<img class="scrn-img" style="max-width: 200px" src="" alt="">' +
                            '</div></div>' +
                            '<script>' +
                            '$("#bnrar-photo-upload").click(function (e) {' +
                            '$("#bnrar_photo").click();});<\/script>');
                }else
                {
                    $("#pdt_image_val_"+data.id).remove();
                }
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            }
            })
    }
</script>
@endpush