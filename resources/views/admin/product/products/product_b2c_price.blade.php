<form id="frm_create_b2c_price" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body">
            <div class="row">
                @if($product_type == 'complex' && !empty($pdt_att_var))
                <div class="col-md-12 mt-3">
                    <label class="control-label"><strong>PRODUCT VARIANT</strong></label>
                </div>
                <div class="col-md-3"><label class="control-label"><b>{{ $pdt_att_var->lang[0]['name'] ?? '' }} variant</b></div>
                <div class="col-md-3"><label class="control-label"><b>Price</b> <span class="text-danger">*</span></label></div>
                <div class="col-md-3">Discount</div>
                <div class="col-md-3"></div>
                <input type="hidden"  name="var_attribute_id" value="{{ $pdt_att_var['id'] }}"/>
                @if($variants = \App\Models\ProductVariant::with('variant')->where(['product_id' => $pdt_id])->get())
                @foreach($variants as $variant_val)
                @php
                $pdt_b2c_price = \App\Models\ProductB2CPrice::with('attribute')->with('variant')->where('product_id', $pdt_id)
                ->where('variant_id', $variant_val->variant->id)->first();
                @endphp
                <div class="col-md-3 mt-2">
                    <input type="hidden"  name="varient_id_main" value="{{ $variant_val->variant->variant_id }}"/>
                    <input type="hidden" name="variant_id[]" value="{{$variant_val->variant->id }}">
                    <label class="control-label ml-1">{{ $variant_val->variant->name ?? '' }}</label>
                </div>
                <div class="col-md-3">
                    <input class="form-control form-white" placeholder="Enter price" type="text" name="price_var[]" id="price_var_{{$variant_val->variant->id}}" value="{{ isset($pdt_b2c_price['price']) ? $pdt_b2c_price['price'] : '' }}"/>
                </div>
                <div class="col-md-3">
                    <input class="form-control form-white" placeholder="Enter discount price" type="text" name="discount_price_var[]" id="discount_price_var_{{$variant_val->variant->id}}" value="{{ isset($pdt_b2c_price['discount_price']) ? $pdt_b2c_price['discount_price'] : '' }}"/>
                </div>
                <div class="col-md-3">
                </div>
                @endforeach
                @endif
                @else
                <div class="col-md-4">
                    <label class="control-label">Price <span class="text-danger">*</span></label>
                    <input class="form-control form-white" placeholder="Enter price" type="text" name="price" value="{{ isset($b2c_simple)? $b2c_simple['price'] : ''}}" id="simple_price"/>
                </div>
                <div class="col-md-4">
                    <label class="control-label">Discount Price</label>
                    <input class="form-control form-white" placeholder="Enter discount price" type="text" name="discount_price" value="{{ isset($b2c_simple)? $b2c_simple['discount_price'] : ''}}"/>
                </div>
                @endif

            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" id="pdt_id" name="pdt_id" value="{{ $pdt_id }}">
            <input type="hidden" id="product_type" name="product_type" value="{{ $product_type }}">
            <input type="hidden" id="submit_action" value="" />
            @if(!empty($pdt_id))
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                Save
            </button>
            @endif
            <button type="submit" class="btn btn-info waves-effect waves-light save-and-continue">
                Save & Continue
            </button>
            <a class="btn btn-default waves-effect tab_back" data-id='{{ $pdt_id }}' href="javascript:void(0);">Back</a>
        </div>
    </div>
</form>
<script>
    $(".save-and-continue").on('click', function () {
        $("#submit_action").val('continue');
    });
    $(".save-btn").on('click', function () {
        $("#submit_action").val('save');
    });
    $('.tab_back').on('click', function () {
        var pdt_id = $(this).data("id");
        $.ajax({
            type: "GET",
            url: "{{route('product_tabs')}}",
            data: {'activeTab': 'SPEC & STOCK', pdt_id: pdt_id},
            success: function (result) {
                $('#pdt_tab a[href="#spec_stock"]').tab('show');
                $('.tab-content').html(result);
            }
        });
    });
    $("#frm_create_b2c_price").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
       rules: {
            price: {
                required: true,
                number: true
            },
            discount_price: {
                number: true,
                
            },
            "price_var[]": {
                required: true,
                number: true
            },
            "discount_price_var[]": {
                required: true,
                number: true,
            },
        },
        messages: {
            price: {
                required: 'Price is required',
                number: "Decimal numbers only"
            },
            discount_price: {
                required: 'Discount price is required',
                number: "Decimal numbers only"
            },
            "price_var[]": {
                required: 'Price is required',
                number: "Decimal numbers only"
            },
            "discount_price_var[]": {
                required: 'Discount price is required',
                number: "Decimal numbers only"
            },
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "{{route('save_product_b2c_price')}}",
                data: $('#frm_create_b2c_price').serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 1) {
                        if ($("#submit_action").val() == 'continue') {
                        Toast.fire({
                            icon: 'success',
                            title: data.message
                        });
                        $('#pdt_tab a[href="#photos"]').tab('show');
                        $('.tab-content').html(data.result);
                    }else{
                       Toast.fire({
                            icon: 'success',
                            title: data.message
                        }); 
                    }
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
                    $('button:submit').attr('disabled', false);
                },
                error: function (err) {
                    $('button:submit').attr('disabled', false);
                }
            });
            return false;
        }
    });

    $.validator.addMethod('lessThan', function(value, element, param) {
    var i = parseInt(value);
    var j = parseInt($(param).val());
    return i < j;
}, "Discount price should be less than the price");

</script>