<form id="frm_create_stock" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label class="control-label"><strong>PRODUCT ATTRIBUTES</strong></label>
                </div>
                @if (count($pdt_att) > 0)
                @foreach ($pdt_att as $row_data)
                @php
                $pdt_attribute = \App\Models\ProductAttribute::with('lang')->with('attribute')->with('product')
                ->where('attribute_id', $row_data->id)
                ->where('product_id', $pdt_id)->first();
                @endphp
                @if($row_data->attribute_type == 'textbox')
                <input type="hidden" name="attribute_id[]" value="{{$row_data->id}}"/>
                <input type="hidden" name="attribute_value_type[]" value="textbox"/>
                <div class="col-md-6">
                    <label class="control-label">{{ $row_data->lang[0]->name ?? '' }} <span class="text-danger">{{ ($row_data->is_mandatory == 'yes') ? '*' : ''}}</span></label>
                    <input class="form-control form-white" placeholder="Enter {{ $row_data->lang[0]->name ?? '' }}" type="text" name="att_name_{{$row_data->id}}_en" id="{{ ($row_data->is_mandatory == 'yes') ? 'att_name_en_'.$row_data->id : ''}}"  value="{{ isset($pdt_attribute) ? $pdt_attribute->lang[0]['name'] : '' }}"/>
                </div>
                <div class="col-md-6 text-right">
                    <label class="control-label">{{ $row_data->lang[1]->name ?? '' }} <span class="text-danger">{{ ($row_data->is_mandatory == 'yes') ? '*' : ''}}</span></label>
                    <input class="form-control form-white text-right" placeholder="Enter {{ $row_data->lang[1]->name ?? '' }}" type="text" name="att_name_{{$row_data->id}}_ar" id="{{ ($row_data->is_mandatory == 'yes') ? 'att_name_ar_'.$row_data->id : ''}}" value="{{ isset($pdt_attribute) ? $pdt_attribute->lang[1]['name'] : '' }}"/>
                </div>
                @else
                <input type="hidden" name="attribute_id[]" value="{{$row_data->id}}"/>
                <input type="hidden" name="attribute_value_type[]" value="textarea"/>
                <div class="col-md-6">
                    <label class="control-label">{{ $row_data->lang[0]->name ?? '' }} <span class="text-danger">{{ ($row_data->is_mandatory == 'yes') ? '*' : ''}}</span></label>
                    <textarea class="form-control area" name="att_name_{{$row_data->id}}_en" placeholder="Enter {{ $row_data->lang[0]->name ?? '' }}" id="{{ ($row_data->is_mandatory == 'yes') ? 'att_name_en_'.$row_data->id : ''}}">{{ isset($pdt_attribute) ? $pdt_attribute->lang[0]['name'] : '' }}</textarea>
                </div>
                <div class="col-md-6 text-right">
                    <label class="control-label">{{ $row_data->lang[1]->name ?? '' }} <span class="text-danger">{{ ($row_data->is_mandatory == 'yes') ? '*' : ''}}</span></label>
                    <textarea class="form-control area text-right" name="att_name_{{$row_data->id}}_ar" placeholder="Enter {{ $row_data->lang[1]->name ?? '' }}" id="{{ ($row_data->is_mandatory == 'yes') ? 'att_name_ar_'.$row_data->id : ''}}">{{ isset($pdt_attribute) ? $pdt_attribute->lang[1]['name'] : '' }}</textarea>
                </div>
                @endif
                @endforeach
                @endif
                @if(count($pdt_var) > 0)
                @foreach ($pdt_var as $row_data_val)
                @php
                $pdt_var_attribute = \App\Models\ProductAttribute::with('lang')->with('attribute')->with('variant')
                ->where('attribute_value_type', 'dropdown')
                ->where('attribute_id', $row_data_val->id)
                ->where('product_id', $pdt_id)->first();
                @endphp
                <div class="col-md-6">
                    <input type="hidden"  name="attribute_id[]" value="{{$row_data_val->id}}"/>
                    <input type="hidden" name="attribute_value_type[]" value="dropdown"/>
                    <label class="control-label">{{ $row_data_val->lang[0]->name ?? '' }} <span class="text-danger">{{ ($row_data_val->is_mandatory == 'yes') ? '*' : ''}}</label>
                    @if($variants = \App\Models\Variant::leftJoin('variant_i18n', 'variant_i18n.variant_id', '=', 'variant.id')
                    ->select('variant_i18n.name as var_name', 'variant_i18n.id as id', 'variant.id as variant_id')
                    ->where('variant_i18n.language', '=', 'en')
                    ->where('variant.attribute_id', '=', $row_data_val->id)
                    ->get(['id', 'var_name', 'variant_id']))
                    <input type="hidden"  name="variant_id{{$row_data_val->id}}" value="{{$variants[0]->variant_id}}"/>
                    <select class="form-control" name="att_val_{{$row_data_val->id}}_id" id="{{ ($row_data_val->is_mandatory == 'yes') ? 'att_val_'.$row_data_val->id : ''}}">
                        <option value="">Select {{ $row_data_val->lang[0]->name ?? '' }}</option>
                        @foreach($variants as $variant_val)
                        <option value="{{$variant_val['id']}}" {{ (isset($pdt_var_attribute['id']) &&  $variant_val['id'] == $pdt_var_attribute['attrinute_value_id']) ? 'selected' : '' }}>{{ $variant_val->var_name ?? '' }}</option>
                        @endforeach
                    </select>
                    @endif
                </div>
                <div class="col-md-6"></div>
                @endforeach
                @endif
                @php
                $pdt_stock = \App\Models\Product::where('id', $pdt_id)->first();
                @endphp
                <div class="col-md-6">
                    <label class="control-label">Stock <span class="text-danger">*</span></label>
                    <input class="form-control form-white" placeholder="Enter Stock" type="text" name="stock" value="{{isset($pdt_stock->stock) ? $pdt_stock->stock : ''}}"/>
                </div>
                <div class="col-md-6"></div>
                @if(!empty($pdt_att_var) && $product_type == 'complex')
                <div class="col-md-12 mt-3">
                    <label class="control-label"><strong>PRODUCT VARIANTS & STOCK </strong></label>
                </div>
                <div class="col-md-3"><label class="control-label"><b>{{ $pdt_att_var->lang[0]['name'] ?? '' }}</b> <span class="text-danger">*</span></label></div>
                <div class="col-md-3"><label class="control-label"><b>Stock</b> <span class="text-danger">*</span></label></div>
                <div class="col-md-6"></div>
                <input type="hidden"  name="var_attribute_id" value="{{ $pdt_att_var['id'] }}"/>
                <input type="hidden" name="var_attribute_value_type" value="dropdown"/>
                @if($variants = \App\Models\Variant::leftJoin('variant_i18n', 'variant_i18n.variant_id', '=', 'variant.id')
                ->select('variant_i18n.name as var_name', 'variant_i18n.id as id', 'variant_i18n.variant_id as var_id')
                ->where('variant_i18n.language', '=', 'en')
                ->where('variant.attribute_id', '=', $pdt_att_var['id'])
                ->get(['id', 'var_name', 'var_id']))
                @php $i=0; @endphp
                @foreach($variants as $variant_val)
                @php $i++; @endphp
                @php $pdt_var_data = \App\Models\ProductVariant::with('attribute')->with('variant')
                ->where('attribute_variant_id', $variant_val['id'])
                ->where('attribute_id', $pdt_att_var['id'])
                ->where('product_id', $pdt_id)->first();
                @endphp
                <input type="hidden"  name="var_id" value="{{ $variant_val['var_id'] }}"/>
                <div class="col-md-3 mt-2">
                    <input type="checkbox" class="chk-input" name="attribute_variant_id[]" id="attribute_variant_id_{{$i}}" value="{{$variant_val['id']}}" {{ isset($pdt_var_data->attribute_variant_id) ? "checked=checked" : ''}}>
                           <label class="control-label ml-1" id="error_chk">{{ $variant_val->var_name ?? '' }}</label>
                </div>
                <div class="col-md-3">
                    <input class="form-control form-white" placeholder="Enter Stock" type="text" name="var_stock_{{$variant_val['id']}}" id="var_stock_{{$i}}" value="{{isset($pdt_var_data) ? $pdt_var_data->stock : ''}}"/>
                </div>
                <div class="col-md-6"></div>
                @endforeach
                @endif
                @endif
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" id="pdt_id" name="pdt_id" value="{{ $pdt_id }}">
            <input type="hidden" id="product_type" name="product_type" value="{{ $product_type }}">
            <input type="hidden" id="submit_action" value="" />
            @if(!empty($pdt_id))
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                Save
            </button>
            @endif
            <button type="submit" class="btn btn-info waves-effect waves-light save-and-continue">
                Save & Continue
            </button>
            <a class="btn btn-default waves-effect tab_back" data-id='{{ $pdt_id }}' href="javascript:void(0);">Back</a>
        </div>
    </div>
</form>
<script>
    $(".save-and-continue").on('click', function () {
        $("#submit_action").val('continue');
    });
    $(".save-btn").on('click', function () {
        $("#submit_action").val('save');
    });
    $('.tab_back').on('click', function () {
        var pdt_id = $(this).data("id");
        $.ajax({
            type: "GET",
            url: "{{route('product_tabs')}}",
            data: {'activeTab': 'PRODUCT INFO', pdt_id: pdt_id},
            success: function (result) {
                $('#pdt_tab a[href="#pdt_info"]').tab('show');
                $('.tab-content').html(result);
            }
        });
    });
    $("#frm_create_stock").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            stock: {
                required: true,
                number: true,
            }
        },
        messages: {
            stock: {
                required: 'Stock is required',
                number: 'Please enter a number'
            }
        },
        errorElement: "label",
        errorPlacement: function (error, element) {
            if (element.attr("type") == "checkbox") {
                error.insertAfter("#error_chk");
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "{{route('save_product_stock')}}",
                data: $('#frm_create_stock').serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 1) {
                        if ($("#submit_action").val() == 'continue') {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#pdt_tab a[href="#b2cprice"]').tab('show');
                            $('.tab-content').html(data.result);
                        } else
                        {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                        }
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
                    $('button:submit').attr('disabled', false);
                },
                error: function (err) {
                    $('button:submit').attr('disabled', false);
                }
            });
            return false;
        }
    });
    $('input[id^="att_name_en_"]').each(function () {
        $(this).rules('add', {
            required: true,
            messages: {
                required: "Attribute value (EN) is required"
            }
        });
    });
    $('input[id^="att_name_ar_"]').each(function () {
        $(this).rules('add', {
            required: true,
            messages: {
                required: "Attribute value (AR) is required"
            }
        });
    });
    $('textarea[id^="att_name_en_"]').each(function () {
        $(this).rules('add', {
            required: true,
            messages: {
                required: "Attribute value (EN) is required"
            }
        });
    });
    $('textarea[id^="att_name_ar_"]').each(function () {
        $(this).rules('add', {
            required: true,
            messages: {
                required: "Attribute value (EN) is required"
            }
        });
    });
    $('select[id^="att_val_"]').each(function () {
        $(this).rules('add', {
            required: true,
            messages: {
                required: "Select this field"
            }
        });
    });
    $('input[id^="attribute_variant_id_"]').each(function () {
        $(this).rules('add', {
            required: true,
            messages: {
                required: 'Select variant',
            }
        });
    });
</script>