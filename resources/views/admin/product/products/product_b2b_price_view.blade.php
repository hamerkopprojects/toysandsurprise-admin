<form id="frm_create_b2b_price" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body">
            @if($product_type[0] == 'complex')
            <div class="row">
                @if(!empty($pdt_att_var))
                <div class="col-md-12 mt-3">
                    <label class="control-label">{{ $pdt_att_var[0]->attribute->lang[0]->name }}</label>
                </div>
                <div class="col-md-2"><label class="control-label">{{ $pdt_att_var[0]->attribute->lang[0]->name }} variant</div>
                <div class="col-md-2"><label class="control-label">No of items </label></div>
                <div class="col-md-3">Container Type</div>
                <div class="col-md-2"><label class="control-label">Price(Per item)</label></div>
                <div class="col-md-2"><label class="control-label">Discount Price</label></div>
                <div class="col-md-1"></div>
               
               
                @foreach($pdt_att_var as $variant_val)
              
                <div class="col-md-12 mt-3">
                   
                </div>
                <div class="col-md-2">{{ $variant_val->variant->name}}</div>
                <div class="col-md-2">{{ $variant_val->quantity}}</div>
                <div class="col-md-3"> {{ (Config::get('constants.container_type'))[$variant_val->container_type]}}</div>
                <div class="col-md-2"> {{ $variant_val->price}}</div>
                <div class="col-md-2">{{ $variant_val->discount_price}}</div>
                <div class="col-md-1"></div>
                @endforeach
               
             @endif 

            </div>
            @else
            @if(!empty($prdt))
            <div class="cls_outer_div mb-3">
                <div class="row ml-0 cls_content_div1">
                    <div class="col-md-3"><label class="control-label">No of items </label></div>
                    <div class="col-md-3"><label class="control-label">Container Type</label></div>
                    <div class="col-md-2"><label class="control-label">Price(Per item)</label></div>
                    <div class="col-md-2"><label class="control-label">Discount Price</label></div>
                    <div class="col-md-1"></div>
                    @foreach($prdt as $variant_val)
            
                  
                    <div class="col-md-3">{{ $variant_val->quantity}}</div>
                    <div class="col-md-3"> {{ (Config::get('constants.container_type'))[$variant_val->container_type]}}</div>
                    <div class="col-md-2"> {{ $variant_val->price}}</div>
                    <div class="col-md-2">{{ $variant_val->discount_price}}</div>
                    <div class="col-md-1"></div>
                    @endforeach
                </div>
            </div>
            @endif
            @endif
        </div>
        <div class="modal-footer">
           
        </div>
    </div>
</form>
<style>

    label.control-label {
        font-size: 16px;
        color: #373757;
    }
        </style>
    