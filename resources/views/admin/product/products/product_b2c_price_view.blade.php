<form id="frm_create_b2c_price" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body">
            <div class="row">
            @if($product_type[0] == 'complex')
            <div class="col-md-12 mt-3">
                {{-- <label class="control-label"><strong>PRODUCT VARIANT</strong></label> --}}
            </div>
            @if(!empty($pdt_att_var[0]))
            <div class="col-md-3"><label class="control-label">{{ $pdt_att_var[0]->attribute->lang[0]->name}} variant</label></div>
            <div class="col-md-3"><label class="control-label">Price </label></div>
            <div class="col-md-3"><label class="control-label">Discount</label></div>
            <div class="col-md-3"></div>
           
            @foreach($pdt_att_var  as $variant_val)
            <div class="col-md-3 mt-2">
                
                <label class="control-label ml-1">{{ $variant_val->variant->name}}</label>
            </div>
            <div class="col-md-3">
                {{ $variant_val->price}}
            </div>
            <div class="col-md-3">
                {{ $variant_val->discount_price}}
            </div>
            <div class="col-md-3">
            </div>
            @endforeach
           @endif
           @else
           @if(!empty($prdt[0]))
                <div class="col-md-4">
                    <label class="control-label">Price</label><br>
                    {{ $prdt[0]->price }}
                </div>
                <div class="col-md-4">
                    <label class="control-label">Discounted Price </label><br>
                    {{$prdt[0]->discount_price  }}
                </div>
                @endif
           @endif
           
            </div>
        </div>
        <div class="modal-footer">
           
        </div>
    </div>
</form>
<style>

    label.control-label {
        font-size: 16px;
        color: #373757;
    }
        </style>
    