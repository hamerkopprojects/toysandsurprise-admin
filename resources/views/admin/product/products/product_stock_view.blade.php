<form id="frm_create_stock" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    {{-- <label class="control-label"><strong>PRODUCT ATTRIBUTES</strong></label> --}}
                </div>
                @if (count($pdt_att) > 0)
                @foreach ($pdt_att as $row_data)
                <div class="col-md-6">
                    <label class="control-label">{{ $row_data->attribute->lang[0]->name }} </label>
                  <br>
                  {{ $row_data->lang[0]->name ?? '' }}
                </div>
                <div class="col-md-6 text-right">
                    <label class="control-label">{{ $row_data->attribute->lang[1]->name }} </label><br>
                    {{ $row_data->lang[1]->name ?? '' }}
                </div>
                @endforeach
                @if(count($pdt_var) > 0)
                @foreach ($pdt_var as $row_data_val)
                    <div class="col-md-3"><label class="control-label">{{ $row_data_val->attribute->lang[0]->name }}</label></div>
                <div class="col-md-3"><label class="control-label">Stock</label></div>

                <div class="col-md-6"></div>
               
                    <div class="col-md-3">{{ $row_data_val->variant->name }}</div>
                    <div class="col-md-3">{{ $row_data_val->product->stock }}</div>
                <div class="col-md-6"></div>
                @endforeach
                @else
                <div class="col-md-6"><label class="control-label">Stock </label>
                   <br> {{ $row_data->product->stock }}
                </div>
               
                @endif
                @endif
                <div class="col-md-6">
                    <label class="control-label">Stock</label><br>
                    {{$pro[0]['stock'] }}
                </div>
                @if(!empty($pdt_att_var && $product_type[0] == 'complex'))
                <div class="col-md-12 mt-3">
                    {{-- <label class="control-label"><strong>PRODUCT VARIANTS & STOCK </strong></label> --}}
                </div>
                <div class="col-md-3"><label class="control-label">{{ $pdt_att_var[0]->attribute->lang[0]->name }}</label></div>
                <div class="col-md-3"><label class="control-label">Stock</label></div>
                <div class="col-md-6"></div>
                
              
                @foreach($pdt_att_var as $variant_val)
                <div class="col-md-3 mt-2">
                   
                    <label class="control-label ml-1">{{ $variant_val->variant->name}}</label>
                </div>
                <div class="col-md-3">
                    {{ $variant_val->stock}}
                </div>
                <div class="col-md-6"></div>
                @endforeach
               
                @endif
            </div>
        </div>
        <div class="modal-footer">
          
        </div>
    </div>
</form>
<style>

    label.control-label {
        font-size: 16px;
        color: #373757;
    }
        </style>
    