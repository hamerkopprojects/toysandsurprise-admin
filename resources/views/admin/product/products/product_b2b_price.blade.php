<form id="frm_create_b2b_price" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body">
            @php
            $j = 1;
            @endphp
            @if($product_type == 'complex' && !empty($pdt_att_var))
            <div class="row">
                <div class="col-md-12 mt-3">
                    <label class="control-label"><strong>{{ $pdt_att_var->lang[0]['name'] ?? '' }}</strong></label>
                </div>
                <div class="col-md-2"><label class="control-label"><b>{{ $pdt_att_var->lang[0]['name'] ?? '' }} variant</b></div>
                <div class="col-md-2"><label class="control-label"><b>No of items</b> <span class="text-danger">*</span></label></div>
                <div class="col-md-3"></div>
                <div class="col-md-2"><label class="control-label"><b>Price(Per item)</b> <span class="text-danger">*</span></label></div>
                <div class="col-md-2"><label class="control-label"><b>Discount Price</b> <span class="text-danger">*</span></label></div>
                <div class="col-md-1"></div>
                <input type="hidden"  name="var_attribute_id" value="{{ $pdt_att_var['id'] }}"/>
                @if($variants = \App\Models\ProductVariant::with('variant')->where(['product_id' => $pdt_id])->get())
                @php
                $i = 0;
                @endphp
                @foreach($variants as $variant_val)
                @php
                $i++;
                @endphp
                <div class="cls_outer_var_div{{$i}}">
                    <input type="hidden"  name="varient_id_main" value="{{ $variant_val->variant->variant_id }}"/>
                    @php
                    $pdt_b2b_complex = \App\Models\ProductB2BPrice::with('attribute')->with('variant')->where(['product_id' => $pdt_id, 'variant_id' => $variant_val->variant->id])->first();
                    @endphp
                    @if(!empty($pdt_b2b_complex))
                    <div class="row ml-0 cls_content_var_div{{$i}}">
                        <div class="col-md-2 mt-2">
                            <input type="hidden" name="variant_id[]" value="{{ $variant_val->variant->id }}">
                            <label class="control-label ml-1">{{ $variant_val->variant->name ?? '' }}</label>
                        </div>
                        <div class="col-md-2">
                            <input class="form-control form-white" placeholder="Enter no of items" type="text" name="quantity[]" id="qty_var_{{$i}}" value="{{$pdt_b2b_complex['quantity']}}"/>
                        </div>
                        <div class="col-md-3">
                            <select class="form-control search_val" name="container_type[]" id="container_var_{{$i}}">
                                <option value="">Select Type</option>
                                @foreach($container_type as $key => $value)
                                <option value="{{$key}}" {{ ($pdt_b2b_complex['container_type'] == $key) ? 'selected' : '' }}>{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2">
                            <input class="form-control form-white" placeholder="Enter price" type="text" name="price[]" value="{{$pdt_b2b_complex['price']}}" id="price_var_{{$i}}"/>
                        </div>
                        <div class="col-md-2">
                            <input class="form-control form-white" placeholder="Enter discount price" type="text" name="discount_price[]" value="{{$pdt_b2b_complex['discount_price']}}" id="discount_price_var{{$i}}"/>
                        </div>
                        <div class="col-md-1 text-left">
                            <a class="btn btn-sm btn-success text-white add_more" title="Add New" data-id="{{$i}}" data-outer-id="{{$i}}" data-var_id="{{$variant_val->variant->id}}" id="add_price_complex"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                    @php
                    $pdt_b2b_complex_inner = \App\Models\ProductB2BPrice::with('attribute')->with('variant')->where(['product_id' => $pdt_id, 'variant_id' => $variant_val->variant->id])->get();
                    @endphp
                    @if(count($pdt_b2b_complex_inner) > 1)
                    @foreach($pdt_b2b_complex_inner as $row_b2b_complex_inner)
                    @php
                    $i++;
                    @endphp
                    @if($pdt_b2b_complex['id'] != $row_b2b_complex_inner['id'])
                    <div class="row ml-0 cls_content_var_div{{$i}}">
                        <div class="col-md-2 mt-2">
                            <input type="hidden" name="variant_id[]" value="{{$variant_val->variant->id}}"></div>
                        <div class="col-md-2"><input class="form-control form-white" placeholder="Enter no of items" type="text" name="quantity[]" id="qty_var_{{$i}}" value="{{$row_b2b_complex_inner['quantity']}}"/></div>
                        <div class="col-md-3">
                            <select class="form-control search_val" name="container_type[]" id="container_var_{{$i}}">
                                <option value="">Select Type</option>
                                @foreach($container_type as $key => $value)
                                <option value="{{$key}}" {{ ($row_b2b_complex_inner['container_type'] == $key) ? 'selected' : '' }}>{{$value}}</option>
                                @endforeach
                            </select></div>
                        <div class="col-md-2"><input class="form-control form-white" placeholder="Enter price" type="text" name="price[]" value="{{$row_b2b_complex_inner['price']}}" id="price_var_{{$i}}"/></div>
                        <div class="col-md-2"><input class="form-control form-white" placeholder="Enter discount price" type="text" name="discount_price[]" value="{{$row_b2b_complex_inner['discount_price']}}" id="discount_price_var{{$i}}"/></div>
                        <div class="col-md-1 text-left"><button class="btn btn-sm btn-danger text-white btn_remove" title="Remove Attributes">
                                <i class="fa fa-minus"></i></button></div></div>
                    @endif
                    @endforeach
                    @endif
                    @else
                    <div class="row ml-0 cls_content_var_div{{$i}}">
                        <div class="col-md-2 mt-2">
                            <input type="hidden" name="variant_id[]" value="{{$variant_val->variant->id}}">
                            <label class="control-label ml-1">{{ $variant_val->variant->name ?? '' }}</label>
                        </div>
                        <div class="col-md-2">
                            <input class="form-control form-white" placeholder="Enter no of items" type="text" name="quantity[]" id="qty_var_{{$i}}" value=""/>
                        </div>
                        <div class="col-md-3">
                            <select class="form-control search_val" name="container_type[]" id="container_var_{{$i}}">
                                <option value="">Select Type</option>
                                @foreach($container_type as $key => $value)
                                <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2">
                            <input class="form-control form-white" placeholder="Enter price" type="text" name="price[]" value="" id="price_var_{{$i}}"/>
                        </div>
                        <div class="col-md-2">
                            <input class="form-control form-white" placeholder="Enter discount price" type="text" name="discount_price[]" value="" id="discount_price_var{{$i}}"/>
                        </div>
                        <div class="col-md-1 text-left">
                            <a class="btn btn-sm btn-success text-white add_more" title="Add New" data-id="{{$i}}" data-outer-id="{{$i}}" data-var_id="{{$variant_val->variant->id}}" id="add_price_complex"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                    @endif
                </div>
                @endforeach
                <script>
//    var cnt = 0;
                    $(".add_more").click(function () {
                        var cnt = $(this).attr("data-id");
                        var outer_cnt = $(this).attr("data-outer-id");
                        var var_id = $(this).data("var_id");
//        cnt++;
                        var div_cnt = cnt++;

                        $(".cls_outer_var_div" + outer_cnt).append('<div class="row ml-0 cls_content_var_div' + outer_cnt + div_cnt + '"><div class="col-md-2 mt-2"><input type="hidden" name="variant_id[]" value="' + var_id + '"></div>\n\
                                <div class="col-md-2"><input class="form-control form-white" placeholder="Enter no of items" type="text" name="quantity[]" id="qty_var_' + outer_cnt + div_cnt + '" value=""/></div>' +
                                '<div class="col-md-3"><select class="form-control search_val" name="container_type[]" id="container_var_' + outer_cnt + div_cnt + '"><option value="">Select Type</option>' +
                                '@foreach($container_type as $key => $value)' +
                                '<option value="{{$key}}">{{$value}}</option>' +
                                '@endforeach' +
                                '</select></div>' +
                                '<div class="col-md-2"><input class="form-control form-white" placeholder="Enter price" type="text" name="price[]" value="" id="price_var_' + outer_cnt + div_cnt + '"/></div>' +
                                '<div class="col-md-2"><input class="form-control form-white" placeholder="Enter discount price" type="text" name="discount_price[]" value="" id="discount_price_var' + outer_cnt + div_cnt + '"/></div>' +
                                '<div class="col-md-1 text-left"><button class="btn btn-sm btn-danger text-white btn_remove" title="Remove Attributes">' +
                                '<i class="fa fa-minus"></i></button></div></div>');
                        cnt++;
                        $('#add_price_complex').attr('data-id', cnt);
                    });
                    $('body').on('click', '.btn_remove', function () {
                        $(this).parent().parent().remove();
//                        cnt--;
                    });
                </script>
                @endif

            </div>
            @else
            <div class="cls_outer_div1 mb-3">
                @php
                $pdt_b2b_simple = \App\Models\ProductB2BPrice::with('attribute')->with('variant')->where('product_id', $pdt_id)->get();
                @endphp
                @if(count($pdt_b2b_simple)> 0)
                @php
                $j = 0;
                @endphp
                @foreach($pdt_b2b_simple as $row_b2b_simple)
                @php
                $j++;
                @endphp
                <div class="row ml-0 cls_content_div{{$j}}">
                    <div class="col-md-3">
                        @if($j == 1)<label class="control-label">No of items <span class="text-danger">*</span></label>@endif
                        <input class="form-control form-white" placeholder="Enter no of items" type="text" name="quantity[]" value="{{$row_b2b_simple['quantity']}}" id="qty_var_{{$j}}"/>
                    </div>
                    <div class="col-md-2">
                        @if($j == 1)<label class="control-label">&nbsp;</label>@endif
                        <select class="form-control search_val" name="container_type[]" id="container_var_{{$j}}">
                            <option value="">Select Type</option>
                            @foreach($container_type as $key => $value)
                            <option value="{{$key}}" {{ ($row_b2b_simple['container_type'] == $key) ? 'selected' : '' }}>{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        @if($j == 1)<label class="control-label">Price(Per item) <span class="text-danger">*</span></label>@endif
                        <input class="form-control form-white" placeholder="Enter price" type="text" name="price[]" value="{{$row_b2b_simple['price']}}" id="price_var_{{$j}}"/>
                    </div>
                    <div class="col-md-3">
                        @if($j == 1)<label class="control-label">Discount Price <span class="text-danger">*</span></label>@endif
                        <input class="form-control form-white" placeholder="Enter discount price" type="text" name="discount_price[]" value="{{$row_b2b_simple['discount_price']}}" id="discount_price_var_{{$j}}"/>
                    </div>
                    @if($j == 1)
                    <div class="col-md-1 text-left">
                        <label class="control-label"></label><br/>
                        <a class="btn btn-sm btn-success text-white mt-3 add_price_btn_simple" data-id="{{$j}}" title="Add New" id="add_price_simple"><i class="fa fa-plus"></i></a>
                    </div>
                    @else
                    <div class="col-md-1 text-left">
                        <button class="btn btn-sm btn-danger text-white btn_remove" title="Remove Attributes"><i class="fa fa-minus"></i></button>
                    </div>
                    @endif
                </div>
                @endforeach
                @else
                <div class="row ml-0 cls_content_div1">
                    <div class="col-md-3">
                        <label class="control-label">No of items <span class="text-danger">*</span></label>
                        <input class="form-control form-white" placeholder="Enter no of items" type="text" name="quantity[]" value="" id="qty_var_1"/>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">&nbsp;</label>
                        <select class="form-control search_val" name="container_type[]" id="container_var_1">
                            <option value="">Select Type</option>
                            @foreach($container_type as $key => $value)
                            <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label class="control-label">Price(Per item) <span class="text-danger">*</span></label>
                        <input class="form-control form-white" placeholder="Enter price" type="text" name="price[]" value="" id="price_var_1"/>
                    </div>
                    <div class="col-md-3">
                        <label class="control-label">Discount Price <span class="text-danger">*</span></label>
                        <input class="form-control form-white" placeholder="Enter discount price" type="text" name="discount_price[]" value="" id="discount_price_var_1"/>
                    </div>
                    <div class="col-md-1 text-left">
                        <label class="control-label"></label><br/>
                        <a class="btn btn-sm btn-success text-white mt-3 add_price_btn_simple" data-id="1" title="Add New" id="add_price_simple"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                @endif
            </div>
            @endif
        </div>
        <div class="modal-footer">
            <input type="hidden" id="pdt_id" name="pdt_id" value="{{ $pdt_id }}">
            <input type="hidden" id="submit_action" value="" />
            @if(!empty($pdt_id))
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                Save
            </button>
            @endif
            <button type="submit" class="btn btn-info waves-effect waves-light save-and-continue">
                Save & Continue
            </button>
            <a class="btn btn-default waves-effect tab_back" data-id='{{ $pdt_id }}' href="javascript:void(0);">Back</a>
        </div>
    </div>
</form>
<script>
    $(".save-and-continue").on('click', function () {
        $("#submit_action").val('continue');
    });
    $(".save-btn").on('click', function () {
        $("#submit_action").val('save');
    });

    $(".add_price_btn_simple").click(function () {
        var cnt = $(this).attr("data-id");
        var div_cnt = cnt + 1;
        $(".cls_outer_div1").append('<div class="row ml-0 cls_content_div' + div_cnt + '"><div class="col-md-3">' +
                '<input class="form-control form-white" placeholder="Enter no of items" type="text" name="quantity[]" value="" id="qty_var_' + div_cnt + '"/></div>' +
                '<div class="col-md-2"><select class="form-control search_val" name="container_type[]" id="container_var_' + div_cnt + '"><option value="">Select Type</option>' +
                '@foreach($container_type as $key => $value)' +
                '<option value="{{$key}}">{{$value}}</option>' +
                '@endforeach' +
                '</select></div>' +
                '<div class="col-md-3"><input class="form-control form-white" placeholder="Enter price" type="text" name="price[]" value="" id="price_var_' + div_cnt + '"/></div>' +
                '<div class="col-md-3"><input class="form-control form-white" placeholder="Enter discount price" type="text" name="discount_price[]" value="" id="discount_price_var_' + div_cnt + '"/></div>' +
                '<div class="col-md-1 text-left"><button class="btn btn-sm btn-danger text-white btn_remove" title="Remove Attributes">' +
                '<i class="fa fa-minus"></i></button></div></div>');
        cnt++;
        $('#add_price_simple').attr('data-id', cnt);
    });
    $('body').on('click', '.btn_remove', function () {
        $(this).parent().parent().remove();
//        cnt--;
    });




    $('.tab_back').on('click', function () {
        var pdt_id = $(this).data("id");
        $.ajax({
            type: "GET",
            url: "{{route('product_tabs')}}",
            data: {'activeTab': 'B2C PRICE', pdt_id: pdt_id},
            success: function (result) {
                $('#pdt_tab a[href="#b2cprice"]').tab('show');
                $('.tab-content').html(result);
            }
        });
    });
    $("#frm_create_b2b_price").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            "quantity[]": {
                required: true,
                number: true,
            },
            "container_type[]": {
                required: true,
            },
            "price[]": {
                required: true,
                number: true,
            },
            "discount_price[]": {
                required: true,
                number: true,
            },
        },
        messages: {
            "quantity[]": {
                required: 'Quantity is required',
                number: "Invalid number"
            },
            "container_type[]": {
                required: 'Type is required'
            },
            "price[]": {
                required: 'Price is required',
                number: "Decimal numbers only"
            },
            "discount_price[]": {
                required: 'Discount price is required',
                number: "Decimal numbers only"
            },
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "{{route('save_product_b2b_price')}}",
                data: $('#frm_create_b2b_price').serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 1) {
                        if ($("#submit_action").val() == 'continue') {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#pdt_tab a[href="#photos"]').tab('show');
                            $('.tab-content').html(data.result);
                        } else {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                        }
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
                    $('button:submit').attr('disabled', false);
                },
                error: function (err) {
                    $('button:submit').attr('disabled', false);
                }
            });
            return false;
        }
    });

</script>