<form id="frm_create_product" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <label class="control-label">SKU <span class="text-danger">*</span></label>
                    <input class="form-control form-white" placeholder="Enter SKU" type="text" name="sku" value="{{ isset($row_data['id']) ? $row_data['sku'] : $sku}}" readonly/>
                </div>
                <div class="col-md-6">
                </div>
                <div class="col-md-6">
                    <label class="control-label">Product Name (EN) <span class="text-danger">*</span></label>
                    <input class="form-control form-white" placeholder="Enter product name (EN)" type="text" name="name_en" value="{{ isset($row_data['id']) ? $row_data['lang'][0]['name'] : ''}}"/>
                </div>
                <div class="col-md-6 text-right">
                    <label class="control-label">Product Name (AR) <span class="text-danger">*</span></label>
                    <input class="form-control form-white text-right" placeholder="Enter product name (AR)" type="text" name="name_ar" value="{{ isset($row_data['id']) ? $row_data['lang'][1]['name'] : ''}}"/>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Category <span class="text-danger">*</span></label><br/>
                    <select class="select2_multiple" name="sec_category[]" multiple="multiple" id="sec_category">
                        @foreach($multiple_cat as $row_val)
                        <option value="{{ $row_val->category_id }}" {{ !empty($cat_ids) && in_array($row_val->category_id, $cat_ids) ? 'selected' : '' }}>{{ $row_val->name }}</option>
                        @endforeach
                    </select>
                    <label class="control-label" id="error_chk"></label>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Attribute Mapping Category <span class="text-danger">*</span></label>
                    <select class="form-control" name="category">
                        <option value=""> Select category </option>
                        @foreach($cat_data as $cat_data_val)
                        <option value="{{ $cat_data_val->id }}" {{ (isset($row_data['id']) &&  $cat_data_val->id == $row_data['category_id']) ? 'selected' : '' }}>{{ $cat_data_val->lang[0]->name }}</option>
                        @if(count($cat_data_val->subcategory))
                        @foreach($cat_data_val->subcategory as $sub_cat_val)
                        <option value="{{ $sub_cat_val->id }}" {{ (isset($row_data['id']) &&  $sub_cat_val->id == $row_data['category_id']) ? 'selected' : '' }}>&nbsp;&nbsp;{{ $sub_cat_val->lang[0]->name }}</option>
                        @php
                        $sub_sub_cat = \App\Models\Category::with('lang')->where(['parent_id' => $sub_cat_val->id, 'status' => 'active'])->get();
                        @endphp
                        @if(count($sub_sub_cat))
                        @foreach($sub_sub_cat as $sub_sub_cat_val)
                        <option value="{{ $sub_sub_cat_val->id }}" {{ (isset($row_data['id']) &&  $sub_sub_cat_val->id == $row_data['category_id']) ? 'selected' : '' }}>&nbsp;&nbsp;&nbsp;&nbsp;{{ $sub_sub_cat_val->lang[0]->name }}</option>
                        @endforeach
                        @endif
                        @endforeach
                        @endif
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Brand <span class="text-danger">*</span></label>
                    <select class="form-control" name="brand">
                        <option value=""> Select brand </option>
                        @foreach($brand_data as $brand_data_val)
                        <option value="{{ $brand_data_val->brand_id }}" {{ (isset($row_data['category_id']) &&  $brand_data_val->brand_id == $row_data['brand_id']) ? 'selected' : '' }}>{{ $brand_data_val->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Tag </label><br/>
                    <select class="select2_multiple_tag" name="tag[]" multiple="multiple" id="tag">
                        @foreach($tag as $row_val)
                        <option value="{{ $row_val->tag_id }}" {{ !empty($tag_ids) && in_array($row_val->tag_id, $tag_ids) ? 'selected' : '' }}>{{ $row_val->name }}</option>
                        @endforeach
                    </select>
                    <label class="control-label" id="error_chk"></label>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Age </label><br/>
                    <select class="select2_multiple_age" name="age[]" multiple="multiple" id="age">
                        @foreach($age as $row_val)
                        <option value="{{ $row_val->age_id }}" {{ !empty($age_ids) && in_array($row_val->age_id, $age_ids) ? 'selected' : '' }}>{{ $row_val->name }}</option>
                        @endforeach
                    </select>
                    <label class="control-label" id="error_chk"></label>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Gender </label><br/>
                    <select class="select2_multiple_gender" name="gender[]" multiple="multiple" id="gender">
                        @foreach($gender as $row_val)
                        <option value="{{ $row_val->gender_id }}" {{ !empty($gender_ids) && in_array($row_val->gender_id, $gender_ids) ? 'selected' : '' }}>{{ $row_val->name }}</option>
                        @endforeach
                    </select>
                    <label class="control-label" id="error_chk"></label>
                </div>
                    <input type="hidden" name="product_type" value="simple" >
               <div class="col-md-6">
                    <label class="control-label">Description (EN) <span class="text-danger">*</span></label>
                    <textarea class="form-control area" name="description_en" placeholder="Enter description (EN)">{{ isset($row_data['id']) ? $row_data['lang'][0]['description'] : ''}}</textarea>
                </div>
                <div class="col-md-6 text-right">
                    <label class="control-label">Description (AR) <span class="text-danger">*</span></label>
                    <textarea class="form-control area text-right" name="description_ar" placeholder="Enter description (AR)">{{ isset($row_data['id']) ? $row_data['lang'][1]['description'] : ''}}</textarea>
                </div>
                <div class="col-md-6">
                    <label>Specification (EN) <sup class="star">*</sup></label>
                    <textarea class="form-control" id="summaryen" name="summaryen">{{ isset($row_data['id']) ? $row_data['lang'][0]['specification'] : ''}}</textarea>
                    <label class="summaryen_error"></label>
                </div>
                <div class="col-md-6">
                    <label>Specification (AR) <sup class="star">*</sup></label>
                    <textarea class="form-control" id="summaryar" name="summaryar">{{ isset($row_data['id']) ? $row_data['lang'][1]['specification'] : ''}}</textarea>
                    <label class="summaryar_error"></label>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" id="pdt_id" name="pdt_id" value="{{ isset($row_data['id']) ? $row_data['id'] : '' }}">
            @if(isset($row_data['id']))
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                Save
            </button>
            @endif
            <button type="submit" class="btn btn-info waves-effect waves-light save-and-continue">
                Save & Continue
            </button>

            <input type="hidden" id="submit_action" value="" />
            <a class="btn btn-default waves-effect" href="{{ route('products') }}">Back</a>
        </div>
    </div>
</form>
<script>
    CKEDITOR.replace('summaryen', {
    removePlugins: 'link',
    // fullPage:true
} );
    CKEDITOR.replace('summaryar', {
    removePlugins: 'link',
    contentsLangDirection: 'rtl'
    } );

    $(".select2_multiple").select2({
        placeholder: "Select Multiple Category",
        allowClear: true
    });
//    $(".select2-selection--multiple").addClass("form-control");
    $(".select2-selection--multiple").css({"border": "1px solid #ccc", "border-radius": "0", "padding": "3px 0px 9px 0px", "height": "auto", "width": "432"});

    $(".select2_multiple_tag").select2({
        placeholder: "Select Multiple Tag",
        allowClear: true
    });
    $(".select2-selection--multiple").css({"border": "1px solid #ccc", "border-radius": "0", "padding": "3px 0px 9px 0px", "height": "auto", "width": "432"});

    $(".select2_multiple_age").select2({
        placeholder: "Select Multiple Age",
        allowClear: true
    });
    $(".select2-selection--multiple").css({"border": "1px solid #ccc", "border-radius": "0", "padding": "3px 0px 9px 0px", "height": "auto", "width": "432"});

    $(".select2_multiple_gender").select2({
        placeholder: "Select Multiple Gender",
        allowClear: true
    });
    $(".select2-selection--multiple").css({"border": "1px solid #ccc", "border-radius": "0", "padding": "3px 0px 9px 0px", "height": "auto", "width": "432"});
    

    $(".save-and-continue").on('click', function (e) {
        $("#submit_action").val('continue');
    });
    $(".save-btn").on('click', function (e) {
        $("#submit_action").val('save');
    });

    $("#frm_create_product").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            sku: {
                required: true,
            },
            name_en: {
                required: true,
            },
            name_ar: {
                required: true,
            },
            category: {
                required: true,
            },
            "sec_category[]": {
                required: true,
            },
            brand: {
                required: true,
            },
            description_en: {
                required: true,
            },
            description_ar: {
                required: true,
            }
        },
        messages: {
            sku: {
                required: 'SKU is required.'
            },
            name_en: {
                required: 'Product name(EN) is required.'
            },
            name_ar: {
                required: 'Product name(AR) is required.'
            },
            category: {
                required: 'Variant category is required.'
            },
            "sec_category[]": {
                required: 'Category is required.'
            },
            brand: {
                required: 'Brand is required.'
            },
            description_en: {
                required: 'Description(EN) is required.'
            },
            description_ar: {
                required: 'Description(AR) is required.'
            },
        },
        errorElement: "label",
        errorPlacement: function (error, element) {
            if (element.attr("id") == "sec_category") {
                error.insertAfter("#error_chk");
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "{{route('save_product_info')}}",
                data: $('#frm_create_product').serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 1) {
                        if ($("#submit_action").val() == 'continue') {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#pdt_tab a[href="#spec_stock"]').tab('show');
                            $('.tab-content').html(data.result);
                        } else {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                        }
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                        $("#pdt_tab").tabs({cache: true});
                    }
                    $('button:submit').attr('disabled', false);
                },
                error: function (err) {
                    $('button:submit').attr('disabled', false);
                }
            });
            return false;
        }
    });
</script>