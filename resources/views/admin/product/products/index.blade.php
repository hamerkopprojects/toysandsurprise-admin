@extends('layouts.master')
@section('content-title')
PRODUCTS

@endsection
@section('stock-update')
<select class="form-control" name="discount_type" id="discount_type" onchange="location = this.value;">
    <option disabled selected hidden> Stock Update </option>
    <option value="{{ route('stock_update',['type'=>"simpleb2c"]) }}"> Stock Update</option> 
   
</select> 
@endsection
@section('price-update')
<select class="form-control" name="discount_type" id="discount_type" onchange="location = this.value;">
    <option disabled selected hidden> Price Update </option>
    <option value="{{ route('price_update',['type'=>"simpleb2c"]) }}"> Price Update </option>
</select>
@endsection
@section('add-import')
<a href="{{ route('import_products') }}" class="btn btn-info">
    <font style="vertical-align: inherit;"><i class="ti-download"></i> Import Simple Product
    </font>
</a>

@endsection
@section('add-btn')
<a href="javascript:void(0);" class="btn btn-info pdt_modal_btn">
    <font style="vertical-align: inherit;"><i class="ti-plus"></i> Add New Products
    </font>
</a>

@endsection
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="posts-filter" method="get" action="{{ route('products') }}">
                    <div class="row tablenav top text-left">
                        <div class="col-md-4 ml-0">
                            <select class="form-control search_val" name="cat_id">
                                <option value="">By Main Category</option>
                                @foreach($cat_data as $value)
                                <option value="{{$value['id']}}" {{ $value['id'] == $category_id ? 'selected' : '' }}>{{$value['cat_name']}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4 ml-0">
                            <input class="form-control" type="text" name="search" value="{{ $search }}" placeholder="Search by SKU / Product name">
                        </div>
                        <div class="col-md-4 text-left">
                            <button type="submit" class="btn btn-info">
                                <font style="vertical-align: inherit;">Search</font>
                            </button>
                            <a href="{{ route('products') }}" class="btn btn-default">Reset</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th>SKU</th>
                                <th>Product Name</th>
                                <th>Category</th>
                                {{-- <th>Product Type</th> --}}
                                <th>Price</th>
                                <th>Discount</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($pdt_data) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($pdt_data as $row_data)
                            @php $price = \App\Models\ProductB2CPrice::with('product')->select('price', 'discount_price')->where('product_id', $row_data->id)->first();@endphp
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->sku }}</td>
                                <td>{{ $row_data->lang[0]->name ?? '' }}</td>
                                <td>{{ $row_data->category->lang[0]->name ?? '' }}</td>
                                {{-- <td>{{ ucfirst($row_data->product_type) }}</td> --}}
                                <td>{{ $price->price ?? '' }}</td>
                                <td>{{ $price->discount_price ?? '' }}</td>
                                <td class="text-center">
                                    <button type="button" class="change-status btn btn-sm btn-toggle ml-0 {{ $row_data['status'] == 'active' ? 'active' : ''}}" data-toggle="button" data-id="{{ $row_data->id }}" data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" aria-pressed="true" autocomplete="off">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white view_btn" title="View Product Details" data-id="{{ $row_data->id }}"><i class="fa fa-eye"></i></a>
                                    <a class="btn btn-sm btn-success text-white pdt_modal_btn" title="Edit Product" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white" title="Delete Product" onclick="deleteProducts({{ $row_data->id }})"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $pdt_data->appends(request()->input())->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal pooup -->
<div class="modal none-border" id="formModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="max-height:85%;  margin-top: 50px; margin-bottom:50px;max-width: 100%">
        <div class="modal-content">

        </div>
    </div>
</div>
<!-- END MODAL -->
@endsection
@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/lib/cropper/cropper.min.css') }}">
@endpush
@push('scripts')

{{-- Sweet alert --}}
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
{{-- select2 --}}
<script src="{{ asset('assets/js/lib/cropper/cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/cropper/jquery-cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/custom_crop.js') }}"></script>
<script src="//cdn.ckeditor.com/4.14.0/basic/ckeditor.js"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.pdt_modal_btn').on('click', function() {
        var id = $(this).data("id");
        $.ajax({
            type: "GET",
            url: "{{route('create_product')}}",
            data: {
                id: id
            },
            success: function(data) {
                $('.modal-content').html(data);
                $('#formModal').modal('show');
            }
        });
    });
    $('.view_btn').on('click', function() {
        var id = $(this).data("id");
        $.ajax({
            type: "GET",
            url: "products/details/" + id,
            success: function(data) {
                $('.modal-content').html(data);
                $('#formModal').modal('show');
            }
        });
    });
    $('.change-status').on('click', function() {
        var pdt_id = $(this).data("id");
        var act_value = $(this).data("activate");
        $.confirm({
            title: act_value + ' Product',
            content: 'Are you sure to ' + act_value + ' the product?',
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('activate_product')}}",
                        data: {
                            id: pdt_id
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("products")}}';
                                }, 1000);
                                //                    window.location.reload();
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                                 window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                            }
                        }
                    });
                },
                No: function() {
                    window.location.reload();
                }
            }
        });
    });

    function deleteProducts(id) {
        $.confirm({
            title: '<span class="small">Are you sure to delete this product?</span>',
            content: 'You wont be able to revert this',
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('deleteProduct')}}",
                        data: {
                            id: id
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    }
</script>
@endpush