<div class="modal-header">
    <h4 class="modal-title"><strong>Product Details</strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="msg_div"></div>
    <div class="row">
        <div class="col-md-12">
            <label class="control-label"><b>SKU</b></label>
            <p>{{ $row_data['sku'] }}</p>
        </div>
        <div class="col-md-6">
            <label class="control-label"><b>Product Name (EN)</b></label>
            <p>{{ $row_data['product_name_en'] }}</p>
        </div>
        <div class="col-md-6 text-right">
            <label class="control-label"><b>Product Name (AR)</b></label>
            <p>{{ $row_data['product_name_ar'] }}</p>
        </div>
        <div class="col-md-6">
            <label class="control-label"><b>Category</b></label>
            <p>{{ $row_data['category'] }}</p>
        </div>
        <div class="col-md-6">
            <label class="control-label"><b>Brand</b></label>
            <p>{{ $row_data['brand'] }}</p>
        </div>
        <div class="col-md-6">
            <label class="control-label"><b>Tag</b></label>
            <p>{{ $row_data['tag'] }}</p>
        </div>
        <div class="col-md-6">
            <label class="control-label"><b>Age</b></label>
            <p>{{ $row_data['age'] }}</p>
        </div>
        <div class="col-md-6">
            <label class="control-label"><b>Gender</b></label>
            <p>{{ $row_data['gender'] }}</p>
        </div>
        <div class="col-md-6">
            
        </div>
        <div class="col-md-6">
            <label class="control-label"><b>Description (EN)</b></label>
            <p>{{ $row_data['description_en'] }}</p>
        </div>
        <div class="col-md-6 text-right">
            <label class="control-label"><b>Description (AR)</b></label>
            <p>{{ $row_data['description_ar'] }}</p>
        </div>
        <div class="col-md-6">
            <label class="control-label"><b>Ingredients & Nutrition Facts(EN)</b></label>
            <p>{{ $row_data['ingredients_en'] }}</p>
        </div>
        <div class="col-md-6 text-right">
            <label class="control-label"><b>Ingredients & Nutrition Facts(AR)</b></label>
            <p>{{ $row_data['ingredients_ar'] }}</p>
        </div>
        <div class="col-md-6">
            <label class="control-label"><b>How To Use (EN)</b></label>
            <p>{{ $row_data['how_to_use_en'] }}</p>
        </div>
        <div class="col-md-6 text-right">
            <label class="control-label"><b>How To Use (AR)</b></label>
            <p>{{ $row_data['how_to_use_ar'] }}</p>
        </div>
        <div class="col-md-6">
            <label class="control-label"><b>Reasons To Buy (EN)</b></label>
            <p>{{ $row_data['reasons_to_buy_en'] }}</p>
        </div>
        <div class="col-md-6 text-right">
            <label class="control-label"><b>Reasons To Buy (AR)</b></label>
            <p>{{ $row_data['reasons_to_buy_ar'] }}</p>
        </div>
        <div class="col-md-6">
            <label class="control-label"><b>Stock</b></label>
            <p>{{ $row_data['stock'] }}</p>
        </div>
        <div class="col-md-6">
            <label class="control-label"><b>Price</b></label>
            <p>{{ $row_data['price'] }}</p>
        </div>
        <div class="col-md-6">
            <label class="control-label"><b>Discount Price</b></label>
            <p>{{ $row_data['discount_price'] }}</p>
        </div>
    </div>
</div>
