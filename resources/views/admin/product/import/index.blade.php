@extends('layouts.master')
@section('content-title')
Import Simple Product - Step 1
@endsection
@section('add-btn')
<a href="{{ route('products') }}">
    <font style="vertical-align: inherit;">Back
    </font>
</a>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="frm-excel-import" action="javascript:;" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-12">
                            <p><strong>Import Product List</strong></p>
                            <p class="small"><b>Insructions </b><br/>
                                1. Supported file formats: <b>xlsx</b><br>
                                2. Max. file size: <b>2 MB</b><br>
                                3. Follow the excel fields and order of fields in the format
                            </p>
                            <hr>
                            <a href="{{ asset('sample/products/Toys-Product-Sample.xlsx') }}" class="cl-btn-dwnld float-right" style="color: blue;"><i class="fa fa-download"></i> Download the sample Excel file</a>
                            <div class="form-group upload_btn">
                                <div class="tower-file">
                                    <input type="file" name="select_file" id="select_file" style="display:none">
                                    <label for="select_file" class="btn btn-primary" data-default-text="Upload Photo">
                                        <span class="mdi mdi-upload"></span>Choose the Excel File to Import
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 text-left">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-info" id="btn-import-submit">Import</button>
                            <button type="button" class="btn btn-default" onclick="location.href ='{{route('products')}}'">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script>
    $("#frm-excel-import").submit(function () {
        $('.loading_box').show();
        $('.loading_box_overlay').show();
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $("#btn-import-submit").on('click', function () {
        $('.loading_box').show();
        $('.loading_box_overlay').show();
        var form_data = new FormData($('form')[0]);
        $.ajax({
            type: "POST",
            url: "{{route('import_submit')}}",
            data: form_data,
            dataType: "json",
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function () {
                $('#btn-import-submit').button('loading');
            },
            complete: function () {
                $('#btn-import-submit').button('reset');
            },
            success: function (data) {
                if (data.status == 1) {
                    window.location.href = '{{route("product_excel_view")}}';
                } else {
                    $('.loading_box').hide();
                    $('.loading_box_overlay').hide();
                    Toast.fire({
                        icon: 'error',
                        title: data.message
                    });
                }
            },
            error: function () {}
        });
        return false;
    });
</script>
@endpush