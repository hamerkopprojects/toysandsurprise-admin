@extends('layouts.master')

@section('content-title')
RATING SEGMENTS
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Segment title (EN)</th>
                                <th>Segment title (AR)</th>
                                <th width="25%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($rating) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($rating as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->lang[0]->name }}</td>
                                <td class="right-align">{{ $row_data->lang[1]->name }}</td>
                                <td class="text-center">
                                    <button
                                        type="button"
                                        class="change-status active_segment btn btn-sm btn-toggle mr-md-4 ml-0 {{ $row_data['status']}}"
                                        data-toggle="button"
                                        data-id="{{ $row_data->id }}"
                                        data-activate="{{ $row_data['status']}}"
                                        aria-pressed="true"
                                        autocomplete="off"
                                        id="active_segment">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white edit_btn edit-style" title="Edit"
                                        data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    {{-- <a class="btn btn-sm btn-danger text-white  cust_delete" title="Delete "
                                        data-id={{ $row_data->id }}><i class="fa fa-trash"></i></a> --}}
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    
                </div>
            </div>
        </div>
    </div>
</div>
{{-- <div class="card">
    <div class="card-body">
        <div id="msgDiv"></div>
        <div class="table-responsive">
            {{$dataTable->table(['width' => '100%', 'class' => 'table table-borderd table-hover table-striped table-bordered  '])}}
        </div>
    </div>
</div> --}}
    <!-- /# column -->

<div class="modal fade" id="segment-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">EDIT SEGMENT</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"  data-no-padding="no-padding">
                <form id="segment_rating" method="POST" action="#" >
                    
                    @csrf
                    {{-- @method('PUT') --}}
                    <input type="hidden" id="segment_id" name="segment_id"/>
                    <div class="row ">
                        <div class="col-md-6">
                            <label>Segment title (EN)</label>
                            <input class="form-control" name="segment_title_en" 
                            id="segment_title_en" />
                            {{-- @error('english')
                            <span class="error">{{ $message }}</span>
                           
                            @enderror --}}
                        </div>
                        <div class="col-md-6">
                            <label>Segment title (AR)</label>
                            <input class="form-control" name="segment_title_ar" id="segment_title_ar" style="text-align:right !important" />
                            {{-- @error('arabic')
                            <span class="error">{{ $message }}</span>
                            @enderror --}}
                        </div>
                        <div class="col-lg-12">
                            <div class="row 5">
                                <div class="col-md-12 text-md-left">
                                    <button type="submit" id="save_segment" class="btn btn-info waves-effect waves-light">
                                        Save
                                    </button>
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{{-- <div class="table-responsive">
    {{$dataTable->table(['width' => '100%'])}}
</div> --}}
@endsection

@push('css')
<link href="{{ asset('assets/css/lib/data-table/dataTables.bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/css/cust-style.css') }}" rel="stylesheet" />
@endpush

@push('scripts')
{{-- {{$dataTable->scripts()}} --}}

{{-- Sweet alert --}}
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
{{-- select2 --}}
<script>
    var segment;
    $('.edit-style').on('click', function(e) {
        segment = $(this).data('id')
    // console.log('custt', segment);
        var url = "segments/edit/";
       
       $.get(url  + segment, function (data) {
           //success data
           $('#segment_title_en').val(data.rating.lang[0].name);
           $('#segment_title_ar').val(data.rating.lang[1].name);
           $('#segment_id').val(data.rating.id);
           $('#segment-popup').modal({
            show: true

            });
       }) 
        
       
    })
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
   

    $('#save_segment').on('click',function(){
        $("#segment_rating").validate({
            rules: {
                segment_title_en: {           //input name: fullName
                    required: true,   //required boolean: true/false      
                },
                segment_title_ar: {            //input name: message
                    required: true,
                }
            },
            messages: {               //messages to appear on error
                segment_title_en: {
                      required:"Segment title(EN) Required",
                      
                      },
               
                      segment_title_ar: {
                      required: "Segment title(AR) Required",
                      }
            },
            submitHandler: function(form) {
                //    $(form).({
                //             url:"{{route('segments.update')}}",
                //             type:"post",
                //             success: function(){
                //               alert('inside');
                             
                //       }
                //     });
                sementTitleEn= $('#segment_title_en').val();
                sementTitleAr= $('#segment_title_ar').val();
                segmentId= $('#segment_id').val();
                $.ajax({
                type:"POST",
                url: "{{route('segments.update')}}",
                data:{ 
                    segmentTitleEn:sementTitleEn,
                    segmentTitleAr:sementTitleAr,
                    id:segmentId,
                },
    
                success: function(result){
                    $('#segment-popup').modal('hide')
                   window.location.reload();
                    Toast.fire({
                             icon: 'success',
                                title: ' Segment update successfully'
                            });
                }
          })
        }
    })
    })
    $('.active_segment').on('click',function(e){
        segmentId = $(this).data('id');
        status=$(this).data('activate');
        console.log("segment",segmentId ,"status",status);
        $.ajax({
                type:"POST",
                url: "{{route('segments.status')}}",
                data:{ 
                    id:segmentId,
                    status:status
                },
    
                success: function(result){
                    window.location.reload();
                    Toast.fire({
                        icon: 'success',
                        title: ' Status updated successfully'
                });
                }
          })
    })
</script>
@endpush