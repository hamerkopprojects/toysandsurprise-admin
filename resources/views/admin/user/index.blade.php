@extends('layouts.master')
@section('content-title')
USER
@endsection
@section('add-btn')
<button class="btn btn-primary" id="add_new_user">
    <i class="ti-plus"></i> Add New User
</button>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="posts-filter" method="get" action="{{ route('user-list.get') }}">
                    <div class="row tablenav top text-right">
                        <div class="col-md-5 ml-0">
                            <input class="form-control" type="text" name="search" value="{{$search}}" placeholder="Search by Name / Email / Phone/User id">
                        </div>
                        <div class="col-md-4 ml-0">
                            <select class="form-control search_val" name="select_role" id="select_role">
                                <option value="">By Role</option>
                                @foreach($role as $rolelist)
                                <option value="{{$rolelist->id}}" {{ $rolelist->id ==  $role_id  ? 'selected' : '' }}>{{$rolelist->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3 text-left">
                            <button type="submit" class="btn btn-info">
                                <font style="vertical-align: inherit;">Search</font>
                            </button>
                            <a href="{{ route('user-list.get') }}" class="btn btn-default reset_style">Reset</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th>Name</th>
                                <th>User ID</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Role</th>
                                <th width="25%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($user) > 0)
                            @php
                            $i = 1;
                            @endphp

                            @foreach ($user as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->name }}</td>
                                <td>{{ $row_data->user_id }}</td>
                                <td>{{ $row_data->email }}</td>
                                <td>{{ $row_data->phone }}</td>
                                <td>{{ $row_data->roles[0]->name ?? ''}}</td>
                                <td class="text-left">
                                    <button type="button" class="change-status btn btn-sm btn-toggle mr-md-4 ml-0 {{$row_data->status}}" data-toggle="button" data-id="{{ $row_data->id }}" data-status="{{$row_data->status}}" aria-pressed="true" autocomplete="off">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    {{-- <a href="{{ route('customer_details', ['id' => $row_data->id]) }}" class="btn btn-sm btn-info text-white view_btn" title="View"><i class="fa fa-eye"></i></a> --}}
                                    <a class="btn btn-sm btn-success text-white edit_btn edit_user" title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>

                                    <a class="btn btn-sm btn-danger text-white delete_user" title="Delete " data-id="{{ $row_data->id }}"><i class="fa fa-trash"></i></a>
                                    <a class="btn btn-sm btn-danger text-white password_send" title="Send password" data-id="{{ $row_data->id }}"><i class="fa fa-envelope"></i></a>

                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $user->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Pop Up --}}
<div class="modal fade" id="user_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="name_change"><strong>
                        ADD USER
                    </strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form id="user_form" action="javascript:;" method="POST">
                <div class="modal-body">
                    <div class="msg_div"></div>
                    <div class="row">
                        <input type="hidden" name="user_unique" id='user_unique'>
                        <div class="col-md-6">
                            <label class="control-label">Name <span class="text-danger">*</span></label>
                            <input class="form-control form-white" placeholder="Enter name" type="text" name="user_name" id="user_name" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Email <span class="text-danger">*</span></label>
                            <input class="form-control form-white" placeholder="Enter email address" type="email" id="email" name="email" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Phone <span class="text-danger">*</span></label>
                            <input class="form-control form-white" placeholder="Enter phone number" type="text" name="phone" id="phone" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Role <span class="text-danger">*</span></label>
                            <select class="form-control" name="role" id="role">
                                <option value=""> Select Role </option>
                                @foreach($role as $rolelist)
                                <option value="{{$rolelist->id}}">{{$rolelist->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">National Id</label>
                            <input class="form-control form-white" placeholder="Enter National id" type="text" name="national_id" id="national_id">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {{-- <input type="hidden" id="_customer_id" name="_customer_id" value="@if(isset($row_data['id'])) {{ $row_data['id'] }}@endif"> --}}
                    <button type="submit" class="btn btn-info waves-effect waves-light save-categorys" id="save_data">
                        ADD
                    </button>
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- end popup --}}
@endsection
@push('css')
<style>
    .reset_style {
        margin-left: 15px;
    }
</style>
@endpush
@push('scripts')

<script>
    

    $('#add_new_user').on('click', function() {
        $('#name_change').html('Add User');
        $('#save_data').text('Add').button("refresh");
        $("#user_form")[0].reset();
        $('#user_popup').modal({
            show: true
        });
    })
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.save-categorys').on('click', function(e) {
        // e.preventDefault();
        // console.log("insiu");

        $("#user_form").validate({
            rules: {
                user_name: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true

                },
                phone: {
                    required: true,
                    number: true,
                    minlength: 6, // will count space
                    maxlength: 12,

                },
                role: {
                    required: true,
                    // email:true

                },
            },
            messages: {
                user_name: {
                    required: "User name required",
                },
                email: {
                    required: "Email required",
                    email: "Enter valid email"
                },
                phone: {
                    required: "Phone number required",
                    number: "Enter a valid phone number"
                },
                role: {
                    required: 'Role required'

                },
                //  field3: "Please specify the City and State and Country",
                //  field4: "Please specify the City and State and Country"
            },
            submitHandler: function(form) {
                user_unique = $("#user_unique").val();
                if (user_unique) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('user-list.update')}}",
                        data: {
                            name: $('#user_name').val(),
                            email: $('#email').val(),
                            phone: $('#phone').val(),
                            role: $('#role').val(),
                            nationalId: $('#national_id').val(),
                            user_unique: user_unique


                        },

                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#user_form")[0].reset();
                            } else {
                                // console.log(data.message);
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });


                } else {
                    $.ajax({
                        type: "POST",
                        url: "{{route('user-list.store')}}",
                        data: {
                            name: $('#user_name').val(),
                            email: $('#email').val(),
                            phone: $('#phone').val(),
                            role: $('#role').val(),
                            nationalId: $('#national_id').val(),


                        },

                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#user_form")[0].reset();
                            } else {
                                // console.log(data.message);
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });

                }



            }
        })
    });

    $('.edit_user').on('click', function(e) {
        page = $(this).data('id')
        // console.log("id",page);
        // console.log('editInside');

        $('#name_change').html('Edit User');
        $('#save_data').text('Save').button("refresh");


        var url = "user-list/edit/";

        $.get(url + page, function(data) {
            console.log(data);
            $('#user_name').val(data.user.name),
                $('#email').val(data.user.email),
                $('#phone').val(data.user.phone),
                $('#role').val(data.user.role_id),
                $('#national_id').val(data.user.national_id)
            $('#user_unique').val(data.user.id)
            $('#user_popup').modal({
                show: true

            });
        });
    });

    $('.change-status').on('click', function(e) {

        id = $(this).data('id');
        status = $(this).data('status');
        console.log("status", status);
        $.ajax({
            type: "POST",
            url: "{{route('user-list.status.update')}}",
            data: {
                id: id,
                status: status
            },

            success: function(data) {

                Toast.fire({
                    icon: 'success',
                    title: data.message
                });

            }
        });

    })

    $('.delete_user').on('click', function() {
        Swal.fire({
                title: 'Are you sure to delete?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            })
            .then((result) => {
                if (result.value) {
                    $.ajax({
                        url: "{{route('user-list.delete')}}",
                        type: 'POST',
                        data: {
                            id: $(this).data('id')
                        },
                        success: function(data) {
                            console.log(data);
                            if (data.type === 'error') {

                                Swal.fire({
                                    icon: 'error',
                                    text: data.msg,
                                })
                                //     swal({
                                //         title: 'Error!',
                                //         text: data.msg,
                                //         type: 'error',
                                //         confirmButtonText: 'OK'
                                //     }).then(() => {
                                //          console.log('triggered redirect here');
                                // });
                            } else {
                                Toast.fire({
                                    icon: 'success',
                                    title: 'user deleted successfully'
                                });
                                window.location.reload();
                            }

                        }
                    });
                }
            });

    });
    $('.password_send').on('click', function(e) {
        e.preventDefault();

        Swal.fire({
                title: 'Confirmation',
                text: "Are you sure you want to send password",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            })
            .then((result) => {
                if (result.value) {
                    $.ajax({
                        url: "{{route('user-list.setPassword')}}",
                        type: 'POST',
                        data: {
                            id: $(this).data('id')
                        },
                        success: function(data) {
                            // console.log(data);
                            if (data.type === 'success') {

                                Swal.fire({
                                    icon: 'success',
                                    text: data.msg,
                                })

                            }
                        }
                    })
                }
            });
    })
</script>
@endpush