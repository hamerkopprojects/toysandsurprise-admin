@extends('layouts.master')

@section('content-title')
SHOP MANAGEMENT
@endsection
@section('add-btn')
<button  class="btn btn-info create_btn" id="shop_management_add_btn">
    <i class="ti-plus"></i> Add New Shop Management
</button>
@endsection
@section('content')
{{-- @include('admin.cms.faqs.popup') --}}
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row tablenav top text-right">
                    <div class="col-md-6 ml-0">
                        <form action="{{route('shop.management')}}" id="search-shop-management-form" method="get">
                        <input type="text" value ="{{$search_field ?? ''}}"class="form-control" id="search_field" name="search_field" placeholder="Search shop management">
                            <input type="hidden" name="shop_management_select" id="shop_management_select">
                    </form>
                       
                    </div>
                    <div class="col-md-6 text-left">
                        <button type="button" onclick="event.preventDefault(); 
                        document.getElementById('search-shop-management-form').submit();" class="btn btn-info"><font style="vertical-align: inherit;">Search</font></button>
                        <a href="{{route('shop.management')}}" class="btn btn-default cancel_style">Reset</a>
                        
                    </div>
                </div>
            </div>
        </div>                  
            <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Name (EN)</th>
                                <th>Name (AR)</th>
                                <th>Address (EN)</th>
                                <th>Address (AR)</th>
                                <th>Location</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- @php 
                                 dd($jobManagement);
                            @endphp --}}
                            @if (count($shopManagement) > 0)
                            @php
                            $i = 1;
                                
                            @endphp
                            @foreach ($shopManagement as $row_data)
                            <tr>
                                {{-- <th>{{ $i++ }}</th> --}}
                                <td>{{ $row_data->lang[0]->name ?? '' }}</td>
                                <td class="right-align">{{ $row_data->lang[1]->name ?? '' }}</td>
                                <td>{{ $row_data->lang[0]->address ?? '' }}</td>
                                <td class="right-align">{{ $row_data->lang[1]->address ?? '' }}</td> 
                                <td>{{ $row_data->location ?? '' }}</td>
                                <td class="text-center">
                                    <button
                                        type="button"
                                        class="change-status btn btn-sm btn-toggle mr-md-4 ml-0 {{ $row_data['status']}}"
                                        data-toggle="button"
                                        data-id="{{ $row_data->id }}"
                                        data-activate="{{ $row_data['status']}}"
                                        aria-pressed="true"
                                        autocomplete="off"
                                        id="active_shop_category">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white edit_btn page_edit shop_management_edit"  title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white shop_management_id_del" data-id="{{ $row_data->id }}" title="Delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $shopManagement->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- popup --}}
<div class="modal fade" id="management-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg popup-promo" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Add Shop Management</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" data-no-padding="no-padding">
                <form id="add-management" method="POST" action="#">
                    <input type="hidden" id="id_pg" name="id_pg">
                    @csrf
                   
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label>Name (EN) <sup class="star">*</sup></label>
                            <textarea class="form-control area" name="title_en" id="title_en" placeholder="Enter your management name"></textarea>
                            <label class="title_en_error"></label>
                        </div>
                        <div class="col-md-6">
                            <label>Name (AR) <sup class="star">*</sup></label>
                            <textarea class="form-control area" name="title_ar" id="title_ar"
                                style="text-align:right !important" placeholder="أدخل اسم الفئة" ></textarea>
                            <label class="title_ar_error"></label>
                        </div>
                        <div class="col-md-6 english_label">
                            <label>Address (EN)</label>
                            <textarea class="form-control area" id="address_en" name="address_en"></textarea>
                            @error('address_en')
                            <span class="error"></span>
                            @enderror
                        </div>
                        <div class="col-md-6 arabic_label">
                            <label>Address (AR)</label>
                            <textarea class="form-control area" id="address_ar" name="address_ar" style="text-align:right !important"></textarea>
                            @error('address_ar')
                            <span class="error"></span>
                            @enderror
                        </div>
                        
                            <div class="col-md-12">
                                <label>Location <sup class="star">*</sup></label>
                                <input 
                                    id="location" 
                                    class="form-control" 
                                    name="location" 
                                    value="{{ old('locations','Riyadh Saudi Arabia') }}"
                                    placeholder="Enter your location" 
                                />
                                @error('location')
                                <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="col-md-12" style="">
                                <div id="map"></div>
                            </div>
                            <input id="latitude" type="hidden" value="{{ old('latitude', 24.7136) }}" name="latitude">
                            <input id="longitude" type="hidden" value="{{ old('longitude', 46.6753) }}" name="longitude">
                   
                                           
                        <div class="col-lg-12">
                            <div class="row 5">
                                <div class="col-md-12 text-md-left">
                                    <button type="submit"  class="btn btn-info waves-effect waves-light save_page">
                                        Save
                                    </button>
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    
    .cancel_style{
        margin-left: 20px;
    }
    #site-error{
    float:right;
    margin-right: 260px;
    margin-top: 10px;

}
.area{
        height: 80px;
}
#map {
        widows: 100%;
        height: 400px;
    }
.star{
    color:red;
}
.popup-promo {
        max-height: 100%;
        margin-top: 30px;
        margin-bottom: 50px;
        max-width: 60%;
    }
</style>

@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdn.ckeditor.com/4.14.0/basic/ckeditor.js"></script>

<script>
    let map;

    function initMap() {
        // The location of Uluru
        var lat =   $('#latitude').val();
        var long =   $('#longitude').val();
    
        if(lat.length === 0){
            var uluru = {lat: {{old('latitude', 24.7136) }} , lng: {{old('longitude', 46.6753)}} };
        }else{       
                var uluru = {lat: {{ old('latitude', 'JSON.parse(lat)') }}, lng: {{ old('longitude', 'JSON.parse(long)') }} };    
        }
        // The map, centered at Uluru
         map = new google.maps.Map(
            document.getElementById('map'), 
            {zoom: 8, center: uluru,
            gestureHandling: 'greedy'}
        );
        // The marker, positioned at Uluru
        marker = new google.maps.Marker({
            position: uluru, 
            map: map,
            draggable: true,
        });

        marker.addListener('dragend', setLatLng);

        const input = document.getElementById('location');
        let autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.bindTo('bounds', map);

       
        autocomplete.addListener('place_changed', function() {
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }

            let address = place.formatted_address.split(',');
            let formattedAddress = address.length > 1
                ? address[0] + ', ' +address[address.length - 1]
                : address[0];

            $('#location').val(formattedAddress);
            $('#latitude').val(place.geometry.location.lat());
            $('#longitude').val(place.geometry.location.lng());

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
        }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);
        });
    }

    function setLatLng(event) {
        const lat = event.latLng.lat();
        const lng = event.latLng.lng();
        $('#latitude').val(lat);
        $('#longitude').val(lng);

        setAddress(lat, lng)
    }

    function setAddress(lat, lng) {
        const geocoder = new google.maps.Geocoder();
        geocoder.geocode({'location': {lat, lng}}, function(results, status) {
            if (status === 'OK' && results[0]) {
                let address = results[0].formatted_address.split(',');
                let formattedAddress = address.length > 1
                    ? address[0] + ', ' +address[address.length - 1]
                    : address[0];

                $('#location').val(formattedAddress);
            } 
        });
    }
    
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?libraries=places,geocoder,drawing&key=AIzaSyBG_t-XkmJAtPKIpyhOoiXz6QuXphkaBQI&callback=initMap">
</script>
<script>
   
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }});
        
        $('#shop_management_add_btn').on('click',function(e){
            $('#add-management').trigger("reset")
            $('#exampleModalLabel').html('Add Shop Category');
            $('#management-popup').modal({
                show:true
            })
        })

        $('.shop_management_edit').on('click',function(e){
            e.preventDefault();
            $('#exampleModalLabel').html('Edit Shop Management');

            page = $(this).data('id');
            var url = "shop-management/edit/";
        
            $.get(url  + page, function (data) {
                $('#title_en').val(data.page.lang[0].name);
                $('#title_ar').val(data.page.lang[1].name ?? '');
                $('#address_en').val(data.page.lang[0].address ?? '');
                $('#address_ar').val(data.page.lang[1].address ?? '');
                $('#location').val(data.page.location);
                $('#latitude').val(data.page.latitude);
                $('#longitude').val(data.page.longitude);
                
                $('#id_pg').val(data.page.id);
                     
                $('#management-popup').modal({
                    show: true

                });
            }) 
        })

        $('.save_page').on('click',function(e){
          
            $("#add-management").validate({
                ignore: [],
                rules: {
                title_en  : {        
                    required: true,         
                },
                title_ar: {          
                    required: true,
                },
                },
                messages: {               
                title_en: {
                      required:"Name(EN) required",
                      
                      },
                title_ar: {
                      required: "Name (AR) required",
                      },
                },
                 errorPlacement: function(error, element) {
                   
                    if (element.attr("name") == "title_ar" ) {
                        $(".title_ar_error").html(error);
                    }
                    if (element.attr("name") == "title_en" ) {
                        $(".title_en_error").html(error);
                    }
                    
                 },
                 submitHandler: function(form) {
                    let edit_val=$('#id_pg').val();
                    var test = new Array();
               
                    if(edit_val){
                    $.ajax({
                        type:"POST",
                        url: "{{route('shop.management.update')}}",
                        data:{ 
                            title_en:$('#title_en').val(),
                            title_ar:$('#title_ar').val(),
                            address_en:$('#address_en').val(),
                            address_ar:$('#address_ar').val(),
                            location:$('#location').val(),
                            latitude:$('#latitude').val(),
                            longitude:$('#longitude').val(),
                            id:edit_val
                   
                        },
                        success: function(result){
                            console.log(result.msg)
                            $('#management-popup').modal('hide')
                                Toast.fire({
                                icon: 'success',
                                title: 'Shop management updated successfully'
                                });
                                window.location.href = '{{route("shop.management")}}';
                         }       
                    });
                }else{
                    $.ajax({
                        type:"POST",
                        url: "{{route('shop.management.store')}}",
                        data:{ 
                            title_en:$('#title_en').val(),
                            title_ar:$('#title_ar').val(),
                            address_en:$('#address_en').val(),
                            address_ar:$('#address_ar').val(),
                            location:$('#location').val(),
                            latitude:$('#latitude').val(),
                            longitude:$('#longitude').val(),       
                        },
    
                        success: function(result){
                            console.log(result.msg)
                            $('#management-popup').modal('hide')
                            if(result.msg ==="success")
                                   {
                                    Toast.fire({
                                    icon: 'success',
                                    title: 'Shop management added successfully'
                                    });
                                    window.location.href = '{{route("shop.management")}}';
                                   }else{
                                    Toast.fire({
                                    icon: 'error',
                                    title: 'some errors'
                                    });
                                   }
                             }
                         }) ;
                    }
                
                }
            });
            
           
        });

        $('.change-status').on('click',function(){
            
            activate = $(this).data('activate');
            $.ajax({
                        type:"POST",
                        url: "{{route('shop.management.status.update')}}",
                        data:{ 
                           status:activate,
                           id: $(this).data('id')
                   
                        },
                        success: function(result){                        
                                    Toast.fire({
                                    icon: 'success',
                                    title: 'Status updated successfully'
                                    });
                                    window.location.reload();
                                   
                        }
                })
        });
        $('.shop_management_id_del').on('click',function(){
           
            Swal.fire({  
                title: 'Are you sure to delete?',  
                text: "You won't be able to revert this!",  
                icon: 'warning',  
                showCancelButton: true,  
                confirmButtonColor: '#3085d6',  
                cancelButtonColor: '#d33',  
                confirmButtonText: 'Yes, delete it!'
            })
            .then((result) => {  
                if (result.value) {    
                    $.ajax({
                        url: "{{route('shop.management.delete')}}" ,
                        type: 'POST',
                        data:{
                            id:$(this).data('id')
                        },
                        success: function(data) {
                            Toast.fire({
                                    icon: 'success',
                                    title: 'Shop management deleted successfully'
                                    });
                                    window.location.reload();
                        }
                    });
                }
            })
        });
        $( "#search_field" ).autocomplete({
            source: function( request, response ) {
        $.ajax( {
          url: "{{route('shop.management.search')}}",
          method:'post',
          data: {
            search: request.term
          },
          success: function( data ) {
            response( data );
          }
        } );
      },
      minLength: 1,
      select: function( event, ui ) {
        $('#search_field').val(ui.item.label); // display the selected text
        $('#shop_management_select').val(ui.item.value); // save selected id to input
           return false;
      }
    });

   

    </script>
@endpush