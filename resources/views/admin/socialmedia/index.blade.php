@extends('layouts.master')

@section('content-title')
SOCIAL MEDIA LINK
@endsection
@section('add-btn')
<button  class="btn btn-info create_btn" id="link_add_btn">
    <i class="ti-plus"></i> Add New Link
</button>
@endsection
@section('content')
{{-- @include('admin.cms.faqs.popup') --}}
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row tablenav top text-right">
                    <div class="col-md-6 ml-0">
                        <form action="{{route('social-media-link')}}" id="search-shop-management-form" method="get">
                        <input type="text" value ="{{$search_field ?? ''}}"class="form-control" id="search_field" name="search_field" placeholder="Search social media">
                            <input type="hidden" name="social_media_select" id="social_media_select">
                    </form>
                       
                    </div>
                    <div class="col-md-6 text-left">
                        <button type="button" onclick="event.preventDefault(); 
                        document.getElementById('search-shop-management-form').submit();" class="btn btn-info"><font style="vertical-align: inherit;">Search</font></button>
                        <a href="{{route('social-media-link')}}" class="btn btn-default cancel_style">Reset</a>
                        
                    </div>
                </div>
            </div>
        </div>                  
            <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Name (EN)</th>
                                <th>Link</th>
                                <th>Visibility</th>
                                <th>Position</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($socialMedia) > 0)
                            @php
                            $i = 1;
                                
                            @endphp
                            @foreach ($socialMedia as $row_data)
                            <tr>
                                {{-- <th>{{ $i++ }}</th> --}}
                                <td>{{ $row_data->name ?? '' }}</td>
                                <td>{{ $row_data->link ?? '' }}</td>
                                 <td>{{ $row_data->visibility ?? '' }}</td>
                                  <td>{{ $row_data->position ?? '' }}</td>
                                <td class="text-center">
                                    <button
                                        type="button"
                                        class="change-status btn btn-sm btn-toggle mr-md-4 ml-0 {{ $row_data['status']}}"
                                        data-toggle="button"
                                        data-id="{{ $row_data->id }}"
                                        data-activate="{{ $row_data['status']}}"
                                        aria-pressed="true"
                                        autocomplete="off"
                                        id="active_shop_category">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white edit_btn page_edit link_edit"  title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    {{-- <a class="btn btn-sm btn-danger text-white shop_management_id_del" data-id="{{ $row_data->id }}" title="Delete"><i class="fa fa-trash"></i></a> --}}
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $socialMedia->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- popup --}}
<div class="modal fade" id="link-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Add Link</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="brdar_photo_loader" class="loader" style="display: none;"> </div>
            <div class="modal-body" data-no-padding="no-padding">
                <form id="add-management" method="POST" action="#">
                    <input type="hidden" id="id_pg" name="id_pg">
                    @csrf
                   
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label>Name (EN) <sup class="star">*</sup></label>
                            <textarea class="form-control area" name="title_en" id="title_en" placeholder="Enter social media name"></textarea>
                            <label class="title_en_error"></label>
                        </div>
                        <div class="col-md-6">
                            <label>Link <sup class="star">*</sup></label>
                            <textarea class="form-control area" name="link" id="link" placeholder="Enter social media link"></textarea>
                            <label class="link_error"></label>
                        </div>
                         <div class="col-md-6">
                            <label class="control-label">Select visibility </label>
                            <select class="form-control" name="visibility" id="visibility">
                                  <option value="YES"> YES </option>
                                <option value="NO"> NO </option>
                            </select>    
                        </div>
                        <div class="col-md-6">
                            <label>Position <sup class="star">*</sup></label>
                            <input type="text" class="form-control" name="position" id="position"  placeholder="Enter position" >
                            <label class="position_error"></label>
                        </div>                
                        <div class="col-lg-12">
                            <div class="row 5">
                                <div class="col-md-12 text-md-left">
                                    <button type="submit"  class="btn btn-info waves-effect waves-light save_page">
                                        Save
                                    </button>
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    .cancel_style{
        margin-left: 20px;
    }
    #site-error{
    float:right;
    margin-right: 260px;
    margin-top: 10px;

}
.area{
        height: 80px;
}
.star{
    color:red;
} 
.loader{
    position: fixed;
    top:0px;
    right:0px;
    width:100%;
    height:100%;
    background-color:#eceaea;
    background-image:url('../assets/images/loader.gif');
    background-repeat:no-repeat;
    background-size: 50px;
    z-index:10000000;
    opacity: 0.4;
    filter: alpha(opacity=40);
}
</style>

@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdn.ckeditor.com/4.14.0/basic/ckeditor.js"></script>

<script>
   
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }});
        
        $('#link_add_btn').on('click',function(e){
            $('#add-management').trigger("reset")
            $('#exampleModalLabel').html('Add Link');
            $('#link-popup').modal({
                show:true
            })
        })

        $('.link_edit').on('click',function(e){
            e.preventDefault();
            $('#exampleModalLabel').html('Edit Link');

            page = $(this).data('id');
            var url = "social-media-link/edit/";
        
            $.get(url  + page, function (data) {
                $('#title_en').val(data.page.name);
                $('#link').val(data.page.link);
                $('#visivility').val(data.page.visivility);
                $('#position').val(data.page.position);
                $('#id_pg').val(data.page.id);
                     
                $('#link-popup').modal({
                    show: true

                });
            }) 
        })

        $('.save_page').on('click',function(e){
          
            $("#add-management").validate({
                ignore: [],
                rules: {
                title_en  : {        
                    required: true,         
                },
                link  : {        
                    required: true,         
                },
                position  : {        
                    required: true,
                    number: true,        
                },
                },
                messages: {               
                title_en: {
                      required:"Name (EN) required",
                      
                      },
                link  : {        
                    required: "Link required",         
                }, 
                 position  : {        
                    required: "Position required", 
                    number: "Only numeric value accept",         
                }, 
                },
                 errorPlacement: function(error, element) {
                   
                    if (element.attr("name") == "title_en" ) {
                        $(".title_en_error").html(error);
                    }
                    if (element.attr("name") == "link" ) {
                        $(".link_error").html(error);
                    }
                    if (element.attr("name") == "position" ) {
                        $(".position_error").html(error);
                    }
                    
                 },
                 submitHandler: function(form) {
                    $(`#brdar_photo_loader`).show();
                    let edit_val=$('#id_pg').val();
                    var test = new Array();
               
                    if(edit_val){
                    $.ajax({
                        type:"POST",
                        url: "{{route('social-media-link.update')}}",
                        data:{ 
                            title_en:$('#title_en').val(),
                            link:$('#link').val(),
                            position:$('#position').val(),
                            visibility:$('#visibility').val(),
                            id:edit_val
                   
                        },
                        success: function(result){
                            $(`brdar_photo_loader`).hide();
                            console.log(result.msg)
                            $('#link-popup').modal('hide')
                                Toast.fire({
                                icon: 'success',
                                title: 'Link updated successfully'
                                });
                                window.location.href = '{{route("social-media-link")}}';
                         }       
                    });
                }else{
                    $.ajax({
                        type:"POST",
                        url: "{{route('social-media-link.store')}}",
                        data:{ 
                           title_en:$('#title_en').val(),
                            link:$('#link').val(),
                            position:$('#position').val(),
                            visibility:$('#visibility').val(),      
                        },
    
                        success: function(result){
                            $(`brdar_photo_loader`).hide();
                            console.log(result.msg)
                            $('#link-popup').modal('hide')
                            if(result.msg ==="success")
                                   {
                                    Toast.fire({
                                    icon: 'success',
                                    title: 'Link added successfully'
                                    });
                                    window.location.href = '{{route("social-media-link")}}';
                                   }else{
                                    $(`brdar_photo_loader`).hide();
                                    Toast.fire({
                                    icon: 'error',
                                    title: 'some errors'
                                    });
                                   }
                             }
                         }) ;
                    }
                
                }
            });
            
           
        });

        $('.change-status').on('click',function(){
            
            activate = $(this).data('activate');
            $.ajax({
                        type:"POST",
                        url: "{{route('social-media-link.status.update')}}",
                        data:{ 
                           status:activate,
                           id: $(this).data('id')
                   
                        },
                        success: function(result){                        
                                    Toast.fire({
                                    icon: 'success',
                                    title: 'Status updated successfully'
                                    });
                                    window.location.reload();
                                   
                        }
                })
        });
        
        $( "#search_field" ).autocomplete({
            source: function( request, response ) {
        $.ajax( {
          url: "{{route('social-media-link.search')}}",
          method:'post',
          data: {
            search: request.term
          },
          success: function( data ) {
            response( data );
          }
        } );
      },
      minLength: 1,
      select: function( event, ui ) {
        $('#search_field').val(ui.item.label); // display the selected text
        $('#social_media_select').val(ui.item.value); // save selected id to input
           return false;
      }
    });

    $("#position").keyup(function(){
        $(".position_error").html('');
        pos = $("#position").val();
        $.ajax( {
          url: "{{route('social-media-link.number-exist')}}",
          method:'post',
          data: {
            position: pos
          },
          success: function( data ) {
            if(data == 'true'){
                 $(".position_error").html("Number already existed");
                $(".position_error").css({ 'color': 'red', 'font-size': '12px' });
            }
            else{
                 $(".position_error").html('');
            }
          }
        } );
    });

    </script>
@endpush