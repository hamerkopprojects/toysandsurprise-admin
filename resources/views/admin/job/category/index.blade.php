@extends('layouts.master')

@section('content-title')
JOB CATEGORY
@endsection
@section('add-btn')
<button  class="btn btn-info create_btn" id="job_category_add_btn">
    <i class="ti-plus"></i> Add New Job Category
</button>
@endsection
@section('content')
{{-- @include('admin.cms.faqs.popup') --}}
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row tablenav top text-right">
                    <div class="col-md-6 ml-0">
                        <form action="{{route('job.category')}}" id="search-job-category-form" method="get">
                        <input type="text" value ="{{$search_field ?? ''}}"class="form-control" id="search_field" name="search_field" placeholder="Search Job Category">
                            <input type="hidden" name="job_category_select" id="job_category_select">
                    </form>
                       
                    </div>
                    <div class="col-md-6 text-left">
                        <button type="button" onclick="event.preventDefault(); 
                        document.getElementById('search-job-category-form').submit();" class="btn btn-info"><font style="vertical-align: inherit;">Search</font></button>
                        <a href="{{route('job.category')}}" class="btn btn-default cancel_style">Reset</a>
                        
                    </div>
                </div>
            </div>
        </div>                  
            <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Category ID</th>
                                <th>Category (EN)</th>
                                <th>Category (AR)</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($jobCategory) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($jobCategory as $row_data)
                            <tr>
                                {{-- <th>{{ $i++ }}</th> --}}
                                <td>{{ $row_data->category_unique_id ?? '' }}</td>
                                 <td>{{ $row_data->lang[0]->name ?? '' }}</td>
                                <td class="right-align">{{ $row_data->lang[1]->name ?? '' }}</td>
                                <td class="text-center">
                                    <button
                                        type="button"
                                        class="change-status btn btn-sm btn-toggle mr-md-4 ml-0 {{ $row_data['status']}}"
                                        data-toggle="button"
                                        data-id="{{ $row_data->id }}"
                                        data-activate="{{ $row_data['status']}}"
                                        aria-pressed="true"
                                        autocomplete="off"
                                        id="active_job_category">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white edit_btn page_edit job_category_edit"  title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white job_category_id_del" data-id="{{ $row_data->id }}" title="Delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $jobCategory->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- popup --}}
<div class="modal fade" id="category-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Add Job Category</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" data-no-padding="no-padding">
                <form id="add-category" method="POST" action="#">
                    <input type="hidden" id="id_pg" name="id_pg">
                    @csrf
                   
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label>Category Name (EN) <sup class="star">*</sup></label>
                            <textarea class="form-control area" name="title_en" id="title_en" placeholder="Enter your category name"></textarea>
                            <label class="title_en_error"></label>
                        </div>
                        <div class="col-md-6">
                            <label>Category Name (AR) <sup class="star">*</sup></label>
                            <textarea class="form-control area" name="title_ar" id="title_ar"
                                style="text-align:right !important" placeholder="أدخل اسم الفئة" ></textarea>
                            <label class="title_ar_error"></label>
                        </div>                     
                        <div class="col-lg-12">
                            <div class="row 5">
                                <div class="col-md-12 text-md-left">
                                    <button type="submit"  class="btn btn-info waves-effect waves-light save_page">
                                        Save
                                    </button>
                                    <button type="button" class="btn btn-default waves-effect data_cancel" data-dismiss="modal">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
     input[type=checkbox],input[type=radio]{
        box-sizing: border-box;
        padding: 0;
        margin: 0 10px 0 5px;
    }
    .cancel_style{
        margin-left: 20px;
    }
    #site-error{
    float:right;
    margin-right: 260px;
    margin-top: 10px;

}
.area{
        height: 80px;
    }
.star{
    color:red;
}    

</style>

@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdn.ckeditor.com/4.14.0/basic/ckeditor.js"></script>

<script>
   
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }});
        
        $('#job_category_add_btn').on('click',function(e){
            $('#add-category').trigger("reset")
            $('#exampleModalLabel').html('Add Job Category');
            $('#category-popup').modal({
                show:true
            })
        })

        $('.job_category_edit').on('click',function(e){
            e.preventDefault();
            $('#exampleModalLabel').html('Edit Job Category');

            page = $(this).data('id');
            var url = "job-category/edit/";
        
            $.get(url  + page, function (data) {
                $('#title_en').val(data.page.lang[0].name);
                $('#title_ar').val(data.page.lang[1].name ?? '');
                $('#id_pg').val(data.page.id);
                     
                $('#category-popup').modal({
                    show: true

                });
            }) 
        })

        $('.save_page').on('click',function(e){
          
            $("#add-category").validate({
                ignore: [],
                rules: {
                title_en  : {        
                    required: true,         
                },
                title_ar: {          
                    required: true,
                },
                },
                messages: {               
                title_en: {
                      required:"Category Name (EN) required",
                      
                      },
                title_ar: {
                      required: "Category Name (AR) required",
                      },
                },
                 errorPlacement: function(error, element) {
                   
                    if (element.attr("name") == "title_ar" ) {
                        $(".title_ar_error").html(error);
                    }
                    if (element.attr("name") == "title_en" ) {
                        $(".title_en_error").html(error);
                    }
                    
                 },
                 submitHandler: function(form) {
                    let edit_val=$('#id_pg').val();
                    var test = new Array();
                    
                    if(edit_val){
                    $.ajax({
                        type:"POST",
                        url: "{{route('job.category.update')}}",
                        data:{ 
                            title_en:$('#title_en').val(),
                            title_ar:$('#title_ar').val(),
                            id:edit_val
                   
                        },
                        success: function(result){
                            console.log(result.msg)
                            $('#category-popup').modal('hide')
                                Toast.fire({
                                icon: 'success',
                                title: 'Job category updated successfully'
                                });
                            window.location.reload();
                         }       
                    });
                }else{
                    $.ajax({
                        type:"POST",
                        url: "{{route('job.category.store')}}",
                        data:{ 
                            title_en:$('#title_en').val(),
                            title_ar:$('#title_ar').val(),
                        },
    
                        success: function(result){
                            
                            $('#category-popup').modal('hide')
                                if(result.msg ==="success")
                                {
                                    Toast.fire({
                                        icon: 'success',
                                        title: 'Job category added successfully'                                                                            
                                    });
                                    window.location.reload();
                                }else{
                                    Toast.fire({
                                        icon: 'error',
                                        title: 'some errors'
                                    });
                                }
                            }
                        });
                    }
                
                }
            });
            
           
        });

        $('.change-status').on('click',function(){
            
            activate = $(this).data('activate');
            $.ajax({
                        type:"POST",
                        url: "{{route('job.category.status.update')}}",
                        data:{ 
                           status:activate,
                           id: $(this).data('id')
                   
                        },
                        success: function(result){                        
                                    Toast.fire({
                                    icon: 'success',
                                    title: 'Status updated successfully'
                                    });
                                    window.location.reload();
                                   
                        }
                })
        });
        $('.job_category_id_del').on('click',function(){
           
            Swal.fire({  
                title: 'Are you sure to delete?',  
                text: "You won't be able to revert this!",  
                icon: 'warning',  
                showCancelButton: true,  
                confirmButtonColor: '#3085d6',  
                cancelButtonColor: '#d33',  
                confirmButtonText: 'Yes, delete it!'
            })
            .then((result) => {  
                if (result.value) {    
                    $.ajax({
                        url: "{{route('job.category.delete')}}" ,
                        type: 'POST',
                        data:{
                            id:$(this).data('id')
                        },
                        success: function(data) {
                            Toast.fire({
                                    icon: 'success',
                                    title: 'Deleted successfully'
                                    });
                                    window.location.reload();
                        }
                    });
                }
            })
        });
        $( "#search_field" ).autocomplete({
            source: function( request, response ) {
        $.ajax( {
          url: "{{route('job.category.search')}}",
          method:'post',
          data: {
            search: request.term
          },
          success: function( data ) {
            response( data );
          }
        } );
      },
      minLength: 1,
      select: function( event, ui ) {
        $('#search_field').val(ui.item.label); // display the selected text
        $('#job_category_select').val(ui.item.value); // save selected id to input
           return false;
      }
    });

    $('.data_cancel').on('click',function(){
        console.log("clear");
            $('.error').text('');
    });
    $('.close').on('click',function(){
       console.log("clear");
        $('.error').text('');
    })

    </script>
@endpush