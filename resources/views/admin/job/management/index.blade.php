@extends('layouts.master')

@section('content-title')
JOBS
@endsection
@section('add-btn')
<button  class="btn btn-info create_btn" id="job_management_add_btn">
    <i class="ti-plus"></i> Add New Job
</button>
@endsection
@section('content')
{{-- @include('admin.cms.faqs.popup') --}}
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row tablenav top text-right">
                    <div class="col-md-6 ml-0">
                        <form action="{{route('job.management')}}" id="search-job-category-form" method="get">
                        <input type="text" value ="{{$search_field ?? ''}}"class="form-control" id="search_field" name="search_field" placeholder="Search Job">
                            <input type="hidden" name="job_management_select" id="job_management_select">
                    </form>
                       
                    </div>
                    <div class="col-md-6 text-left">
                        <button type="button" onclick="event.preventDefault(); 
                        document.getElementById('search-job-category-form').submit();" class="btn btn-info"><font style="vertical-align: inherit;">Search</font></button>
                        <a href="{{route('job.management')}}" class="btn btn-default cancel_style">Reset</a>
                        
                    </div>
                </div>
            </div>
        </div>                  
            <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Title (EN)</th>
                                <th>Title (AR)</th>
                                <th>Description (EN)</th>
                                <th>Description (AR)</th>
                                <th>Job Category</th>
                                <th>Package</th>
                                <th>Experience</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- @php 
                                 dd($jobManagement);
                            @endphp --}}
                            @if (count($jobManagement) > 0)
                            @php
                            $i = 1;
                                
                            @endphp
                            @foreach ($jobManagement as $row_data)
                            <tr>
                                {{-- <th>{{ $i++ }}</th> --}}
                                <td>{{ $row_data->lang[0]->name ?? '' }}</td>
                                <td class="right-align">{{ $row_data->lang[1]->name ?? '' }}</td>
                                <td>{{ $row_data->lang[0]->description ?? '' }}</td>
                                <td class="right-align">{{ $row_data->lang[1]->description ?? '' }}</td>
                                <td>{{ $row_data->jobCategory->lang[0]->name ?? '' }}</td>
                                <td>{{ $row_data->package ?? '' }}</td>
                                <td>{{ $row_data->experience ?? '' }}</td>
                                <td class="text-center">
                                    <button
                                        type="button"
                                        class="change-status btn btn-sm btn-toggle mr-md-4 ml-0 {{ $row_data['status']}}"
                                        data-toggle="button"
                                        data-id="{{ $row_data->id }}"
                                        data-activate="{{ $row_data['status']}}"
                                        aria-pressed="true"
                                        autocomplete="off"
                                        id="active_job_category">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white edit_btn page_edit job_management_edit"  title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white job_management_id_del" data-id="{{ $row_data->id }}" title="Delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $jobManagement->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- popup --}}
<div class="modal fade" id="management-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Add Job</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" data-no-padding="no-padding">
                <form id="add-management" method="POST" action="#">
                    <input type="hidden" id="id_pg" name="id_pg">
                    @csrf
                   
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label>Title Name (EN) <sup class="star">*</sup></label>
                            <textarea class="form-control area" name="title_en" id="title_en" placeholder="Enter your management name"></textarea>
                            <label class="title_en_error"></label>
                        </div>
                        <div class="col-md-6">
                            <label>Title Name (AR) <sup class="star">*</sup></label>
                            <textarea class="form-control area" name="title_ar" id="title_ar"
                                style="text-align:right !important" placeholder="أدخل اسم الفئة" ></textarea>
                            <label class="title_ar_error"></label>
                        </div>
                        <div class="col-md-6 english_label">
                            <label>Description (EN) <sup class="star">*</sup></label>
                            <textarea class="form-control area" id="description_en" name="description_en"></textarea>
                           
                            <label class="description_en_error"></label>
                           
                        </div>
                        <div class="col-md-6 arabic_label">
                            <label>Description (AR) <sup class="star">*</sup></label>
                            <textarea class="form-control area" id="description_ar" name="description_ar" style="text-align:right !important"></textarea>
                           
                            <label class="description_ar_error"></label>
                            
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Select job category <sup class="star">*</sup></label>
                            <select class="form-control" name="category" id="category">
                                <option value=""> Select Category </option>
                                @foreach($jobCategory as $categorylist)
                                    <option value="{{$categorylist->id}}">{{$categorylist->lang[0]->name}}</option>   
                                @endforeach
                            </select>
                             <label class="category_error"></label>
                        </div>
                        <div class="col-md-6">
                            <label>Package <sup class="star">*</sup></label>
                            <input type="text" class="form-control" name="package" id="package" placeholder="Enter your package">
                            <label class="package_error"></label>
                        </div> 
                        <div class="col-md-6">
                            <label>Experience <sup class="star">*</sup></label>
                            <input type="text" class="form-control" name="experience" id="experience" placeholder="Enter your experience">
                            <label class="experience_error"></label>
                        </div>                    
                        <div class="col-lg-12">
                            <div class="row 5">
                                <div class="col-md-12 text-md-left">
                                    <button type="submit"  class="btn btn-info waves-effect waves-light save_page">
                                        Save
                                    </button>
                                    <button type="button" class="btn btn-default waves-effect data_cancel" data-dismiss="modal">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    .cancel_style{
        margin-left: 20px;
    }
    #site-error{
    float:right;
    margin-right: 260px;
    margin-top: 10px;

}
.area{
        height: 80px;
    }
.star{
    color:red;
} 
</style>

@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdn.ckeditor.com/4.14.0/basic/ckeditor.js"></script>

<script>
   
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }});
        
        $('#job_management_add_btn').on('click',function(e){
            $('#add-management').trigger("reset")
            $('#exampleModalLabel').html('Add Job');
            $('#management-popup').modal({
                show:true
            })
        })

        $('.job_management_edit').on('click',function(e){
            e.preventDefault();
            $('#exampleModalLabel').html('Edit Job');

            page = $(this).data('id');
            var url = "job-management/edit/";
        
            $.get(url  + page, function (data) {
                $('#title_en').val(data.page.lang[0].name);
                $('#title_ar').val(data.page.lang[1].name ?? '');
                $('#description_en').val(data.page.lang[0].description ?? '');
                $('#description_ar').val(data.page.lang[1].description ?? '');
                
                $('#category').val(data.page.job_category_id ?? '');
                $('#package').val(data.page.package ?? '');
                $('#experience').val(data.page.experience ?? '');
                $('#id_pg').val(data.page.id);
                     
                $('#management-popup').modal({
                    show: true

                });
            }) 
        })

        $('.save_page').on('click',function(e){
          
            $("#add-management").validate({
                ignore: [],
                rules: {
                    title_en  : {        
                        required: true,         
                    },
                    title_ar: {          
                        required: true,
                    },
                    description_en: {          
                        required: true,
                    },
                    description_ar: {          
                        required: true,
                    },
                    category: {          
                        required: true,
                    },
                    experience: {          
                        required: true,
                    },
                    experience: {          
                        required: true,
                        number: true,   
                    },
                    package: {          
                        required: true,
                        number: true,   
                    },
                },
                messages: {               
                    title_en: {
                        required:"Management Name(EN) required",     
                    },
                    title_ar: {
                        required: "Management Name (AR) required",
                    },
                    description_en: {
                        required: "Description (EN) required",
                    },
                    description_ar: {
                        required: "Description (AR) required",
                    }, 
                    category: {          
                        required: "Category required",
                    },
                    experience: {          
                        required: "Experience required",
                        number: "Only numeric value accept",  
                    },
                    package: {          
                        required: "Package required",
                        number: "Only numeric value accept",
                    },           
                },
                 errorPlacement: function(error, element) {
                   
                    if (element.attr("name") == "title_ar" ) {
                        $(".title_ar_error").html(error);
                    }
                    if (element.attr("name") == "title_en" ) {
                        $(".title_en_error").html(error);
                    }
                    if (element.attr("name") == "description_en" ) {
                        $(".description_en_error").html(error);
                    }
                    if (element.attr("name") == "description_ar" ) {
                        $(".description_ar_error").html(error);
                    }
                    if (element.attr("name") == "category" ) {
                        $(".category_error").html(error);
                    }
                    if (element.attr("name") == "experience" ) {
                        $(".experience_error").html(error);
                    }
                    if (element.attr("name") == "package" ) {
                        $(".package_error").html(error);
                    }
                    
                 },
                 submitHandler: function(form) {
                    let edit_val=$('#id_pg').val();
                    var test = new Array();
               
                    if(edit_val){
                    $.ajax({
                        type:"POST",
                        url: "{{route('job.management.update')}}",
                        data:{ 
                            title_en:$('#title_en').val(),
                            title_ar:$('#title_ar').val(),
                            category:$('#category').val(),
                            description_en:$('#description_en').val(),
                            description_ar:$('#description_ar').val(),
                            package:$('#package').val(),
                            experience:$('#experience').val(),
                            id:edit_val
                   
                        },
                        success: function(result){
                            console.log(result.msg)
                            $('#management-popup').modal('hide')
                                Toast.fire({
                                icon: 'success',
                                title: 'Job updated successfully'
                                });
                            window.location.reload();
                         }       
                    });
                }else{
                    $.ajax({
                        type:"POST",
                        url: "{{route('job.management.store')}}",
                        data:{ 
                            title_en:$('#title_en').val(),
                            title_ar:$('#title_ar').val(),
                            category:$('#category').val(),
                            description_en:$('#description_en').val(),
                            description_ar:$('#description_ar').val(),
                            package:$('#package').val(),
                            experience:$('#experience').val(),
                        },
    
                        success: function(result){
                            console.log(result.msg)
                            $('#management-popup').modal('hide')
                            if(result.msg ==="success")
                                   {
                                    Toast.fire({
                                    icon: 'success',
                                    title: 'Job added successfully'
                                    });
                                    window.location.reload();
                                   }else{
                                    Toast.fire({
                                    icon: 'error',
                                    title: 'some errors'
                                    });
                                   }
                             }
                         }) ;
                    }
                
                }
            });
            
           
        });

        $('.change-status').on('click',function(){
            
            activate = $(this).data('activate');
            $.ajax({
                        type:"POST",
                        url: "{{route('job.management.status.update')}}",
                        data:{ 
                           status:activate,
                           id: $(this).data('id')
                   
                        },
                        success: function(result){                        
                                    Toast.fire({
                                    icon: 'success',
                                    title: 'Status updated successfully'
                                    });
                                    window.location.reload();
                                   
                        }
                })
        });
        $('.job_management_id_del').on('click',function(){
           
            Swal.fire({  
                title: 'Are you sure to delete?',  
                text: "You won't be able to revert this!",  
                icon: 'warning',  
                showCancelButton: true,  
                confirmButtonColor: '#3085d6',  
                cancelButtonColor: '#d33',  
                confirmButtonText: 'Yes, delete it!'
            })
            .then((result) => {  
                if (result.value) {    
                    $.ajax({
                        url: "{{route('job.management.delete')}}" ,
                        type: 'POST',
                        data:{
                            id:$(this).data('id')
                        },
                        success: function(data) {
                            Toast.fire({
                                    icon: 'success',
                                    title: 'Deleted successfully'
                                    });
                                    window.location.reload();
                        }
                    });
                }
            })
        });
        $( "#search_field" ).autocomplete({
            source: function( request, response ) {
        $.ajax( {
          url: "{{route('job.management.search')}}",
          method:'post',
          data: {
            search: request.term
          },
          success: function( data ) {
            response( data );
          }
        } );
      },
      minLength: 1,
      select: function( event, ui ) {
        $('#search_field').val(ui.item.label); // display the selected text
        $('#job_management_select').val(ui.item.value); // save selected id to input
           return false;
      }
    });

    $('.data_cancel').on('click',function(){
        console.log("clear");
            $('.error').text('');
    });
    $('.close').on('click',function(){
       console.log("clear");
        $('.error').text('');
    })

    </script>
@endpush