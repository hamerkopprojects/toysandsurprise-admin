<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropForeignKeyProductVariantB2bPriceTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('product_variant_b2c_price', function (Blueprint $table) {
            $table->dropForeign('product_variant_b2c_price_variant_attribute_id_foreign');
            $table->dropIndex('product_variant_b2c_price_variant_attribute_id_foreign');
            
            $table->dropForeign('product_variant_b2c_price_variant_id_foreign');
            $table->dropIndex('product_variant_b2c_price_variant_id_foreign');
        });
        Schema::table('product_variant_b2c_price', function (Blueprint $table) {
            $table->integer('variant_attribute_id')->nullable()->change();
            $table->integer('variant_id')->nullable()->change();
        });
        
        Schema::table('product_variant_b2b_price', function (Blueprint $table) {
            $table->dropForeign('product_variant_b2b_price_variant_attribute_id_foreign');
            $table->dropIndex('product_variant_b2b_price_variant_attribute_id_foreign');
            
            $table->dropForeign('product_variant_b2b_price_variant_id_foreign');
            $table->dropIndex('product_variant_b2b_price_variant_id_foreign');
        });
        Schema::table('product_variant_b2b_price', function (Blueprint $table) {
            $table->integer('variant_attribute_id')->nullable()->change();
            $table->integer('variant_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        
    }

}
