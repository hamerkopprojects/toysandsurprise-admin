<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAttributeTypeToAttributeTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('attribute', function (Blueprint $table) {
            $table->dropColumn('attribute_type');
        });
        Schema::table('attribute', function (Blueprint $table) {
            $table->enum('attribute_type', ['textbox', 'dropdown', 'textarea'])->default('textbox')->after('category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('attribute', function (Blueprint $table) {
            $table->dropColumn('attribute_type');
        });
    }

}
