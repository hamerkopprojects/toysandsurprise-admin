<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldToProductVariantB2cPriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_variant_b2c_price', function (Blueprint $table) {
            $table->integer('varient_id_main')->nullable()->after('variant_attribute_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_variant_b2c_price', function (Blueprint $table) {
           $table->dropColumn(['varient_id_main']);
        });
    }
}
