<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_management', function (Blueprint $table) {
            $table->increments('id');
            $table->engine = 'InnoDB';
            $table->unsignedInteger('job_category_id');
            $table->text('package')->nullable();
            $table->bigInteger('experience')->nullable();
            $table->enum('status', ['active', 'deactive']);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('job_category_id')
                ->references('id')
                ->on('job_category')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_management');
    }
}
