<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialMediaLinkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_media_link', function (Blueprint $table) {
            $table->increments('id');
            $table->engine = 'InnoDB';
            $table->text('name');
            $table->text('link');
            $table->enum('visibility', ['YES', 'NO']);
            $table->bigInteger('position')->nullable();
            $table->enum('status', ['active', 'deactive']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social_media_link');
    }
}
