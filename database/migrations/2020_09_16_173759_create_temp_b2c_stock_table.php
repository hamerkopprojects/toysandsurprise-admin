<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempB2cStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_b2c_stock', function (Blueprint $table) {
            $table->id();
            $table->string('sku', 60)->nullable();
            $table->integer('stock')->nullable();
            $table->string('varient', 60)->nullable();
            $table->text('errors')->nullable();
            $table->enum('error_flag', ['Y', 'N'])->default('N');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_b2c_stock');
    }
}
