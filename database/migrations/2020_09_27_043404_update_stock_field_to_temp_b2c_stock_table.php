<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateStockFieldToTempB2cStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('temp_b2c_stock', function (Blueprint $table) {
            $table->enum('cron_flag', ['I', 'S', 'C'])->default('I')->comment('I: Initiated, S: Started, C: Completed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('temp_b2c_stock', function (Blueprint $table) {
            //
        });
    }
}
