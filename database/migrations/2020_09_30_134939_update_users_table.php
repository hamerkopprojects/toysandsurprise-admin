<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('verify_token')->after('remember_token')->nullable();
            $table->string('phone', 15)->after('verify_token')->nullable();
            $table->unsignedBigInteger('role_id')->after('phone')->nullable();
            $table->string('user_id', 20)->after('role_id')->nullable();
            $table->string('national_id', 20)->after('user_id')->nullable();
            $table->enum('status', ['active', 'deactive'])->after('national_id')->default('deactive');

            $table->foreign('role_id')
                ->references('id')
                ->on('user_roles')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('verify_token');
            $table->dropColumn('phone');
            $table->dropColumn('role_id');
            $table->dropColumn('user_id');
            $table->dropColumn('national_id');
            $table->dropColumn('status');
        });
    }
}
