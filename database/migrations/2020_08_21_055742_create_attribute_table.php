<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttributeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->integer('category_id')->nullable();
            $table->enum('attribute_type', ['textbox', 'dropdown'])->default('textbox');
            $table->enum('status', ['active', 'deactive'])->default('active');
            $table->enum('is_mandatory', ['yes', 'no'])->default('no');
            $table->enum('is_variant', ['yes', 'no'])->default('no');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attribute');
    }
}
