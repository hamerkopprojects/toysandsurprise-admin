<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('cust_name', 60)->nullable();
            $table->string('cust_id', 200)->nullable();
            $table->string('otp', 10)->nullable()->default(NULL);
            $table->string('email', 60)->nullable();
            $table->string('password', 100)->nullable();
            $table->string('phone', 60)->nullable();
            $table->enum('gender', ['Male', 'Female', 'Other']);
            $table->enum('status', ['Y', 'N'])->default('Y');
            $table->dateTime('otp_generated_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}
