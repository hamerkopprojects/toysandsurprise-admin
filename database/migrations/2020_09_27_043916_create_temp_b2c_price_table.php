<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempB2cPriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_b2c_price', function (Blueprint $table) {
            $table->id();
            $table->string('sku', 60)->nullable();
            $table->integer('price')->nullable();
            $table->integer('discount_price')->nullable();
            $table->string('varient', 60)->nullable();
            $table->text('errors')->nullable();
            $table->enum('error_flag', ['Y', 'N'])->default('N');
            $table->enum('cron_flag', ['I', 'S', 'C'])->default('I')->comment('I: Initiated, S: Started, C: Completed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_b2c_price');
    }
}
