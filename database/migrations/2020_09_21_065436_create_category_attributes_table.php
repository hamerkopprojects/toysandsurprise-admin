<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryAttributesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('category_attributes', function (Blueprint $table) {
            $table->id();
            $table->engine = 'InnoDB';
            $table->unsignedInteger('category_id');
            $table->integer('attribute_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('category_id')
                    ->references('id')
                    ->on('category')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('category_attributes');
    }

}
