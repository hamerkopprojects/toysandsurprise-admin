<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin.toys@gmail.com',
            'status' => 'active',
            'phone' => '9998586874',
            'user_id' => 'EMTYUSER00001',
            'role_id' => '1',
            'email_verified_at' => now(),
            'password' => Hash::make('toysandsurprise'),
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
