<?php

use App\User;
use App\Models\Age;
use App\Models\Tag;
use App\Models\Pages;
use App\Models\Gender;
use App\Models\AgeLang;
use App\Models\AppType;
use App\Models\TagLang;
use App\Models\HowtoUse;
use App\Models\PagesLang;
use App\Models\UserRoles;
use App\Models\GenderLang;
use App\Models\HowtoUseLang;
use App\Models\RatingSegments;
use Illuminate\Database\Seeder;
use App\Models\RatingSegmentsLang;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        RatingSegments::truncate();
        RatingSegmentsLang::truncate();
        HowtoUse::truncate();
        HowtoUseLang::truncate();
        Pages::truncate();
        PagesLang::truncate();
        // User::truncate();
        // UserRoles::truncate();
        // AppType::truncate();
        Tag::truncate();
        TagLang::truncate();
        Age::truncate();
        AgeLang::truncate();
        Gender::truncate();
        GenderLang::truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        // $this->call(UserRoleSeeder::class);
        // $this->call(UserAppType::class);
        // $this->call(UserSeeder::class);
        $this->call(HowtoUseAppTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(SegmentSeeder::class);
        $this->call(TagSeeder::class);
        $this->call(AgeSeeder::class);
        $this->call(GenderSeeder::class);
        $this->call(SliderSeeder::class);
    }
}
