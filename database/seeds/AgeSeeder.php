<?php

use Illuminate\Database\Seeder;

class AgeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('age')->insert([
            [
                'status'=>'active',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'status'=>'active',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'status'=>'active',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'status'=>'active',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'status'=>'active',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'status'=>'active',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
        ]);
        DB::table('age_i18n')->insert([
            [
                'age_id' => '1',
                'name'=>'0-12 Months',
                 'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'age_id' => '1',
                'name'=>'0-12 شهرًا',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'age_id' => '2',
                'name'=>'1-2 Years',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'age_id' => '2',
                'name'=>'1-2 سنوات',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'age_id' => '3',
                'name'=>'3-5 Years',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'age_id' => '3',
                'name'=>'3-5 سنوات',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'age_id' => '4',
                'name'=>'6-12 Years',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'age_id' => '4',
                'name'=>'6-12 سنة',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'age_id' => '5',
                'name'=>'13-18 Years',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'age_id' => '5',
                'name'=>'13-18 سنة',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'age_id' => '6',
                'name'=>'Plus 18 years',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'age_id' => '6',
                'name'=>'بالإضافة إلى 18 سنة',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
        ]);
    }
}
