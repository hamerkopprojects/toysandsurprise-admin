<?php

use Illuminate\Database\Seeder;

class GenderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gender')->insert([
            [
                'status'=>'active',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'status'=>'active',
                'created_at' => now(),
                'updated_at' => now()
            
            ]
        ]);
        DB::table('gender_i18n')->insert([
            [
                'gender_id' => '1',
                'name'=>'Boy',
                 'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'gender_id' => '1',
                'name'=>'صبي',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'gender_id' => '2',
                'name'=>'Girl',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'gender_id' => '2',
                'name'=>'فتاة',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ]
        ]);
    }
}
