<?php

use Illuminate\Database\Seeder;

class SliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('slider')->insert([
            [
                'slider_type' => 'Slider1',
                'created_at'=>now(),
            ],
            [
                'slider_type' => 'Slider2',
                'created_at'=>now(),
            ],
            [
                'slider_type' => 'Slider3',
                'created_at'=>now(),
            ],
            [
                'slider_type' => 'Slider4',
                'created_at'=>now(),
            ],
            [
                'slider_type' => 'Slider5',
                'created_at'=>now(),
            ],
            [
                'slider_type' => 'Slider6',
                'created_at'=>now(),
            ],
        ]);
    }
}
