<?php

use Illuminate\Database\Seeder;

class SegmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('rating_segments_i18n')->truncate();
        // DB::table('rating_segments')->truncate();
        DB::table('rating_segments')->insert([
            [
                
                'status'=>'active',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'status'=>'active',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
        ]);
        DB::table('rating_segments_i18n')->insert([
            [
                'rating_segment_id' => '1',
                'name'=>'About as',
                 'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'rating_segment_id' => '1',
                'name'=>'النوع وتدافعت عليه لصنع',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'rating_segment_id' => '2',
                'name'=>'terms and condition',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'rating_segment_id' => '2',
                'name'=>'النوع وتدافعت عليه لصنع',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
        ]);
    }
}
