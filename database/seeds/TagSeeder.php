<?php

use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tag')->insert([
            [
                'status'=>'active',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'status'=>'active',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'status'=>'active',
                'created_at' => now(),
                'updated_at' => now()
            
            ]
        ]);
        DB::table('tag_i18n')->insert([
            [
                'tag_id' => '1',
                'name'=>'New',
                 'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'tag_id' => '1',
                'name'=>'جديد',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'tag_id' => '2',
                'name'=>'Offer',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'tag_id' => '2',
                'name'=>'عرض',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'tag_id' => '3',
                'name'=>'Limited Edition',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'tag_id' => '3',
                'name'=>'نسخة محدودة',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ]
        ]);
    }
}
